﻿namespace Cotton
{
	partial class LayoutPane
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (false)
			{
				if (disposing && (components != null))
				{
					components.Dispose();
				}
				base.Dispose(disposing);

			}
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LayoutPane));
            this.panelMenu = new System.Windows.Forms.Panel();
            this.pnlSubmenuLoans = new System.Windows.Forms.Panel();
            this.btnAddLoan = new System.Windows.Forms.Button();
            this.btnListLoan = new System.Windows.Forms.Button();
            this.pnlBtn3Menu = new System.Windows.Forms.Panel();
            this.btnPrestamos = new FontAwesome.Sharp.IconButton();
            this.pnlSubmenuUsers = new System.Windows.Forms.Panel();
            this.btnAddUser = new System.Windows.Forms.Button();
            this.btnListUser = new System.Windows.Forms.Button();
            this.pnlBtn4Menu = new System.Windows.Forms.Panel();
            this.btnUsuarios = new FontAwesome.Sharp.IconButton();
            this.pnlSubmenuInstitutions = new System.Windows.Forms.Panel();
            this.btnAddInstitution = new System.Windows.Forms.Button();
            this.btnListInstitutions = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnInstituciones = new FontAwesome.Sharp.IconButton();
            this.pnlSubmenuEquipos = new System.Windows.Forms.Panel();
            this.btnAddEquipos = new System.Windows.Forms.Button();
            this.btnListEquipos = new System.Windows.Forms.Button();
            this.pnlBtn2Menu = new System.Windows.Forms.Panel();
            this.btnEquipos = new FontAwesome.Sharp.IconButton();
            this.pnlSubmenuActivities = new System.Windows.Forms.Panel();
            this.btnActivitiesAdd = new System.Windows.Forms.Button();
            this.btnActivitiesList = new System.Windows.Forms.Button();
            this.pnlActivities = new System.Windows.Forms.Panel();
            this.btnActivities = new FontAwesome.Sharp.IconButton();
            this.pnlUbicacionesSubMenu = new System.Windows.Forms.Panel();
            this.btnAddUbication = new System.Windows.Forms.Button();
            this.btnUbicationsList = new System.Windows.Forms.Button();
            this.pnlUbicacion = new System.Windows.Forms.Panel();
            this.btnUbication = new FontAwesome.Sharp.IconButton();
            this.pnlCategoriesSubmenu = new System.Windows.Forms.Panel();
            this.btnCategoriesAdd = new System.Windows.Forms.Button();
            this.btnCategoriesList = new System.Windows.Forms.Button();
            this.pnlBtn5menu = new System.Windows.Forms.Panel();
            this.btnCategories = new FontAwesome.Sharp.IconButton();
            this.pnlBtn1Menu = new System.Windows.Forms.Panel();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.btnDashboard = new FontAwesome.Sharp.IconButton();
            this.pnlLogoTop = new System.Windows.Forms.Panel();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btnRestore = new System.Windows.Forms.PictureBox();
            this.btnMaximize = new System.Windows.Forms.PictureBox();
            this.btnClose = new System.Windows.Forms.PictureBox();
            this.toggleSideBar = new FontAwesome.Sharp.IconPictureBox();
            this.lblFromLink = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.dragMoverApp = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.panelDesktop = new System.Windows.Forms.Panel();
            this.pnlContainerForm = new System.Windows.Forms.Panel();
            this.breadcrumb = new System.Windows.Forms.Panel();
            this.iconCurrentForm = new FontAwesome.Sharp.IconPictureBox();
            this.panelMenu.SuspendLayout();
            this.pnlSubmenuLoans.SuspendLayout();
            this.pnlBtn3Menu.SuspendLayout();
            this.pnlSubmenuUsers.SuspendLayout();
            this.pnlBtn4Menu.SuspendLayout();
            this.pnlSubmenuInstitutions.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlSubmenuEquipos.SuspendLayout();
            this.pnlBtn2Menu.SuspendLayout();
            this.pnlSubmenuActivities.SuspendLayout();
            this.pnlActivities.SuspendLayout();
            this.pnlUbicacionesSubMenu.SuspendLayout();
            this.pnlUbicacion.SuspendLayout();
            this.pnlCategoriesSubmenu.SuspendLayout();
            this.pnlBtn5menu.SuspendLayout();
            this.pnlBtn1Menu.SuspendLayout();
            this.pnlLogoTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.pnlTitleBar.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRestore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSideBar)).BeginInit();
            this.panelDesktop.SuspendLayout();
            this.breadcrumb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconCurrentForm)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.AllowDrop = true;
            this.panelMenu.AutoScroll = true;
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.panelMenu.Controls.Add(this.pnlSubmenuLoans);
            this.panelMenu.Controls.Add(this.pnlBtn3Menu);
            this.panelMenu.Controls.Add(this.pnlSubmenuUsers);
            this.panelMenu.Controls.Add(this.pnlBtn4Menu);
            this.panelMenu.Controls.Add(this.pnlSubmenuInstitutions);
            this.panelMenu.Controls.Add(this.panel2);
            this.panelMenu.Controls.Add(this.pnlSubmenuEquipos);
            this.panelMenu.Controls.Add(this.pnlBtn2Menu);
            this.panelMenu.Controls.Add(this.pnlSubmenuActivities);
            this.panelMenu.Controls.Add(this.pnlActivities);
            this.panelMenu.Controls.Add(this.pnlUbicacionesSubMenu);
            this.panelMenu.Controls.Add(this.pnlUbicacion);
            this.panelMenu.Controls.Add(this.pnlCategoriesSubmenu);
            this.panelMenu.Controls.Add(this.pnlBtn5menu);
            this.panelMenu.Controls.Add(this.pnlBtn1Menu);
            this.panelMenu.Controls.Add(this.pnlLogoTop);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(200, 640);
            this.panelMenu.TabIndex = 0;
            // 
            // pnlSubmenuLoans
            // 
            this.pnlSubmenuLoans.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.pnlSubmenuLoans.Controls.Add(this.btnAddLoan);
            this.pnlSubmenuLoans.Controls.Add(this.btnListLoan);
            this.pnlSubmenuLoans.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSubmenuLoans.Location = new System.Drawing.Point(0, 1241);
            this.pnlSubmenuLoans.Name = "pnlSubmenuLoans";
            this.pnlSubmenuLoans.Size = new System.Drawing.Size(183, 96);
            this.pnlSubmenuLoans.TabIndex = 14;
            this.pnlSubmenuLoans.Visible = false;
            // 
            // btnAddLoan
            // 
            this.btnAddLoan.AutoSize = true;
            this.btnAddLoan.BackColor = System.Drawing.Color.Transparent;
            this.btnAddLoan.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddLoan.FlatAppearance.BorderSize = 0;
            this.btnAddLoan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnAddLoan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddLoan.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddLoan.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAddLoan.Location = new System.Drawing.Point(0, 46);
            this.btnAddLoan.Name = "btnAddLoan";
            this.btnAddLoan.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAddLoan.Size = new System.Drawing.Size(183, 46);
            this.btnAddLoan.TabIndex = 1;
            this.btnAddLoan.Text = "Agregar";
            this.btnAddLoan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddLoan.UseVisualStyleBackColor = false;
            this.btnAddLoan.Click += new System.EventHandler(this.btnAddLoan_Click);
            // 
            // btnListLoan
            // 
            this.btnListLoan.AutoSize = true;
            this.btnListLoan.BackColor = System.Drawing.Color.Transparent;
            this.btnListLoan.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListLoan.FlatAppearance.BorderSize = 0;
            this.btnListLoan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnListLoan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListLoan.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListLoan.ForeColor = System.Drawing.SystemColors.Control;
            this.btnListLoan.Location = new System.Drawing.Point(0, 0);
            this.btnListLoan.Name = "btnListLoan";
            this.btnListLoan.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnListLoan.Size = new System.Drawing.Size(183, 46);
            this.btnListLoan.TabIndex = 0;
            this.btnListLoan.Text = "Lista";
            this.btnListLoan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListLoan.UseVisualStyleBackColor = false;
            this.btnListLoan.Click += new System.EventHandler(this.btnListLoan_Click);
            // 
            // pnlBtn3Menu
            // 
            this.pnlBtn3Menu.Controls.Add(this.btnPrestamos);
            this.pnlBtn3Menu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBtn3Menu.Location = new System.Drawing.Point(0, 1175);
            this.pnlBtn3Menu.Name = "pnlBtn3Menu";
            this.pnlBtn3Menu.Size = new System.Drawing.Size(183, 66);
            this.pnlBtn3Menu.TabIndex = 3;
            // 
            // btnPrestamos
            // 
            this.btnPrestamos.AutoSize = true;
            this.btnPrestamos.FlatAppearance.BorderSize = 0;
            this.btnPrestamos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrestamos.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnPrestamos.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrestamos.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnPrestamos.IconChar = FontAwesome.Sharp.IconChar.Algolia;
            this.btnPrestamos.IconColor = System.Drawing.Color.Gainsboro;
            this.btnPrestamos.IconSize = 26;
            this.btnPrestamos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrestamos.Location = new System.Drawing.Point(0, 0);
            this.btnPrestamos.Name = "btnPrestamos";
            this.btnPrestamos.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnPrestamos.Rotation = 0D;
            this.btnPrestamos.Size = new System.Drawing.Size(200, 66);
            this.btnPrestamos.TabIndex = 3;
            this.btnPrestamos.Text = "      Prestados";
            this.btnPrestamos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrestamos.UseVisualStyleBackColor = true;
            this.btnPrestamos.Click += new System.EventHandler(this.btnPrestamos_Click);
            // 
            // pnlSubmenuUsers
            // 
            this.pnlSubmenuUsers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.pnlSubmenuUsers.Controls.Add(this.btnAddUser);
            this.pnlSubmenuUsers.Controls.Add(this.btnListUser);
            this.pnlSubmenuUsers.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSubmenuUsers.Location = new System.Drawing.Point(0, 1079);
            this.pnlSubmenuUsers.Name = "pnlSubmenuUsers";
            this.pnlSubmenuUsers.Size = new System.Drawing.Size(183, 96);
            this.pnlSubmenuUsers.TabIndex = 7;
            this.pnlSubmenuUsers.Visible = false;
            // 
            // btnAddUser
            // 
            this.btnAddUser.AutoSize = true;
            this.btnAddUser.BackColor = System.Drawing.Color.Transparent;
            this.btnAddUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddUser.FlatAppearance.BorderSize = 0;
            this.btnAddUser.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnAddUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddUser.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddUser.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAddUser.Location = new System.Drawing.Point(0, 46);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAddUser.Size = new System.Drawing.Size(183, 46);
            this.btnAddUser.TabIndex = 1;
            this.btnAddUser.Text = "Agregar";
            this.btnAddUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddUser.UseVisualStyleBackColor = false;
            this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
            // 
            // btnListUser
            // 
            this.btnListUser.AutoSize = true;
            this.btnListUser.BackColor = System.Drawing.Color.Transparent;
            this.btnListUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListUser.FlatAppearance.BorderSize = 0;
            this.btnListUser.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnListUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListUser.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListUser.ForeColor = System.Drawing.SystemColors.Control;
            this.btnListUser.Location = new System.Drawing.Point(0, 0);
            this.btnListUser.Name = "btnListUser";
            this.btnListUser.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnListUser.Size = new System.Drawing.Size(183, 46);
            this.btnListUser.TabIndex = 0;
            this.btnListUser.Text = "Lista";
            this.btnListUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListUser.UseVisualStyleBackColor = false;
            this.btnListUser.Click += new System.EventHandler(this.btnListUser_Click);
            // 
            // pnlBtn4Menu
            // 
            this.pnlBtn4Menu.Controls.Add(this.btnUsuarios);
            this.pnlBtn4Menu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBtn4Menu.Location = new System.Drawing.Point(0, 1013);
            this.pnlBtn4Menu.Name = "pnlBtn4Menu";
            this.pnlBtn4Menu.Size = new System.Drawing.Size(183, 66);
            this.pnlBtn4Menu.TabIndex = 4;
            // 
            // btnUsuarios
            // 
            this.btnUsuarios.AutoSize = true;
            this.btnUsuarios.FlatAppearance.BorderSize = 0;
            this.btnUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsuarios.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnUsuarios.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsuarios.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnUsuarios.IconChar = FontAwesome.Sharp.IconChar.Users;
            this.btnUsuarios.IconColor = System.Drawing.Color.Gainsboro;
            this.btnUsuarios.IconSize = 26;
            this.btnUsuarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUsuarios.Location = new System.Drawing.Point(0, 0);
            this.btnUsuarios.Name = "btnUsuarios";
            this.btnUsuarios.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnUsuarios.Rotation = 0D;
            this.btnUsuarios.Size = new System.Drawing.Size(200, 66);
            this.btnUsuarios.TabIndex = 4;
            this.btnUsuarios.Text = "      Usuarios";
            this.btnUsuarios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUsuarios.UseVisualStyleBackColor = true;
            this.btnUsuarios.Click += new System.EventHandler(this.btnUsuarios_Click);
            // 
            // pnlSubmenuInstitutions
            // 
            this.pnlSubmenuInstitutions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.pnlSubmenuInstitutions.Controls.Add(this.btnAddInstitution);
            this.pnlSubmenuInstitutions.Controls.Add(this.btnListInstitutions);
            this.pnlSubmenuInstitutions.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSubmenuInstitutions.Location = new System.Drawing.Point(0, 913);
            this.pnlSubmenuInstitutions.Name = "pnlSubmenuInstitutions";
            this.pnlSubmenuInstitutions.Size = new System.Drawing.Size(183, 100);
            this.pnlSubmenuInstitutions.TabIndex = 9;
            // 
            // btnAddInstitution
            // 
            this.btnAddInstitution.AutoSize = true;
            this.btnAddInstitution.BackColor = System.Drawing.Color.Transparent;
            this.btnAddInstitution.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddInstitution.FlatAppearance.BorderSize = 0;
            this.btnAddInstitution.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnAddInstitution.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddInstitution.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddInstitution.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAddInstitution.Location = new System.Drawing.Point(0, 46);
            this.btnAddInstitution.Name = "btnAddInstitution";
            this.btnAddInstitution.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAddInstitution.Size = new System.Drawing.Size(183, 46);
            this.btnAddInstitution.TabIndex = 1;
            this.btnAddInstitution.Text = "Agregar";
            this.btnAddInstitution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddInstitution.UseVisualStyleBackColor = false;
            this.btnAddInstitution.Click += new System.EventHandler(this.btnAddInstitution_Click);
            // 
            // btnListInstitutions
            // 
            this.btnListInstitutions.AutoSize = true;
            this.btnListInstitutions.BackColor = System.Drawing.Color.Transparent;
            this.btnListInstitutions.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListInstitutions.FlatAppearance.BorderSize = 0;
            this.btnListInstitutions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnListInstitutions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListInstitutions.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListInstitutions.ForeColor = System.Drawing.SystemColors.Control;
            this.btnListInstitutions.Location = new System.Drawing.Point(0, 0);
            this.btnListInstitutions.Name = "btnListInstitutions";
            this.btnListInstitutions.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnListInstitutions.Size = new System.Drawing.Size(183, 46);
            this.btnListInstitutions.TabIndex = 0;
            this.btnListInstitutions.Text = "Lista";
            this.btnListInstitutions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListInstitutions.UseVisualStyleBackColor = false;
            this.btnListInstitutions.Click += new System.EventHandler(this.btnListInstitutions_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnInstituciones);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 847);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(183, 66);
            this.panel2.TabIndex = 8;
            this.panel2.Visible = false;
            // 
            // btnInstituciones
            // 
            this.btnInstituciones.AutoSize = true;
            this.btnInstituciones.FlatAppearance.BorderSize = 0;
            this.btnInstituciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInstituciones.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnInstituciones.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInstituciones.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnInstituciones.IconChar = FontAwesome.Sharp.IconChar.School;
            this.btnInstituciones.IconColor = System.Drawing.Color.Gainsboro;
            this.btnInstituciones.IconSize = 26;
            this.btnInstituciones.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInstituciones.Location = new System.Drawing.Point(0, 0);
            this.btnInstituciones.Name = "btnInstituciones";
            this.btnInstituciones.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnInstituciones.Rotation = 0D;
            this.btnInstituciones.Size = new System.Drawing.Size(202, 66);
            this.btnInstituciones.TabIndex = 3;
            this.btnInstituciones.Text = "      Instituciones";
            this.btnInstituciones.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnInstituciones.UseVisualStyleBackColor = true;
            this.btnInstituciones.Click += new System.EventHandler(this.btnInstituciones_Click);
            // 
            // pnlSubmenuEquipos
            // 
            this.pnlSubmenuEquipos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.pnlSubmenuEquipos.Controls.Add(this.btnAddEquipos);
            this.pnlSubmenuEquipos.Controls.Add(this.btnListEquipos);
            this.pnlSubmenuEquipos.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSubmenuEquipos.Location = new System.Drawing.Point(0, 751);
            this.pnlSubmenuEquipos.Name = "pnlSubmenuEquipos";
            this.pnlSubmenuEquipos.Size = new System.Drawing.Size(183, 96);
            this.pnlSubmenuEquipos.TabIndex = 6;
            this.pnlSubmenuEquipos.Visible = false;
            // 
            // btnAddEquipos
            // 
            this.btnAddEquipos.AutoSize = true;
            this.btnAddEquipos.BackColor = System.Drawing.Color.Transparent;
            this.btnAddEquipos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddEquipos.FlatAppearance.BorderSize = 0;
            this.btnAddEquipos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnAddEquipos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddEquipos.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddEquipos.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAddEquipos.Location = new System.Drawing.Point(0, 46);
            this.btnAddEquipos.Name = "btnAddEquipos";
            this.btnAddEquipos.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAddEquipos.Size = new System.Drawing.Size(183, 46);
            this.btnAddEquipos.TabIndex = 1;
            this.btnAddEquipos.Text = "Agregar";
            this.btnAddEquipos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddEquipos.UseVisualStyleBackColor = false;
            this.btnAddEquipos.Click += new System.EventHandler(this.btnAddEquipos_Click);
            // 
            // btnListEquipos
            // 
            this.btnListEquipos.AutoSize = true;
            this.btnListEquipos.BackColor = System.Drawing.Color.Transparent;
            this.btnListEquipos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListEquipos.FlatAppearance.BorderSize = 0;
            this.btnListEquipos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnListEquipos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListEquipos.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListEquipos.ForeColor = System.Drawing.SystemColors.Control;
            this.btnListEquipos.Location = new System.Drawing.Point(0, 0);
            this.btnListEquipos.Name = "btnListEquipos";
            this.btnListEquipos.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnListEquipos.Size = new System.Drawing.Size(183, 46);
            this.btnListEquipos.TabIndex = 0;
            this.btnListEquipos.Text = "Lista";
            this.btnListEquipos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListEquipos.UseVisualStyleBackColor = false;
            this.btnListEquipos.Click += new System.EventHandler(this.btnListEquipos_Click);
            // 
            // pnlBtn2Menu
            // 
            this.pnlBtn2Menu.Controls.Add(this.btnEquipos);
            this.pnlBtn2Menu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBtn2Menu.Location = new System.Drawing.Point(0, 685);
            this.pnlBtn2Menu.Name = "pnlBtn2Menu";
            this.pnlBtn2Menu.Size = new System.Drawing.Size(183, 66);
            this.pnlBtn2Menu.TabIndex = 2;
            // 
            // btnEquipos
            // 
            this.btnEquipos.AutoSize = true;
            this.btnEquipos.FlatAppearance.BorderSize = 0;
            this.btnEquipos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEquipos.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnEquipos.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEquipos.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnEquipos.IconChar = FontAwesome.Sharp.IconChar.TabletAlt;
            this.btnEquipos.IconColor = System.Drawing.Color.Gainsboro;
            this.btnEquipos.IconSize = 26;
            this.btnEquipos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEquipos.Location = new System.Drawing.Point(0, 0);
            this.btnEquipos.Name = "btnEquipos";
            this.btnEquipos.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnEquipos.Rotation = 0D;
            this.btnEquipos.Size = new System.Drawing.Size(200, 66);
            this.btnEquipos.TabIndex = 2;
            this.btnEquipos.Text = "      Equipos";
            this.btnEquipos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEquipos.UseVisualStyleBackColor = true;
            this.btnEquipos.Click += new System.EventHandler(this.btnEquipos_Click);
            // 
            // pnlSubmenuActivities
            // 
            this.pnlSubmenuActivities.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.pnlSubmenuActivities.Controls.Add(this.btnActivitiesAdd);
            this.pnlSubmenuActivities.Controls.Add(this.btnActivitiesList);
            this.pnlSubmenuActivities.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSubmenuActivities.Location = new System.Drawing.Point(0, 585);
            this.pnlSubmenuActivities.Name = "pnlSubmenuActivities";
            this.pnlSubmenuActivities.Size = new System.Drawing.Size(183, 100);
            this.pnlSubmenuActivities.TabIndex = 11;
            this.pnlSubmenuActivities.Visible = false;
            // 
            // btnActivitiesAdd
            // 
            this.btnActivitiesAdd.AutoSize = true;
            this.btnActivitiesAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnActivitiesAdd.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnActivitiesAdd.FlatAppearance.BorderSize = 0;
            this.btnActivitiesAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnActivitiesAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActivitiesAdd.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivitiesAdd.ForeColor = System.Drawing.SystemColors.Control;
            this.btnActivitiesAdd.Location = new System.Drawing.Point(0, 46);
            this.btnActivitiesAdd.Name = "btnActivitiesAdd";
            this.btnActivitiesAdd.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnActivitiesAdd.Size = new System.Drawing.Size(183, 46);
            this.btnActivitiesAdd.TabIndex = 1;
            this.btnActivitiesAdd.Text = "Agregar";
            this.btnActivitiesAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActivitiesAdd.UseVisualStyleBackColor = false;
            this.btnActivitiesAdd.Click += new System.EventHandler(this.btnActivitiesAdd_Click);
            // 
            // btnActivitiesList
            // 
            this.btnActivitiesList.AutoSize = true;
            this.btnActivitiesList.BackColor = System.Drawing.Color.Transparent;
            this.btnActivitiesList.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnActivitiesList.FlatAppearance.BorderSize = 0;
            this.btnActivitiesList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnActivitiesList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActivitiesList.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivitiesList.ForeColor = System.Drawing.SystemColors.Control;
            this.btnActivitiesList.Location = new System.Drawing.Point(0, 0);
            this.btnActivitiesList.Name = "btnActivitiesList";
            this.btnActivitiesList.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnActivitiesList.Size = new System.Drawing.Size(183, 46);
            this.btnActivitiesList.TabIndex = 0;
            this.btnActivitiesList.Text = "Lista";
            this.btnActivitiesList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActivitiesList.UseVisualStyleBackColor = false;
            this.btnActivitiesList.Click += new System.EventHandler(this.btnActivitiesList_Click);
            // 
            // pnlActivities
            // 
            this.pnlActivities.Controls.Add(this.btnActivities);
            this.pnlActivities.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlActivities.Location = new System.Drawing.Point(0, 519);
            this.pnlActivities.Name = "pnlActivities";
            this.pnlActivities.Size = new System.Drawing.Size(183, 66);
            this.pnlActivities.TabIndex = 10;
            this.pnlActivities.Visible = false;
            // 
            // btnActivities
            // 
            this.btnActivities.AutoSize = true;
            this.btnActivities.FlatAppearance.BorderSize = 0;
            this.btnActivities.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActivities.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnActivities.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivities.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnActivities.IconChar = FontAwesome.Sharp.IconChar.Calendar;
            this.btnActivities.IconColor = System.Drawing.Color.Gainsboro;
            this.btnActivities.IconSize = 26;
            this.btnActivities.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActivities.Location = new System.Drawing.Point(0, 0);
            this.btnActivities.Name = "btnActivities";
            this.btnActivities.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnActivities.Rotation = 0D;
            this.btnActivities.Size = new System.Drawing.Size(202, 66);
            this.btnActivities.TabIndex = 3;
            this.btnActivities.Text = "    Actividades";
            this.btnActivities.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnActivities.UseVisualStyleBackColor = true;
            this.btnActivities.Click += new System.EventHandler(this.btnActivities_Click);
            // 
            // pnlUbicacionesSubMenu
            // 
            this.pnlUbicacionesSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.pnlUbicacionesSubMenu.Controls.Add(this.btnAddUbication);
            this.pnlUbicacionesSubMenu.Controls.Add(this.btnUbicationsList);
            this.pnlUbicacionesSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUbicacionesSubMenu.Location = new System.Drawing.Point(0, 419);
            this.pnlUbicacionesSubMenu.Name = "pnlUbicacionesSubMenu";
            this.pnlUbicacionesSubMenu.Size = new System.Drawing.Size(183, 100);
            this.pnlUbicacionesSubMenu.TabIndex = 13;
            this.pnlUbicacionesSubMenu.Visible = false;
            // 
            // btnAddUbication
            // 
            this.btnAddUbication.AutoSize = true;
            this.btnAddUbication.BackColor = System.Drawing.Color.Transparent;
            this.btnAddUbication.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddUbication.FlatAppearance.BorderSize = 0;
            this.btnAddUbication.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnAddUbication.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddUbication.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddUbication.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAddUbication.Location = new System.Drawing.Point(0, 46);
            this.btnAddUbication.Name = "btnAddUbication";
            this.btnAddUbication.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAddUbication.Size = new System.Drawing.Size(183, 46);
            this.btnAddUbication.TabIndex = 1;
            this.btnAddUbication.Text = "Agregar";
            this.btnAddUbication.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddUbication.UseVisualStyleBackColor = false;
            this.btnAddUbication.Click += new System.EventHandler(this.btnAddUbication_Click);
            // 
            // btnUbicationsList
            // 
            this.btnUbicationsList.AutoSize = true;
            this.btnUbicationsList.BackColor = System.Drawing.Color.Transparent;
            this.btnUbicationsList.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnUbicationsList.FlatAppearance.BorderSize = 0;
            this.btnUbicationsList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnUbicationsList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUbicationsList.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUbicationsList.ForeColor = System.Drawing.SystemColors.Control;
            this.btnUbicationsList.Location = new System.Drawing.Point(0, 0);
            this.btnUbicationsList.Name = "btnUbicationsList";
            this.btnUbicationsList.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnUbicationsList.Size = new System.Drawing.Size(183, 46);
            this.btnUbicationsList.TabIndex = 0;
            this.btnUbicationsList.Text = "Lista";
            this.btnUbicationsList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUbicationsList.UseVisualStyleBackColor = false;
            this.btnUbicationsList.Click += new System.EventHandler(this.btnUbicationsList_Click);
            // 
            // pnlUbicacion
            // 
            this.pnlUbicacion.Controls.Add(this.btnUbication);
            this.pnlUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUbicacion.Location = new System.Drawing.Point(0, 353);
            this.pnlUbicacion.Name = "pnlUbicacion";
            this.pnlUbicacion.Size = new System.Drawing.Size(183, 66);
            this.pnlUbicacion.TabIndex = 12;
            // 
            // btnUbication
            // 
            this.btnUbication.AutoSize = true;
            this.btnUbication.FlatAppearance.BorderSize = 0;
            this.btnUbication.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUbication.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnUbication.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUbication.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnUbication.IconChar = FontAwesome.Sharp.IconChar.MapMarkedAlt;
            this.btnUbication.IconColor = System.Drawing.Color.Gainsboro;
            this.btnUbication.IconSize = 26;
            this.btnUbication.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUbication.Location = new System.Drawing.Point(0, 0);
            this.btnUbication.Name = "btnUbication";
            this.btnUbication.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnUbication.Rotation = 0D;
            this.btnUbication.Size = new System.Drawing.Size(200, 66);
            this.btnUbication.TabIndex = 4;
            this.btnUbication.Text = "      Ubicaciones";
            this.btnUbication.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUbication.UseVisualStyleBackColor = true;
            this.btnUbication.Click += new System.EventHandler(this.btnUbication_Click);
            // 
            // pnlCategoriesSubmenu
            // 
            this.pnlCategoriesSubmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.pnlCategoriesSubmenu.Controls.Add(this.btnCategoriesAdd);
            this.pnlCategoriesSubmenu.Controls.Add(this.btnCategoriesList);
            this.pnlCategoriesSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCategoriesSubmenu.Location = new System.Drawing.Point(0, 257);
            this.pnlCategoriesSubmenu.Name = "pnlCategoriesSubmenu";
            this.pnlCategoriesSubmenu.Size = new System.Drawing.Size(183, 96);
            this.pnlCategoriesSubmenu.TabIndex = 0;
            // 
            // btnCategoriesAdd
            // 
            this.btnCategoriesAdd.AutoSize = true;
            this.btnCategoriesAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnCategoriesAdd.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCategoriesAdd.FlatAppearance.BorderSize = 0;
            this.btnCategoriesAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnCategoriesAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCategoriesAdd.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCategoriesAdd.ForeColor = System.Drawing.SystemColors.Control;
            this.btnCategoriesAdd.Location = new System.Drawing.Point(0, 46);
            this.btnCategoriesAdd.Name = "btnCategoriesAdd";
            this.btnCategoriesAdd.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnCategoriesAdd.Size = new System.Drawing.Size(183, 46);
            this.btnCategoriesAdd.TabIndex = 1;
            this.btnCategoriesAdd.Text = "Agregar";
            this.btnCategoriesAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCategoriesAdd.UseVisualStyleBackColor = false;
            this.btnCategoriesAdd.Click += new System.EventHandler(this.btnCategoriesAdd_Click_1);
            // 
            // btnCategoriesList
            // 
            this.btnCategoriesList.AutoSize = true;
            this.btnCategoriesList.BackColor = System.Drawing.Color.Transparent;
            this.btnCategoriesList.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCategoriesList.FlatAppearance.BorderSize = 0;
            this.btnCategoriesList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(45)))), ((int)(((byte)(58)))));
            this.btnCategoriesList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCategoriesList.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCategoriesList.ForeColor = System.Drawing.SystemColors.Control;
            this.btnCategoriesList.Location = new System.Drawing.Point(0, 0);
            this.btnCategoriesList.Name = "btnCategoriesList";
            this.btnCategoriesList.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnCategoriesList.Size = new System.Drawing.Size(183, 46);
            this.btnCategoriesList.TabIndex = 0;
            this.btnCategoriesList.Text = "Lista";
            this.btnCategoriesList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCategoriesList.UseVisualStyleBackColor = false;
            this.btnCategoriesList.Click += new System.EventHandler(this.btnCategoriesList_Click_1);
            // 
            // pnlBtn5menu
            // 
            this.pnlBtn5menu.Controls.Add(this.btnCategories);
            this.pnlBtn5menu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBtn5menu.Location = new System.Drawing.Point(0, 191);
            this.pnlBtn5menu.Name = "pnlBtn5menu";
            this.pnlBtn5menu.Size = new System.Drawing.Size(183, 66);
            this.pnlBtn5menu.TabIndex = 5;
            // 
            // btnCategories
            // 
            this.btnCategories.AutoSize = true;
            this.btnCategories.FlatAppearance.BorderSize = 0;
            this.btnCategories.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCategories.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnCategories.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCategories.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnCategories.IconChar = FontAwesome.Sharp.IconChar.Cubes;
            this.btnCategories.IconColor = System.Drawing.Color.Gainsboro;
            this.btnCategories.IconSize = 26;
            this.btnCategories.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCategories.Location = new System.Drawing.Point(0, 0);
            this.btnCategories.Name = "btnCategories";
            this.btnCategories.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnCategories.Rotation = 0D;
            this.btnCategories.Size = new System.Drawing.Size(200, 66);
            this.btnCategories.TabIndex = 4;
            this.btnCategories.Text = "      Categorías";
            this.btnCategories.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCategories.UseVisualStyleBackColor = true;
            this.btnCategories.Click += new System.EventHandler(this.btnCategories_Click);
            // 
            // pnlBtn1Menu
            // 
            this.pnlBtn1Menu.Controls.Add(this.materialDivider1);
            this.pnlBtn1Menu.Controls.Add(this.btnDashboard);
            this.pnlBtn1Menu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBtn1Menu.Location = new System.Drawing.Point(0, 125);
            this.pnlBtn1Menu.Name = "pnlBtn1Menu";
            this.pnlBtn1Menu.Size = new System.Drawing.Size(183, 66);
            this.pnlBtn1Menu.TabIndex = 0;
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(243)))), ((int)(((byte)(233)))), ((int)(((byte)(211)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Dock = System.Windows.Forms.DockStyle.Top;
            this.materialDivider1.Location = new System.Drawing.Point(0, 0);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(183, 2);
            this.materialDivider1.TabIndex = 0;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // btnDashboard
            // 
            this.btnDashboard.AutoSize = true;
            this.btnDashboard.FlatAppearance.BorderSize = 0;
            this.btnDashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDashboard.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnDashboard.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDashboard.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnDashboard.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.btnDashboard.IconColor = System.Drawing.Color.Gainsboro;
            this.btnDashboard.IconSize = 26;
            this.btnDashboard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDashboard.Location = new System.Drawing.Point(0, 0);
            this.btnDashboard.Name = "btnDashboard";
            this.btnDashboard.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnDashboard.Rotation = 0D;
            this.btnDashboard.Size = new System.Drawing.Size(200, 66);
            this.btnDashboard.TabIndex = 1;
            this.btnDashboard.Text = "      Dashboard";
            this.btnDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDashboard.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDashboard.UseVisualStyleBackColor = true;
            this.btnDashboard.Click += new System.EventHandler(this.btnDashboard_Click);
            // 
            // pnlLogoTop
            // 
            this.pnlLogoTop.Controls.Add(this.picLogo);
            this.pnlLogoTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLogoTop.Location = new System.Drawing.Point(0, 0);
            this.pnlLogoTop.Name = "pnlLogoTop";
            this.pnlLogoTop.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.pnlLogoTop.Size = new System.Drawing.Size(183, 125);
            this.pnlLogoTop.TabIndex = 0;
            // 
            // picLogo
            // 
            this.picLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLogo.Image = global::Cotton.Properties.Resources.logo;
            this.picLogo.Location = new System.Drawing.Point(0, 10);
            this.picLogo.Margin = new System.Windows.Forms.Padding(3, 30, 3, 30);
            this.picLogo.Name = "picLogo";
            this.picLogo.Padding = new System.Windows.Forms.Padding(0, 30, 0, 30);
            this.picLogo.Size = new System.Drawing.Size(183, 105);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 1;
            this.picLogo.TabStop = false;
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.White;
            this.pnlTitleBar.Controls.Add(this.panel1);
            this.pnlTitleBar.Controls.Add(this.toggleSideBar);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(200, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(670, 52);
            this.pnlTitleBar.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.btnRestore);
            this.panel1.Controls.Add(this.btnMaximize);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(476, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 52);
            this.panel1.TabIndex = 1;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(62, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(33, 52);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // btnRestore
            // 
            this.btnRestore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRestore.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnRestore.Image = ((System.Drawing.Image)(resources.GetObject("btnRestore.Image")));
            this.btnRestore.Location = new System.Drawing.Point(95, 0);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(33, 52);
            this.btnRestore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnRestore.TabIndex = 5;
            this.btnRestore.TabStop = false;
            this.btnRestore.Visible = false;
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // btnMaximize
            // 
            this.btnMaximize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMaximize.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnMaximize.Image = ((System.Drawing.Image)(resources.GetObject("btnMaximize.Image")));
            this.btnMaximize.Location = new System.Drawing.Point(128, 0);
            this.btnMaximize.Name = "btnMaximize";
            this.btnMaximize.Size = new System.Drawing.Size(33, 52);
            this.btnMaximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMaximize.TabIndex = 4;
            this.btnMaximize.TabStop = false;
            this.btnMaximize.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // btnClose
            // 
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(161, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(33, 52);
            this.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnClose.TabIndex = 3;
            this.btnClose.TabStop = false;
            this.btnClose.Click += new System.EventHandler(this.pictureBox1_Click);
            this.btnClose.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClose_MouseDown);
            // 
            // toggleSideBar
            // 
            this.toggleSideBar.BackColor = System.Drawing.Color.Transparent;
            this.toggleSideBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(84)))), ((int)(((byte)(65)))));
            this.toggleSideBar.IconChar = FontAwesome.Sharp.IconChar.Bars;
            this.toggleSideBar.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(84)))), ((int)(((byte)(65)))));
            this.toggleSideBar.Location = new System.Drawing.Point(13, 12);
            this.toggleSideBar.Name = "toggleSideBar";
            this.toggleSideBar.Size = new System.Drawing.Size(32, 32);
            this.toggleSideBar.TabIndex = 2;
            this.toggleSideBar.TabStop = false;
            this.toggleSideBar.Click += new System.EventHandler(this.toggleSideBar_Click);
            // 
            // lblFromLink
            // 
            this.lblFromLink.AutoSize = true;
            this.lblFromLink.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromLink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(84)))), ((int)(((byte)(65)))));
            this.lblFromLink.Location = new System.Drawing.Point(49, 22);
            this.lblFromLink.Name = "lblFromLink";
            this.lblFromLink.Size = new System.Drawing.Size(45, 18);
            this.lblFromLink.TabIndex = 1;
            this.lblFromLink.Text = "Home";
            // 
            // dragMoverApp
            // 
            this.dragMoverApp.Fixed = true;
            this.dragMoverApp.Horizontal = true;
            this.dragMoverApp.TargetControl = this.pnlTitleBar;
            this.dragMoverApp.Vertical = true;
            // 
            // panelDesktop
            // 
            this.panelDesktop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.panelDesktop.Controls.Add(this.pnlContainerForm);
            this.panelDesktop.Controls.Add(this.breadcrumb);
            this.panelDesktop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDesktop.Location = new System.Drawing.Point(200, 52);
            this.panelDesktop.Name = "panelDesktop";
            this.panelDesktop.Size = new System.Drawing.Size(670, 588);
            this.panelDesktop.TabIndex = 3;
            // 
            // pnlContainerForm
            // 
            this.pnlContainerForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainerForm.Location = new System.Drawing.Point(0, 61);
            this.pnlContainerForm.Name = "pnlContainerForm";
            this.pnlContainerForm.Size = new System.Drawing.Size(670, 527);
            this.pnlContainerForm.TabIndex = 1;
            // 
            // breadcrumb
            // 
            this.breadcrumb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(224)))), ((int)(((byte)(227)))));
            this.breadcrumb.Controls.Add(this.lblFromLink);
            this.breadcrumb.Controls.Add(this.iconCurrentForm);
            this.breadcrumb.Dock = System.Windows.Forms.DockStyle.Top;
            this.breadcrumb.Location = new System.Drawing.Point(0, 0);
            this.breadcrumb.Name = "breadcrumb";
            this.breadcrumb.Size = new System.Drawing.Size(670, 61);
            this.breadcrumb.TabIndex = 0;
            // 
            // iconCurrentForm
            // 
            this.iconCurrentForm.BackColor = System.Drawing.Color.Transparent;
            this.iconCurrentForm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(84)))), ((int)(((byte)(65)))));
            this.iconCurrentForm.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.iconCurrentForm.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(84)))), ((int)(((byte)(65)))));
            this.iconCurrentForm.IconSize = 30;
            this.iconCurrentForm.Location = new System.Drawing.Point(13, 16);
            this.iconCurrentForm.Name = "iconCurrentForm";
            this.iconCurrentForm.Size = new System.Drawing.Size(30, 32);
            this.iconCurrentForm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.iconCurrentForm.TabIndex = 0;
            this.iconCurrentForm.TabStop = false;
            // 
            // LayoutPane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 640);
            this.Controls.Add(this.panelDesktop);
            this.Controls.Add(this.pnlTitleBar);
            this.Controls.Add(this.panelMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.Name = "LayoutPane";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LayoutPane";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LayoutPane_FormClosing);
            this.Load += new System.EventHandler(this.LayoutPane_Load);
            this.panelMenu.ResumeLayout(false);
            this.pnlSubmenuLoans.ResumeLayout(false);
            this.pnlSubmenuLoans.PerformLayout();
            this.pnlBtn3Menu.ResumeLayout(false);
            this.pnlBtn3Menu.PerformLayout();
            this.pnlSubmenuUsers.ResumeLayout(false);
            this.pnlSubmenuUsers.PerformLayout();
            this.pnlBtn4Menu.ResumeLayout(false);
            this.pnlBtn4Menu.PerformLayout();
            this.pnlSubmenuInstitutions.ResumeLayout(false);
            this.pnlSubmenuInstitutions.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlSubmenuEquipos.ResumeLayout(false);
            this.pnlSubmenuEquipos.PerformLayout();
            this.pnlBtn2Menu.ResumeLayout(false);
            this.pnlBtn2Menu.PerformLayout();
            this.pnlSubmenuActivities.ResumeLayout(false);
            this.pnlSubmenuActivities.PerformLayout();
            this.pnlActivities.ResumeLayout(false);
            this.pnlActivities.PerformLayout();
            this.pnlUbicacionesSubMenu.ResumeLayout(false);
            this.pnlUbicacionesSubMenu.PerformLayout();
            this.pnlUbicacion.ResumeLayout(false);
            this.pnlUbicacion.PerformLayout();
            this.pnlCategoriesSubmenu.ResumeLayout(false);
            this.pnlCategoriesSubmenu.PerformLayout();
            this.pnlBtn5menu.ResumeLayout(false);
            this.pnlBtn5menu.PerformLayout();
            this.pnlBtn1Menu.ResumeLayout(false);
            this.pnlBtn1Menu.PerformLayout();
            this.pnlLogoTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.pnlTitleBar.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRestore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSideBar)).EndInit();
            this.panelDesktop.ResumeLayout(false);
            this.breadcrumb.ResumeLayout(false);
            this.breadcrumb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconCurrentForm)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panelMenu;
		private FontAwesome.Sharp.IconButton btnEquipos;
		private FontAwesome.Sharp.IconButton btnDashboard;
		private System.Windows.Forms.Panel pnlLogoTop;
		private FontAwesome.Sharp.IconButton btnPrestamos;
		private System.Windows.Forms.PictureBox picLogo;
		private FontAwesome.Sharp.IconButton btnUsuarios;
		private System.Windows.Forms.Panel pnlTitleBar;
		private FontAwesome.Sharp.IconPictureBox iconCurrentForm;
		private Bunifu.Framework.UI.BunifuDragControl dragMoverApp;
		private Bunifu.Framework.UI.BunifuCustomLabel lblFromLink;
		private System.Windows.Forms.Panel panelDesktop;
		private FontAwesome.Sharp.IconPictureBox toggleSideBar;
		private System.Windows.Forms.Panel pnlBtn4Menu;
		private System.Windows.Forms.Panel pnlBtn3Menu;
		private System.Windows.Forms.Panel pnlBtn2Menu;
		private System.Windows.Forms.Panel pnlBtn1Menu;
		private System.Windows.Forms.Panel breadcrumb;
		private System.Windows.Forms.PictureBox btnClose;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox pictureBox4;
		private System.Windows.Forms.PictureBox btnRestore;
		private System.Windows.Forms.PictureBox btnMaximize;
		private System.Windows.Forms.Panel pnlContainerForm;
		private System.Windows.Forms.Panel pnlBtn5menu;
		private System.Windows.Forms.Panel pnlCategoriesSubmenu;
		private System.Windows.Forms.Button btnCategoriesList;
		private System.Windows.Forms.Button btnCategoriesAdd;
		private System.Windows.Forms.Panel pnlSubmenuEquipos;
		private System.Windows.Forms.Button btnAddEquipos;
		private System.Windows.Forms.Button btnListEquipos;
		private System.Windows.Forms.Panel pnlSubmenuUsers;
		private System.Windows.Forms.Button btnAddUser;
		private System.Windows.Forms.Button btnListUser;
		private FontAwesome.Sharp.IconButton btnCategories;
		private System.Windows.Forms.Panel panel2;
		private FontAwesome.Sharp.IconButton btnInstituciones;
		private System.Windows.Forms.Panel pnlSubmenuInstitutions;
		private System.Windows.Forms.Button btnAddInstitution;
		private System.Windows.Forms.Button btnListInstitutions;
		private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.Panel pnlSubmenuActivities;
        private System.Windows.Forms.Button btnActivitiesAdd;
        private System.Windows.Forms.Button btnActivitiesList;
        private System.Windows.Forms.Panel pnlActivities;
        private FontAwesome.Sharp.IconButton btnActivities;
		private System.Windows.Forms.Panel pnlUbicacionesSubMenu;
		private System.Windows.Forms.Button btnAddUbication;
		private System.Windows.Forms.Button btnUbicationsList;
		private System.Windows.Forms.Panel pnlUbicacion;
		private FontAwesome.Sharp.IconButton btnUbication;
		private System.Windows.Forms.Panel pnlSubmenuLoans;
		private System.Windows.Forms.Button btnAddLoan;
		private System.Windows.Forms.Button btnListLoan;
	}
}