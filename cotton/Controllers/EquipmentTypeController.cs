﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections;
using Cotton.Models;

namespace Cotton.Controllers
{
    class EquipmentTypeController
    {

        public static async Task<Hashtable> list(CancellationToken ct)
        {
			List<Category> equipmentTypeList = null;
			await Task.Run(() =>
			{
				equipmentTypeList = EquipmentTypeModel.List();
			});
			if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
			Hashtable equipmentTypes = new Hashtable();
            foreach (Category type in equipmentTypeList)
            {
                equipmentTypes.Add(type.Id, type);
            }
            return equipmentTypes;
        }

        public static async Task<Hashtable> searchByName(CancellationToken ct, string param)
        {
            List<Category> equipmentTypeList = null;
            await Task.Run(() =>
            {
                equipmentTypeList = EquipmentTypeModel.SearchByName(param);
            });
            if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
            Hashtable equipmentTypes = new Hashtable();
            foreach (Category type in equipmentTypeList)
            {
                equipmentTypes.Add(type.Id, type);
            }
            return equipmentTypes;
        }

        public static async Task<Hashtable> find(CancellationToken ct, string id)
        {
            Category equipmentType = null;
            await Task.Run(() =>
            {
                equipmentType = EquipmentTypeModel.GetById(id);
            });
            if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
            Hashtable equipmentTypes = new Hashtable();
            equipmentTypes.Add(equipmentType.Id, equipmentType);

            return equipmentTypes;
        }

		public static void insert(CategoryTypes type)
		{
			type.Insert();
		}
    }
}
