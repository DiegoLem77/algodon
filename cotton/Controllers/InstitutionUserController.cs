﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cotton.Models;
using System.Data.Entity;


namespace Cotton.Controllers
{
    class InstitutionUserController
    {
        public static Boolean store(InstitutionsUsers row)
        {
            try
            {
                using (var ctx = new CottonContext())
                {
                    ctx.InstitutionsUsers.Add(row);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Boolean delete(int id)
        {
            try
            {
                using (var ctx = new CottonContext())
                {
                    DbSet<InstitutionsUsers> DbSet = ctx.Set<InstitutionsUsers>();
                    InstitutionsUsers iu = DbSet.Find(id);
                    DbSet.Remove(iu);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static List<User> getUnrelated(string institution)
        {
            try
            {
                using (var ctx = new CottonContext())
                {
                    var result = from u in ctx.Users
                                 join r in ctx.UsersUserTypes on u.Id equals r.user_id
                                 where r.type_id == 4 && !u.InstitutionUsers.Any(iu => iu.InstitutionId == institution)
                                 select u;
                    return result.ToList();
                }
            }
            catch (Exception )
            {
                return null;
            }
        }

        public static List<UserActivity> getLinked(String institution)
        {
            try
            {
                using (var ctx = new CottonContext())
                {
                    var results = from u in ctx.Users
                                 join iu in ctx.InstitutionsUsers on u.Id equals iu.UserId
                                 where iu.InstitutionId == institution
                                 select new UserActivity { Id = iu.Id, Name = u.Name, LastName = u.Lastname, DUI = u.Dui, Institution = iu.InstitutionId, Iu = iu.Id };
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
