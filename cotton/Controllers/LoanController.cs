﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Cotton.Models;
using System.Threading;

namespace Cotton.Controllers
{
    class LoanController
    {
        private static LoanModel loanModel = new LoanModel();

        public static async Task<Hashtable> list(CancellationToken ct)
        {
			List<loan> loans = null;
			await Task.Run(() =>
			{
				loans = LoanModel.List();
			});
			if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
			Hashtable loanHT = new Hashtable();
			foreach (loan _loan in loans)
			{
				_loan.users = User.GetById(_loan.user_id);
				_loan.equipments = EquipmentModel.GetById(_loan.equipment_id);
				loanHT.Add(_loan.Id, _loan);
			}
			return loanHT;
        }

		public static async Task<loan> find(CancellationToken ct, string id)
		{
			loan _loan = null;
			await Task.Run(() =>
			{
				_loan = loan.GetById(id);
				_loan.users = User.GetById(_loan.user_id);
				_loan.equipments = EquipmentModel.GetById(_loan.equipment_id);
			});
			if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
			return _loan;
		}
		public static void Insert(loan _l)
		{
			_l.Insert();
		}
	}
}
