﻿using Cotton.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Cotton.Controllers
{
    class EmailController
    {
        private readonly SmtpClient client;
        private static readonly Random random = new Random();
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private const string CredentialEmail = "keepeer.sv@gmail.com";
        private const string CredentialToken = "KPbbcg29";

        //private const string CredentialEmail = "frank.esquivel115@gmail.com";
        //private const string CredentialToken = "20150126";

        public EmailController()
        {
            client = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(CredentialEmail, CredentialToken),
            };
        }

        private string GenRandomString()
        {
            int length = 8;
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private string HashString(string str)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(str));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }

                return builder.ToString();
            }
        }

        public int ResetPassword(string email)
        {
            try
            {
                using (var ctx = new CottonContext())
                {
                    User u = (from b in ctx.Users where b.Email == email select b).FirstOrDefault();

                    if (u == null)
                    {
                        return -1; //Not found
                    }

                    string newPass = GenRandomString();
                    string hashedPass = HashString(newPass);
                    u.Password = hashedPass;
                    ctx.SaveChanges();


                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(CredentialEmail),
                        Subject = "Centro de Alcance 'El Algodón' - Recuperación de Contraseña",
                        Body = $"Nueva contraseña: {newPass}",
                    };

                    mail.To.Add(email);

                    client.Send(mail);
                    return 1;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return 0; //Error
            }
        }
    }
}
