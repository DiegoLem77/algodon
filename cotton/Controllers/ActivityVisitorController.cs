﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cotton.Models;
using System.Data.Entity;

namespace Cotton.Controllers
{
    class ActivityVisitorController
    {
        public static bool Store(activities_visitors row)
        {
            try
            {
                using (var ctx = new CottonContext())
                {
                    ctx.activities_visitors.Add(row);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static List<UserActivity> FindByActivity(String activity)
        {
            try
            {
                using (var ctx = new CottonContext())
                {
                    var result = from u in ctx.Users
                                 join iu in ctx.InstitutionsUsers on u.Id equals iu.UserId
                                 join i in ctx.Institutions on iu.InstitutionId equals i.Id
                                 join av in ctx.activities_visitors on iu.Id equals av.visitor_id
                                 where av.activity_id == activity
                                 select new UserActivity { Id = av.id, Name = u.Name, LastName = u.Lastname, DUI = u.Dui, Institution = i.Name, Iu = iu.Id };
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool Delete(int id)
        {
            try
            {
                using (var ctx = new CottonContext())
                {
                    DbSet<activities_visitors> DbSet = ctx.Set<activities_visitors>();
                    activities_visitors av = DbSet.Find(id);
                    DbSet.Remove(av);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
