﻿using Cotton.Models;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cotton.Controllers
{
    class AuthController
    {
        static public bool IsLogged = false;
        static public string LoggedId = null;
        static public User LoggedUser = null;

        public static async Task<bool> Login(string email, string password, CancellationToken ct)
        {
            using (var ctx = new CottonContext())
            {
                User _user = await Task.Run(() =>
                {
                    return (from b in ctx.Users where b.Email == email select b).FirstOrDefault();
                });

                if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();

                if (_user != null)
                {
                    using (SHA256 sha256Hash = SHA256.Create())
                    {
                        byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(password));

                        StringBuilder builder = new StringBuilder();
                        for (int i = 0; i < bytes.Length; i++)
                        {
                            builder.Append(bytes[i].ToString("x2"));
                        }

                        string hashedPassword = builder.ToString();

                        bool correctPassword = hashedPassword.Equals(_user.Password);
                        bool isCoordinator = _user.UsersUserTypes.Where(assignation => assignation.user_types.code == "COORDINATOR").Count() > 0;

                        if (correctPassword && isCoordinator)
                        {
                            SetUser(_user);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        private static void SetUser(User _user)
        {
            IsLogged = true;
            LoggedId = _user.Id;
            LoggedUser = _user;
        }

        public static void Logout()
        {
            IsLogged = false;
            LoggedId = null;
            LoggedUser = null;
        }

    }
}
