﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections;
using Cotton.Models;

namespace Cotton.Controllers
{
    class UbicationController
    {
        public static async Task<Hashtable> list(CancellationToken ct)
        {
            List<ubications> ubicationList = null;
            await Task.Run(() =>
            {
                ubicationList = UbicationModel.List();
            });
            if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
            Hashtable ubications = new Hashtable();
            foreach (ubications ubic in ubicationList)
            {
                ubications.Add(ubic.Id, ubic);
            }
            return ubications;
        }

        public static async Task<Hashtable> searchByUbication(CancellationToken ct, string param)
        {
            List<ubications> ubicationList = null;
            await Task.Run(() =>
            {
                ubicationList = UbicationModel.SearchByUbication(param);
            });
            if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
            Hashtable ubications = new Hashtable();
            foreach (ubications ubic in ubicationList)
            {
                ubications.Add(ubic.Id, ubic);
            }
            return ubications;
        }

        public static async Task<Hashtable> find(CancellationToken ct, string id)
        {
            ubications ubication = null;
            await Task.Run(() =>
            {
                ubication = UbicationModel.GetById(id);
            });
            if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
            Hashtable ubications = new Hashtable();
            ubications.Add(ubication.Id, ubication);

            return ubications;
        }
    }
}
