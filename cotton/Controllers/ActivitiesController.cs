﻿using Cotton.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.Entity;

namespace Cotton.Controllers
{
    class ActivitiesController : RestControler<Activity>
    {
        readonly string appFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + "Cotton";

        public bool Store(Activity activity, Stack<string> resources)
        {
            using (var ctx = new CottonContext())
            {
                using (var transaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        bool status = activity.Insert();

                        if (status)
                        {
                            bool resourcesStatus = SetActivityResources(activity, resources);

                            if (resourcesStatus)
                            {
                                transaction.Commit();
                                return true;
                            }
                            else
                            {
                                throw new Exception("Error in resources");
                            }
                        }
                        else
                        {
                            throw new Exception("Error in entity");
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        DeleteActivityFolder(activity);
                        transaction.Rollback();
                        return false;
                    }
                }

            }
        }

        private void CheckIfAppDirExists()
        {
            bool exits = Directory.Exists(appFolderPath);

            if (!exits)
            {
                Directory.CreateDirectory(appFolderPath);
            }
        }

        private void CheckIfActivityDirExists(Activity activity)
        {
            string path = $"{appFolderPath}{Path.DirectorySeparatorChar}{activity.Id}";
            bool exits = Directory.Exists(path);

            if (!exits)
            {
                Directory.CreateDirectory(path);
            }
        }

        public bool SetActivityResources(Activity activity, Stack<string> resources)
        {
            using (var ctx = new CottonContext())
            {
                using (var transaction = ctx.Database.BeginTransaction())
                {
                    CheckIfAppDirExists();
                    CheckIfActivityDirExists(activity);

                    while (resources.Count > 0)
                    {
                        string f = resources.Pop();
                        bool status = this.CopyFileToAppFolder(f, activity);

                        if (!status)
                        {
                            transaction.Rollback();
                            return false;
                        }

                    }

                    transaction.Commit();
                    return true;
                }
            }
        }

        public List<ActivityResource> GetResources(String id)
        {
            try
            {
                using (var ctx = new CottonContext())
                {
                    DbSet<Activity> DbSet = ctx.Set<Activity>();
                    Activity activity = DbSet.Find(id);
                    return activity.Resources.ToList<ActivityResource>();
                }
            }
            catch
            {
                return null;
            }
        }

        public bool DeleteResource(Queue<String> ids)
        {
            try
            {
                FileInfo file;
                using (var ctx = new CottonContext())
                {
                    DbSet<ActivityResource> DbSet = ctx.Set<ActivityResource>();
                    foreach (String id in ids)
                    {
                        ActivityResource resource = DbSet.Find(id);
                        file = new FileInfo(resource.Path);
                        if (file.Exists) file.Delete();
                        DbSet.Remove(resource);
                    }
                    ctx.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void DeleteActivityFolder(Activity activity)
        {
            string path = $"{appFolderPath}{Path.DirectorySeparatorChar}{activity.Id}";

            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path);
                foreach (string f in files)
                {
                    File.Delete(f);
                }

                Directory.Delete(path);
            }
        }

        private bool CopyFileToAppFolder(string file_path, Activity activity)
        {
            try
            {
                FileInfo file = new FileInfo(file_path);
                string fileAppPath = $"{appFolderPath}|{activity.Id}|{file.Name}".Replace('|', Path.DirectorySeparatorChar);
                File.Copy(file_path, fileAppPath);

                if (File.Exists(fileAppPath))
                {
                    ActivityResource ar = new ActivityResource
                    {
                        Name = file.Name,
                        StatusId = 1,
                        Description = file.Extension,
                        Path = fileAppPath,
                        ActivityId = activity.Id,
                    };

                    return ar.Insert();
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public List<ActivityResource> GetActivityResources(Activity a)
        {
            using (var ctx = new CottonContext())
            {
                try
                {
                    return (from b in ctx.activity_resource where b.ActivityId == a.Id select b).ToList();
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }
    }
}
