﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Cotton.Models;
using System.Threading;

namespace Cotton.Controllers
{
    class EquipmentController
    {
        private static EquipmentModel equipmentModel = new EquipmentModel();

        public static async Task<Hashtable> list(CancellationToken ct)
        {
            List<equipment> equipmentList = null;
            await Task.Run(() =>
            {
                equipmentList = EquipmentModel.List();
            });
            if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
            Hashtable equipments = new Hashtable();
            foreach (equipment equipment in equipmentList)
            {
                equipment.categories = EquipmentTypeModel.GetById(equipment.type_id);
                equipments.Add(equipment.Id, equipment);
            }
            return equipments;
        }
		public static async Task<Hashtable> GetAvailableEquipments(CancellationToken ct)
		{
			List<equipment> equipmentList = null;
			await Task.Run(() =>
			{
				equipmentList = EquipmentModel.List().Where(equipment => equipment.available_stock > 0).ToList();
			});
			if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
			Hashtable equipments = new Hashtable();
			foreach (equipment equipment in equipmentList)
			{
				equipment.categories = EquipmentTypeModel.GetById(equipment.type_id);
				equipments.Add(equipment.Id, equipment);
			}
			return equipments;
		}

		public static async Task<Hashtable> searchByNameCategory(CancellationToken ct, string param)
        {
            List<equipment> equipmentList = null;
            await Task.Run(() =>
            {
                equipmentList = EquipmentModel.SearchByNameCategory(param);
            });
            if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
            Hashtable equipments = new Hashtable();
            foreach (equipment equipment in equipmentList)
            {
                equipment.categories = EquipmentTypeModel.GetById(equipment.type_id);
                equipments.Add(equipment.Id, equipment);
            }
            return equipments;
        }

        public static async Task<Hashtable> find(CancellationToken ct, string id)
        {
            equipment equip = null;
            await Task.Run(() =>
            {
                equip = EquipmentModel.GetById(id);
            });
            if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
            Hashtable equipment = new Hashtable();
            equip.categories = EquipmentTypeModel.GetById(equip.type_id);
            equipment.Add(equip.Id, equip);
            return equipment;
        }

        public static void insert(equipment equip)
        {
            equip.Insert();
        }
    }
}
