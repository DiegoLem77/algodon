﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cotton.Controllers
{
    abstract class RestControler<Model> where Model : AbstractModel<Model>
    {
        public virtual List<Model> All() => AbstractModel<Model>.List();

        public virtual Model Get(string id) => AbstractModel<Model>.GetById(id);

        public virtual bool Store(Model model) => model.Insert();

        public virtual bool Update(Model model, string id) => model.Update(id);

        public virtual bool Delete(Model model) => model.Delete();
    }
}
