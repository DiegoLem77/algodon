﻿using Cotton.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cotton.Controllers
{
    class UserController : RestControler<User>
    {
        public static User getLastInsertered()
        {
            using (var ctx = new CottonContext())
            {
                DbSet<User> DbSet = ctx.Set<User>();
                return DbSet.OrderByDescending(u => u.CreatedAt).FirstOrDefault();
            }
        }

        public static List<User> search(string param)
        {
            using (var ctx = new CottonContext())
            {
                DbSet<User> DbSet = ctx.Set<User>();
                return DbSet.Where(u => u.Name.Contains(param) || u.Lastname.Contains(param) || u.Dui.Contains(param)).ToList();
            }
        }
		public static async Task<User> find(CancellationToken ct, string id)
		{
			User user = null;
			await Task.Run(() =>
			{
				user = User.GetById(id);
			});
			if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
			return user;
		}

        public static bool delete(string id)
        {
            using (var ctx = new CottonContext())
            {
                try
                {
                    User user = ctx.Users.Find(id);
                    ctx.Users.Remove(user);
                    ctx.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool storeAgent(User user, string institution)
        {
            try
            {
                using (var ctx = new CottonContext())
                {
                    // Insert user
                    bool f = false;
                    String uuid;
                    int number = 0;
                    do
                    {
                        uuid = Guid.NewGuid().ToString();
                        number = (from b in ctx.Users where b.Id == uuid select b).Count();
                        f = number > 0;
                    } while (f);
                    user.Id = uuid;
                    user.CreatedAt = DateTime.Now;
                    user.Code = institution + number;
                    ctx.Users.Add(user);

                    // Insert type-user
                    users_user_types rol = new users_user_types();
                    rol.user_id = user.Id;
                    rol.type_id = 4; // Visitor type
                    ctx.UsersUserTypes.Add(rol);

                    // Insert institution-user
                    InstitutionsUsers institutionsUsers = new InstitutionsUsers();
                    institutionsUsers.InstitutionId = institution;
                    institutionsUsers.UserId = user.Id;
                    ctx.InstitutionsUsers.Add(institutionsUsers);

                    ctx.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
