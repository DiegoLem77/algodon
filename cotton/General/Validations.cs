﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cotton.General
{
    class Validations
    {
        public static bool isNotEmpty(string str)
        {
            return str.Trim() == String.Empty;
        }

        public static bool isGreatherThanOrEqual(decimal number, decimal reference)
        {
            return number >= reference;
        }

        public static bool isBeforeOrEqualToNow(DateTime date)
        {
            return date <= DateTime.Now;
        }
    }
}
