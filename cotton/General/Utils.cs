﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Forms;

namespace Cotton.General
{
	class Utils
	{

		public static PrivateFontCollection ImportFont(string font)
		{
			PrivateFontCollection pfc = new PrivateFontCollection();
			font = font.Replace('_', '-');
			string fontName = string.Format("{0}Resources\\Fonts\\{1}.ttf", Utils.GetBasePath(), font);
			pfc.AddFontFile(fontName);
			return pfc;
		}

		public static string GetBasePath()
		{
			string basePath = AppDomain.CurrentDomain.BaseDirectory;
			return Path.GetFullPath(Path.Combine(basePath, @"..\..\"));
		}
	}

}
