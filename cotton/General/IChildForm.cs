﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cotton.General
{
	public partial class ChildForm : Form
	{
		public delegate void EndForm();
		public event EndForm CloseForm;

		public void ChildForm_CloseForm()
		{
			CloseForm?.Invoke();
		}
	}
}
