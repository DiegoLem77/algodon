﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cotton.Controls
{
	public partial class ErrorMessage : UserControl
	{
		public ErrorMessage()
		{
			InitializeComponent();
			this.errorIcon.IconSize = 25;
		}
		private Font _Font = UserControl.DefaultFont;
		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		public override Color ForeColor
		{
			get
			{
				return lblMessageError.ForeColor;
			}

			set
			{
				this.lblMessageError.ForeColor = value;
			}
		}

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		public override string Text
		{
			get
			{
				return lblMessageError.Text;
			}
			set
			{
				lblMessageError.Text = value;
			}

		}

		[Description("Sets error font")]
		public override Font Font
		{
			get
			{
				return _Font;
			}
			set
			{
				_Font = lblMessageError.Font = value;
			}

		}

		private void ErrorMessage_Load(object sender, EventArgs e)
		{ 
			this.errorIcon.IconSize = 25;
		}
	}
}
