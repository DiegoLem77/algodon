﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.General;
namespace Cotton.Controls
{
	public partial class InfoAlert : UserControl
	{
		FontFamily Poppins;
		public InfoAlert()
		{
			InitializeComponent();
			Poppins = Utils.ImportFont(nameof(Properties.Resources.Poppins_Medium)).Families[0];
			lblMessage.Font = new Font(Poppins, lblMessage.Font.Size);
		}

		public string Message
		{
			get
			{
				return this.lblMessage.Text;
			}

			set
			{
				this.lblMessage.Text = value;
			}
		}

		public void setMessage(string message)
		{
			this.lblMessage.Text = message;
		}
	}
}
