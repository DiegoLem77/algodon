﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace Cotton.Controls
{

	public enum AlertType
	{
		Success,
		Info,
		Danger,
		Warning
	}

	public partial class Alert : UserControl
	{

		private Font _Font = UserControl.DefaultFont;
		public Alert()
		{
			InitializeComponent();
			base.AutoScaleMode = AutoScaleMode.None;
			this.AlertType = AlertType.Danger;
			this.TimeLapse = 0;
		}

		[Description("Sets message font")]
		public override Font Font
		{
			get { return _Font; }
			set { _Font = base.Font = value; }
		}


		public float FontSizeTitle
		{
			get { return this.lblMessage.Font.Size; }
			set { this.lblMessage.Font = new Font(_Font.FontFamily, value);}
		}
		public float FontSizeBody
		{
			get { return this.lblTextMessage.Font.Size; }
			set { this.lblTextMessage.Font = new Font(_Font.FontFamily, value); }
		}

		public AlertType AlertType { get; set; }

		public int TimeLapse { get; set; }

		public string Title
		{
			get
			{
				return this.lblMessage.Text;
			}

			set
			{
				this.lblMessage.Text = value;
			}
		}

		public string Message
		{
			get
			{
				return this.lblTextMessage.Text;
			}

			set
			{
				this.lblTextMessage.Text = value;
			}
		}

		public void setTitle(string message)
		{
			this.lblMessage.Text = message;
		}
		public void setMessage(string message)
		{
			this.lblTextMessage.Text = message;
		}
		private void Alert_Load(object sender, System.EventArgs e)
		{
			this.iconAlert.IconSize = 40;
			this.changeAlertType(AlertType);
			if (TimeLapse > 0)
			{
				tmrAlert.Interval = TimeLapse;
				this.Visible = false;
			}
			else
			{
				this.Visible = true;
			}
		}

		public void Start()
		{
			fadeIn.Show(this);
			this.Visible = true;
			tmrAlert.Start();
		}

		public void changeAlertType(AlertType alertType)
		{
			this.AlertType = alertType;
			iconAlert.IconColor = Color.White;
			lblMessage.ForeColor = Color.SeaGreen;
			switch (alertType)
			{
				case AlertType.Info:
					//bunifuGradientPanel1.GradientBottomLeft = Color.FromArgb(161, 196, 253);
					//bunifuGradientPanel1.GradientTopLeft = Color.FromArgb(161, 196, 253);
					//bunifuGradientPanel1.GradientBottomRight = Color.FromArgb(194, 233, 251);
					//bunifuGradientPanel1.GradientTopRight = Color.FromArgb(194, 233, 251);
					iconAlert.IconChar = FontAwesome.Sharp.IconChar.InfoCircle;
					iconAlert.IconColor = Color.CadetBlue;
					lblMessage.ForeColor = Color.CadetBlue;
					break;
				case AlertType.Success:
					//bunifuGradientPanel1.GradientBottomLeft = Color.FromArgb(56, 249, 215);
					//bunifuGradientPanel1.GradientTopLeft = Color.FromArgb(56, 249, 215);
					//bunifuGradientPanel1.GradientBottomRight = Color.FromArgb(67, 233, 123);
					//bunifuGradientPanel1.GradientTopRight = Color.FromArgb(67, 233, 123);
					iconAlert.IconChar = FontAwesome.Sharp.IconChar.Check;
					lblMessage.ForeColor = Color.SeaGreen;
					iconAlert.IconColor = Color.SeaGreen;
					break;
				case AlertType.Warning:
					break;
				case AlertType.Danger:
					//bunifuGradientPanel1.GradientBottomLeft = Color.FromArgb(255, 177, 153);
					//bunifuGradientPanel1.GradientTopLeft = Color.FromArgb(255, 177, 153);
					//bunifuGradientPanel1.GradientBottomRight = Color.FromArgb(255, 8, 68);
					//bunifuGradientPanel1.GradientTopRight = Color.FromArgb(255, 8, 68);
					iconAlert.IconChar = FontAwesome.Sharp.IconChar.TimesCircle;
					lblMessage.ForeColor = Color.DarkRed;
					iconAlert.IconColor = Color.FromArgb(228, 27, 87);
					break;
				default:
					break;
			}
		}

		private void tmrAlert_Tick(object sender, System.EventArgs e)
		{
			tmrAlert.Stop();
			fadeIn.Hide(this);
			this.Visible = false;
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			fadeIn.Hide(this);
			this.Visible = false;
		}

		private void lblTextMessage_Click(object sender, System.EventArgs e)
		{

		}

		private void Alert_MouseHover(object sender, System.EventArgs e)
		{
			//MessageBox.Show("se pausa");
			this.tmrAlert.Stop();
		}

		private void Alert_MouseLeave(object sender, System.EventArgs e)
		{
			this.tmrAlert.Start();
		}

		private void splitContainer2_MouseEnter(object sender, System.EventArgs e)
		{
			//MessageBox.Show("hola");
		}
	}
}
