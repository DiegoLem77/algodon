﻿namespace Cotton.Controls
{
	partial class InfoAlert
	{
		/// <summary> 
		/// Variable del diseñador necesaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Limpiar los recursos que se estén usando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de componentes

		/// <summary> 
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido de este método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InfoAlert));
			this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.iconAlert = new FontAwesome.Sharp.IconPictureBox();
			this.lblMessage = new System.Windows.Forms.Label();
			this.bunifuGradientPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.iconAlert)).BeginInit();
			this.SuspendLayout();
			// 
			// bunifuGradientPanel1
			// 
			this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
			this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.bunifuGradientPanel1.Controls.Add(this.splitContainer1);
			this.bunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(196)))), ((int)(((byte)(253)))));
			this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(233)))), ((int)(((byte)(251)))));
			this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(196)))), ((int)(((byte)(253)))));
			this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(233)))), ((int)(((byte)(251)))));
			this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 0);
			this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
			this.bunifuGradientPanel1.Quality = 10;
			this.bunifuGradientPanel1.Size = new System.Drawing.Size(305, 57);
			this.bunifuGradientPanel1.TabIndex = 2;
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.iconAlert);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.lblMessage);
			this.splitContainer1.Size = new System.Drawing.Size(305, 57);
			this.splitContainer1.SplitterDistance = 54;
			this.splitContainer1.TabIndex = 3;
			// 
			// iconAlert
			// 
			this.iconAlert.BackColor = System.Drawing.Color.Transparent;
			this.iconAlert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.iconAlert.Dock = System.Windows.Forms.DockStyle.Fill;
			this.iconAlert.IconChar = FontAwesome.Sharp.IconChar.InfoCircle;
			this.iconAlert.IconColor = System.Drawing.Color.White;
			this.iconAlert.IconSize = 54;
			this.iconAlert.Location = new System.Drawing.Point(0, 0);
			this.iconAlert.Name = "iconAlert";
			this.iconAlert.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
			this.iconAlert.Size = new System.Drawing.Size(54, 57);
			this.iconAlert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.iconAlert.TabIndex = 2;
			this.iconAlert.TabStop = false;
			// 
			// lblMessage
			// 
			this.lblMessage.BackColor = System.Drawing.Color.Transparent;
			this.lblMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMessage.ForeColor = System.Drawing.Color.White;
			this.lblMessage.Location = new System.Drawing.Point(0, 0);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(247, 57);
			this.lblMessage.TabIndex = 0;
			this.lblMessage.Text = "Message Alert";
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// InfoAlert
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.bunifuGradientPanel1);
			this.Name = "InfoAlert";
			this.Size = new System.Drawing.Size(305, 57);
			this.bunifuGradientPanel1.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.iconAlert)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private FontAwesome.Sharp.IconPictureBox iconAlert;
		private System.Windows.Forms.Label lblMessage;
	}
}
