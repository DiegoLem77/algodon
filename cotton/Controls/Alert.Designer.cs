﻿namespace Cotton.Controls
{
	partial class Alert
	{
		/// <summary> 
		/// Variable del diseñador necesaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Limpiar los recursos que se estén usando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de componentes

		/// <summary> 
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido de este método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Alert));
			this.tmrAlert = new System.Windows.Forms.Timer(this.components);
			this.fadeIn = new BunifuAnimatorNS.BunifuTransition(this.components);
			this.panel2 = new System.Windows.Forms.Panel();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.iconAlert = new FontAwesome.Sharp.IconPictureBox();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.PictureBox();
			this.lblMessage = new System.Windows.Forms.Label();
			this.lblTextMessage = new System.Windows.Forms.Label();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.iconAlert)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
			this.SuspendLayout();
			// 
			// tmrAlert
			// 
			this.tmrAlert.Tick += new System.EventHandler(this.tmrAlert_Tick);
			// 
			// fadeIn
			// 
			this.fadeIn.AnimationType = BunifuAnimatorNS.AnimationType.Transparent;
			this.fadeIn.Cursor = null;
			animation1.AnimateOnlyDifferences = true;
			animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
			animation1.LeafCoeff = 0F;
			animation1.MaxTime = 1F;
			animation1.MinTime = 0F;
			animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
			animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
			animation1.MosaicSize = 0;
			animation1.Padding = new System.Windows.Forms.Padding(0);
			animation1.RotateCoeff = 0F;
			animation1.RotateLimit = 0F;
			animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
			animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
			animation1.TimeCoeff = 0F;
			animation1.TransparencyCoeff = 1F;
			this.fadeIn.DefaultAnimation = animation1;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.splitContainer2);
			this.fadeIn.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(374, 116);
			this.panel2.TabIndex = 5;
			// 
			// splitContainer2
			// 
			this.fadeIn.SetDecoration(this.splitContainer2, BunifuAnimatorNS.DecorationType.None);
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer2.IsSplitterFixed = true;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.iconAlert);
			this.fadeIn.SetDecoration(this.splitContainer2.Panel1, BunifuAnimatorNS.DecorationType.None);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.splitContainer1);
			this.fadeIn.SetDecoration(this.splitContainer2.Panel2, BunifuAnimatorNS.DecorationType.None);
			this.splitContainer2.Size = new System.Drawing.Size(374, 116);
			this.splitContainer2.SplitterDistance = 60;
			this.splitContainer2.SplitterWidth = 1;
			this.splitContainer2.TabIndex = 5;
			// 
			// iconAlert
			// 
			this.iconAlert.BackColor = System.Drawing.Color.Transparent;
			this.iconAlert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.fadeIn.SetDecoration(this.iconAlert, BunifuAnimatorNS.DecorationType.None);
			this.iconAlert.Dock = System.Windows.Forms.DockStyle.Top;
			this.iconAlert.ForeColor = System.Drawing.Color.MediumSeaGreen;
			this.iconAlert.IconChar = FontAwesome.Sharp.IconChar.Check;
			this.iconAlert.IconColor = System.Drawing.Color.MediumSeaGreen;
			this.iconAlert.IconSize = 60;
			this.iconAlert.Location = new System.Drawing.Point(0, 0);
			this.iconAlert.Name = "iconAlert";
			this.iconAlert.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
			this.iconAlert.Size = new System.Drawing.Size(60, 80);
			this.iconAlert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.iconAlert.TabIndex = 3;
			this.iconAlert.TabStop = false;
			this.iconAlert.MouseEnter += new System.EventHandler(this.splitContainer2_MouseEnter);
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
			this.fadeIn.SetDecoration(this.splitContainer1, BunifuAnimatorNS.DecorationType.None);
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.panel1);
			this.splitContainer1.Panel1.Controls.Add(this.lblMessage);
			this.fadeIn.SetDecoration(this.splitContainer1.Panel1, BunifuAnimatorNS.DecorationType.None);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.lblTextMessage);
			this.fadeIn.SetDecoration(this.splitContainer1.Panel2, BunifuAnimatorNS.DecorationType.None);
			this.splitContainer1.Size = new System.Drawing.Size(313, 116);
			this.splitContainer1.SplitterDistance = 36;
			this.splitContainer1.TabIndex = 5;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnClose);
			this.fadeIn.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel1.Location = new System.Drawing.Point(277, 0);
			this.panel1.Margin = new System.Windows.Forms.Padding(10);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(36, 36);
			this.panel1.TabIndex = 1;
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
			this.fadeIn.SetDecoration(this.btnClose, BunifuAnimatorNS.DecorationType.None);
			this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
			this.btnClose.Location = new System.Drawing.Point(3, 3);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(30, 30);
			this.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.btnClose.TabIndex = 1;
			this.btnClose.TabStop = false;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.btnClose.MouseEnter += new System.EventHandler(this.splitContainer2_MouseEnter);
			// 
			// lblMessage
			// 
			this.lblMessage.BackColor = System.Drawing.Color.Transparent;
			this.fadeIn.SetDecoration(this.lblMessage, BunifuAnimatorNS.DecorationType.None);
			this.lblMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMessage.ForeColor = System.Drawing.Color.SeaGreen;
			this.lblMessage.Location = new System.Drawing.Point(0, 0);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(313, 36);
			this.lblMessage.TabIndex = 0;
			this.lblMessage.Text = "Message Title Alert";
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblMessage.MouseEnter += new System.EventHandler(this.splitContainer2_MouseEnter);
			// 
			// lblTextMessage
			// 
			this.lblTextMessage.AutoEllipsis = true;
			this.fadeIn.SetDecoration(this.lblTextMessage, BunifuAnimatorNS.DecorationType.None);
			this.lblTextMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTextMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTextMessage.Location = new System.Drawing.Point(0, 0);
			this.lblTextMessage.Name = "lblTextMessage";
			this.lblTextMessage.Size = new System.Drawing.Size(313, 76);
			this.lblTextMessage.TabIndex = 0;
			this.lblTextMessage.Text = resources.GetString("lblTextMessage.Text");
			this.lblTextMessage.Click += new System.EventHandler(this.lblTextMessage_Click);
			this.lblTextMessage.MouseEnter += new System.EventHandler(this.splitContainer2_MouseEnter);
			// 
			// Alert
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.panel2);
			this.fadeIn.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
			this.Name = "Alert";
			this.Size = new System.Drawing.Size(374, 116);
			this.Load += new System.EventHandler(this.Alert_Load);
			this.MouseLeave += new System.EventHandler(this.Alert_MouseLeave);
			this.MouseHover += new System.EventHandler(this.Alert_MouseHover);
			this.panel2.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.iconAlert)).EndInit();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Timer tmrAlert;
		private BunifuAnimatorNS.BunifuTransition fadeIn;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private FontAwesome.Sharp.IconPictureBox iconAlert;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox btnClose;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.Label lblTextMessage;
	}
}
