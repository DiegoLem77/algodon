﻿using Cotton.General;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Cotton.Controls
{
	public partial class MaterialTextBox : UserControl
	{

		//private Font _Font = UserControl.DefaultFont;
		[Bindable(true)]
		[Browsable(true)]
		[Category("Appearance")]
		[Description("The font of the text in the control")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Font Font { get => base.Font; set => base.Font = value; }
		public MaterialTextBox()
		{
			InitializeComponent();
		}

		private void MaterialTextBox_Resize(object sender, EventArgs e)
		{
			txtData.Top = (this.Height - txtData.Height) / 2;
		}


		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		public override string Text
		{
			get
			{
				return this.txtData.Text;
			}
			set
			{
				this.txtData.Text = value;
			}
		}

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		public char PasswordChar
		{
			get
			{
				return txtData.PasswordChar;
			}
			set
			{
				this.txtData.PasswordChar = value;
			}
		}

		private void MaterialTextBox_Load(object sender, EventArgs e)
		{
			txtData.Top = (this.Height - txtData.Height) / 2;
			txtData.Width = this.Width;
		}

		public void SetTextBoxFont(FontFamily font, float sizeFont)
		{
			this.txtData.Font = new Font(font, sizeFont);
		}

		private void MaterialTextBox_FontChanged(object sender, EventArgs e)
		{
			
		}
	}
}
