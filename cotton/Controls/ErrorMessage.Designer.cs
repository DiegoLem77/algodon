﻿namespace Cotton.Controls
{
	partial class ErrorMessage
	{
		/// <summary> 
		/// Variable del diseñador necesaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Limpiar los recursos que se estén usando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de componentes

		/// <summary> 
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido de este método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.errorIcon = new FontAwesome.Sharp.IconPictureBox();
			this.lblMessageError = new System.Windows.Forms.Label();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			((System.ComponentModel.ISupportInitialize)(this.errorIcon)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// errorIcon
			// 
			this.errorIcon.BackColor = System.Drawing.Color.Transparent;
			this.errorIcon.Dock = System.Windows.Forms.DockStyle.Fill;
			this.errorIcon.Flip = FontAwesome.Sharp.FlipOrientation.Horizontal;
			this.errorIcon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(65)))), ((int)(((byte)(56)))));
			this.errorIcon.IconChar = FontAwesome.Sharp.IconChar.ExclamationTriangle;
			this.errorIcon.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(65)))), ((int)(((byte)(56)))));
			this.errorIcon.IconSize = 25;
			this.errorIcon.Location = new System.Drawing.Point(0, 0);
			this.errorIcon.MaximumSize = new System.Drawing.Size(35, 41);
			this.errorIcon.Name = "errorIcon";
			this.errorIcon.Size = new System.Drawing.Size(32, 41);
			this.errorIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.errorIcon.TabIndex = 3;
			this.errorIcon.TabStop = false;
			// 
			// lblMessageError
			// 
			this.lblMessageError.BackColor = System.Drawing.Color.Transparent;
			this.lblMessageError.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMessageError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
			this.lblMessageError.Location = new System.Drawing.Point(0, 0);
			this.lblMessageError.Name = "lblMessageError";
			this.lblMessageError.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
			this.lblMessageError.Size = new System.Drawing.Size(174, 41);
			this.lblMessageError.TabIndex = 4;
			this.lblMessageError.Text = "Message Error";
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.IsSplitterFixed = true;
			this.splitContainer1.Location = new System.Drawing.Point(10, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.errorIcon);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.lblMessageError);
			this.splitContainer1.Size = new System.Drawing.Size(210, 41);
			this.splitContainer1.SplitterDistance = 32;
			this.splitContainer1.TabIndex = 5;
			// 
			// ErrorMessage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.splitContainer1);
			this.Name = "ErrorMessage";
			this.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
			this.Size = new System.Drawing.Size(220, 41);
			this.Load += new System.EventHandler(this.ErrorMessage_Load);
			((System.ComponentModel.ISupportInitialize)(this.errorIcon)).EndInit();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private FontAwesome.Sharp.IconPictureBox errorIcon;
		private System.Windows.Forms.Label lblMessageError;
		private System.Windows.Forms.SplitContainer splitContainer1;
	}
}
