﻿using System.Drawing;
using System.Windows.Forms;
using Cotton.General;
namespace Cotton.Controls
{
	public partial class ErrorAlert : UserControl
	{
		public ErrorAlert()
		{
			InitializeComponent();
		}

		public string Message
		{
			get
			{
				return this.lblMessage.Text;
			}

			set
			{
				this.lblMessage.Text = value;
			}
		}

		public void setMessage(string message)
		{
			this.lblMessage.Text = message;
		}

	}
}
