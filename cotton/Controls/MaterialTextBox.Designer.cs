﻿namespace Cotton.Controls
{
	partial class MaterialTextBox
	{
		/// <summary> 
		/// Variable del diseñador necesaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Limpiar los recursos que se estén usando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de componentes

		/// <summary> 
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido de este método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtData = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// txtData
			// 
			this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtData.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtData.Location = new System.Drawing.Point(10, 0);
			this.txtData.Name = "txtData";
			this.txtData.Size = new System.Drawing.Size(245, 20);
			this.txtData.TabIndex = 0;
			// 
			// MaterialTextBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.Controls.Add(this.txtData);
			this.Name = "MaterialTextBox";
			this.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
			this.Size = new System.Drawing.Size(265, 42);
			this.Load += new System.EventHandler(this.MaterialTextBox_Load);
			this.FontChanged += new System.EventHandler(this.MaterialTextBox_FontChanged);
			this.Resize += new System.EventHandler(this.MaterialTextBox_Resize);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtData;
	}
}
