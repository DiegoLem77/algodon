﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cotton.Models
{
    class UbicationModel : AbstractModel<ubications>
    {
        public static List<ubications> SearchByUbication(string param)
        {
            using (var ctx = new CottonContext())
            {
                DbSet<ubications> DbSet = ctx.Set<ubications>();
                return DbSet.Where(c => c.ubication.Contains(param)).ToList();
            }
        }
    }
}
