﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cotton.Models
{
    class EquipmentModel : AbstractModel<equipment>
    {
        public static List<equipment> SearchByNameCategory(string param)
        {
            using (var ctx = new CottonContext())
            {
                DbSet<equipment> DbSet = ctx.Set<equipment>();
                return DbSet.Where(c => c.name.Contains(param) || c.categories.Name.Contains(param)).ToList();
            }
        }
    }
}
