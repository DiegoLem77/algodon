using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotton.Models
{
    [Table("activity_resources")]
    public partial class ActivityResource : AbstractModel<ActivityResource>
    {

        [Required]
        [StringLength(300)]
        [Column("path")]
        public string Path { get; set; }

        [Required]
        [StringLength(200)]
        [Column("name")]
        public string Name { get; set; }

        [Column("description", TypeName = "text")]
        public string Description { get; set; }

        [Column("status_id")]
        public int StatusId { get; set; }

        [Required]
        [StringLength(36)]
        [Column("activity_id")]
        public string ActivityId { get; set; }

        [Browsable(false)]
        public virtual Activity Activity { get; set; }

        [Browsable(false)]
        public virtual Status Status { get; set; }
    }
}
