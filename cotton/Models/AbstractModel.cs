﻿using Cotton.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

public abstract class AbstractModel<Model> where Model : AbstractModel<Model>
{
    [Key]
    [Column("id")]
    [StringLength(36)]
    public string Id { get; set; }

    [Column("created_at")]
    public DateTime? CreatedAt { get; set; }

    [Column("updated_at")]
    public DateTime? UpdatedAt { get; set; }

    public static List<Model> List()
    {
        using (var ctx = new CottonContext())
        {
            DbSet<Model> DbSet = ctx.Set<Model>();

            var result = from t in DbSet select t;
            return result.ToList();
        }
    }

    public virtual bool Insert()
    {
        using (var ctx = new CottonContext())
        {
            try
            {
                DbSet<Model> DbSet = ctx.Set<Model>();

                bool f = false;
                String uuid;

                do
                {
                    uuid = Guid.NewGuid().ToString();
                    f = (from b in DbSet where b.Id == uuid select b).Count() > 0;
                } while (f);

                this.Id = uuid;
                this.CreatedAt = DateTime.Now;


                DbSet.Add((Model)this);
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }

    public static Model GetById(Object id)
    {
        using (var ctx = new CottonContext())
        {
            DbSet<Model> DbSet = ctx.Set<Model>();
            return DbSet.Find(id);
        }
    }

    public virtual bool Update(String id)
    {
        using (var ctx = new CottonContext())
        {
            DbSet<Model> DbSet = ctx.Set<Model>();

            try
            {
                Model entity = (Model)this;

                Model old = DbSet.Find(id);
                if (old != null)
                {
                    entity.UpdatedAt = DateTime.Now;

                    ctx.Entry(old).CurrentValues.SetValues(entity);
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
    }

    public virtual bool Delete()
    {
        using (var ctx = new CottonContext())
        {
            DbSet<Model> DbSet = ctx.Set<Model>();

            try
            {
                string id = this.Id;

                Model entity = DbSet.Find(id);
                if (entity != null)
                {
                    DbSet.Remove(entity);
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}