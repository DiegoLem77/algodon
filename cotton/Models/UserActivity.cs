﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cotton.Models
{
    class UserActivity
    {
        private int id;
        private String name;
        private String lastName;
        private String dUI;
        private String institution;
        private int iu;

        public String essentialInformation
        {
            get { return DUI + " - " + LastName + ", " + Name; }
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string DUI { get => dUI; set => dUI = value; }
        public string Institution { get => institution; set => institution = value; }
        public int Iu { get => iu; set => iu = value; }
    }
}
