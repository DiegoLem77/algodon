namespace Cotton.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class academic_profile:AbstractModel<academic_profile>
    {

        public byte? study { get; set; }

        [Required]
        [StringLength(36)]
        public string academic_leve_typel_id { get; set; }

        [StringLength(255)]
        public string description_not_study { get; set; }

        [StringLength(200)]
        public string other_courses { get; set; }

        public byte? computation_skills { get; set; }

        [Required]
        [StringLength(36)]
        public string computation_level_id { get; set; }

        public byte? english_skills { get; set; }

        [Required]
        [StringLength(36)]
        public string english_level_id { get; set; }

        public byte? internet_skills { get; set; }

        [Required]
        [StringLength(36)]
        public string internet_level_id { get; set; }

        [StringLength(100)]
        public string other_skills { get; set; }

        [Required]
        [StringLength(36)]
        public string user_id { get; set; }

        public virtual academic_level_type academic_level_type { get; set; }

        public virtual skill_level_type skill_level_type { get; set; }

        public virtual skill_level_type skill_level_type1 { get; set; }

        public virtual skill_level_type skill_level_type2 { get; set; }

        public virtual User user { get; set; }
    }
}
