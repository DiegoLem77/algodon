using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotton.Models
{
    [Table("category_types")]
    public partial class CategoryTypes : AbstractModel<CategoryTypes>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CategoryTypes()
        {
            CategoriesCategoryTypes = new HashSet<CategoriesCategoryTypes>();
        }

        [Required]
        [Column("code")]
        [StringLength(10)]
        public string Code { get; set; }

        [Required]
        [Column("name")]
        [StringLength(200)]
        public string Name { get; set; }

        [Browsable(false)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CategoriesCategoryTypes> CategoriesCategoryTypes { get; set; }
    }
}
