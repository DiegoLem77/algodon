namespace Cotton.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class users_user_types
    {
        public int id { get; set; }

        [Required]
        [StringLength(36)]
        public string user_id { get; set; }

        public int type_id { get; set; }

        public virtual user_types user_types { get; set; }

        public virtual User user { get; set; }
    }
}
