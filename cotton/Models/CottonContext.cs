namespace Cotton.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CottonContext : DbContext
    {
        public CottonContext()
            : base("name=CottonContext")
        {
        }

        public virtual DbSet<academic_level_type> academic_level_type { get; set; }
        public virtual DbSet<academic_profile> academic_profile { get; set; }
        public virtual DbSet<Activity> activities { get; set; }
        public virtual DbSet<activities_categories> activities_categories { get; set; }
        public virtual DbSet<activities_visitors> activities_visitors { get; set; }
        public virtual DbSet<ActivityResource> activity_resource { get; set; }
        public virtual DbSet<attendance> attendance { get; set; }
        public virtual DbSet<Category> categories { get; set; }
        public virtual DbSet<CategoriesCategoryTypes> categories_category_types { get; set; }
        public virtual DbSet<CategoryTypes> category_types { get; set; }
        public virtual DbSet<emergency_contacts> emergency_contacts { get; set; }
        public virtual DbSet<equipment> equipments { get; set; }
        public virtual DbSet<Institution> Institutions { get; set; }
        public virtual DbSet<InstitutionsUsers> InstitutionsUsers { get; set; }
        public virtual DbSet<loan> loans { get; set; }
        public virtual DbSet<skill_level_type> skill_level_type { get; set; }
        public virtual DbSet<Status> status { get; set; }
        public virtual DbSet<ubications> ubications { get; set; }
        public virtual DbSet<user_types> user_types { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<users_user_types> UsersUserTypes { get; set; }
        public virtual DbSet<work_profile> work_profile { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<academic_level_type>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<academic_level_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<academic_level_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<academic_level_type>()
                .HasMany(e => e.academic_profile)
                .WithRequired(e => e.academic_level_type)
                .HasForeignKey(e => e.academic_leve_typel_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<academic_profile>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<academic_profile>()
                .Property(e => e.academic_leve_typel_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<academic_profile>()
                .Property(e => e.description_not_study)
                .IsUnicode(false);

            modelBuilder.Entity<academic_profile>()
                .Property(e => e.other_courses)
                .IsUnicode(false);

            modelBuilder.Entity<academic_profile>()
                .Property(e => e.computation_level_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<academic_profile>()
                .Property(e => e.english_level_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<academic_profile>()
                .Property(e => e.internet_level_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<academic_profile>()
                .Property(e => e.other_skills)
                .IsUnicode(false);

            modelBuilder.Entity<academic_profile>()
                .Property(e => e.user_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Activity>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Activity>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Activity>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.ActivitiesCategories)
                .WithRequired(e => e.activity)
                .HasForeignKey(e => e.activity_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.ActivitiesVisitors)
                .WithRequired(e => e.activity)
                .HasForeignKey(e => e.activity_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.Resources)
                .WithRequired(e => e.Activity)
                .HasForeignKey(e => e.ActivityId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<activities_categories>()
                .Property(e => e.activity_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<activities_categories>()
                .Property(e => e.category_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<activities_visitors>()
                .Property(e => e.activity_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ActivityResource>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ActivityResource>()
                .Property(e => e.Path)
                .IsUnicode(false);

            modelBuilder.Entity<ActivityResource>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ActivityResource>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<ActivityResource>()
                .Property(e => e.ActivityId)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<attendance>()
                .Property(e => e.id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<attendance>()
                .Property(e => e.user_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Category>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Category>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Category>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Category>()
                .HasMany(e => e.activities_categories)
                .WithRequired(e => e.category)
                .HasForeignKey(e => e.category_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Category>()
                .HasMany(e => e.categories_category_types)
                .WithRequired(e => e.Category)
                .HasForeignKey(e => e.CategoryId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Category>()
                .HasMany(e => e.equipments)
                .WithRequired(e => e.categories)
                .HasForeignKey(e => e.type_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CategoriesCategoryTypes>()
                .Property(e => e.CategoryId)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CategoriesCategoryTypes>()
                .Property(e => e.TypeId)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CategoryTypes>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CategoryTypes>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<CategoryTypes>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<CategoryTypes>()
                .HasMany(e => e.CategoriesCategoryTypes)
                .WithRequired(e => e.CategoryType)
                .HasForeignKey(e => e.TypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<emergency_contacts>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<emergency_contacts>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<emergency_contacts>()
                .Property(e => e.last_name)
                .IsUnicode(false);

            modelBuilder.Entity<emergency_contacts>()
                .Property(e => e.relationship)
                .IsUnicode(false);

            modelBuilder.Entity<emergency_contacts>()
                .Property(e => e.landline)
                .IsUnicode(false);

            modelBuilder.Entity<emergency_contacts>()
                .Property(e => e.mobile_phone)
                .IsUnicode(false);

            modelBuilder.Entity<emergency_contacts>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<emergency_contacts>()
                .Property(e => e.user_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.ubication_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.type_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .HasMany(e => e.loans)
                .WithRequired(e => e.equipments)
                .HasForeignKey(e => e.equipment_id)
                .WillCascadeOnDelete(false);

			modelBuilder.Entity<Institution>()
				.Property(e => e.Id)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<Institution>()
				.Property(e => e.Name)
				.IsUnicode(false);

			modelBuilder.Entity<Institution>()
				.Property(e => e.Email)
				.IsUnicode(false);

			modelBuilder.Entity<Institution>()
				.Property(e => e.Adress)
				.IsUnicode(false);

			modelBuilder.Entity<Institution>()
				.HasMany(e => e.InstitutionUsers)
				.WithRequired(e => e.Institution)
				.HasForeignKey(e => e.InstitutionId)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<InstitutionsUsers>()
				.Property(e => e.InstitutionId)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<InstitutionsUsers>()
				.Property(e => e.UserId)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<InstitutionsUsers>()
				.HasMany(e => e.ActivitiesVisitors)
				.WithRequired(e => e.institutions_users)
				.HasForeignKey(e => e.visitor_id)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<loan>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<loan>()
                .Property(e => e.equipment_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<loan>()
                .Property(e => e.user_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<skill_level_type>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<skill_level_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<skill_level_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<skill_level_type>()
                .HasMany(e => e.academic_profile)
                .WithRequired(e => e.skill_level_type)
                .HasForeignKey(e => e.computation_level_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<skill_level_type>()
                .HasMany(e => e.academic_profile1)
                .WithRequired(e => e.skill_level_type1)
                .HasForeignKey(e => e.english_level_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<skill_level_type>()
                .HasMany(e => e.academic_profile2)
                .WithRequired(e => e.skill_level_type2)
                .HasForeignKey(e => e.internet_level_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Status>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.Activities)
                .WithRequired(e => e.Status)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.ActivityResources)
                .WithRequired(e => e.Status)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.Categories)
                .WithRequired(e => e.Status)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.Institutions)
                .WithRequired(e => e.Status)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Status)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ubications>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ubications>()
                .Property(e => e.ubication)
                .IsUnicode(false);

            modelBuilder.Entity<user_types>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<user_types>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<user_types>()
                .HasMany(e => e.users_user_types)
                .WithRequired(e => e.user_types)
                .HasForeignKey(e => e.type_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Lastname)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Dui)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Gender)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Adress)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.AcademicProfile)
                .WithRequired(e => e.user)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Attendances)
                .WithRequired(e => e.user)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.EmergencyContacts)
                .WithRequired(e => e.user)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.InstitutionUsers)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Loans)
                .WithRequired(e => e.users)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UsersUserTypes)
                .WithRequired(e => e.user)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.WorkProfile)
                .WithRequired(e => e.user)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users_user_types>()
                .Property(e => e.user_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<work_profile>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<work_profile>()
                .Property(e => e.vocation)
                .IsUnicode(false);

            modelBuilder.Entity<work_profile>()
                .Property(e => e.work_place)
                .IsUnicode(false);

            modelBuilder.Entity<work_profile>()
                .Property(e => e.work_position)
                .IsUnicode(false);

            modelBuilder.Entity<work_profile>()
                .Property(e => e.user_id)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
