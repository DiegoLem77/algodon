namespace Cotton.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ubications : AbstractModel<ubications>
    {

        [Required]
        [StringLength(500)]
        public string ubication { get; set; }
    }
}
