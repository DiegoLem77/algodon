using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotton.Models
{
    [Table("categories")]
    public partial class Category : AbstractModel<Category>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Category()
        {
            activities_categories = new HashSet<activities_categories>();
            categories_category_types = new HashSet<CategoriesCategoryTypes>();
        }

        [Required]
        [Column("name")]
        [StringLength(200)]
        public string Name { get; set; }

        [Column("description", TypeName = "text")]
        [Required]
        public string Description { get; set; }

        [Column("status_id")]
        public int StatusId { get; set; }

        [Browsable(false)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<activities_categories> activities_categories { get; set; }

        [Browsable(false)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CategoriesCategoryTypes> categories_category_types { get; set; }

        [Browsable(false)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<equipment> equipments { get; set; }

        [Browsable(false)]
        public virtual Status Status { get; set; }
    }
}
