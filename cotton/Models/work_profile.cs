namespace Cotton.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class work_profile:AbstractModel<work_profile>
    {

        public byte? work { get; set; }

        [StringLength(100)]
        public string vocation { get; set; }

        [StringLength(100)]
        public string work_place { get; set; }

        [StringLength(100)]
        public string work_position { get; set; }

        [Required]
        [StringLength(36)]
        public string user_id { get; set; }

        public virtual User user { get; set; }
    }
}
