﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cotton.Models
{
    class EquipmentTypeModel : AbstractModel<Category>
    {
        public static List<Category> SearchByName(string param)
        {
            using (var ctx = new CottonContext())
            {
                DbSet<Category> DbSet = ctx.Set<Category>();
                return DbSet.Where(c => c.Name.Contains(param)).ToList();
            }
        }
    }
}
