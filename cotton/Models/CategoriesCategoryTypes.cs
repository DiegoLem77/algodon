using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotton.Models
{
    [Table("categories_category_types")]
    public partial class CategoriesCategoryTypes
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [StringLength(36)]
        [Column("category_id")]
        public string CategoryId { get; set; }

        [Required]
        [StringLength(36)]
        [Column("type_id")]
        public string TypeId { get; set; }

        [Browsable(false)]
        public virtual Category Category { get; set; }

        [Browsable(false)]
        public virtual CategoryTypes CategoryType { get; set; }
    }
}
