namespace Cotton.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class emergency_contacts:AbstractModel<emergency_contacts>
    {

        [Required]
        [StringLength(45)]
        public string name { get; set; }

        [Required]
        [StringLength(45)]
        public string last_name { get; set; }

        [Required]
        [StringLength(45)]
        public string relationship { get; set; }

        [StringLength(8)]
        public string landline { get; set; }

        [Required]
        [StringLength(8)]
        public string mobile_phone { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        [Required]
        [StringLength(36)]
        public string user_id { get; set; }

        public virtual User user { get; set; }
    }
}
