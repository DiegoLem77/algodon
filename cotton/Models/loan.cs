namespace Cotton.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class loan : AbstractModel<loan>
    {

        [Required]
        [StringLength(36)]
        public string equipment_id { get; set; }

        [Required]
        [StringLength(36)]
        public string user_id { get; set; }

		public int? quantity { get; set; }

		public bool? returned { get; set; }

        public DateTime? devolution_date { get; set; }

        public virtual equipment equipments { get; set; }

        public virtual User users { get; set; }
    }
}
