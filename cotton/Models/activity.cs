using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotton.Models
{
    [Table("activities")]
    public partial class Activity : AbstractModel<Activity>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Activity()
        {
            ActivitiesCategories = new HashSet<activities_categories>();
            ActivitiesVisitors = new HashSet<activities_visitors>();
            Resources = new HashSet<ActivityResource>();
        }

        [Required]
        [Column("name")]
        [StringLength(200)]
        [DisplayName("Nombre")]
        public string Name { get; set; }

        [Column("start_datetime")]
        [DisplayName("Fecha de inicio")]
        public DateTime StartDatetime { get; set; }

        [Column("end_datetime")]
        [DisplayName("Fecha de fin")]
        public DateTime EndDatetime { get; set; }

        [Column("description", TypeName = "text")]
        [DisplayName("Descripción")]
        public string Description { get; set; }

        [Column("status_id")]
        public int StatusId { get; set; }

        [Browsable(false)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<activities_categories> ActivitiesCategories { get; set; }

        [Browsable(false)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<activities_visitors> ActivitiesVisitors { get; set; }

        [Browsable(false)]
        public virtual Status Status { get; set; }

        [Browsable(false)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ActivityResource> Resources { get; set; }
    }
}
