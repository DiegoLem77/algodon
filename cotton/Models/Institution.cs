namespace Cotton.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("institutions")]
    public partial class Institution : AbstractModel<Institution>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Institution()
        {
            InstitutionUsers = new HashSet<InstitutionsUsers>();
        }

        [Required]
        [StringLength(200)]
        [Column("name")]
        [DisplayName("Nombre")]
        public string Name { get; set; }

        [Required]
        [StringLength(200)]
        [Column("email")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [StringLength(400)]
        [Column("adress")]
        [DisplayName("Direcci�n")]
        public string Adress { get; set; }

        [Column("status_id")]
        public int StatusId { get; set; }

        [Browsable(false)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InstitutionsUsers> InstitutionUsers { get; set; }

        [Browsable(false)]
        public virtual Status Status { get; set; }
    }
}
