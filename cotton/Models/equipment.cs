namespace Cotton.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("equipments")]
    public partial class equipment : AbstractModel<equipment>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public equipment()
        {
            loans = new HashSet<loan>();
        }

        [Required]
        [StringLength(100)]
        public string name { get; set; }

        public int? stock { get; set; }

        public int? available_stock { get; set; }

        public DateTime? acquisition_date { get; set; }

        [Required]
        [StringLength(1000)]
        public string description { get; set; }

        [StringLength(36)]
        public string ubication_id { get; set; }

        [Required]
        [StringLength(36)]
        public string type_id { get; set; }

        [Browsable(false)]
        public virtual Category categories { get; set; }

        [Browsable(false)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<loan> loans { get; set; }
    }
}
