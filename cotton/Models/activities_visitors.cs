namespace Cotton.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class activities_visitors
    {
        public int id { get; set; }

        [Required]
        [StringLength(36)]
        public string activity_id { get; set; }

        public int visitor_id { get; set; }

        public virtual Activity activity { get; set; }

        public virtual InstitutionsUsers institutions_users { get; set; }
    }
}
