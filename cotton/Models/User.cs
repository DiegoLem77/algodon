using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotton.Models
{
    [Table("users")]
    public partial class User : AbstractModel<User>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            AcademicProfile = new HashSet<academic_profile>();
            Attendances = new HashSet<attendance>();
            EmergencyContacts = new HashSet<emergency_contacts>();
            InstitutionUsers = new HashSet<InstitutionsUsers>();
            Loans = new HashSet<loan>();
            UsersUserTypes = new HashSet<users_user_types>();
            WorkProfile = new HashSet<work_profile>();
        }

        [Required]
        [Column("name")]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        [Column("lastname")]
        [StringLength(200)]
        public string Lastname { get; set; }

        [Required]
        [Column("code")]
        [StringLength(100)]
        public string Code { get; set; }

        [Required]
        [Column("email")]
        [StringLength(200)]
        public string Email { get; set; }

        [Column("dui")]
        [StringLength(9)]
        public string Dui { get; set; }

        [Column("gender")]
        [StringLength(10)]
        public string Gender { get; set; }

        [Column("password")]
        [StringLength(200)]
        [Browsable(false)]
        public string Password { get; set; }

        [Column("adress")]
        [StringLength(400)]
        public string Adress { get; set; }

        [Column("status_id")]
        public int StatusId { get; set; }

        [NotMapped]
        public string essentialInfo
        {
            get { return Dui + " - " + Lastname + ", " + Name; }
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Browsable(false)]
        public virtual ICollection<academic_profile> AcademicProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Browsable(false)]
        public virtual ICollection<attendance> Attendances { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Browsable(false)]
        public virtual ICollection<emergency_contacts> EmergencyContacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Browsable(false)]
        public virtual ICollection<InstitutionsUsers> InstitutionUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Browsable(false)]
        public virtual ICollection<loan> Loans { get; set; }

        [Browsable(false)]
        public virtual Status Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Browsable(false)]
        public virtual ICollection<users_user_types> UsersUserTypes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Browsable(false)]
        public virtual ICollection<work_profile> WorkProfile { get; set; }
    }

}
