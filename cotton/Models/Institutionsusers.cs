using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotton.Models
{
	[Table("institutions_users")]
	public partial class InstitutionsUsers
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public InstitutionsUsers()
		{
			ActivitiesVisitors = new HashSet<activities_visitors>();
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Required]
		[StringLength(36)]
		[Column("institution_id")]
		public string InstitutionId { get; set; }

		[Required]
		[StringLength(36)]
		[Column("user_id")]
		public string UserId { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<activities_visitors> ActivitiesVisitors { get; set; }

		[Browsable(false)]
		public virtual Institution Institution { get; set; }

		[Browsable(false)]
		public virtual User User { get; set; }
	}
}
