namespace Cotton.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("attendance")]
    public partial class attendance
    {
        [StringLength(36)]
        public string id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? date { get; set; }

        public TimeSpan? entry_time { get; set; }

        public TimeSpan? departure_time { get; set; }

        [Required]
        [StringLength(36)]
        public string user_id { get; set; }

        public virtual User user { get; set; }
    }
}
