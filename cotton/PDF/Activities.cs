﻿using Cotton.Controllers;
using Cotton.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cotton.PDF
{
    class Activities
    {
        public static bool GeneratePDF(string route)
        {
            try
            {
                ActivitiesController ac = new ActivitiesController();
                // Indicamos el tamaño de la pagina de los PDF
                Document doc = new Document(PageSize.A4.Rotate());
                // Indicamos donde se almacenare el PDF
                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(route + @"\Actividades.pdf", FileMode.Create));

                PDFHeaderFooter header = new PDFHeaderFooter();
                writer.PageEvent = header;

                // Colocamos el título y autor del documento
                doc.AddTitle("Reporte: Actividades");
                doc.AddCreator("Centro de Alcance 'El Algodón'");

                // Abrimos el archivo
                doc.Open();
                // Indicamos la tipografia y el tamaño de la misma
                Font standardFont = new Font(Font.FontFamily.COURIER, 11, Font.NORMAL, BaseColor.BLACK);
                Font headerTableFont = new Font(Font.FontFamily.COURIER, 11, Font.BOLDITALIC, BaseColor.WHITE);
                Font titleFont = new Font(Font.FontFamily.COURIER, 14, Font.BOLDITALIC, BaseColor.BLACK);

                //Espacio para separarnos un poco del header
                doc.Add(new Paragraph(""));
                doc.Add(Chunk.NEWLINE);

                // Escribimos el titulo en el documento
                Paragraph title = new Paragraph("Reporte: Actividades", titleFont)
                {
                    Alignment = Element.ALIGN_CENTER
                };

                doc.Add(title);
                doc.Add(Chunk.NEWLINE);

                // Creamos una tabla que contendrá los equipo
                PdfPTable activitiesTable = new PdfPTable(5)
                {
                    WidthPercentage = 100
                };

                // Se asigna el ancho de cada colunma en proporciones
                float[] tableWidths = new float[] { 2f, 4f, 2f, 2f, 1f };
                activitiesTable.SetWidths(tableWidths);

                // Configuramos el header de las columnas de la tabla
                PdfPCell clmName = new PdfPCell(new Phrase("Nombre", headerTableFont))
                {
                    BorderWidth = 0,
                    BorderWidthBottom = 0.75f,
                    BorderColor = BaseColor.WHITE,
                    PaddingBottom = 8,
                    PaddingTop = 5,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = new BaseColor(16, 70, 89)
                };

                PdfPCell clmDescription = new PdfPCell(new Phrase("Descripción", headerTableFont))
                {
                    BorderWidth = 0,
                    BorderWidthBottom = 0.75f,
                    BorderColor = BaseColor.WHITE,
                    PaddingBottom = 8,
                    PaddingTop = 5,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = new BaseColor(16, 70, 89)
                };

                PdfPCell clmStartDate = new PdfPCell(new Phrase("Fecha de inicio", headerTableFont))
                {
                    BorderWidth = 0,
                    BorderWidthBottom = 0.75f,
                    BorderColor = BaseColor.WHITE,
                    PaddingBottom = 8,
                    PaddingTop = 5,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = new BaseColor(16, 70, 89)
                };

                PdfPCell clmEndDate = new PdfPCell(new Phrase("Fecha de fin", headerTableFont))
                {
                    BorderWidth = 0,
                    BorderWidthBottom = 0.75f,
                    BorderColor = BaseColor.WHITE,
                    PaddingBottom = 8,
                    PaddingTop = 5,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = new BaseColor(16, 70, 89)
                };

                PdfPCell clmResources = new PdfPCell(new Phrase("Recursos", headerTableFont))
                {
                    BorderWidth = 0,
                    BorderWidthBottom = 0.75f,
                    BorderColor = BaseColor.WHITE,
                    PaddingBottom = 8,
                    PaddingTop = 5,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = new BaseColor(16, 70, 89)
                };

                // Añadimos el header a la tabla
                activitiesTable.AddCell(clmName);
                activitiesTable.AddCell(clmDescription);
                activitiesTable.AddCell(clmStartDate);
                activitiesTable.AddCell(clmEndDate);
                activitiesTable.AddCell(clmResources);

                // Traemos los datos que del cuerpo de la tabla
                List<Activity> activities = Activity.List();

                // Añadimos los datos a la tabla
                foreach (Activity a in activities)
                {
                    // Creamos las celdas para cada dato y le damos estilo
                    clmName = new PdfPCell(new Phrase(a.Name, standardFont))
                    {
                        BorderWidth = 0.5f,
                        Padding = 10,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };

                    //clmName.HorizontalAlignment = Element.ALIGN_JUSTIFIED;

                    clmDescription = new PdfPCell(new Phrase(a.Description, standardFont))
                    {
                        BorderWidth = 0.5f,
                        Padding = 10,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };

                    clmStartDate = new PdfPCell(new Phrase($"{a.StartDatetime.Day}/{a.StartDatetime.Month}/{a.StartDatetime.Year} {a.StartDatetime.Hour}:{a.StartDatetime.Minute}", standardFont))
                    {
                        BorderWidth = 0.5f,
                        Padding = 10,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };

                    clmEndDate = new PdfPCell(new Phrase($"{a.EndDatetime.Day}/{a.EndDatetime.Month}/{a.EndDatetime.Year} {a.EndDatetime.Hour}:{a.EndDatetime.Minute}", standardFont))
                    {
                        BorderWidth = 0.5f,
                        Padding = 10,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };

                    clmResources = new PdfPCell(new Phrase(ac.GetActivityResources(a).Count().ToString(), standardFont))
                    {
                        BorderWidth = 0.5f,
                        Padding = 10,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };

                    // Añadimos la fila
                    activitiesTable.AddCell(clmName);
                    activitiesTable.AddCell(clmDescription);
                    activitiesTable.AddCell(clmStartDate);
                    activitiesTable.AddCell(clmEndDate);
                    activitiesTable.AddCell(clmResources);

                }

                // Añadimos la tabla
                doc.Add(activitiesTable);

                doc.Close();
                writer.Close();
                return true;
            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
