﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using Cotton.General;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cotton.PDF
{
    class PDFHeaderFooter : PdfPageEventHelper
    {
        // Indicamos la tipografia y el tamaño de la misma
        private Font standardFont = new Font(Font.FontFamily.COURIER, 9, Font.ITALIC, BaseColor.BLACK);

        public override void OnStartPage(PdfWriter writer, Document doc)
        {
            // Obtenemos la imagen
            String rutaLogo =  Utils.GetBasePath() + @"Resources\logo.png";
            Image imgLogo = iTextSharp.text.Image.GetInstance(rutaLogo);
            imgLogo.ScalePercent(6.5f);

            // Creamos la tabla que contendra el header
            PdfPTable header = new PdfPTable(3);
            header.WidthPercentage = 100;

            // Indicamos el tamaño de las columnas en porcentaje
            float[] tableWidths = new float[] { 1f, 2f, 1f};            header.SetWidths(tableWidths);

            // Le damos formato a las partes de nuestro header
            PdfPCell logo = new PdfPCell(imgLogo);
            logo.Border = 0;
            logo.VerticalAlignment = Element.ALIGN_TOP;
            logo.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell name = new PdfPCell(new Phrase("Centro de Alcance \"Por mi barrio\"\nCol. El Algodón", standardFont));
            name.Border = 0;
            name.VerticalAlignment = Element.ALIGN_MIDDLE;
            name.HorizontalAlignment = Element.ALIGN_CENTER;

            // Obteniendo la fecha actual
            DateTime now = DateTime.Now;

            PdfPCell dateNow = new PdfPCell(new Phrase(now.ToString("dd/MM/yyy"), standardFont));
            dateNow.Border = 0;
            dateNow.VerticalAlignment = Element.ALIGN_TOP;
            dateNow.HorizontalAlignment = Element.ALIGN_RIGHT;

            // Añadimos las partes del header al contenedor
            header.AddCell(logo);
            header.AddCell(name);
            header.AddCell(dateNow);
            
            // Añadimos el header
            doc.Add(header);
        }

        public override void OnEndPage(PdfWriter writer, Document doc)
        {
            PdfContentByte cb = writer.DirectContent;
            BaseFont bf = BaseFont.CreateFont(BaseFont.COURIER, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb.SaveState();
            cb.BeginText();
            if(doc.PageSize.Rotation == 0)
                cb.MoveText(445, 20);
            else if(doc.PageSize.Rotation == 90)
                cb.MoveText(691, 30);
            cb.SetFontAndSize(bf, 9);
            cb.ShowText("Universidad Don Bosco");
            cb.EndText();
            cb.RestoreState();
        }
    }
}
