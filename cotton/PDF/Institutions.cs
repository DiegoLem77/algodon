﻿using Cotton.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cotton.PDF
{
    class Institutions
    {
        public static void GeneratePDF(string route)
        {
            // Indicamos el tamaño de la pagina de los PDF
            Document doc = new Document(PageSize.A4.Rotate());
            // Indicamos donde se almacenare el PDF
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(route + @"\Instituciones.pdf", FileMode.Create));

            PDFHeaderFooter header = new PDFHeaderFooter();
            writer.PageEvent = header;

            // Colocamos el título y autor del documento
            doc.AddTitle("Reporte: Instituciones");
            doc.AddCreator("Centro de Alcance 'El Algodón'");

            // Abrimos el archivo
            doc.Open();
            // Indicamos la tipografia y el tamaño de la misma
            Font standardFont = new Font(Font.FontFamily.COURIER, 11, Font.NORMAL, BaseColor.BLACK);
            Font headerTableFont = new Font(Font.FontFamily.COURIER, 11, Font.BOLDITALIC, BaseColor.WHITE);
            Font titleFont = new Font(Font.FontFamily.COURIER, 14, Font.BOLDITALIC, BaseColor.BLACK);

            //Espacio para separarnos un poco del header
            doc.Add(new Paragraph(""));
            doc.Add(Chunk.NEWLINE);

            // Escribimos el titulo en el documento
            Paragraph title = new Paragraph("Reporte: Instituciones", titleFont)
            {
                Alignment = Element.ALIGN_CENTER
            };
            doc.Add(title);
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá los equipo
            PdfPTable institutionsTable = new PdfPTable(3);
            institutionsTable.WidthPercentage = 100;

            // Se asigna el ancho de cada colunma en proporciones
            float[] tableWidths = new float[] { 1f, 1f, 3f };
            institutionsTable.SetWidths(tableWidths);

            // Configuramos el header de las columnas de la tabla
            PdfPCell clmName = new PdfPCell(new Phrase("Nombre", headerTableFont))
            {
                BorderWidth = 0,
                BorderWidthBottom = 0.75f,
                BorderColor = BaseColor.WHITE,
                PaddingBottom = 8,
                PaddingTop = 5,
                VerticalAlignment = Element.ALIGN_MIDDLE,
                HorizontalAlignment = Element.ALIGN_CENTER,
                BackgroundColor = new BaseColor(16, 70, 89)
            };

            PdfPCell clmEmail = new PdfPCell(new Phrase("Email", headerTableFont))
            {
                BorderWidth = 0,
                BorderWidthBottom = 0.75f,
                BorderColor = BaseColor.WHITE,
                PaddingBottom = 8,
                PaddingTop = 5,
                VerticalAlignment = Element.ALIGN_MIDDLE,
                HorizontalAlignment = Element.ALIGN_CENTER,
                BackgroundColor = new BaseColor(16, 70, 89)
            };

            PdfPCell clmAddress = new PdfPCell(new Phrase("Dirección", headerTableFont))
            {
                BorderWidth = 0,
                BorderWidthBottom = 0.75f,
                BorderColor = BaseColor.WHITE,
                PaddingBottom = 8,
                PaddingTop = 5,
                VerticalAlignment = Element.ALIGN_MIDDLE,
                HorizontalAlignment = Element.ALIGN_CENTER,
                BackgroundColor = new BaseColor(16, 70, 89)
            };

            // Añadimos el header a la tabla
            institutionsTable.AddCell(clmName);
            institutionsTable.AddCell(clmEmail);
            institutionsTable.AddCell(clmAddress);

            // Traemos los datos que del cuerpo de la tabla
            List<Institution> institutions = Institution.List();

            // Añadimos los datos a la tabla
            foreach (Institution i in institutions)
            {
                // Creamos las celdas para cada dato y le damos estilo
                clmName = new PdfPCell(new Phrase(i.Name, standardFont))
                {
                    BorderWidth = 0.5f,
                    Padding = 10,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                };

                //clmName.HorizontalAlignment = Element.ALIGN_JUSTIFIED;

                clmEmail = new PdfPCell(new Phrase(i.Email, standardFont))
                {
                    BorderWidth = 0.5f,
                    Padding = 10,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                };

                clmAddress = new PdfPCell(new Phrase(i.Adress, standardFont))
                {
                    BorderWidth = 0.5f,
                    Padding = 10,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                };
                //clmStock.HorizontalAlignment = Element.ALIGN_JUSTIFIED;

                // Añadimos la fila
                institutionsTable.AddCell(clmName);
                institutionsTable.AddCell(clmEmail);
                institutionsTable.AddCell(clmAddress);

            }

            // Añadimos la tabla
            doc.Add(institutionsTable);
            doc.Close();
            writer.Close();
        }
    }
}
