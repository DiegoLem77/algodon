﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cotton.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Cotton.PDF
{
    class InventoryPDF
    {

        // Indicamos la tipografia y el tamaño de la misma
        private static Font standardFont = new Font(Font.FontFamily.COURIER, 11, Font.NORMAL, BaseColor.BLACK);
        private static Font headerTableFont = new Font(Font.FontFamily.COURIER, 11, Font.BOLDITALIC, BaseColor.WHITE);
        private static Font titleFont = new Font(Font.FontFamily.COURIER, 14, Font.BOLDITALIC, BaseColor.BLACK);
        public static void generateEquipmentPDF(string route)
        {
            // Indicamos el tamaño de la pagina de los PDF
            Document doc = new Document(PageSize.A4.Rotate());
            // Indicamos donde se almacenare el PDF
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(route + @"\Equipos.pdf", FileMode.Create));

            PDFHeaderFooter header = new PDFHeaderFooter();
            writer.PageEvent = header;

            // Colocamos el título y autor del documento
            doc.AddTitle("Reporte: Equipos");
            doc.AddCreator("José Cruz");

            // Abrimos el archivo
            doc.Open();

            //Espacio para separarnos un poco del header
            doc.Add(new Paragraph(""));
            doc.Add(Chunk.NEWLINE);

            // Escribimos el titulo en el documento
            Paragraph title = new Paragraph("Reporte: Equipos", titleFont);
            title.Alignment = Element.ALIGN_CENTER;
            doc.Add(title);
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá los equipo
            PdfPTable equipmentTable = new PdfPTable(6);
            equipmentTable.WidthPercentage = 100;

            // Se asigna el ancho de cada colunma en proporciones
            float[] tableWidths = new float[] {3f, 1f, 3f, 5f, 4f, 3f };
            equipmentTable.SetWidths(tableWidths);

            // Configuramos el header de las columnas de la tabla
            PdfPCell clmName = new PdfPCell(new Phrase("Nombre", headerTableFont));
            clmName.BorderWidth = 0;
            clmName.BorderWidthBottom = 0.75f;
            clmName.BorderColor = BaseColor.WHITE;
            clmName.PaddingBottom = 8;
            clmName.PaddingTop = 5;
            clmName.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmName.HorizontalAlignment = Element.ALIGN_CENTER;
            clmName.BackgroundColor = new BaseColor(16, 70, 89);

            PdfPCell clmStock = new PdfPCell(new Phrase("Stock", headerTableFont));
            clmStock.BorderWidth = 0;
            clmStock.BorderWidthBottom = 0.75f;
            clmStock.BorderColor = BaseColor.WHITE;
            clmStock.PaddingBottom = 8;
            clmStock.PaddingTop = 5;
            clmStock.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmStock.HorizontalAlignment = Element.ALIGN_CENTER;
            clmStock.BackgroundColor = new BaseColor(16, 70, 89);

            PdfPCell clmAcquisitionDate = new PdfPCell(new Phrase("Fecha adquisición", headerTableFont));
            clmAcquisitionDate.BorderWidth = 0;
            clmAcquisitionDate.BorderWidthBottom = 0.75f;
            clmAcquisitionDate.BorderColor = BaseColor.WHITE;
            clmAcquisitionDate.PaddingBottom = 8;
            clmAcquisitionDate.PaddingTop = 5;
            clmAcquisitionDate.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmAcquisitionDate.HorizontalAlignment = Element.ALIGN_CENTER;
            clmAcquisitionDate.BackgroundColor = new BaseColor(16, 70, 89);

            PdfPCell clmDescription = new PdfPCell(new Phrase("Descripción", headerTableFont));
            clmDescription.BorderWidth = 0;
            clmDescription.BorderWidthBottom = 0.75f;
            clmDescription.BorderColor = BaseColor.WHITE;
            clmDescription.PaddingBottom = 8;
            clmDescription.PaddingTop = 5;
            clmDescription.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmDescription.HorizontalAlignment = Element.ALIGN_CENTER;
            clmDescription.BackgroundColor = new BaseColor(16, 70, 89);

            PdfPCell clmUbication = new PdfPCell(new Phrase("Ubicación", headerTableFont));
            clmUbication.BorderWidth = 0;
            clmUbication.BorderWidthBottom = 0.75f;
            clmUbication.BorderColor = BaseColor.WHITE;
            clmUbication.PaddingBottom = 8;
            clmUbication.PaddingTop = 5;
            clmUbication.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmUbication.HorizontalAlignment = Element.ALIGN_CENTER;
            clmUbication.BackgroundColor = new BaseColor(16, 70, 89);

            PdfPCell clmType = new PdfPCell(new Phrase("Tipo", headerTableFont));
            clmType.BorderWidth = 0;
            clmType.BorderWidthBottom = 0.75f;
            clmType.BorderColor = BaseColor.WHITE;
            clmType.PaddingBottom = 8;
            clmType.PaddingTop = 5;
            clmType.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmType.HorizontalAlignment = Element.ALIGN_CENTER;
            clmType.BackgroundColor = new BaseColor(16, 70, 89);

            // Añadimos el header a la tabla
            equipmentTable.AddCell(clmName);
            equipmentTable.AddCell(clmStock);
            equipmentTable.AddCell(clmAcquisitionDate);
            equipmentTable.AddCell(clmDescription);
            equipmentTable.AddCell(clmUbication);
            equipmentTable.AddCell(clmType);

            // Traemos los datos que del cuerpo de la tabla
            List<equipment> equipments = equipment.List();

            // Añadimos los datos a la tabla
            foreach(equipment equip in equipments)
            {
                //obtenemos la ubicación
                ubications ubication = ubications.GetById(equip.ubication_id);
                Category cat = Category.GetById(equip.type_id);
                // Creamos las celdas para cada dato y le damos estilo
                clmName = new PdfPCell(new Phrase(equip.name, standardFont));
                clmName.BorderWidth = 0.5f;
                clmName.Padding = 10;
                clmName.VerticalAlignment = Element.ALIGN_MIDDLE;

                clmStock = new PdfPCell(new Phrase(equip.stock.ToString(), standardFont));
                clmStock.BorderWidth = 0.5f;
                clmStock.Padding = 10;
                clmStock.VerticalAlignment = Element.ALIGN_MIDDLE;

                // Le damos formato a la fecha
                DateTime date = (DateTime)equip.acquisition_date;

                clmAcquisitionDate = new PdfPCell(new Phrase(date.ToString("dd/MM/yyyy"), standardFont));
                clmAcquisitionDate.BorderWidth = 0.5f;
                clmAcquisitionDate.Padding = 10;
                clmAcquisitionDate.VerticalAlignment = Element.ALIGN_MIDDLE;

                clmDescription = new PdfPCell(new Phrase(equip.description, standardFont));
                clmDescription.BorderWidth = 0.5f;
                clmDescription.Padding = 10;
                clmDescription.VerticalAlignment = Element.ALIGN_MIDDLE;

                clmUbication = new PdfPCell(new Phrase(ubication.ubication != null ? ubication.ubication : "-", standardFont));
                clmUbication.BorderWidth = 0.5f;
                clmUbication.Padding = 10;
                clmUbication.VerticalAlignment = Element.ALIGN_MIDDLE;

                clmType = new PdfPCell(new Phrase(cat.Name, standardFont));
                clmType.BorderWidth = 0.5f;
                clmType.Padding = 10;
                clmType.VerticalAlignment = Element.ALIGN_MIDDLE;

                // Añadimos la fila
                equipmentTable.AddCell(clmName);
                equipmentTable.AddCell(clmStock);
                equipmentTable.AddCell(clmAcquisitionDate);
                equipmentTable.AddCell(clmDescription);
                equipmentTable.AddCell(clmUbication);
                equipmentTable.AddCell(clmType);

            }

            // Añadimos la tabla
            doc.Add(equipmentTable);

            doc.Close();
            writer.Close();
        }

        public static void generateLoanPDF(string route)
        {
            // Indicamos el tamaño de la pagina de los PDF
            Document doc = new Document(PageSize.A4);
            // Indicamos donde se almacenare el PDF
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(route + @"\Prestamos.pdf", FileMode.Create));

            PDFHeaderFooter header = new PDFHeaderFooter();
            writer.PageEvent = header;

            // Colocamos el título y autor del documento
            doc.AddTitle("Reporte: Prestamos");
            doc.AddCreator("José Cruz");

            // Abrimos el archivo
            doc.Open();

            //Espacio para separarnos un poco del header
            doc.Add(new Paragraph(""));
            doc.Add(Chunk.NEWLINE);

            // Escribimos el titulo en el documento
            Paragraph title = new Paragraph("Reporte: Prestamos", titleFont);
            title.Alignment = Element.ALIGN_CENTER;
            doc.Add(title);
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá los equipo
            PdfPTable loanTable = new PdfPTable(6);
            loanTable.WidthPercentage = 100;

            // Se asigna el ancho de cada colunma en proporciones
            float[] tableWidths = new float[] { 1f, 1f, 1f, 1f, 1f, 1f };
            loanTable.SetWidths(tableWidths);

            // Configuramos el header de las columnas de la tabla
            PdfPCell clmName = new PdfPCell(new Phrase("Nombre", headerTableFont));
            clmName.BorderWidth = 0;
            clmName.BorderWidthBottom = 0.75f;
            clmName.BorderColor = BaseColor.WHITE;
            clmName.PaddingBottom = 8;
            clmName.PaddingTop = 5;
            clmName.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmName.HorizontalAlignment = Element.ALIGN_CENTER;
            clmName.BackgroundColor = new BaseColor(16, 70, 89);

            PdfPCell clmQuantity = new PdfPCell(new Phrase("Cantidad", headerTableFont));
            clmQuantity.BorderWidth = 0;
            clmQuantity.BorderWidthBottom = 0.75f;
            clmQuantity.BorderColor = BaseColor.WHITE;
            clmQuantity.PaddingBottom = 8;
            clmQuantity.PaddingTop = 5;
            clmQuantity.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmQuantity.HorizontalAlignment = Element.ALIGN_CENTER;
            clmQuantity.BackgroundColor = new BaseColor(16, 70, 89);

            PdfPCell clmUser = new PdfPCell(new Phrase("Usuario", headerTableFont));
            clmUser.BorderWidth = 0;
            clmUser.BorderWidthBottom = 0.75f;
            clmUser.BorderColor = BaseColor.WHITE;
            clmUser.PaddingBottom = 8;
            clmUser.PaddingTop = 5;
            clmUser.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmUser.HorizontalAlignment = Element.ALIGN_CENTER;
            clmUser.BackgroundColor = new BaseColor(16, 70, 89);

            PdfPCell clmDate1 = new PdfPCell(new Phrase("Prestado desde", headerTableFont));
            clmDate1.BorderWidth = 0;
            clmDate1.BorderWidthBottom = 0.75f;
            clmDate1.BorderColor = BaseColor.WHITE;
            clmDate1.PaddingBottom = 8;
            clmDate1.PaddingTop = 5;
            clmDate1.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmDate1.HorizontalAlignment = Element.ALIGN_CENTER;
            clmDate1.BackgroundColor = new BaseColor(16, 70, 89);

            PdfPCell clmDate2 = new PdfPCell(new Phrase("Prestado hasta", headerTableFont));
            clmDate2.BorderWidth = 0;
            clmDate2.BorderWidthBottom = 0.75f;
            clmDate2.BorderColor = BaseColor.WHITE;
            clmDate2.PaddingBottom = 8;
            clmDate2.PaddingTop = 5;
            clmDate2.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmDate2.HorizontalAlignment = Element.ALIGN_CENTER;
            clmDate2.BackgroundColor = new BaseColor(16, 70, 89);

            PdfPCell clmStatus = new PdfPCell(new Phrase("Estado", headerTableFont));
            clmStatus.BorderWidth = 0;
            clmStatus.BorderWidthBottom = 0.75f;
            clmStatus.BorderColor = BaseColor.WHITE;
            clmStatus.PaddingBottom = 8;
            clmStatus.PaddingTop = 5;
            clmStatus.VerticalAlignment = Element.ALIGN_MIDDLE;
            clmStatus.HorizontalAlignment = Element.ALIGN_CENTER;
            clmStatus.BackgroundColor = new BaseColor(16, 70, 89);

            // Añadimos el header a la tabla
            loanTable.AddCell(clmName);
            loanTable.AddCell(clmQuantity);
            loanTable.AddCell(clmUser);
            loanTable.AddCell(clmDate1);
            loanTable.AddCell(clmDate2);
            loanTable.AddCell(clmStatus);

            // Traemos los datos que del cuerpo de la tabla
            List<loan> loans = loan.List();

            // Añadimos los datos a la tabla
            foreach (loan loan in loans)
            {
                //obtenemos la informacion correspondiente
                User user = User.GetById(loan.user_id);
                equipment equip = equipment.GetById(loan.equipment_id);

                // Creamos las celdas para cada dato y le damos estilo
                clmName = new PdfPCell(new Phrase(equip.name, standardFont));
                clmName.BorderWidth = 0.5f;
                clmName.Padding = 10;
                clmName.VerticalAlignment = Element.ALIGN_MIDDLE;

                clmQuantity = new PdfPCell(new Phrase(loan.quantity.ToString(), standardFont));
                clmQuantity.BorderWidth = 0.5f;
                clmQuantity.Padding = 10;
                clmQuantity.VerticalAlignment = Element.ALIGN_MIDDLE;

                clmUser = new PdfPCell(new Phrase(user.Name, standardFont));
                clmUser.BorderWidth = 0.5f;
                clmUser.Padding = 10;
                clmUser.VerticalAlignment = Element.ALIGN_MIDDLE;

                // Le damos formato a la fecha
                DateTime date1 = (DateTime)loan.CreatedAt;

                clmDate1 = new PdfPCell(new Phrase(date1.ToString("dd/MM/yyyy HH:mm"), standardFont));
                clmDate1.BorderWidth = 0.5f;
                clmDate1.Padding = 10;
                clmDate1.VerticalAlignment = Element.ALIGN_MIDDLE;

                // Le damos formato a la fecha
                DateTime date2 = (DateTime)loan.devolution_date;

                clmDate2 = new PdfPCell(new Phrase(date2.ToString("dd/MM/yyyy HH:mm"), standardFont));
                clmDate2.BorderWidth = 0.5f;
                clmDate2.Padding = 10;
                clmDate2.VerticalAlignment = Element.ALIGN_MIDDLE;

                string status;
                if (loan.returned == null || loan.returned == false)
                {
                    var devolutionTime = (loan.devolution_date is DateTime toD) ? toD : DateTime.Now;
                    status = (DateTime.Compare(DateTime.Now, devolutionTime) > 0) ? "Retrasado" : "Pendiente";
                }
                else
                    status = "Entregado";

                clmStatus = new PdfPCell(new Phrase(status, standardFont));
                clmStatus.BorderWidth = 0.5f;
                clmStatus.Padding = 10;
                clmStatus.VerticalAlignment = Element.ALIGN_MIDDLE;

                // Añadimos la fila
                loanTable.AddCell(clmName);
                loanTable.AddCell(clmQuantity);
                loanTable.AddCell(clmUser);
                loanTable.AddCell(clmDate1);
                loanTable.AddCell(clmDate2);
                loanTable.AddCell(clmStatus);

            }

            // Añadimos la tabla
            doc.Add(loanTable);

            doc.Close();
            writer.Close();
        }
    }
}
