﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Cotton.General;

using Cotton.Controllers;

namespace Cotton.Views
{
	public partial class Login : Form
	{
		Thread th;
		FontFamily Montserrat, Poppins, Montserrat_Bold;
		private CancellationTokenSource tokenSource;
		private ForgotPassword FrmForgotPassword = new ForgotPassword();

		public Login()
		{
			InitializeComponent();
			
		}

		private void Login_Load(object sender, EventArgs e)
		{
			Montserrat_Bold = Utils.ImportFont(nameof(Properties.Resources.Montserrat_Bold)).Families[0];
			Montserrat = Utils.ImportFont(nameof(Properties.Resources.Montserrat_Light)).Families[0];
			Poppins = Utils.ImportFont(nameof(Properties.Resources.Poppins_Bold)).Families[0];
			errorEmail.ForeColor = errorPassword.ForeColor = Color.FromArgb(200, 200, 200);
			lblTitle.Font = new Font(Poppins, lblTitle.Font.Size);
			lblEmail.Font = new Font(Montserrat, lblEmail.Font.Size);
			lblPassword.Font = new Font(Montserrat, lblEmail.Font.Size);
			txtEmail.Font = new Font(Montserrat, txtEmail.Font.Size);
			txtPassword.Font = new Font(Montserrat, txtEmail.Font.Size);
			errorEmail.Font = new Font(Montserrat_Bold, errorEmail.Font.Size);
			errorPassword.Font = new Font(Montserrat_Bold, errorPassword.Font.Size);
		}

		private void Login_FormClosing(object sender, FormClosingEventArgs e)
		{
			if(tokenSource != null)
				tokenSource.Cancel();
			e.Cancel = false;
		}

		private async void btnLogin_Click_1(object sender, EventArgs e)
		{
			if (!isValidForm())
			{
				return;
			}
			tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;
			splitContainer1.Visible = false;
			loadPanel.BringToFront();
			try
			{

				bool isAuthenticated = await AuthController.Login(txtEmail.Text, txtPassword.Text, token);
				if (isAuthenticated)
				{
					this.Close();
					th = new Thread(openGeneral);
					th.SetApartmentState(ApartmentState.STA);
					th.Start();
				}
				else
				{
                    loadPanel.SendToBack();
					MessageBox.Show("Credenciales incorrectas");
					this.loadPanel.SendToBack();
					splitContainer1.Visible = true;
				}

			}
			catch (OperationCanceledException)
			{
				MessageBox.Show("Actualmente, no puede conectarse a la base de datos, contacte al técnico encargado de este software");
				this.Close();
			}
		}

		private void BtnRecoverPassword_Click(object sender, EventArgs e)
		{
			FrmForgotPassword.Show();
		}

		private void openGeneral(object obj)
		{
			Application.Run(new LayoutPane());
		}

		private bool isValidForm()
		{
			bool flag = true;
			if (txtEmail.Text.Trim().Length == 0)
			{
				errorEmail.Visible = true;
				flag = false;
			}else
			{
				errorEmail.Visible = false;
			}
			if(txtPassword.Text.Trim().Length == 0)
			{
				errorPassword.Visible = true;
				flag = false;
			}else
			{
				errorPassword.Visible = false;
			}
			return flag;
				
		}
	}
}
