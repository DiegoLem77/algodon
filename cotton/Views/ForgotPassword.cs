﻿using Cotton.Controllers;
using Cotton.Controls;
using Cotton.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cotton.Views
{
    public partial class ForgotPassword : Form
    {
        private readonly FontFamily PoppinsL;
        private readonly EmailController emailC = new EmailController();

        public ForgotPassword()
        {
            InitializeComponent();
            PoppinsL = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
            errorMessage.Font = new Font(PoppinsL, 8f);
            TxtEmail.Focus();
        }

        private void BtnRecover_Click(object sender, EventArgs e)
        {
            string email = TxtEmail.Text;
            int res = emailC.ResetPassword(email);

            if (res == 1)
            {
                MessageBox.Show("Tu contraseña ha sido reseteada! La nueva contraseña ha sido enviada a tu correo", "He olvidado mi contraseña", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                if (res == -1)
                {
                    errorMessage.Text = String.Format("El correo que ingresaste no está registrado en nuestro sistema.");
                }
                else
                {
                    errorMessage.Text = String.Format("Ocurrió un error al intentar mandar el correo, inténtalo más tarde...");
                }
                errorMessage.Visible = true;
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TmrAlert_Tick(object sender, EventArgs e)
        {
            TmrAlert.Stop();
        }
    }
}
