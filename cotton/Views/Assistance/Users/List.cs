﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Cotton.General;
using Cotton.Models;
using Cotton.Controllers;
using Cotton.Controls;

namespace Cotton.Views.Assistance.Users
{
    public partial class List : Form
    {
        // Indica si el fomulario está listo para usarse
        private Task<List<User>> filled;
        private CancellationTokenSource tokenSource;
		public delegate void ResponseEvent(User user);
		public event ResponseEvent ResponseUser;
        private List<string> listId = new List<string>();

        private FontFamily Poppins, PoppinsL, Monserrat = null;

		public List(int action = 0, bool isForAChoice = false)
		{
			InitializeComponent();
			if (isForAChoice)
			{
				this.lblTableTitle.Text = "Elija un equipo:";
                btnDelete.Visible = false;
                btnUpdate.Visible = false;
                btnSelect.Visible = true;
                btnCancel.Visible = true;
            }
            Poppins = Utils.ImportFont(nameof(Properties.Resources.Poppins_Medium)).Families[0];
            PoppinsL = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
            Monserrat = Utils.ImportFont(nameof(Properties.Resources.Montserrat_Light)).Families[0];
        }

        private async void List_Load(object sender, EventArgs e)
        {
            fillDGV(false);
        }

        private async void fillDGV(bool isSearching)
        {
            if (isSearching)
            {
                tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                try
                {
                    filled = Task.Run(() => UserController.search(txtSearch.Text.Trim()), token);
                    List<User> users = await filled;
                    fadeOut.Hide(pnlLoading);
                    foreach (User user in users)
                    {
                        int n = dgvEquipos.Rows.Add();

                        listId.Add(user.Id);
                        dgvEquipos.Rows[n].Cells[0].Value = user.Name;
                        dgvEquipos.Rows[n].Cells[1].Value = user.Lastname;
                        dgvEquipos.Rows[n].Cells[2].Value = user.Dui;
                        dgvEquipos.Rows[n].Cells[3].Value = user.Email;
                        dgvEquipos.Rows[n].Cells[4].Value = user.Gender == "M" ? "Hombre" : "Mujer";
                        dgvEquipos.Rows[n].Cells[5].Value = user.Id;
                    }
                }
                catch (OperationCanceledException)
                {

                    filled.Dispose();
                }
            }
            else
            {
                tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                try
                {
                    filled = Task.Run(() => User.List(), token);
                    List<User> users = await filled;
                    fadeOut.Hide(pnlLoading);
                    foreach (User user in users)
                    {
                        int n = dgvEquipos.Rows.Add();

                        listId.Add(user.Id);
                        dgvEquipos.Rows[n].Cells[0].Value = user.Name;
                        dgvEquipos.Rows[n].Cells[1].Value = user.Lastname;
                        dgvEquipos.Rows[n].Cells[2].Value = user.Dui;
                        dgvEquipos.Rows[n].Cells[3].Value = user.Email;
                        dgvEquipos.Rows[n].Cells[4].Value = user.Gender == "M" ? "Hombre" : "Mujer";
                        dgvEquipos.Rows[n].Cells[5].Value = user.Id;
                    }
                }
                catch (OperationCanceledException)
                {

                    filled.Dispose();
                }
            }
        }

        private void List_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!filled.IsCompleted) tokenSource.Cancel();
            e.Cancel = false;
        }

        private async void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtSearch.Text.Trim().Length == 0)
            {
                dgvEquipos.Rows.Clear();
                fillDGV(false);
            }
            else
            {
                dgvEquipos.Rows.Clear();
                fillDGV(true);
            }
        }

		private async void btnSelect_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
			tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;
			try
			{
				var userResponse = Task.Run(() => UserController.find(token, dgvEquipos.Rows[dgvEquipos.CurrentRow.Index].Cells["id"].Value.ToString()), token);
				User user = await userResponse;
				ResponseUser?.Invoke(user);
				this.Close();
			}
			catch (OperationCanceledException)
			{
				filled.Dispose();
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}
		private void dgvEquipos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
            btnSelect.Enabled = true;
        }

        private void dgvEquipos_Sorted(object sender, EventArgs e)
        {
            drpSortColumn.SelectedIndex = dgvEquipos.SortedColumn.Index;
            this.chkDesc.Checked = dgvEquipos.SortOrder.Equals(SortOrder.Descending);
        }

        private void lblDesc_Click(object sender, EventArgs e)
        {
            this.chkDesc.Checked = !chkDesc.Checked;
        }
        private void chkDesc_OnChange(object sender, EventArgs e)
        {
            if (drpSortColumn.SelectedIndex > -1)
            {
                dgvEquipos.Sort(dgvEquipos.Columns[drpSortColumn.SelectedIndex],
                (chkDesc.Checked) ? ListSortDirection.Descending : ListSortDirection.Ascending);
            }
        }
    }
}

