﻿﻿namespace Cotton.Views.Assistance.Users
{
	partial class Add
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.lblTitle = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splRightTop = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label36 = new System.Windows.Forms.Label();
            this.cboUserTypes = new Bunifu.Framework.UI.BunifuDropdown();
            this.btnConfirm = new Bunifu.Framework.UI.BunifuFlatButton();
            this.splFormTeacher = new System.Windows.Forms.SplitContainer();
            this.pnlOthers = new System.Windows.Forms.Panel();
            this.lblOthers = new System.Windows.Forms.Label();
            this.bunifuSeparator2 = new Bunifu.Framework.UI.BunifuSeparator();
            this.pnlWorkProfile = new System.Windows.Forms.Panel();
            this.lblWorkProfile = new System.Windows.Forms.Label();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.pnlAcademicProfile = new System.Windows.Forms.Panel();
            this.lblAcademicInfo = new System.Windows.Forms.Label();
            this.pnlGeneralData = new System.Windows.Forms.Panel();
            this.lblGeneralData = new System.Windows.Forms.Label();
            this.pnlOtrosForm = new Bunifu.Framework.UI.BunifuCards();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitContainer14 = new System.Windows.Forms.SplitContainer();
            this.splitContainer15 = new System.Windows.Forms.SplitContainer();
            this.label26 = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.bunifuSeparator6 = new Bunifu.Framework.UI.BunifuSeparator();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.drdRelationship = new Bunifu.Framework.UI.BunifuDropdown();
            this.label35 = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.txtLastname2 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label32 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.txtName2 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label31 = new System.Windows.Forms.Label();
            this.splitContainer17 = new System.Windows.Forms.SplitContainer();
            this.panel47 = new System.Windows.Forms.Panel();
            this.btnFinish = new FontAwesome.Sharp.IconButton();
            this.btnGoToWorkProfile = new FontAwesome.Sharp.IconButton();
            this.button3 = new System.Windows.Forms.Button();
            this.panel49 = new System.Windows.Forms.Panel();
            this.txtEmail2 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label34 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.splitContainer18 = new System.Windows.Forms.SplitContainer();
            this.panel55 = new System.Windows.Forms.Panel();
            this.txtCellphone = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label37 = new System.Windows.Forms.Label();
            this.panel53 = new System.Windows.Forms.Panel();
            this.txtLandline = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label27 = new System.Windows.Forms.Label();
            this.pnlPerfilTrabajoForm = new Bunifu.Framework.UI.BunifuCards();
            this.panel37 = new System.Windows.Forms.Panel();
            this.splitContainer12 = new System.Windows.Forms.SplitContainer();
            this.splitContainer13 = new System.Windows.Forms.SplitContainer();
            this.label24 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.bunifuSeparator5 = new Bunifu.Framework.UI.BunifuSeparator();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.txtVocation = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label28 = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.panel48 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.ckbWork = new Bunifu.Framework.UI.BunifuCheckbox();
            this.label29 = new System.Windows.Forms.Label();
            this.splitContainer16 = new System.Windows.Forms.SplitContainer();
            this.panel50 = new System.Windows.Forms.Panel();
            this.btnGoToOthers = new FontAwesome.Sharp.IconButton();
            this.btnGoToPerfilAcademico = new FontAwesome.Sharp.IconButton();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtWorkStatus = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label25 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.txtWorkPlace = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label33 = new System.Windows.Forms.Label();
            this.pnlPerfilAcademicoForm = new Bunifu.Framework.UI.BunifuCards();
            this.panel4 = new System.Windows.Forms.Panel();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.bunifuSeparator4 = new Bunifu.Framework.UI.BunifuSeparator();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.splitContainer8 = new System.Windows.Forms.SplitContainer();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.ckbComputation = new Bunifu.Framework.UI.BunifuCheckbox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.drdComputationLevel = new Bunifu.Framework.UI.BunifuDropdown();
            this.label17 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtOtherCourses = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtReasons = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.splitContainer9 = new System.Windows.Forms.SplitContainer();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.ckbStudent = new Bunifu.Framework.UI.BunifuCheckbox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.drdAcademicLevel = new Bunifu.Framework.UI.BunifuDropdown();
            this.label15 = new System.Windows.Forms.Label();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btnNextWorkProfile = new FontAwesome.Sharp.IconButton();
            this.btnGoToGeneralData = new FontAwesome.Sharp.IconButton();
            this.button1 = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.txtOtherHabilities = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.splitContainer11 = new System.Windows.Forms.SplitContainer();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.ckbInternet = new Bunifu.Framework.UI.BunifuCheckbox();
            this.label22 = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.drdInternet = new Bunifu.Framework.UI.BunifuDropdown();
            this.label23 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.splitContainer10 = new System.Windows.Forms.SplitContainer();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.ckbEnglish = new Bunifu.Framework.UI.BunifuCheckbox();
            this.label19 = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.drdEnglish = new Bunifu.Framework.UI.BunifuDropdown();
            this.label20 = new System.Windows.Forms.Label();
            this.pnlDatosGenerales = new Bunifu.Framework.UI.BunifuCards();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splContainer = new System.Windows.Forms.SplitContainer();
            this.leftForm = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.bunifuSeparator3 = new Bunifu.Framework.UI.BunifuSeparator();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.txtEmail1 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.txtLastname1 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtName1 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label4 = new System.Windows.Forms.Label();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnGeneralDataEnd = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtAdress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtDUI = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.drdGender = new Bunifu.Framework.UI.BunifuDropdown();
            this.label7 = new System.Windows.Forms.Label();
            this.fadeIn = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.tmrAlert = new System.Windows.Forms.Timer(this.components);
            this.ErrorName1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorLastname1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorEmail1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorDUI = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorGenero = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorAdress = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorReazons = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorAcademicLevel = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorComputatitonLevel = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorEnglishLevel = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorInternetLevel = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorVocation = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorWorkPlace = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorWorkStatus = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorName2 = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorLastname2 = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorRelationship = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorCellphone = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorLandline = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorEmail2 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splRightTop)).BeginInit();
            this.splRightTop.Panel1.SuspendLayout();
            this.splRightTop.Panel2.SuspendLayout();
            this.splRightTop.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splFormTeacher)).BeginInit();
            this.splFormTeacher.Panel1.SuspendLayout();
            this.splFormTeacher.Panel2.SuspendLayout();
            this.splFormTeacher.SuspendLayout();
            this.pnlOthers.SuspendLayout();
            this.pnlWorkProfile.SuspendLayout();
            this.pnlAcademicProfile.SuspendLayout();
            this.pnlGeneralData.SuspendLayout();
            this.pnlOtrosForm.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer14)).BeginInit();
            this.splitContainer14.Panel1.SuspendLayout();
            this.splitContainer14.Panel2.SuspendLayout();
            this.splitContainer14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).BeginInit();
            this.splitContainer15.Panel1.SuspendLayout();
            this.splitContainer15.Panel2.SuspendLayout();
            this.splitContainer15.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer17)).BeginInit();
            this.splitContainer17.Panel2.SuspendLayout();
            this.splitContainer17.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panel42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer18)).BeginInit();
            this.splitContainer18.Panel1.SuspendLayout();
            this.splitContainer18.Panel2.SuspendLayout();
            this.splitContainer18.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel53.SuspendLayout();
            this.pnlPerfilTrabajoForm.SuspendLayout();
            this.panel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).BeginInit();
            this.splitContainer12.Panel1.SuspendLayout();
            this.splitContainer12.Panel2.SuspendLayout();
            this.splitContainer12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer13)).BeginInit();
            this.splitContainer13.Panel1.SuspendLayout();
            this.splitContainer13.Panel2.SuspendLayout();
            this.splitContainer13.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel48.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer16)).BeginInit();
            this.splitContainer16.Panel2.SuspendLayout();
            this.splitContainer16.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel51.SuspendLayout();
            this.pnlPerfilAcademicoForm.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).BeginInit();
            this.splitContainer8.Panel1.SuspendLayout();
            this.splitContainer8.Panel2.SuspendLayout();
            this.splitContainer8.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).BeginInit();
            this.splitContainer9.Panel1.SuspendLayout();
            this.splitContainer9.Panel2.SuspendLayout();
            this.splitContainer9.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).BeginInit();
            this.splitContainer11.Panel1.SuspendLayout();
            this.splitContainer11.Panel2.SuspendLayout();
            this.splitContainer11.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).BeginInit();
            this.splitContainer10.Panel1.SuspendLayout();
            this.splitContainer10.Panel2.SuspendLayout();
            this.splitContainer10.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel32.SuspendLayout();
            this.pnlDatosGenerales.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splContainer)).BeginInit();
            this.splContainer.Panel1.SuspendLayout();
            this.splContainer.Panel2.SuspendLayout();
            this.splContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftForm)).BeginInit();
            this.leftForm.Panel1.SuspendLayout();
            this.leftForm.Panel2.SuspendLayout();
            this.leftForm.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorLastname1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorEmail1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorDUI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorGenero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorAdress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorReazons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorAcademicLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorComputatitonLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorEnglishLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorInternetLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorVocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorWorkPlace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorWorkStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorLastname2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorRelationship)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorCellphone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorLandline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorEmail2)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.fadeIn.SetDecoration(this.splitContainer1, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.fadeIn.SetDecoration(this.splitContainer1.Panel1, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.fadeIn.SetDecoration(this.splitContainer1.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.splitContainer1.Size = new System.Drawing.Size(984, 749);
            this.splitContainer1.SplitterDistance = 72;
            this.splitContainer1.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.materialDivider1);
            this.flowLayoutPanel1.Controls.Add(this.lblTitle);
            this.fadeIn.SetDecoration(this.flowLayoutPanel1, BunifuAnimatorNS.DecorationType.None);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(20, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(964, 72);
            this.flowLayoutPanel1.TabIndex = 1;
            this.flowLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel1_Paint);
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.fadeIn.SetDecoration(this.materialDivider1, BunifuAnimatorNS.DecorationType.None);
            this.materialDivider1.Depth = 0;
            this.materialDivider1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.materialDivider1.Location = new System.Drawing.Point(10, 67);
            this.materialDivider1.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(131, 2);
            this.materialDivider1.TabIndex = 2;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.fadeIn.SetDecoration(this.lblTitle, BunifuAnimatorNS.DecorationType.None);
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.lblTitle.Location = new System.Drawing.Point(3, 39);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(216, 25);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Añadir nuevo usuario";
            // 
            // splitContainer2
            // 
            this.fadeIn.SetDecoration(this.splitContainer2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(20, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splRightTop);
            this.fadeIn.SetDecoration(this.splitContainer2.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splFormTeacher);
            this.fadeIn.SetDecoration(this.splitContainer2.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer2.Size = new System.Drawing.Size(944, 673);
            this.splitContainer2.SplitterDistance = 47;
            this.splitContainer2.TabIndex = 0;
            this.splitContainer2.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer2_SplitterMoved);
            // 
            // splRightTop
            // 
            this.fadeIn.SetDecoration(this.splRightTop, BunifuAnimatorNS.DecorationType.None);
            this.splRightTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splRightTop.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splRightTop.Location = new System.Drawing.Point(0, 0);
            this.splRightTop.Name = "splRightTop";
            // 
            // splRightTop.Panel1
            // 
            this.splRightTop.Panel1.Controls.Add(this.flowLayoutPanel2);
            this.fadeIn.SetDecoration(this.splRightTop.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splRightTop.Panel2
            // 
            this.splRightTop.Panel2.Controls.Add(this.btnConfirm);
            this.fadeIn.SetDecoration(this.splRightTop.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splRightTop.Size = new System.Drawing.Size(944, 47);
            this.splRightTop.SplitterDistance = 417;
            this.splRightTop.TabIndex = 8;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.label36);
            this.flowLayoutPanel2.Controls.Add(this.cboUserTypes);
            this.fadeIn.SetDecoration(this.flowLayoutPanel2, BunifuAnimatorNS.DecorationType.None);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(417, 47);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // label36
            // 
            this.fadeIn.SetDecoration(this.label36, BunifuAnimatorNS.DecorationType.None);
            this.label36.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(3, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(174, 42);
            this.label36.TabIndex = 0;
            this.label36.Text = "Ingresar nuevo usuario:";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboUserTypes
            // 
            this.cboUserTypes.BackColor = System.Drawing.Color.Transparent;
            this.cboUserTypes.BorderRadius = 3;
            this.fadeIn.SetDecoration(this.cboUserTypes, BunifuAnimatorNS.DecorationType.None);
            this.cboUserTypes.DisabledColor = System.Drawing.Color.Gray;
            this.cboUserTypes.ForeColor = System.Drawing.Color.White;
            this.cboUserTypes.Items = new string[] {
        "Docente",
        "Alumno",
        "Voluntario"};
            this.cboUserTypes.Location = new System.Drawing.Point(183, 10);
            this.cboUserTypes.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.cboUserTypes.Name = "cboUserTypes";
            this.cboUserTypes.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.cboUserTypes.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.cboUserTypes.selectedIndex = -1;
            this.cboUserTypes.Size = new System.Drawing.Size(217, 28);
            this.cboUserTypes.TabIndex = 1;
            this.cboUserTypes.onItemSelected += new System.EventHandler(this.cboUserTypes_onItemSelected);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.btnConfirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(98)))), ((int)(((byte)(56)))));
            this.btnConfirm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConfirm.BorderRadius = 0;
            this.btnConfirm.ButtonText = "Confirmar";
            this.btnConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeIn.SetDecoration(this.btnConfirm, BunifuAnimatorNS.DecorationType.None);
            this.btnConfirm.DisabledColor = System.Drawing.Color.Gray;
            this.btnConfirm.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirm.Iconcolor = System.Drawing.Color.Transparent;
            this.btnConfirm.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnConfirm.Iconimage")));
            this.btnConfirm.Iconimage_right = null;
            this.btnConfirm.Iconimage_right_Selected = null;
            this.btnConfirm.Iconimage_Selected = null;
            this.btnConfirm.IconMarginLeft = 0;
            this.btnConfirm.IconMarginRight = 0;
            this.btnConfirm.IconRightVisible = false;
            this.btnConfirm.IconRightZoom = 0D;
            this.btnConfirm.IconVisible = false;
            this.btnConfirm.IconZoom = 90D;
            this.btnConfirm.IsTab = false;
            this.btnConfirm.Location = new System.Drawing.Point(9, 10);
            this.btnConfirm.Margin = new System.Windows.Forms.Padding(4);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(98)))), ((int)(((byte)(56)))));
            this.btnConfirm.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.btnConfirm.OnHoverTextColor = System.Drawing.Color.White;
            this.btnConfirm.selected = false;
            this.btnConfirm.Size = new System.Drawing.Size(123, 30);
            this.btnConfirm.TabIndex = 7;
            this.btnConfirm.Text = "Confirmar";
            this.btnConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnConfirm.Textcolor = System.Drawing.Color.White;
            this.btnConfirm.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // splFormTeacher
            // 
            this.fadeIn.SetDecoration(this.splFormTeacher, BunifuAnimatorNS.DecorationType.None);
            this.splFormTeacher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splFormTeacher.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splFormTeacher.IsSplitterFixed = true;
            this.splFormTeacher.Location = new System.Drawing.Point(0, 0);
            this.splFormTeacher.Name = "splFormTeacher";
            this.splFormTeacher.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splFormTeacher.Panel1
            // 
            this.splFormTeacher.Panel1.Controls.Add(this.pnlOthers);
            this.splFormTeacher.Panel1.Controls.Add(this.bunifuSeparator2);
            this.splFormTeacher.Panel1.Controls.Add(this.pnlWorkProfile);
            this.splFormTeacher.Panel1.Controls.Add(this.bunifuSeparator1);
            this.splFormTeacher.Panel1.Controls.Add(this.pnlAcademicProfile);
            this.splFormTeacher.Panel1.Controls.Add(this.pnlGeneralData);
            this.fadeIn.SetDecoration(this.splFormTeacher.Panel1, BunifuAnimatorNS.DecorationType.None);
            this.splFormTeacher.Panel1.Padding = new System.Windows.Forms.Padding(5, 13, 0, 0);
            // 
            // splFormTeacher.Panel2
            // 
            this.splFormTeacher.Panel2.Controls.Add(this.pnlOtrosForm);
            this.splFormTeacher.Panel2.Controls.Add(this.pnlPerfilTrabajoForm);
            this.splFormTeacher.Panel2.Controls.Add(this.pnlPerfilAcademicoForm);
            this.splFormTeacher.Panel2.Controls.Add(this.pnlDatosGenerales);
            this.fadeIn.SetDecoration(this.splFormTeacher.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splFormTeacher.Panel2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.splFormTeacher.Size = new System.Drawing.Size(944, 622);
            this.splFormTeacher.SplitterDistance = 45;
            this.splFormTeacher.SplitterWidth = 1;
            this.splFormTeacher.TabIndex = 0;
            this.splFormTeacher.Visible = false;
            // 
            // pnlOthers
            // 
            this.pnlOthers.Controls.Add(this.lblOthers);
            this.fadeIn.SetDecoration(this.pnlOthers, BunifuAnimatorNS.DecorationType.None);
            this.pnlOthers.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlOthers.Location = new System.Drawing.Point(553, 13);
            this.pnlOthers.Name = "pnlOthers";
            this.pnlOthers.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.pnlOthers.Size = new System.Drawing.Size(70, 32);
            this.pnlOthers.TabIndex = 7;
            // 
            // lblOthers
            // 
            this.lblOthers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(23)))), ((int)(((byte)(30)))));
            this.fadeIn.SetDecoration(this.lblOthers, BunifuAnimatorNS.DecorationType.None);
            this.lblOthers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOthers.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOthers.ForeColor = System.Drawing.Color.Silver;
            this.lblOthers.Location = new System.Drawing.Point(0, 2);
            this.lblOthers.Name = "lblOthers";
            this.lblOthers.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblOthers.Size = new System.Drawing.Size(70, 30);
            this.lblOthers.TabIndex = 2;
            this.lblOthers.Text = "Otros";
            this.lblOthers.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblOthers.Click += new System.EventHandler(this.lblOthers_Click);
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.fadeIn.SetDecoration(this.bunifuSeparator2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator2.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bunifuSeparator2.LineThickness = 1;
            this.bunifuSeparator2.Location = new System.Drawing.Point(552, 13);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Size = new System.Drawing.Size(1, 32);
            this.bunifuSeparator2.TabIndex = 6;
            this.bunifuSeparator2.Transparency = 255;
            this.bunifuSeparator2.Vertical = true;
            // 
            // pnlWorkProfile
            // 
            this.pnlWorkProfile.Controls.Add(this.lblWorkProfile);
            this.fadeIn.SetDecoration(this.pnlWorkProfile, BunifuAnimatorNS.DecorationType.None);
            this.pnlWorkProfile.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlWorkProfile.Location = new System.Drawing.Point(386, 13);
            this.pnlWorkProfile.Name = "pnlWorkProfile";
            this.pnlWorkProfile.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.pnlWorkProfile.Size = new System.Drawing.Size(166, 32);
            this.pnlWorkProfile.TabIndex = 4;
            // 
            // lblWorkProfile
            // 
            this.lblWorkProfile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(23)))), ((int)(((byte)(30)))));
            this.fadeIn.SetDecoration(this.lblWorkProfile, BunifuAnimatorNS.DecorationType.None);
            this.lblWorkProfile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWorkProfile.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorkProfile.ForeColor = System.Drawing.Color.Silver;
            this.lblWorkProfile.Location = new System.Drawing.Point(0, 2);
            this.lblWorkProfile.Name = "lblWorkProfile";
            this.lblWorkProfile.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblWorkProfile.Size = new System.Drawing.Size(166, 30);
            this.lblWorkProfile.TabIndex = 2;
            this.lblWorkProfile.Text = "Perfil de trabajo";
            this.lblWorkProfile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblWorkProfile.Click += new System.EventHandler(this.lblWorkProfile_Click);
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.fadeIn.SetDecoration(this.bunifuSeparator1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator1.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(385, 13);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(1, 32);
            this.bunifuSeparator1.TabIndex = 5;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = true;
            // 
            // pnlAcademicProfile
            // 
            this.pnlAcademicProfile.Controls.Add(this.lblAcademicInfo);
            this.fadeIn.SetDecoration(this.pnlAcademicProfile, BunifuAnimatorNS.DecorationType.None);
            this.pnlAcademicProfile.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlAcademicProfile.Location = new System.Drawing.Point(195, 13);
            this.pnlAcademicProfile.Name = "pnlAcademicProfile";
            this.pnlAcademicProfile.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.pnlAcademicProfile.Size = new System.Drawing.Size(190, 32);
            this.pnlAcademicProfile.TabIndex = 2;
            // 
            // lblAcademicInfo
            // 
            this.lblAcademicInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(23)))), ((int)(((byte)(30)))));
            this.fadeIn.SetDecoration(this.lblAcademicInfo, BunifuAnimatorNS.DecorationType.None);
            this.lblAcademicInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAcademicInfo.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcademicInfo.ForeColor = System.Drawing.Color.Silver;
            this.lblAcademicInfo.Location = new System.Drawing.Point(0, 2);
            this.lblAcademicInfo.Name = "lblAcademicInfo";
            this.lblAcademicInfo.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblAcademicInfo.Size = new System.Drawing.Size(190, 30);
            this.lblAcademicInfo.TabIndex = 2;
            this.lblAcademicInfo.Text = "Perfil Académico";
            this.lblAcademicInfo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblAcademicInfo.Click += new System.EventHandler(this.lblAcademicInfo_Click);
            // 
            // pnlGeneralData
            // 
            this.pnlGeneralData.Controls.Add(this.lblGeneralData);
            this.fadeIn.SetDecoration(this.pnlGeneralData, BunifuAnimatorNS.DecorationType.None);
            this.pnlGeneralData.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlGeneralData.Location = new System.Drawing.Point(5, 13);
            this.pnlGeneralData.Name = "pnlGeneralData";
            this.pnlGeneralData.Size = new System.Drawing.Size(190, 32);
            this.pnlGeneralData.TabIndex = 3;
            // 
            // lblGeneralData
            // 
            this.lblGeneralData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.fadeIn.SetDecoration(this.lblGeneralData, BunifuAnimatorNS.DecorationType.None);
            this.lblGeneralData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGeneralData.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGeneralData.ForeColor = System.Drawing.Color.White;
            this.lblGeneralData.Location = new System.Drawing.Point(0, 0);
            this.lblGeneralData.Name = "lblGeneralData";
            this.lblGeneralData.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblGeneralData.Size = new System.Drawing.Size(190, 32);
            this.lblGeneralData.TabIndex = 1;
            this.lblGeneralData.Text = "Datos Generales";
            this.lblGeneralData.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblGeneralData.Click += new System.EventHandler(this.lblGeneralData_Click);
            // 
            // pnlOtrosForm
            // 
            this.pnlOtrosForm.BackColor = System.Drawing.Color.White;
            this.pnlOtrosForm.BorderRadius = 5;
            this.pnlOtrosForm.BottomSahddow = true;
            this.pnlOtrosForm.color = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.pnlOtrosForm.Controls.Add(this.panel2);
            this.fadeIn.SetDecoration(this.pnlOtrosForm, BunifuAnimatorNS.DecorationType.None);
            this.pnlOtrosForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOtrosForm.LeftSahddow = true;
            this.pnlOtrosForm.Location = new System.Drawing.Point(5, 0);
            this.pnlOtrosForm.Name = "pnlOtrosForm";
            this.pnlOtrosForm.RightSahddow = true;
            this.pnlOtrosForm.ShadowDepth = 20;
            this.pnlOtrosForm.Size = new System.Drawing.Size(939, 576);
            this.pnlOtrosForm.TabIndex = 3;
            this.pnlOtrosForm.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer14);
            this.fadeIn.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(939, 576);
            this.panel2.TabIndex = 6;
            // 
            // splitContainer14
            // 
            this.fadeIn.SetDecoration(this.splitContainer14, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer14.IsSplitterFixed = true;
            this.splitContainer14.Location = new System.Drawing.Point(0, 0);
            this.splitContainer14.Name = "splitContainer14";
            // 
            // splitContainer14.Panel1
            // 
            this.splitContainer14.Panel1.Controls.Add(this.splitContainer15);
            this.fadeIn.SetDecoration(this.splitContainer14.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer14.Panel2
            // 
            this.splitContainer14.Panel2.Controls.Add(this.splitContainer17);
            this.fadeIn.SetDecoration(this.splitContainer14.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer14.Size = new System.Drawing.Size(939, 576);
            this.splitContainer14.SplitterDistance = 468;
            this.splitContainer14.SplitterWidth = 1;
            this.splitContainer14.TabIndex = 4;
            // 
            // splitContainer15
            // 
            this.fadeIn.SetDecoration(this.splitContainer15, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer15.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer15.IsSplitterFixed = true;
            this.splitContainer15.Location = new System.Drawing.Point(0, 0);
            this.splitContainer15.Name = "splitContainer15";
            this.splitContainer15.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer15.Panel1
            // 
            this.splitContainer15.Panel1.Controls.Add(this.label26);
            this.fadeIn.SetDecoration(this.splitContainer15.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer15.Panel2
            // 
            this.splitContainer15.Panel2.AutoScroll = true;
            this.splitContainer15.Panel2.Controls.Add(this.panel40);
            this.fadeIn.SetDecoration(this.splitContainer15.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer15.Size = new System.Drawing.Size(468, 576);
            this.splitContainer15.SplitterDistance = 56;
            this.splitContainer15.TabIndex = 0;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.fadeIn.SetDecoration(this.label26, BunifuAnimatorNS.DecorationType.None);
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.label26.Location = new System.Drawing.Point(6, 30);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(389, 20);
            this.label26.TabIndex = 4;
            this.label26.Text = "Para finalizar proporcione un contacto de emergencia:";
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.bunifuSeparator6);
            this.panel40.Controls.Add(this.panel41);
            this.fadeIn.SetDecoration(this.panel40, BunifuAnimatorNS.DecorationType.None);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel40.Location = new System.Drawing.Point(0, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(468, 431);
            this.panel40.TabIndex = 4;
            // 
            // bunifuSeparator6
            // 
            this.bunifuSeparator6.BackColor = System.Drawing.Color.Transparent;
            this.fadeIn.SetDecoration(this.bunifuSeparator6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator6.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuSeparator6.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator6.LineThickness = 1;
            this.bunifuSeparator6.Location = new System.Drawing.Point(463, 0);
            this.bunifuSeparator6.Name = "bunifuSeparator6";
            this.bunifuSeparator6.Size = new System.Drawing.Size(5, 431);
            this.bunifuSeparator6.TabIndex = 9;
            this.bunifuSeparator6.Transparency = 255;
            this.bunifuSeparator6.Vertical = true;
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.panel52);
            this.panel41.Controls.Add(this.panel46);
            this.panel41.Controls.Add(this.panel43);
            this.fadeIn.SetDecoration(this.panel41, BunifuAnimatorNS.DecorationType.None);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel41.Location = new System.Drawing.Point(0, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(468, 431);
            this.panel41.TabIndex = 5;
            // 
            // panel52
            // 
            this.panel52.Controls.Add(this.drdRelationship);
            this.panel52.Controls.Add(this.label35);
            this.fadeIn.SetDecoration(this.panel52, BunifuAnimatorNS.DecorationType.None);
            this.panel52.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel52.Location = new System.Drawing.Point(0, 182);
            this.panel52.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel52.Name = "panel52";
            this.panel52.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel52.Size = new System.Drawing.Size(468, 91);
            this.panel52.TabIndex = 13;
            // 
            // drdRelationship
            // 
            this.drdRelationship.BackColor = System.Drawing.Color.Transparent;
            this.drdRelationship.BorderRadius = 3;
            this.fadeIn.SetDecoration(this.drdRelationship, BunifuAnimatorNS.DecorationType.None);
            this.drdRelationship.DisabledColor = System.Drawing.Color.Gray;
            this.drdRelationship.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.drdRelationship.ForeColor = System.Drawing.Color.White;
            this.drdRelationship.Items = new string[] {
        "Padre",
        "Madre",
        "Tio/a",
        "Hermano/a",
        "Abuelo/a",
        "Amigo/a",
        "Otro"};
            this.drdRelationship.Location = new System.Drawing.Point(15, 44);
            this.drdRelationship.Name = "drdRelationship";
            this.drdRelationship.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.drdRelationship.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.drdRelationship.selectedIndex = -1;
            this.drdRelationship.Size = new System.Drawing.Size(438, 35);
            this.drdRelationship.TabIndex = 2;
            // 
            // label35
            // 
            this.fadeIn.SetDecoration(this.label35, BunifuAnimatorNS.DecorationType.None);
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(15, 12);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(438, 28);
            this.label35.TabIndex = 1;
            this.label35.Text = "Parentesco:";
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.txtLastname2);
            this.panel46.Controls.Add(this.label32);
            this.fadeIn.SetDecoration(this.panel46, BunifuAnimatorNS.DecorationType.None);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel46.Location = new System.Drawing.Point(0, 91);
            this.panel46.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel46.Name = "panel46";
            this.panel46.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel46.Size = new System.Drawing.Size(468, 91);
            this.panel46.TabIndex = 12;
            // 
            // txtLastname2
            // 
            this.txtLastname2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtLastname2, BunifuAnimatorNS.DecorationType.None);
            this.txtLastname2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtLastname2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtLastname2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtLastname2.HintForeColor = System.Drawing.Color.Gray;
            this.txtLastname2.HintText = "Ingrese apellidos";
            this.txtLastname2.isPassword = false;
            this.txtLastname2.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtLastname2.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtLastname2.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtLastname2.LineThickness = 3;
            this.txtLastname2.Location = new System.Drawing.Point(15, 46);
            this.txtLastname2.Margin = new System.Windows.Forms.Padding(4);
            this.txtLastname2.Name = "txtLastname2";
            this.txtLastname2.Size = new System.Drawing.Size(438, 33);
            this.txtLastname2.TabIndex = 0;
            this.txtLastname2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label32
            // 
            this.fadeIn.SetDecoration(this.label32, BunifuAnimatorNS.DecorationType.None);
            this.label32.Dock = System.Windows.Forms.DockStyle.Top;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(15, 12);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(438, 28);
            this.label32.TabIndex = 1;
            this.label32.Text = "Apellidos:";
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.txtName2);
            this.panel43.Controls.Add(this.label31);
            this.fadeIn.SetDecoration(this.panel43, BunifuAnimatorNS.DecorationType.None);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel43.Location = new System.Drawing.Point(0, 0);
            this.panel43.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel43.Name = "panel43";
            this.panel43.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel43.Size = new System.Drawing.Size(468, 91);
            this.panel43.TabIndex = 11;
            // 
            // txtName2
            // 
            this.txtName2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtName2, BunifuAnimatorNS.DecorationType.None);
            this.txtName2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtName2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtName2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtName2.HintForeColor = System.Drawing.Color.Gray;
            this.txtName2.HintText = "Ingrese nombres";
            this.txtName2.isPassword = false;
            this.txtName2.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtName2.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtName2.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtName2.LineThickness = 3;
            this.txtName2.Location = new System.Drawing.Point(15, 46);
            this.txtName2.Margin = new System.Windows.Forms.Padding(4);
            this.txtName2.Name = "txtName2";
            this.txtName2.Size = new System.Drawing.Size(438, 33);
            this.txtName2.TabIndex = 0;
            this.txtName2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label31
            // 
            this.fadeIn.SetDecoration(this.label31, BunifuAnimatorNS.DecorationType.None);
            this.label31.Dock = System.Windows.Forms.DockStyle.Top;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(15, 12);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(438, 28);
            this.label31.TabIndex = 1;
            this.label31.Text = "Nombres:";
            // 
            // splitContainer17
            // 
            this.fadeIn.SetDecoration(this.splitContainer17, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer17.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer17.IsSplitterFixed = true;
            this.splitContainer17.Location = new System.Drawing.Point(0, 0);
            this.splitContainer17.Name = "splitContainer17";
            this.splitContainer17.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer17.Panel1
            // 
            this.fadeIn.SetDecoration(this.splitContainer17.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer17.Panel2
            // 
            this.splitContainer17.Panel2.AutoScroll = true;
            this.splitContainer17.Panel2.Controls.Add(this.panel47);
            this.splitContainer17.Panel2.Controls.Add(this.panel49);
            this.splitContainer17.Panel2.Controls.Add(this.panel42);
            this.fadeIn.SetDecoration(this.splitContainer17.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer17.Size = new System.Drawing.Size(470, 576);
            this.splitContainer17.SplitterDistance = 56;
            this.splitContainer17.TabIndex = 1;
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.btnFinish);
            this.panel47.Controls.Add(this.btnGoToWorkProfile);
            this.panel47.Controls.Add(this.button3);
            this.fadeIn.SetDecoration(this.panel47, BunifuAnimatorNS.DecorationType.None);
            this.panel47.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel47.Location = new System.Drawing.Point(0, 182);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(470, 54);
            this.panel47.TabIndex = 5;
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.AutoSize = true;
            this.btnFinish.BackColor = System.Drawing.Color.SeaGreen;
            this.fadeIn.SetDecoration(this.btnFinish, BunifuAnimatorNS.DecorationType.None);
            this.btnFinish.FlatAppearance.BorderSize = 0;
            this.btnFinish.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinish.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnFinish.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinish.ForeColor = System.Drawing.Color.White;
            this.btnFinish.IconChar = FontAwesome.Sharp.IconChar.Save;
            this.btnFinish.IconColor = System.Drawing.Color.White;
            this.btnFinish.IconSize = 24;
            this.btnFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFinish.Location = new System.Drawing.Point(266, 4);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnFinish.Rotation = 0D;
            this.btnFinish.Size = new System.Drawing.Size(197, 44);
            this.btnFinish.TabIndex = 12;
            this.btnFinish.Text = "INGRESAR USUARIO";
            this.btnFinish.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnFinish.UseVisualStyleBackColor = false;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnGoToWorkProfile
            // 
            this.btnGoToWorkProfile.AutoSize = true;
            this.btnGoToWorkProfile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.fadeIn.SetDecoration(this.btnGoToWorkProfile, BunifuAnimatorNS.DecorationType.None);
            this.btnGoToWorkProfile.FlatAppearance.BorderSize = 0;
            this.btnGoToWorkProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGoToWorkProfile.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnGoToWorkProfile.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoToWorkProfile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.btnGoToWorkProfile.IconChar = FontAwesome.Sharp.IconChar.ArrowLeft;
            this.btnGoToWorkProfile.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.btnGoToWorkProfile.IconSize = 24;
            this.btnGoToWorkProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGoToWorkProfile.Location = new System.Drawing.Point(11, 4);
            this.btnGoToWorkProfile.Name = "btnGoToWorkProfile";
            this.btnGoToWorkProfile.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnGoToWorkProfile.Rotation = 0D;
            this.btnGoToWorkProfile.Size = new System.Drawing.Size(138, 44);
            this.btnGoToWorkProfile.TabIndex = 5;
            this.btnGoToWorkProfile.Text = "REGRESAR";
            this.btnGoToWorkProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGoToWorkProfile.UseVisualStyleBackColor = false;
            this.btnGoToWorkProfile.Click += new System.EventHandler(this.btnGoToWorkProfile_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeIn.SetDecoration(this.button3, BunifuAnimatorNS.DecorationType.None);
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.button3.Location = new System.Drawing.Point(168, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(111, 44);
            this.button3.TabIndex = 1;
            this.button3.Text = "LIMPIAR";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel49
            // 
            this.panel49.Controls.Add(this.txtEmail2);
            this.panel49.Controls.Add(this.label34);
            this.fadeIn.SetDecoration(this.panel49, BunifuAnimatorNS.DecorationType.None);
            this.panel49.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel49.Location = new System.Drawing.Point(0, 91);
            this.panel49.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel49.Name = "panel49";
            this.panel49.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel49.Size = new System.Drawing.Size(470, 91);
            this.panel49.TabIndex = 6;
            // 
            // txtEmail2
            // 
            this.txtEmail2.AutoScroll = true;
            this.txtEmail2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtEmail2, BunifuAnimatorNS.DecorationType.None);
            this.txtEmail2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtEmail2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtEmail2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtEmail2.HintForeColor = System.Drawing.Color.Gray;
            this.txtEmail2.HintText = "usuario@example.com";
            this.txtEmail2.isPassword = false;
            this.txtEmail2.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtEmail2.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtEmail2.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtEmail2.LineThickness = 3;
            this.txtEmail2.Location = new System.Drawing.Point(15, 46);
            this.txtEmail2.Margin = new System.Windows.Forms.Padding(4);
            this.txtEmail2.Name = "txtEmail2";
            this.txtEmail2.Size = new System.Drawing.Size(440, 33);
            this.txtEmail2.TabIndex = 0;
            this.txtEmail2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label34
            // 
            this.fadeIn.SetDecoration(this.label34, BunifuAnimatorNS.DecorationType.None);
            this.label34.Dock = System.Windows.Forms.DockStyle.Top;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(15, 12);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(440, 28);
            this.label34.TabIndex = 1;
            this.label34.Text = "Email del contacto:";
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.splitContainer18);
            this.fadeIn.SetDecoration(this.panel42, BunifuAnimatorNS.DecorationType.None);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel42.Location = new System.Drawing.Point(0, 0);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(470, 91);
            this.panel42.TabIndex = 12;
            // 
            // splitContainer18
            // 
            this.fadeIn.SetDecoration(this.splitContainer18, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer18.IsSplitterFixed = true;
            this.splitContainer18.Location = new System.Drawing.Point(0, 0);
            this.splitContainer18.Name = "splitContainer18";
            // 
            // splitContainer18.Panel1
            // 
            this.splitContainer18.Panel1.Controls.Add(this.panel55);
            this.fadeIn.SetDecoration(this.splitContainer18.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer18.Panel2
            // 
            this.splitContainer18.Panel2.Controls.Add(this.panel53);
            this.fadeIn.SetDecoration(this.splitContainer18.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer18.Size = new System.Drawing.Size(470, 91);
            this.splitContainer18.SplitterDistance = 232;
            this.splitContainer18.SplitterWidth = 1;
            this.splitContainer18.TabIndex = 0;
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.txtCellphone);
            this.panel55.Controls.Add(this.label37);
            this.fadeIn.SetDecoration(this.panel55, BunifuAnimatorNS.DecorationType.None);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel55.Location = new System.Drawing.Point(0, 0);
            this.panel55.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel55.Name = "panel55";
            this.panel55.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel55.Size = new System.Drawing.Size(232, 91);
            this.panel55.TabIndex = 4;
            // 
            // txtCellphone
            // 
            this.txtCellphone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtCellphone, BunifuAnimatorNS.DecorationType.None);
            this.txtCellphone.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtCellphone.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtCellphone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCellphone.HintForeColor = System.Drawing.Color.Gray;
            this.txtCellphone.HintText = "61211212";
            this.txtCellphone.isPassword = false;
            this.txtCellphone.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtCellphone.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtCellphone.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtCellphone.LineThickness = 3;
            this.txtCellphone.Location = new System.Drawing.Point(15, 46);
            this.txtCellphone.Margin = new System.Windows.Forms.Padding(4);
            this.txtCellphone.Name = "txtCellphone";
            this.txtCellphone.Size = new System.Drawing.Size(202, 33);
            this.txtCellphone.TabIndex = 3;
            this.txtCellphone.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label37
            // 
            this.fadeIn.SetDecoration(this.label37, BunifuAnimatorNS.DecorationType.None);
            this.label37.Dock = System.Windows.Forms.DockStyle.Top;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(15, 12);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(202, 28);
            this.label37.TabIndex = 1;
            this.label37.Text = "Teléfono celular:";
            // 
            // panel53
            // 
            this.panel53.Controls.Add(this.txtLandline);
            this.panel53.Controls.Add(this.label27);
            this.fadeIn.SetDecoration(this.panel53, BunifuAnimatorNS.DecorationType.None);
            this.panel53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel53.Location = new System.Drawing.Point(0, 0);
            this.panel53.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel53.Name = "panel53";
            this.panel53.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel53.Size = new System.Drawing.Size(237, 91);
            this.panel53.TabIndex = 4;
            // 
            // txtLandline
            // 
            this.txtLandline.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtLandline, BunifuAnimatorNS.DecorationType.None);
            this.txtLandline.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtLandline.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtLandline.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtLandline.HintForeColor = System.Drawing.Color.Gray;
            this.txtLandline.HintText = "20201212";
            this.txtLandline.isPassword = false;
            this.txtLandline.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtLandline.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtLandline.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtLandline.LineThickness = 3;
            this.txtLandline.Location = new System.Drawing.Point(15, 46);
            this.txtLandline.Margin = new System.Windows.Forms.Padding(4);
            this.txtLandline.Name = "txtLandline";
            this.txtLandline.Size = new System.Drawing.Size(207, 33);
            this.txtLandline.TabIndex = 2;
            this.txtLandline.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label27
            // 
            this.fadeIn.SetDecoration(this.label27, BunifuAnimatorNS.DecorationType.None);
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(15, 12);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(207, 28);
            this.label27.TabIndex = 1;
            this.label27.Text = "Teléfono fijo:";
            // 
            // pnlPerfilTrabajoForm
            // 
            this.pnlPerfilTrabajoForm.BackColor = System.Drawing.Color.White;
            this.pnlPerfilTrabajoForm.BorderRadius = 5;
            this.pnlPerfilTrabajoForm.BottomSahddow = true;
            this.pnlPerfilTrabajoForm.color = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.pnlPerfilTrabajoForm.Controls.Add(this.panel37);
            this.fadeIn.SetDecoration(this.pnlPerfilTrabajoForm, BunifuAnimatorNS.DecorationType.None);
            this.pnlPerfilTrabajoForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPerfilTrabajoForm.LeftSahddow = true;
            this.pnlPerfilTrabajoForm.Location = new System.Drawing.Point(5, 0);
            this.pnlPerfilTrabajoForm.Name = "pnlPerfilTrabajoForm";
            this.pnlPerfilTrabajoForm.RightSahddow = true;
            this.pnlPerfilTrabajoForm.ShadowDepth = 20;
            this.pnlPerfilTrabajoForm.Size = new System.Drawing.Size(939, 576);
            this.pnlPerfilTrabajoForm.TabIndex = 2;
            this.pnlPerfilTrabajoForm.Visible = false;
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.splitContainer12);
            this.fadeIn.SetDecoration(this.panel37, BunifuAnimatorNS.DecorationType.None);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel37.Location = new System.Drawing.Point(0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(939, 576);
            this.panel37.TabIndex = 6;
            // 
            // splitContainer12
            // 
            this.fadeIn.SetDecoration(this.splitContainer12, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer12.IsSplitterFixed = true;
            this.splitContainer12.Location = new System.Drawing.Point(0, 0);
            this.splitContainer12.Name = "splitContainer12";
            // 
            // splitContainer12.Panel1
            // 
            this.splitContainer12.Panel1.Controls.Add(this.splitContainer13);
            this.fadeIn.SetDecoration(this.splitContainer12.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer12.Panel2
            // 
            this.splitContainer12.Panel2.Controls.Add(this.splitContainer16);
            this.fadeIn.SetDecoration(this.splitContainer12.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer12.Size = new System.Drawing.Size(939, 576);
            this.splitContainer12.SplitterDistance = 468;
            this.splitContainer12.SplitterWidth = 1;
            this.splitContainer12.TabIndex = 4;
            // 
            // splitContainer13
            // 
            this.fadeIn.SetDecoration(this.splitContainer13, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer13.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer13.IsSplitterFixed = true;
            this.splitContainer13.Location = new System.Drawing.Point(0, 0);
            this.splitContainer13.Name = "splitContainer13";
            this.splitContainer13.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer13.Panel1
            // 
            this.splitContainer13.Panel1.Controls.Add(this.label24);
            this.fadeIn.SetDecoration(this.splitContainer13.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer13.Panel2
            // 
            this.splitContainer13.Panel2.AutoScroll = true;
            this.splitContainer13.Panel2.Controls.Add(this.panel38);
            this.fadeIn.SetDecoration(this.splitContainer13.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer13.Size = new System.Drawing.Size(468, 576);
            this.splitContainer13.SplitterDistance = 56;
            this.splitContainer13.TabIndex = 0;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.fadeIn.SetDecoration(this.label24, BunifuAnimatorNS.DecorationType.None);
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.label24.Location = new System.Drawing.Point(6, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(313, 20);
            this.label24.TabIndex = 4;
            this.label24.Text = "Continue con los datos del perfil de trabajo:";
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.bunifuSeparator5);
            this.panel38.Controls.Add(this.panel39);
            this.fadeIn.SetDecoration(this.panel38, BunifuAnimatorNS.DecorationType.None);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel38.Location = new System.Drawing.Point(0, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(468, 431);
            this.panel38.TabIndex = 4;
            // 
            // bunifuSeparator5
            // 
            this.bunifuSeparator5.BackColor = System.Drawing.Color.Transparent;
            this.fadeIn.SetDecoration(this.bunifuSeparator5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator5.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuSeparator5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator5.LineThickness = 1;
            this.bunifuSeparator5.Location = new System.Drawing.Point(463, 0);
            this.bunifuSeparator5.Name = "bunifuSeparator5";
            this.bunifuSeparator5.Size = new System.Drawing.Size(5, 431);
            this.bunifuSeparator5.TabIndex = 9;
            this.bunifuSeparator5.Transparency = 255;
            this.bunifuSeparator5.Vertical = true;
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.panel44);
            this.panel39.Controls.Add(this.panel45);
            this.fadeIn.SetDecoration(this.panel39, BunifuAnimatorNS.DecorationType.None);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel39.Location = new System.Drawing.Point(0, 0);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(468, 431);
            this.panel39.TabIndex = 5;
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.txtVocation);
            this.panel44.Controls.Add(this.label28);
            this.fadeIn.SetDecoration(this.panel44, BunifuAnimatorNS.DecorationType.None);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel44.Location = new System.Drawing.Point(0, 119);
            this.panel44.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel44.Name = "panel44";
            this.panel44.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel44.Size = new System.Drawing.Size(468, 119);
            this.panel44.TabIndex = 10;
            // 
            // txtVocation
            // 
            this.txtVocation.AutoScroll = true;
            this.txtVocation.AutoSize = true;
            this.txtVocation.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtVocation, BunifuAnimatorNS.DecorationType.None);
            this.txtVocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVocation.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtVocation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtVocation.HintForeColor = System.Drawing.Color.Gray;
            this.txtVocation.HintText = "Ingresar vocación del aspirante ...";
            this.txtVocation.isPassword = false;
            this.txtVocation.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtVocation.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtVocation.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtVocation.LineThickness = 3;
            this.txtVocation.Location = new System.Drawing.Point(15, 40);
            this.txtVocation.Margin = new System.Windows.Forms.Padding(4);
            this.txtVocation.Name = "txtVocation";
            this.txtVocation.Size = new System.Drawing.Size(438, 67);
            this.txtVocation.TabIndex = 0;
            this.txtVocation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label28
            // 
            this.fadeIn.SetDecoration(this.label28, BunifuAnimatorNS.DecorationType.None);
            this.label28.Dock = System.Windows.Forms.DockStyle.Top;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(15, 12);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(438, 28);
            this.label28.TabIndex = 1;
            this.label28.Text = "Vocación:";
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.panel48);
            this.panel45.Controls.Add(this.label29);
            this.fadeIn.SetDecoration(this.panel45, BunifuAnimatorNS.DecorationType.None);
            this.panel45.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel45.Location = new System.Drawing.Point(0, 0);
            this.panel45.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel45.Name = "panel45";
            this.panel45.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel45.Size = new System.Drawing.Size(468, 119);
            this.panel45.TabIndex = 9;
            // 
            // panel48
            // 
            this.panel48.Controls.Add(this.label30);
            this.panel48.Controls.Add(this.ckbWork);
            this.fadeIn.SetDecoration(this.panel48, BunifuAnimatorNS.DecorationType.None);
            this.panel48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel48.Location = new System.Drawing.Point(15, 40);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(438, 67);
            this.panel48.TabIndex = 3;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.fadeIn.SetDecoration(this.label30, BunifuAnimatorNS.DecorationType.None);
            this.label30.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(40, 17);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(72, 17);
            this.label30.TabIndex = 4;
            this.label30.Text = "Si, labora.";
            // 
            // ckbWork
            // 
            this.ckbWork.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.ckbWork.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.ckbWork.Checked = true;
            this.ckbWork.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.fadeIn.SetDecoration(this.ckbWork, BunifuAnimatorNS.DecorationType.None);
            this.ckbWork.ForeColor = System.Drawing.Color.White;
            this.ckbWork.Location = new System.Drawing.Point(14, 15);
            this.ckbWork.Name = "ckbWork";
            this.ckbWork.Size = new System.Drawing.Size(20, 20);
            this.ckbWork.TabIndex = 3;
            this.ckbWork.OnChange += new System.EventHandler(this.ckbWork_OnChange);
            // 
            // label29
            // 
            this.fadeIn.SetDecoration(this.label29, BunifuAnimatorNS.DecorationType.None);
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(15, 12);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(438, 28);
            this.label29.TabIndex = 1;
            this.label29.Text = "Posee otro trabajo:";
            // 
            // splitContainer16
            // 
            this.fadeIn.SetDecoration(this.splitContainer16, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer16.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer16.IsSplitterFixed = true;
            this.splitContainer16.Location = new System.Drawing.Point(0, 0);
            this.splitContainer16.Name = "splitContainer16";
            this.splitContainer16.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer16.Panel1
            // 
            this.fadeIn.SetDecoration(this.splitContainer16.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer16.Panel2
            // 
            this.splitContainer16.Panel2.AutoScroll = true;
            this.splitContainer16.Panel2.Controls.Add(this.panel50);
            this.splitContainer16.Panel2.Controls.Add(this.panel1);
            this.splitContainer16.Panel2.Controls.Add(this.panel51);
            this.fadeIn.SetDecoration(this.splitContainer16.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer16.Size = new System.Drawing.Size(470, 576);
            this.splitContainer16.SplitterDistance = 56;
            this.splitContainer16.TabIndex = 1;
            // 
            // panel50
            // 
            this.panel50.Controls.Add(this.btnGoToOthers);
            this.panel50.Controls.Add(this.btnGoToPerfilAcademico);
            this.panel50.Controls.Add(this.button2);
            this.fadeIn.SetDecoration(this.panel50, BunifuAnimatorNS.DecorationType.None);
            this.panel50.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel50.Location = new System.Drawing.Point(0, 238);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(470, 54);
            this.panel50.TabIndex = 5;
            // 
            // btnGoToOthers
            // 
            this.btnGoToOthers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGoToOthers.AutoSize = true;
            this.btnGoToOthers.BackColor = System.Drawing.Color.SeaGreen;
            this.fadeIn.SetDecoration(this.btnGoToOthers, BunifuAnimatorNS.DecorationType.None);
            this.btnGoToOthers.FlatAppearance.BorderSize = 0;
            this.btnGoToOthers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGoToOthers.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnGoToOthers.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoToOthers.ForeColor = System.Drawing.Color.White;
            this.btnGoToOthers.IconChar = FontAwesome.Sharp.IconChar.ArrowRight;
            this.btnGoToOthers.IconColor = System.Drawing.Color.White;
            this.btnGoToOthers.IconSize = 24;
            this.btnGoToOthers.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGoToOthers.Location = new System.Drawing.Point(328, 4);
            this.btnGoToOthers.Name = "btnGoToOthers";
            this.btnGoToOthers.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnGoToOthers.Rotation = 0D;
            this.btnGoToOthers.Size = new System.Drawing.Size(138, 44);
            this.btnGoToOthers.TabIndex = 12;
            this.btnGoToOthers.Text = "SIGUIENTE";
            this.btnGoToOthers.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnGoToOthers.UseVisualStyleBackColor = false;
            this.btnGoToOthers.Click += new System.EventHandler(this.btnGoToOthers_Click);
            // 
            // btnGoToPerfilAcademico
            // 
            this.btnGoToPerfilAcademico.AutoSize = true;
            this.btnGoToPerfilAcademico.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.fadeIn.SetDecoration(this.btnGoToPerfilAcademico, BunifuAnimatorNS.DecorationType.None);
            this.btnGoToPerfilAcademico.FlatAppearance.BorderSize = 0;
            this.btnGoToPerfilAcademico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGoToPerfilAcademico.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnGoToPerfilAcademico.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoToPerfilAcademico.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.btnGoToPerfilAcademico.IconChar = FontAwesome.Sharp.IconChar.ArrowLeft;
            this.btnGoToPerfilAcademico.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.btnGoToPerfilAcademico.IconSize = 24;
            this.btnGoToPerfilAcademico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGoToPerfilAcademico.Location = new System.Drawing.Point(11, 4);
            this.btnGoToPerfilAcademico.Name = "btnGoToPerfilAcademico";
            this.btnGoToPerfilAcademico.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnGoToPerfilAcademico.Rotation = 0D;
            this.btnGoToPerfilAcademico.Size = new System.Drawing.Size(138, 44);
            this.btnGoToPerfilAcademico.TabIndex = 5;
            this.btnGoToPerfilAcademico.Text = "REGRESAR";
            this.btnGoToPerfilAcademico.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGoToPerfilAcademico.UseVisualStyleBackColor = false;
            this.btnGoToPerfilAcademico.Click += new System.EventHandler(this.btnGoToPerfilAcademico_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeIn.SetDecoration(this.button2, BunifuAnimatorNS.DecorationType.None);
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.button2.Location = new System.Drawing.Point(211, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(111, 44);
            this.button2.TabIndex = 1;
            this.button2.Text = "LIMPIAR";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtWorkStatus);
            this.panel1.Controls.Add(this.label25);
            this.fadeIn.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 119);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel1.Size = new System.Drawing.Size(470, 119);
            this.panel1.TabIndex = 6;
            // 
            // txtWorkStatus
            // 
            this.txtWorkStatus.AutoScroll = true;
            this.txtWorkStatus.AutoSize = true;
            this.txtWorkStatus.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtWorkStatus, BunifuAnimatorNS.DecorationType.None);
            this.txtWorkStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWorkStatus.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtWorkStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtWorkStatus.HintForeColor = System.Drawing.Color.Gray;
            this.txtWorkStatus.HintText = "Operador, Secretario, Supervisor, etc...";
            this.txtWorkStatus.isPassword = false;
            this.txtWorkStatus.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtWorkStatus.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtWorkStatus.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtWorkStatus.LineThickness = 3;
            this.txtWorkStatus.Location = new System.Drawing.Point(15, 40);
            this.txtWorkStatus.Margin = new System.Windows.Forms.Padding(4);
            this.txtWorkStatus.Name = "txtWorkStatus";
            this.txtWorkStatus.Size = new System.Drawing.Size(440, 67);
            this.txtWorkStatus.TabIndex = 0;
            this.txtWorkStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label25
            // 
            this.fadeIn.SetDecoration(this.label25, BunifuAnimatorNS.DecorationType.None);
            this.label25.Dock = System.Windows.Forms.DockStyle.Top;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(15, 12);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(440, 28);
            this.label25.TabIndex = 1;
            this.label25.Text = "Puesto o estatus en el que se desempeña:";
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.txtWorkPlace);
            this.panel51.Controls.Add(this.label33);
            this.fadeIn.SetDecoration(this.panel51, BunifuAnimatorNS.DecorationType.None);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel51.Location = new System.Drawing.Point(0, 0);
            this.panel51.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel51.Name = "panel51";
            this.panel51.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel51.Size = new System.Drawing.Size(470, 119);
            this.panel51.TabIndex = 2;
            // 
            // txtWorkPlace
            // 
            this.txtWorkPlace.AutoScroll = true;
            this.txtWorkPlace.AutoSize = true;
            this.txtWorkPlace.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtWorkPlace, BunifuAnimatorNS.DecorationType.None);
            this.txtWorkPlace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWorkPlace.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtWorkPlace.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtWorkPlace.HintForeColor = System.Drawing.Color.Gray;
            this.txtWorkPlace.HintText = "Ingrese el lugar donde se encuentra trabajando";
            this.txtWorkPlace.isPassword = false;
            this.txtWorkPlace.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtWorkPlace.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtWorkPlace.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtWorkPlace.LineThickness = 3;
            this.txtWorkPlace.Location = new System.Drawing.Point(15, 40);
            this.txtWorkPlace.Margin = new System.Windows.Forms.Padding(4);
            this.txtWorkPlace.Name = "txtWorkPlace";
            this.txtWorkPlace.Size = new System.Drawing.Size(440, 67);
            this.txtWorkPlace.TabIndex = 0;
            this.txtWorkPlace.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label33
            // 
            this.fadeIn.SetDecoration(this.label33, BunifuAnimatorNS.DecorationType.None);
            this.label33.Dock = System.Windows.Forms.DockStyle.Top;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(15, 12);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(440, 28);
            this.label33.TabIndex = 1;
            this.label33.Text = "Lugar de trabajo:";
            // 
            // pnlPerfilAcademicoForm
            // 
            this.pnlPerfilAcademicoForm.BackColor = System.Drawing.Color.White;
            this.pnlPerfilAcademicoForm.BorderRadius = 5;
            this.pnlPerfilAcademicoForm.BottomSahddow = true;
            this.pnlPerfilAcademicoForm.color = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.pnlPerfilAcademicoForm.Controls.Add(this.panel4);
            this.fadeIn.SetDecoration(this.pnlPerfilAcademicoForm, BunifuAnimatorNS.DecorationType.None);
            this.pnlPerfilAcademicoForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPerfilAcademicoForm.LeftSahddow = true;
            this.pnlPerfilAcademicoForm.Location = new System.Drawing.Point(5, 0);
            this.pnlPerfilAcademicoForm.Name = "pnlPerfilAcademicoForm";
            this.pnlPerfilAcademicoForm.RightSahddow = true;
            this.pnlPerfilAcademicoForm.ShadowDepth = 20;
            this.pnlPerfilAcademicoForm.Size = new System.Drawing.Size(939, 576);
            this.pnlPerfilAcademicoForm.TabIndex = 1;
            this.pnlPerfilAcademicoForm.Visible = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.splitContainer3);
            this.fadeIn.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(939, 576);
            this.panel4.TabIndex = 6;
            // 
            // splitContainer3
            // 
            this.fadeIn.SetDecoration(this.splitContainer3, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer4);
            this.fadeIn.SetDecoration(this.splitContainer3.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer7);
            this.fadeIn.SetDecoration(this.splitContainer3.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer3.Size = new System.Drawing.Size(939, 576);
            this.splitContainer3.SplitterDistance = 468;
            this.splitContainer3.SplitterWidth = 1;
            this.splitContainer3.TabIndex = 4;
            // 
            // splitContainer4
            // 
            this.fadeIn.SetDecoration(this.splitContainer4, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.label2);
            this.fadeIn.SetDecoration(this.splitContainer4.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.AutoScroll = true;
            this.splitContainer4.Panel2.Controls.Add(this.panel5);
            this.fadeIn.SetDecoration(this.splitContainer4.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer4.Size = new System.Drawing.Size(468, 576);
            this.splitContainer4.SplitterDistance = 56;
            this.splitContainer4.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.fadeIn.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.label2.Location = new System.Drawing.Point(6, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(319, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Continue con los datos del perfil académico:";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.bunifuSeparator4);
            this.panel5.Controls.Add(this.panel6);
            this.fadeIn.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(468, 431);
            this.panel5.TabIndex = 4;
            // 
            // bunifuSeparator4
            // 
            this.bunifuSeparator4.BackColor = System.Drawing.Color.Transparent;
            this.fadeIn.SetDecoration(this.bunifuSeparator4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator4.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuSeparator4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator4.LineThickness = 1;
            this.bunifuSeparator4.Location = new System.Drawing.Point(463, 0);
            this.bunifuSeparator4.Name = "bunifuSeparator4";
            this.bunifuSeparator4.Size = new System.Drawing.Size(5, 431);
            this.bunifuSeparator4.TabIndex = 9;
            this.bunifuSeparator4.Transparency = 255;
            this.bunifuSeparator4.Vertical = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel22);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Controls.Add(this.panel14);
            this.fadeIn.SetDecoration(this.panel6, BunifuAnimatorNS.DecorationType.None);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(468, 431);
            this.panel6.TabIndex = 5;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.splitContainer8);
            this.fadeIn.SetDecoration(this.panel22, BunifuAnimatorNS.DecorationType.None);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 329);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(468, 91);
            this.panel22.TabIndex = 11;
            // 
            // splitContainer8
            // 
            this.fadeIn.SetDecoration(this.splitContainer8, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer8.IsSplitterFixed = true;
            this.splitContainer8.Location = new System.Drawing.Point(0, 0);
            this.splitContainer8.Name = "splitContainer8";
            // 
            // splitContainer8.Panel1
            // 
            this.splitContainer8.Panel1.Controls.Add(this.panel23);
            this.fadeIn.SetDecoration(this.splitContainer8.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer8.Panel2
            // 
            this.splitContainer8.Panel2.Controls.Add(this.panel28);
            this.fadeIn.SetDecoration(this.splitContainer8.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer8.Size = new System.Drawing.Size(468, 91);
            this.splitContainer8.SplitterDistance = 231;
            this.splitContainer8.SplitterWidth = 1;
            this.splitContainer8.TabIndex = 0;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Controls.Add(this.label14);
            this.fadeIn.SetDecoration(this.panel23, BunifuAnimatorNS.DecorationType.None);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel23.Name = "panel23";
            this.panel23.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel23.Size = new System.Drawing.Size(231, 91);
            this.panel23.TabIndex = 2;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.label13);
            this.panel24.Controls.Add(this.ckbComputation);
            this.fadeIn.SetDecoration(this.panel24, BunifuAnimatorNS.DecorationType.None);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel24.Location = new System.Drawing.Point(15, 40);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(201, 39);
            this.panel24.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.fadeIn.SetDecoration(this.label13, BunifuAnimatorNS.DecorationType.None);
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(40, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 17);
            this.label13.TabIndex = 4;
            this.label13.Text = "Posee";
            // 
            // ckbComputation
            // 
            this.ckbComputation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.ckbComputation.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.ckbComputation.Checked = true;
            this.ckbComputation.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.fadeIn.SetDecoration(this.ckbComputation, BunifuAnimatorNS.DecorationType.None);
            this.ckbComputation.ForeColor = System.Drawing.Color.White;
            this.ckbComputation.Location = new System.Drawing.Point(14, 15);
            this.ckbComputation.Name = "ckbComputation";
            this.ckbComputation.Size = new System.Drawing.Size(20, 20);
            this.ckbComputation.TabIndex = 3;
            this.ckbComputation.OnChange += new System.EventHandler(this.ckbComputation_OnChange);
            // 
            // label14
            // 
            this.fadeIn.SetDecoration(this.label14, BunifuAnimatorNS.DecorationType.None);
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(15, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(201, 28);
            this.label14.TabIndex = 1;
            this.label14.Text = "Habilidades en computación:";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.drdComputationLevel);
            this.panel28.Controls.Add(this.label17);
            this.fadeIn.SetDecoration(this.panel28, BunifuAnimatorNS.DecorationType.None);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel28.Location = new System.Drawing.Point(0, 0);
            this.panel28.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel28.Name = "panel28";
            this.panel28.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel28.Size = new System.Drawing.Size(236, 91);
            this.panel28.TabIndex = 3;
            // 
            // drdComputationLevel
            // 
            this.drdComputationLevel.BackColor = System.Drawing.Color.Transparent;
            this.drdComputationLevel.BorderRadius = 3;
            this.fadeIn.SetDecoration(this.drdComputationLevel, BunifuAnimatorNS.DecorationType.None);
            this.drdComputationLevel.DisabledColor = System.Drawing.Color.Gray;
            this.drdComputationLevel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.drdComputationLevel.ForeColor = System.Drawing.Color.White;
            this.drdComputationLevel.Items = new string[0];
            this.drdComputationLevel.Location = new System.Drawing.Point(15, 49);
            this.drdComputationLevel.Name = "drdComputationLevel";
            this.drdComputationLevel.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.drdComputationLevel.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.drdComputationLevel.selectedIndex = -1;
            this.drdComputationLevel.Size = new System.Drawing.Size(206, 30);
            this.drdComputationLevel.TabIndex = 7;
            // 
            // label17
            // 
            this.fadeIn.SetDecoration(this.label17, BunifuAnimatorNS.DecorationType.None);
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(15, 12);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(206, 28);
            this.label17.TabIndex = 1;
            this.label17.Text = "Nivel de computación:";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txtOtherCourses);
            this.panel7.Controls.Add(this.label3);
            this.fadeIn.SetDecoration(this.panel7, BunifuAnimatorNS.DecorationType.None);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 210);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel7.Size = new System.Drawing.Size(468, 119);
            this.panel7.TabIndex = 10;
            // 
            // txtOtherCourses
            // 
            this.txtOtherCourses.AutoScroll = true;
            this.txtOtherCourses.AutoSize = true;
            this.txtOtherCourses.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtOtherCourses, BunifuAnimatorNS.DecorationType.None);
            this.txtOtherCourses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOtherCourses.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtOtherCourses.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtOtherCourses.HintForeColor = System.Drawing.Color.Gray;
            this.txtOtherCourses.HintText = "Lista de cursos, diplomas o certificaciones...";
            this.txtOtherCourses.isPassword = false;
            this.txtOtherCourses.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtOtherCourses.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtOtherCourses.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtOtherCourses.LineThickness = 3;
            this.txtOtherCourses.Location = new System.Drawing.Point(15, 40);
            this.txtOtherCourses.Margin = new System.Windows.Forms.Padding(4);
            this.txtOtherCourses.Name = "txtOtherCourses";
            this.txtOtherCourses.Size = new System.Drawing.Size(438, 67);
            this.txtOtherCourses.TabIndex = 0;
            this.txtOtherCourses.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label3
            // 
            this.fadeIn.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(438, 28);
            this.label3.TabIndex = 1;
            this.label3.Text = "Otros cursos y/o diplomas:";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.txtReasons);
            this.panel8.Controls.Add(this.label10);
            this.fadeIn.SetDecoration(this.panel8, BunifuAnimatorNS.DecorationType.None);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 91);
            this.panel8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel8.Size = new System.Drawing.Size(468, 119);
            this.panel8.TabIndex = 9;
            // 
            // txtReasons
            // 
            this.txtReasons.AutoScroll = true;
            this.txtReasons.AutoSize = true;
            this.txtReasons.BackColor = System.Drawing.Color.Silver;
            this.txtReasons.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtReasons, BunifuAnimatorNS.DecorationType.None);
            this.txtReasons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReasons.Enabled = false;
            this.txtReasons.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtReasons.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtReasons.HintForeColor = System.Drawing.Color.Gray;
            this.txtReasons.HintText = "";
            this.txtReasons.isPassword = false;
            this.txtReasons.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtReasons.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtReasons.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtReasons.LineThickness = 3;
            this.txtReasons.Location = new System.Drawing.Point(15, 40);
            this.txtReasons.Margin = new System.Windows.Forms.Padding(4);
            this.txtReasons.Name = "txtReasons";
            this.txtReasons.Size = new System.Drawing.Size(438, 67);
            this.txtReasons.TabIndex = 0;
            this.txtReasons.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label10
            // 
            this.fadeIn.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 12);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(438, 28);
            this.label10.TabIndex = 1;
            this.label10.Text = "Razón por la cual no estudia:";
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.splitContainer9);
            this.fadeIn.SetDecoration(this.panel14, BunifuAnimatorNS.DecorationType.None);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(468, 91);
            this.panel14.TabIndex = 8;
            // 
            // splitContainer9
            // 
            this.fadeIn.SetDecoration(this.splitContainer9, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer9.IsSplitterFixed = true;
            this.splitContainer9.Location = new System.Drawing.Point(0, 0);
            this.splitContainer9.Name = "splitContainer9";
            // 
            // splitContainer9.Panel1
            // 
            this.splitContainer9.Panel1.Controls.Add(this.panel25);
            this.fadeIn.SetDecoration(this.splitContainer9.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer9.Panel2
            // 
            this.splitContainer9.Panel2.Controls.Add(this.panel26);
            this.fadeIn.SetDecoration(this.splitContainer9.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer9.Size = new System.Drawing.Size(468, 91);
            this.splitContainer9.SplitterDistance = 232;
            this.splitContainer9.SplitterWidth = 1;
            this.splitContainer9.TabIndex = 0;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.panel27);
            this.panel25.Controls.Add(this.label11);
            this.fadeIn.SetDecoration(this.panel25, BunifuAnimatorNS.DecorationType.None);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel25.Name = "panel25";
            this.panel25.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel25.Size = new System.Drawing.Size(232, 91);
            this.panel25.TabIndex = 2;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.label16);
            this.panel27.Controls.Add(this.ckbStudent);
            this.fadeIn.SetDecoration(this.panel27, BunifuAnimatorNS.DecorationType.None);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(15, 40);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(202, 39);
            this.panel27.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.fadeIn.SetDecoration(this.label16, BunifuAnimatorNS.DecorationType.None);
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(40, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(117, 17);
            this.label16.TabIndex = 4;
            this.label16.Text = "Si, soy estudiante";
            // 
            // ckbStudent
            // 
            this.ckbStudent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.ckbStudent.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.ckbStudent.Checked = true;
            this.ckbStudent.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.fadeIn.SetDecoration(this.ckbStudent, BunifuAnimatorNS.DecorationType.None);
            this.ckbStudent.ForeColor = System.Drawing.Color.White;
            this.ckbStudent.Location = new System.Drawing.Point(14, 15);
            this.ckbStudent.Name = "ckbStudent";
            this.ckbStudent.Size = new System.Drawing.Size(20, 20);
            this.ckbStudent.TabIndex = 3;
            this.ckbStudent.OnChange += new System.EventHandler(this.ckbStudent_OnChange);
            // 
            // label11
            // 
            this.fadeIn.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(202, 28);
            this.label11.TabIndex = 1;
            this.label11.Text = "Es estudiante activo:";
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.drdAcademicLevel);
            this.panel26.Controls.Add(this.label15);
            this.fadeIn.SetDecoration(this.panel26, BunifuAnimatorNS.DecorationType.None);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel26.Name = "panel26";
            this.panel26.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel26.Size = new System.Drawing.Size(235, 91);
            this.panel26.TabIndex = 3;
            // 
            // drdAcademicLevel
            // 
            this.drdAcademicLevel.BackColor = System.Drawing.Color.Transparent;
            this.drdAcademicLevel.BorderRadius = 3;
            this.fadeIn.SetDecoration(this.drdAcademicLevel, BunifuAnimatorNS.DecorationType.None);
            this.drdAcademicLevel.DisabledColor = System.Drawing.Color.Gray;
            this.drdAcademicLevel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.drdAcademicLevel.ForeColor = System.Drawing.Color.White;
            this.drdAcademicLevel.Items = new string[0];
            this.drdAcademicLevel.Location = new System.Drawing.Point(15, 49);
            this.drdAcademicLevel.Name = "drdAcademicLevel";
            this.drdAcademicLevel.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.drdAcademicLevel.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.drdAcademicLevel.selectedIndex = -1;
            this.drdAcademicLevel.Size = new System.Drawing.Size(205, 30);
            this.drdAcademicLevel.TabIndex = 7;
            // 
            // label15
            // 
            this.fadeIn.SetDecoration(this.label15, BunifuAnimatorNS.DecorationType.None);
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(15, 12);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(205, 28);
            this.label15.TabIndex = 1;
            this.label15.Text = "Nivel académico:";
            // 
            // splitContainer7
            // 
            this.fadeIn.SetDecoration(this.splitContainer7, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer7.IsSplitterFixed = true;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            this.splitContainer7.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer7.Panel1
            // 
            this.fadeIn.SetDecoration(this.splitContainer7.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.AutoScroll = true;
            this.splitContainer7.Panel2.Controls.Add(this.panel18);
            this.splitContainer7.Panel2.Controls.Add(this.panel21);
            this.splitContainer7.Panel2.Controls.Add(this.panel33);
            this.splitContainer7.Panel2.Controls.Add(this.panel29);
            this.fadeIn.SetDecoration(this.splitContainer7.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer7.Size = new System.Drawing.Size(470, 576);
            this.splitContainer7.SplitterDistance = 56;
            this.splitContainer7.TabIndex = 1;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.btnNextWorkProfile);
            this.panel18.Controls.Add(this.btnGoToGeneralData);
            this.panel18.Controls.Add(this.button1);
            this.fadeIn.SetDecoration(this.panel18, BunifuAnimatorNS.DecorationType.None);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 301);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(470, 54);
            this.panel18.TabIndex = 5;
            // 
            // btnNextWorkProfile
            // 
            this.btnNextWorkProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextWorkProfile.AutoSize = true;
            this.btnNextWorkProfile.BackColor = System.Drawing.Color.SeaGreen;
            this.fadeIn.SetDecoration(this.btnNextWorkProfile, BunifuAnimatorNS.DecorationType.None);
            this.btnNextWorkProfile.FlatAppearance.BorderSize = 0;
            this.btnNextWorkProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNextWorkProfile.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnNextWorkProfile.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNextWorkProfile.ForeColor = System.Drawing.Color.White;
            this.btnNextWorkProfile.IconChar = FontAwesome.Sharp.IconChar.ArrowRight;
            this.btnNextWorkProfile.IconColor = System.Drawing.Color.White;
            this.btnNextWorkProfile.IconSize = 24;
            this.btnNextWorkProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNextWorkProfile.Location = new System.Drawing.Point(328, 4);
            this.btnNextWorkProfile.Name = "btnNextWorkProfile";
            this.btnNextWorkProfile.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnNextWorkProfile.Rotation = 0D;
            this.btnNextWorkProfile.Size = new System.Drawing.Size(138, 44);
            this.btnNextWorkProfile.TabIndex = 12;
            this.btnNextWorkProfile.Text = "SIGUIENTE";
            this.btnNextWorkProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnNextWorkProfile.UseVisualStyleBackColor = false;
            this.btnNextWorkProfile.Click += new System.EventHandler(this.btnNextWorkProfile_Click);
            // 
            // btnGoToGeneralData
            // 
            this.btnGoToGeneralData.AutoSize = true;
            this.btnGoToGeneralData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.fadeIn.SetDecoration(this.btnGoToGeneralData, BunifuAnimatorNS.DecorationType.None);
            this.btnGoToGeneralData.FlatAppearance.BorderSize = 0;
            this.btnGoToGeneralData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGoToGeneralData.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnGoToGeneralData.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoToGeneralData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.btnGoToGeneralData.IconChar = FontAwesome.Sharp.IconChar.ArrowLeft;
            this.btnGoToGeneralData.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.btnGoToGeneralData.IconSize = 24;
            this.btnGoToGeneralData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGoToGeneralData.Location = new System.Drawing.Point(11, 4);
            this.btnGoToGeneralData.Name = "btnGoToGeneralData";
            this.btnGoToGeneralData.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnGoToGeneralData.Rotation = 0D;
            this.btnGoToGeneralData.Size = new System.Drawing.Size(138, 44);
            this.btnGoToGeneralData.TabIndex = 5;
            this.btnGoToGeneralData.Text = "REGRESAR";
            this.btnGoToGeneralData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGoToGeneralData.UseVisualStyleBackColor = false;
            this.btnGoToGeneralData.Click += new System.EventHandler(this.btnGoToGeneralData_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeIn.SetDecoration(this.button1, BunifuAnimatorNS.DecorationType.None);
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.button1.Location = new System.Drawing.Point(211, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 44);
            this.button1.TabIndex = 1;
            this.button1.Text = "LIMPIAR";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.txtOtherHabilities);
            this.panel21.Controls.Add(this.label12);
            this.fadeIn.SetDecoration(this.panel21, BunifuAnimatorNS.DecorationType.None);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 182);
            this.panel21.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel21.Name = "panel21";
            this.panel21.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel21.Size = new System.Drawing.Size(470, 119);
            this.panel21.TabIndex = 2;
            // 
            // txtOtherHabilities
            // 
            this.txtOtherHabilities.AutoScroll = true;
            this.txtOtherHabilities.AutoSize = true;
            this.txtOtherHabilities.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtOtherHabilities, BunifuAnimatorNS.DecorationType.None);
            this.txtOtherHabilities.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOtherHabilities.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtOtherHabilities.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtOtherHabilities.HintForeColor = System.Drawing.Color.Gray;
            this.txtOtherHabilities.HintText = "Ingrese otras habilidades que posee y quiera compartir...";
            this.txtOtherHabilities.isPassword = false;
            this.txtOtherHabilities.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtOtherHabilities.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtOtherHabilities.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtOtherHabilities.LineThickness = 3;
            this.txtOtherHabilities.Location = new System.Drawing.Point(15, 40);
            this.txtOtherHabilities.Margin = new System.Windows.Forms.Padding(4);
            this.txtOtherHabilities.Name = "txtOtherHabilities";
            this.txtOtherHabilities.Size = new System.Drawing.Size(440, 67);
            this.txtOtherHabilities.TabIndex = 0;
            this.txtOtherHabilities.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label12
            // 
            this.fadeIn.SetDecoration(this.label12, BunifuAnimatorNS.DecorationType.None);
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(440, 28);
            this.label12.TabIndex = 1;
            this.label12.Text = "Otras habilidades:";
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.splitContainer11);
            this.fadeIn.SetDecoration(this.panel33, BunifuAnimatorNS.DecorationType.None);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel33.Location = new System.Drawing.Point(0, 91);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(470, 91);
            this.panel33.TabIndex = 11;
            // 
            // splitContainer11
            // 
            this.fadeIn.SetDecoration(this.splitContainer11, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer11.IsSplitterFixed = true;
            this.splitContainer11.Location = new System.Drawing.Point(0, 0);
            this.splitContainer11.Name = "splitContainer11";
            // 
            // splitContainer11.Panel1
            // 
            this.splitContainer11.Panel1.Controls.Add(this.panel34);
            this.fadeIn.SetDecoration(this.splitContainer11.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer11.Panel2
            // 
            this.splitContainer11.Panel2.Controls.Add(this.panel36);
            this.fadeIn.SetDecoration(this.splitContainer11.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer11.Size = new System.Drawing.Size(470, 91);
            this.splitContainer11.SplitterDistance = 232;
            this.splitContainer11.SplitterWidth = 1;
            this.splitContainer11.TabIndex = 0;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.panel35);
            this.panel34.Controls.Add(this.label22);
            this.fadeIn.SetDecoration(this.panel34, BunifuAnimatorNS.DecorationType.None);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel34.Location = new System.Drawing.Point(0, 0);
            this.panel34.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel34.Name = "panel34";
            this.panel34.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel34.Size = new System.Drawing.Size(232, 91);
            this.panel34.TabIndex = 2;
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.label21);
            this.panel35.Controls.Add(this.ckbInternet);
            this.fadeIn.SetDecoration(this.panel35, BunifuAnimatorNS.DecorationType.None);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel35.Location = new System.Drawing.Point(15, 40);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(202, 39);
            this.panel35.TabIndex = 2;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.fadeIn.SetDecoration(this.label21, BunifuAnimatorNS.DecorationType.None);
            this.label21.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(40, 17);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 17);
            this.label21.TabIndex = 4;
            this.label21.Text = "Posee";
            // 
            // ckbInternet
            // 
            this.ckbInternet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.ckbInternet.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.ckbInternet.Checked = true;
            this.ckbInternet.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.fadeIn.SetDecoration(this.ckbInternet, BunifuAnimatorNS.DecorationType.None);
            this.ckbInternet.ForeColor = System.Drawing.Color.White;
            this.ckbInternet.Location = new System.Drawing.Point(14, 15);
            this.ckbInternet.Name = "ckbInternet";
            this.ckbInternet.Size = new System.Drawing.Size(20, 20);
            this.ckbInternet.TabIndex = 3;
            this.ckbInternet.OnChange += new System.EventHandler(this.ckbInternet_OnChange);
            // 
            // label22
            // 
            this.fadeIn.SetDecoration(this.label22, BunifuAnimatorNS.DecorationType.None);
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(15, 12);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(202, 28);
            this.label22.TabIndex = 1;
            this.label22.Text = "Habilidades en internet:";
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.drdInternet);
            this.panel36.Controls.Add(this.label23);
            this.fadeIn.SetDecoration(this.panel36, BunifuAnimatorNS.DecorationType.None);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel36.Location = new System.Drawing.Point(0, 0);
            this.panel36.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel36.Name = "panel36";
            this.panel36.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel36.Size = new System.Drawing.Size(237, 91);
            this.panel36.TabIndex = 3;
            // 
            // drdInternet
            // 
            this.drdInternet.BackColor = System.Drawing.Color.Transparent;
            this.drdInternet.BorderRadius = 3;
            this.fadeIn.SetDecoration(this.drdInternet, BunifuAnimatorNS.DecorationType.None);
            this.drdInternet.DisabledColor = System.Drawing.Color.Gray;
            this.drdInternet.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.drdInternet.ForeColor = System.Drawing.Color.White;
            this.drdInternet.Items = new string[0];
            this.drdInternet.Location = new System.Drawing.Point(15, 49);
            this.drdInternet.Name = "drdInternet";
            this.drdInternet.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.drdInternet.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.drdInternet.selectedIndex = -1;
            this.drdInternet.Size = new System.Drawing.Size(207, 30);
            this.drdInternet.TabIndex = 7;
            // 
            // label23
            // 
            this.fadeIn.SetDecoration(this.label23, BunifuAnimatorNS.DecorationType.None);
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(15, 12);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(207, 28);
            this.label23.TabIndex = 1;
            this.label23.Text = "Nivel en internet";
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.splitContainer10);
            this.fadeIn.SetDecoration(this.panel29, BunifuAnimatorNS.DecorationType.None);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(470, 91);
            this.panel29.TabIndex = 10;
            // 
            // splitContainer10
            // 
            this.fadeIn.SetDecoration(this.splitContainer10, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer10.IsSplitterFixed = true;
            this.splitContainer10.Location = new System.Drawing.Point(0, 0);
            this.splitContainer10.Name = "splitContainer10";
            // 
            // splitContainer10.Panel1
            // 
            this.splitContainer10.Panel1.Controls.Add(this.panel30);
            this.fadeIn.SetDecoration(this.splitContainer10.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer10.Panel2
            // 
            this.splitContainer10.Panel2.Controls.Add(this.panel32);
            this.fadeIn.SetDecoration(this.splitContainer10.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer10.Size = new System.Drawing.Size(470, 91);
            this.splitContainer10.SplitterDistance = 232;
            this.splitContainer10.SplitterWidth = 1;
            this.splitContainer10.TabIndex = 0;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.panel31);
            this.panel30.Controls.Add(this.label19);
            this.fadeIn.SetDecoration(this.panel30, BunifuAnimatorNS.DecorationType.None);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel30.Location = new System.Drawing.Point(0, 0);
            this.panel30.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel30.Name = "panel30";
            this.panel30.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel30.Size = new System.Drawing.Size(232, 91);
            this.panel30.TabIndex = 2;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.label18);
            this.panel31.Controls.Add(this.ckbEnglish);
            this.fadeIn.SetDecoration(this.panel31, BunifuAnimatorNS.DecorationType.None);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel31.Location = new System.Drawing.Point(15, 40);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(202, 39);
            this.panel31.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.fadeIn.SetDecoration(this.label18, BunifuAnimatorNS.DecorationType.None);
            this.label18.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(40, 17);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 17);
            this.label18.TabIndex = 4;
            this.label18.Text = "Posee";
            // 
            // ckbEnglish
            // 
            this.ckbEnglish.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.ckbEnglish.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.ckbEnglish.Checked = true;
            this.ckbEnglish.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.fadeIn.SetDecoration(this.ckbEnglish, BunifuAnimatorNS.DecorationType.None);
            this.ckbEnglish.ForeColor = System.Drawing.Color.White;
            this.ckbEnglish.Location = new System.Drawing.Point(14, 15);
            this.ckbEnglish.Name = "ckbEnglish";
            this.ckbEnglish.Size = new System.Drawing.Size(20, 20);
            this.ckbEnglish.TabIndex = 3;
            this.ckbEnglish.OnChange += new System.EventHandler(this.ckbEnglish_OnChange);
            // 
            // label19
            // 
            this.fadeIn.SetDecoration(this.label19, BunifuAnimatorNS.DecorationType.None);
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(15, 12);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(202, 28);
            this.label19.TabIndex = 1;
            this.label19.Text = "Habilidades en inglés:";
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.drdEnglish);
            this.panel32.Controls.Add(this.label20);
            this.fadeIn.SetDecoration(this.panel32, BunifuAnimatorNS.DecorationType.None);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(0, 0);
            this.panel32.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel32.Name = "panel32";
            this.panel32.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel32.Size = new System.Drawing.Size(237, 91);
            this.panel32.TabIndex = 3;
            // 
            // drdEnglish
            // 
            this.drdEnglish.BackColor = System.Drawing.Color.Transparent;
            this.drdEnglish.BorderRadius = 3;
            this.fadeIn.SetDecoration(this.drdEnglish, BunifuAnimatorNS.DecorationType.None);
            this.drdEnglish.DisabledColor = System.Drawing.Color.Gray;
            this.drdEnglish.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.drdEnglish.ForeColor = System.Drawing.Color.White;
            this.drdEnglish.Items = new string[0];
            this.drdEnglish.Location = new System.Drawing.Point(15, 49);
            this.drdEnglish.Name = "drdEnglish";
            this.drdEnglish.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.drdEnglish.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.drdEnglish.selectedIndex = -1;
            this.drdEnglish.Size = new System.Drawing.Size(207, 30);
            this.drdEnglish.TabIndex = 7;
            // 
            // label20
            // 
            this.fadeIn.SetDecoration(this.label20, BunifuAnimatorNS.DecorationType.None);
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(15, 12);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(207, 28);
            this.label20.TabIndex = 1;
            this.label20.Text = "Nivel de idioma inglés:";
            // 
            // pnlDatosGenerales
            // 
            this.pnlDatosGenerales.BackColor = System.Drawing.Color.White;
            this.pnlDatosGenerales.BorderRadius = 5;
            this.pnlDatosGenerales.BottomSahddow = true;
            this.pnlDatosGenerales.color = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.pnlDatosGenerales.Controls.Add(this.panel3);
            this.fadeIn.SetDecoration(this.pnlDatosGenerales, BunifuAnimatorNS.DecorationType.None);
            this.pnlDatosGenerales.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDatosGenerales.LeftSahddow = true;
            this.pnlDatosGenerales.Location = new System.Drawing.Point(5, 0);
            this.pnlDatosGenerales.Name = "pnlDatosGenerales";
            this.pnlDatosGenerales.RightSahddow = true;
            this.pnlDatosGenerales.ShadowDepth = 20;
            this.pnlDatosGenerales.Size = new System.Drawing.Size(939, 576);
            this.pnlDatosGenerales.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splContainer);
            this.fadeIn.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(939, 576);
            this.panel3.TabIndex = 6;
            // 
            // splContainer
            // 
            this.fadeIn.SetDecoration(this.splContainer, BunifuAnimatorNS.DecorationType.None);
            this.splContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splContainer.IsSplitterFixed = true;
            this.splContainer.Location = new System.Drawing.Point(0, 0);
            this.splContainer.Name = "splContainer";
            // 
            // splContainer.Panel1
            // 
            this.splContainer.Panel1.Controls.Add(this.leftForm);
            this.fadeIn.SetDecoration(this.splContainer.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splContainer.Panel2
            // 
            this.splContainer.Panel2.Controls.Add(this.splitContainer5);
            this.fadeIn.SetDecoration(this.splContainer.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splContainer.Size = new System.Drawing.Size(939, 576);
            this.splContainer.SplitterDistance = 468;
            this.splContainer.SplitterWidth = 1;
            this.splContainer.TabIndex = 4;
            // 
            // leftForm
            // 
            this.fadeIn.SetDecoration(this.leftForm, BunifuAnimatorNS.DecorationType.None);
            this.leftForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftForm.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.leftForm.IsSplitterFixed = true;
            this.leftForm.Location = new System.Drawing.Point(0, 0);
            this.leftForm.Name = "leftForm";
            this.leftForm.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // leftForm.Panel1
            // 
            this.leftForm.Panel1.Controls.Add(this.label1);
            this.fadeIn.SetDecoration(this.leftForm.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // leftForm.Panel2
            // 
            this.leftForm.Panel2.AutoScroll = true;
            this.leftForm.Panel2.Controls.Add(this.panel15);
            this.fadeIn.SetDecoration(this.leftForm.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.leftForm.Size = new System.Drawing.Size(468, 576);
            this.leftForm.SplitterDistance = 56;
            this.leftForm.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.fadeIn.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Complete los siguientes campos:";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.bunifuSeparator3);
            this.panel15.Controls.Add(this.panel16);
            this.fadeIn.SetDecoration(this.panel15, BunifuAnimatorNS.DecorationType.None);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(468, 417);
            this.panel15.TabIndex = 4;
            // 
            // bunifuSeparator3
            // 
            this.bunifuSeparator3.BackColor = System.Drawing.Color.Transparent;
            this.fadeIn.SetDecoration(this.bunifuSeparator3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator3.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuSeparator3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator3.LineThickness = 1;
            this.bunifuSeparator3.Location = new System.Drawing.Point(463, 0);
            this.bunifuSeparator3.Name = "bunifuSeparator3";
            this.bunifuSeparator3.Size = new System.Drawing.Size(5, 417);
            this.bunifuSeparator3.TabIndex = 9;
            this.bunifuSeparator3.Transparency = 255;
            this.bunifuSeparator3.Vertical = true;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.panel19);
            this.panel16.Controls.Add(this.panel9);
            this.fadeIn.SetDecoration(this.panel16, BunifuAnimatorNS.DecorationType.None);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(468, 417);
            this.panel16.TabIndex = 5;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.txtEmail1);
            this.panel20.Controls.Add(this.label9);
            this.fadeIn.SetDecoration(this.panel20, BunifuAnimatorNS.DecorationType.None);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 182);
            this.panel20.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel20.Name = "panel20";
            this.panel20.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel20.Size = new System.Drawing.Size(468, 91);
            this.panel20.TabIndex = 7;
            // 
            // txtEmail1
            // 
            this.txtEmail1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtEmail1, BunifuAnimatorNS.DecorationType.None);
            this.txtEmail1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtEmail1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtEmail1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtEmail1.HintForeColor = System.Drawing.Color.Gray;
            this.txtEmail1.HintText = "Ingrese el correo electrónico";
            this.txtEmail1.isPassword = false;
            this.txtEmail1.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtEmail1.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtEmail1.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtEmail1.LineThickness = 3;
            this.txtEmail1.Location = new System.Drawing.Point(15, 46);
            this.txtEmail1.Margin = new System.Windows.Forms.Padding(4);
            this.txtEmail1.Name = "txtEmail1";
            this.txtEmail1.Size = new System.Drawing.Size(438, 33);
            this.txtEmail1.TabIndex = 0;
            this.txtEmail1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label9
            // 
            this.fadeIn.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(438, 28);
            this.label9.TabIndex = 1;
            this.label9.Text = "Email:";
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.txtLastname1);
            this.panel19.Controls.Add(this.label8);
            this.fadeIn.SetDecoration(this.panel19, BunifuAnimatorNS.DecorationType.None);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 91);
            this.panel19.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel19.Name = "panel19";
            this.panel19.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel19.Size = new System.Drawing.Size(468, 91);
            this.panel19.TabIndex = 6;
            // 
            // txtLastname1
            // 
            this.txtLastname1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtLastname1, BunifuAnimatorNS.DecorationType.None);
            this.txtLastname1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtLastname1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtLastname1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtLastname1.HintForeColor = System.Drawing.Color.Gray;
            this.txtLastname1.HintText = "Ingrese apellidos";
            this.txtLastname1.isPassword = false;
            this.txtLastname1.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtLastname1.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtLastname1.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtLastname1.LineThickness = 3;
            this.txtLastname1.Location = new System.Drawing.Point(15, 46);
            this.txtLastname1.Margin = new System.Windows.Forms.Padding(4);
            this.txtLastname1.Name = "txtLastname1";
            this.txtLastname1.Size = new System.Drawing.Size(438, 33);
            this.txtLastname1.TabIndex = 0;
            this.txtLastname1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label8
            // 
            this.fadeIn.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(15, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(438, 28);
            this.label8.TabIndex = 1;
            this.label8.Text = "Apellidos:";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txtName1);
            this.panel9.Controls.Add(this.label4);
            this.fadeIn.SetDecoration(this.panel9, BunifuAnimatorNS.DecorationType.None);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel9.Size = new System.Drawing.Size(468, 91);
            this.panel9.TabIndex = 2;
            // 
            // txtName1
            // 
            this.txtName1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtName1, BunifuAnimatorNS.DecorationType.None);
            this.txtName1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtName1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtName1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtName1.HintForeColor = System.Drawing.Color.Gray;
            this.txtName1.HintText = "Ingrese nombres";
            this.txtName1.isPassword = false;
            this.txtName1.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtName1.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtName1.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtName1.LineThickness = 3;
            this.txtName1.Location = new System.Drawing.Point(15, 46);
            this.txtName1.Margin = new System.Windows.Forms.Padding(4);
            this.txtName1.Name = "txtName1";
            this.txtName1.Size = new System.Drawing.Size(438, 33);
            this.txtName1.TabIndex = 0;
            this.txtName1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label4
            // 
            this.fadeIn.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(438, 28);
            this.label4.TabIndex = 1;
            this.label4.Text = "Nombres:";
            // 
            // splitContainer5
            // 
            this.fadeIn.SetDecoration(this.splitContainer5, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer5.IsSplitterFixed = true;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.fadeIn.SetDecoration(this.splitContainer5.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.AutoScroll = true;
            this.splitContainer5.Panel2.Controls.Add(this.panel10);
            this.splitContainer5.Panel2.Controls.Add(this.panel11);
            this.splitContainer5.Panel2.Controls.Add(this.panel12);
            this.fadeIn.SetDecoration(this.splitContainer5.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer5.Size = new System.Drawing.Size(470, 576);
            this.splitContainer5.SplitterDistance = 56;
            this.splitContainer5.TabIndex = 1;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btnClear);
            this.panel10.Controls.Add(this.btnGeneralDataEnd);
            this.fadeIn.SetDecoration(this.panel10, BunifuAnimatorNS.DecorationType.None);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 210);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(470, 54);
            this.panel10.TabIndex = 5;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeIn.SetDecoration(this.btnClear, BunifuAnimatorNS.DecorationType.None);
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.btnClear.Location = new System.Drawing.Point(143, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(151, 44);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "LIMPIAR";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnGeneralDataEnd
            // 
            this.btnGeneralDataEnd.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnGeneralDataEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGeneralDataEnd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnGeneralDataEnd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGeneralDataEnd.BorderRadius = 0;
            this.btnGeneralDataEnd.ButtonText = "SIGUIENTE";
            this.btnGeneralDataEnd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeIn.SetDecoration(this.btnGeneralDataEnd, BunifuAnimatorNS.DecorationType.None);
            this.btnGeneralDataEnd.DisabledColor = System.Drawing.Color.Gray;
            this.btnGeneralDataEnd.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGeneralDataEnd.Iconcolor = System.Drawing.Color.Transparent;
            this.btnGeneralDataEnd.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnGeneralDataEnd.Iconimage")));
            this.btnGeneralDataEnd.Iconimage_right = null;
            this.btnGeneralDataEnd.Iconimage_right_Selected = null;
            this.btnGeneralDataEnd.Iconimage_Selected = null;
            this.btnGeneralDataEnd.IconMarginLeft = 0;
            this.btnGeneralDataEnd.IconMarginRight = 0;
            this.btnGeneralDataEnd.IconRightVisible = false;
            this.btnGeneralDataEnd.IconRightZoom = 0D;
            this.btnGeneralDataEnd.IconVisible = false;
            this.btnGeneralDataEnd.IconZoom = 90D;
            this.btnGeneralDataEnd.IsTab = false;
            this.btnGeneralDataEnd.Location = new System.Drawing.Point(301, 4);
            this.btnGeneralDataEnd.Margin = new System.Windows.Forms.Padding(4);
            this.btnGeneralDataEnd.Name = "btnGeneralDataEnd";
            this.btnGeneralDataEnd.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnGeneralDataEnd.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnGeneralDataEnd.OnHoverTextColor = System.Drawing.Color.White;
            this.btnGeneralDataEnd.selected = false;
            this.btnGeneralDataEnd.Size = new System.Drawing.Size(165, 44);
            this.btnGeneralDataEnd.TabIndex = 0;
            this.btnGeneralDataEnd.Text = "SIGUIENTE";
            this.btnGeneralDataEnd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnGeneralDataEnd.Textcolor = System.Drawing.Color.White;
            this.btnGeneralDataEnd.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGeneralDataEnd.Click += new System.EventHandler(this.btnGeneralDataEnd_Click);
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.txtAdress);
            this.panel11.Controls.Add(this.label5);
            this.fadeIn.SetDecoration(this.panel11, BunifuAnimatorNS.DecorationType.None);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 91);
            this.panel11.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel11.Name = "panel11";
            this.panel11.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel11.Size = new System.Drawing.Size(470, 119);
            this.panel11.TabIndex = 2;
            // 
            // txtAdress
            // 
            this.txtAdress.AutoScroll = true;
            this.txtAdress.AutoSize = true;
            this.txtAdress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtAdress, BunifuAnimatorNS.DecorationType.None);
            this.txtAdress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAdress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtAdress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtAdress.HintForeColor = System.Drawing.Color.Gray;
            this.txtAdress.HintText = "Ingrese dirección donde reside...";
            this.txtAdress.isPassword = false;
            this.txtAdress.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtAdress.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtAdress.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtAdress.LineThickness = 3;
            this.txtAdress.Location = new System.Drawing.Point(15, 40);
            this.txtAdress.Margin = new System.Windows.Forms.Padding(4);
            this.txtAdress.Name = "txtAdress";
            this.txtAdress.Size = new System.Drawing.Size(440, 67);
            this.txtAdress.TabIndex = 0;
            this.txtAdress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label5
            // 
            this.fadeIn.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(440, 28);
            this.label5.TabIndex = 1;
            this.label5.Text = "Dirección:";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.splitContainer6);
            this.fadeIn.SetDecoration(this.panel12, BunifuAnimatorNS.DecorationType.None);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(470, 91);
            this.panel12.TabIndex = 4;
            // 
            // splitContainer6
            // 
            this.fadeIn.SetDecoration(this.splitContainer6, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.IsSplitterFixed = true;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.panel13);
            this.fadeIn.SetDecoration(this.splitContainer6.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.panel17);
            this.fadeIn.SetDecoration(this.splitContainer6.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer6.Size = new System.Drawing.Size(470, 91);
            this.splitContainer6.SplitterDistance = 233;
            this.splitContainer6.SplitterWidth = 1;
            this.splitContainer6.TabIndex = 0;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.txtDUI);
            this.panel13.Controls.Add(this.label6);
            this.fadeIn.SetDecoration(this.panel13, BunifuAnimatorNS.DecorationType.None);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel13.Name = "panel13";
            this.panel13.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel13.Size = new System.Drawing.Size(233, 91);
            this.panel13.TabIndex = 2;
            // 
            // txtDUI
            // 
            this.txtDUI.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtDUI, BunifuAnimatorNS.DecorationType.None);
            this.txtDUI.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtDUI.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtDUI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtDUI.HintForeColor = System.Drawing.Color.Gray;
            this.txtDUI.HintText = "0000000-0";
            this.txtDUI.isPassword = false;
            this.txtDUI.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtDUI.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtDUI.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtDUI.LineThickness = 3;
            this.txtDUI.Location = new System.Drawing.Point(15, 46);
            this.txtDUI.Margin = new System.Windows.Forms.Padding(4);
            this.txtDUI.Name = "txtDUI";
            this.txtDUI.Size = new System.Drawing.Size(203, 33);
            this.txtDUI.TabIndex = 2;
            this.txtDUI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label6
            // 
            this.fadeIn.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(203, 28);
            this.label6.TabIndex = 1;
            this.label6.Text = "DUI:";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.drdGender);
            this.panel17.Controls.Add(this.label7);
            this.fadeIn.SetDecoration(this.panel17, BunifuAnimatorNS.DecorationType.None);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel17.Name = "panel17";
            this.panel17.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel17.Size = new System.Drawing.Size(236, 91);
            this.panel17.TabIndex = 3;
            // 
            // drdGender
            // 
            this.drdGender.BackColor = System.Drawing.Color.Transparent;
            this.drdGender.BorderRadius = 3;
            this.fadeIn.SetDecoration(this.drdGender, BunifuAnimatorNS.DecorationType.None);
            this.drdGender.DisabledColor = System.Drawing.Color.Gray;
            this.drdGender.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.drdGender.ForeColor = System.Drawing.Color.White;
            this.drdGender.Items = new string[] {
        "Hombre",
        "Mujer"};
            this.drdGender.Location = new System.Drawing.Point(15, 49);
            this.drdGender.Name = "drdGender";
            this.drdGender.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.drdGender.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.drdGender.selectedIndex = -1;
            this.drdGender.Size = new System.Drawing.Size(206, 30);
            this.drdGender.TabIndex = 7;
            // 
            // label7
            // 
            this.fadeIn.SetDecoration(this.label7, BunifuAnimatorNS.DecorationType.None);
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(206, 28);
            this.label7.TabIndex = 1;
            this.label7.Text = "Género:";
            // 
            // fadeIn
            // 
            this.fadeIn.AnimationType = BunifuAnimatorNS.AnimationType.Transparent;
            this.fadeIn.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 1F;
            this.fadeIn.DefaultAnimation = animation1;
            // 
            // tmrAlert
            // 
            this.tmrAlert.Tick += new System.EventHandler(this.tmrAlert_Tick);
            // 
            // ErrorName1
            // 
            this.ErrorName1.ContainerControl = this;
            // 
            // ErrorLastname1
            // 
            this.ErrorLastname1.ContainerControl = this;
            // 
            // ErrorEmail1
            // 
            this.ErrorEmail1.ContainerControl = this;
            // 
            // ErrorDUI
            // 
            this.ErrorDUI.ContainerControl = this;
            // 
            // ErrorGenero
            // 
            this.ErrorGenero.ContainerControl = this;
            // 
            // ErrorAdress
            // 
            this.ErrorAdress.ContainerControl = this;
            // 
            // ErrorReazons
            // 
            this.ErrorReazons.ContainerControl = this;
            // 
            // ErrorAcademicLevel
            // 
            this.ErrorAcademicLevel.ContainerControl = this;
            // 
            // ErrorComputatitonLevel
            // 
            this.ErrorComputatitonLevel.ContainerControl = this;
            // 
            // ErrorEnglishLevel
            // 
            this.ErrorEnglishLevel.ContainerControl = this;
            // 
            // ErrorInternetLevel
            // 
            this.ErrorInternetLevel.ContainerControl = this;
            // 
            // ErrorVocation
            // 
            this.ErrorVocation.ContainerControl = this;
            // 
            // ErrorWorkPlace
            // 
            this.ErrorWorkPlace.ContainerControl = this;
            // 
            // ErrorWorkStatus
            // 
            this.ErrorWorkStatus.ContainerControl = this;
            // 
            // ErrorName2
            // 
            this.ErrorName2.ContainerControl = this;
            // 
            // ErrorLastname2
            // 
            this.ErrorLastname2.ContainerControl = this;
            // 
            // ErrorRelationship
            // 
            this.ErrorRelationship.ContainerControl = this;
            // 
            // ErrorCellphone
            // 
            this.ErrorCellphone.ContainerControl = this;
            // 
            // ErrorLandline
            // 
            this.ErrorLandline.ContainerControl = this;
            // 
            // ErrorEmail2
            // 
            this.ErrorEmail2.ContainerControl = this;
            // 
            // Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(984, 749);
            this.Controls.Add(this.splitContainer1);
            this.fadeIn.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Name = "Add";
            this.Text = "Add";
            this.Load += new System.EventHandler(this.Add_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splRightTop.Panel1.ResumeLayout(false);
            this.splRightTop.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splRightTop)).EndInit();
            this.splRightTop.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.splFormTeacher.Panel1.ResumeLayout(false);
            this.splFormTeacher.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splFormTeacher)).EndInit();
            this.splFormTeacher.ResumeLayout(false);
            this.pnlOthers.ResumeLayout(false);
            this.pnlWorkProfile.ResumeLayout(false);
            this.pnlAcademicProfile.ResumeLayout(false);
            this.pnlGeneralData.ResumeLayout(false);
            this.pnlOtrosForm.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.splitContainer14.Panel1.ResumeLayout(false);
            this.splitContainer14.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer14)).EndInit();
            this.splitContainer14.ResumeLayout(false);
            this.splitContainer15.Panel1.ResumeLayout(false);
            this.splitContainer15.Panel1.PerformLayout();
            this.splitContainer15.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).EndInit();
            this.splitContainer15.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel52.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.splitContainer17.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer17)).EndInit();
            this.splitContainer17.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.panel49.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.splitContainer18.Panel1.ResumeLayout(false);
            this.splitContainer18.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer18)).EndInit();
            this.splitContainer18.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.pnlPerfilTrabajoForm.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.splitContainer12.Panel1.ResumeLayout(false);
            this.splitContainer12.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).EndInit();
            this.splitContainer12.ResumeLayout(false);
            this.splitContainer13.Panel1.ResumeLayout(false);
            this.splitContainer13.Panel1.PerformLayout();
            this.splitContainer13.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer13)).EndInit();
            this.splitContainer13.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            this.panel44.PerformLayout();
            this.panel45.ResumeLayout(false);
            this.panel48.ResumeLayout(false);
            this.panel48.PerformLayout();
            this.splitContainer16.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer16)).EndInit();
            this.splitContainer16.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panel50.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel51.ResumeLayout(false);
            this.panel51.PerformLayout();
            this.pnlPerfilAcademicoForm.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.splitContainer8.Panel1.ResumeLayout(false);
            this.splitContainer8.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).EndInit();
            this.splitContainer8.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.splitContainer9.Panel1.ResumeLayout(false);
            this.splitContainer9.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).EndInit();
            this.splitContainer9.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
            this.splitContainer7.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel33.ResumeLayout(false);
            this.splitContainer11.Panel1.ResumeLayout(false);
            this.splitContainer11.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).EndInit();
            this.splitContainer11.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.splitContainer10.Panel1.ResumeLayout(false);
            this.splitContainer10.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).EndInit();
            this.splitContainer10.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.pnlDatosGenerales.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.splContainer.Panel1.ResumeLayout(false);
            this.splContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splContainer)).EndInit();
            this.splContainer.ResumeLayout(false);
            this.leftForm.Panel1.ResumeLayout(false);
            this.leftForm.Panel1.PerformLayout();
            this.leftForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leftForm)).EndInit();
            this.leftForm.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorLastname1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorEmail1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorDUI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorGenero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorAdress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorReazons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorAcademicLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorComputatitonLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorEnglishLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorInternetLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorVocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorWorkPlace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorWorkStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorLastname2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorRelationship)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorCellphone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorLandline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorEmail2)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private MaterialSkin.Controls.MaterialDivider materialDivider1;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private Bunifu.Framework.UI.BunifuFlatButton btnConfirm;
		private System.Windows.Forms.SplitContainer splRightTop;
		private BunifuAnimatorNS.BunifuTransition fadeIn;
		private System.Windows.Forms.Timer tmrAlert;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.Label label36;
		private Bunifu.Framework.UI.BunifuDropdown cboUserTypes;
		private System.Windows.Forms.SplitContainer splFormTeacher;
		private System.Windows.Forms.Panel pnlOthers;
		private System.Windows.Forms.Label lblOthers;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator2;
		private System.Windows.Forms.Panel pnlWorkProfile;
		private System.Windows.Forms.Label lblWorkProfile;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
		private System.Windows.Forms.Panel pnlAcademicProfile;
		private System.Windows.Forms.Label lblAcademicInfo;
		private System.Windows.Forms.Panel pnlGeneralData;
		private System.Windows.Forms.Label lblGeneralData;
		private Bunifu.Framework.UI.BunifuCards pnlDatosGenerales;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.SplitContainer splContainer;
		private System.Windows.Forms.SplitContainer leftForm;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel15;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator3;
		private System.Windows.Forms.Panel panel16;
		private System.Windows.Forms.Panel panel20;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtEmail1;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Panel panel19;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtLastname1;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Panel panel9;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtName1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.SplitContainer splitContainer5;
		private System.Windows.Forms.Panel panel10;
		private System.Windows.Forms.Button btnClear;
		private Bunifu.Framework.UI.BunifuFlatButton btnGeneralDataEnd;
		private System.Windows.Forms.Panel panel11;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtAdress;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Panel panel12;
		private System.Windows.Forms.SplitContainer splitContainer6;
		private System.Windows.Forms.Panel panel13;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtDUI;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Panel panel17;
		private Bunifu.Framework.UI.BunifuDropdown drdGender;
		private System.Windows.Forms.Label label7;
		private Bunifu.Framework.UI.BunifuCards pnlOtrosForm;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.SplitContainer splitContainer14;
		private System.Windows.Forms.SplitContainer splitContainer15;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Panel panel40;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator6;
		private System.Windows.Forms.Panel panel41;
		private System.Windows.Forms.Panel panel52;
		private Bunifu.Framework.UI.BunifuDropdown drdRelationship;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Panel panel46;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtLastname2;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Panel panel43;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtName2;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.SplitContainer splitContainer17;
		private System.Windows.Forms.Panel panel47;
		private FontAwesome.Sharp.IconButton btnFinish;
		private FontAwesome.Sharp.IconButton btnGoToWorkProfile;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Panel panel49;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtEmail2;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Panel panel42;
		private System.Windows.Forms.SplitContainer splitContainer18;
		private System.Windows.Forms.Panel panel55;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtCellphone;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Panel panel53;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtLandline;
		private System.Windows.Forms.Label label27;
		private Bunifu.Framework.UI.BunifuCards pnlPerfilTrabajoForm;
		private System.Windows.Forms.Panel panel37;
		private System.Windows.Forms.SplitContainer splitContainer12;
		private System.Windows.Forms.SplitContainer splitContainer13;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Panel panel38;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator5;
		private System.Windows.Forms.Panel panel39;
		private System.Windows.Forms.Panel panel44;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtVocation;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Panel panel45;
		private System.Windows.Forms.Panel panel48;
		private System.Windows.Forms.Label label30;
		private Bunifu.Framework.UI.BunifuCheckbox ckbWork;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.SplitContainer splitContainer16;
		private System.Windows.Forms.Panel panel50;
		private FontAwesome.Sharp.IconButton btnGoToOthers;
		private FontAwesome.Sharp.IconButton btnGoToPerfilAcademico;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Panel panel1;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtWorkStatus;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Panel panel51;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtWorkPlace;
		private System.Windows.Forms.Label label33;
		private Bunifu.Framework.UI.BunifuCards pnlPerfilAcademicoForm;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.SplitContainer splitContainer3;
		private System.Windows.Forms.SplitContainer splitContainer4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel5;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator4;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Panel panel22;
		private System.Windows.Forms.SplitContainer splitContainer8;
		private System.Windows.Forms.Panel panel23;
		private System.Windows.Forms.Panel panel24;
		private System.Windows.Forms.Label label13;
		private Bunifu.Framework.UI.BunifuCheckbox ckbComputation;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Panel panel28;
		private Bunifu.Framework.UI.BunifuDropdown drdComputationLevel;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Panel panel7;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtOtherCourses;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panel8;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtReasons;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Panel panel14;
		private System.Windows.Forms.SplitContainer splitContainer9;
		private System.Windows.Forms.Panel panel25;
		private System.Windows.Forms.Panel panel27;
		private System.Windows.Forms.Label label16;
		private Bunifu.Framework.UI.BunifuCheckbox ckbStudent;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Panel panel26;
		private Bunifu.Framework.UI.BunifuDropdown drdAcademicLevel;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.SplitContainer splitContainer7;
		private System.Windows.Forms.Panel panel18;
		private FontAwesome.Sharp.IconButton btnNextWorkProfile;
		private FontAwesome.Sharp.IconButton btnGoToGeneralData;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panel21;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtOtherHabilities;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Panel panel33;
		private System.Windows.Forms.SplitContainer splitContainer11;
		private System.Windows.Forms.Panel panel34;
		private System.Windows.Forms.Panel panel35;
		private System.Windows.Forms.Label label21;
		private Bunifu.Framework.UI.BunifuCheckbox ckbInternet;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Panel panel36;
		private Bunifu.Framework.UI.BunifuDropdown drdInternet;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Panel panel29;
		private System.Windows.Forms.SplitContainer splitContainer10;
		private System.Windows.Forms.Panel panel30;
		private System.Windows.Forms.Panel panel31;
		private System.Windows.Forms.Label label18;
		private Bunifu.Framework.UI.BunifuCheckbox ckbEnglish;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Panel panel32;
		private Bunifu.Framework.UI.BunifuDropdown drdEnglish;
		private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ErrorProvider ErrorName1;
        private System.Windows.Forms.ErrorProvider ErrorLastname1;
        private System.Windows.Forms.ErrorProvider ErrorEmail1;
        private System.Windows.Forms.ErrorProvider ErrorDUI;
        private System.Windows.Forms.ErrorProvider ErrorGenero;
        private System.Windows.Forms.ErrorProvider ErrorAdress;
        private System.Windows.Forms.ErrorProvider ErrorReazons;
        private System.Windows.Forms.ErrorProvider ErrorAcademicLevel;
        private System.Windows.Forms.ErrorProvider ErrorComputatitonLevel;
        private System.Windows.Forms.ErrorProvider ErrorEnglishLevel;
        private System.Windows.Forms.ErrorProvider ErrorInternetLevel;
        private System.Windows.Forms.ErrorProvider ErrorVocation;
        private System.Windows.Forms.ErrorProvider ErrorWorkPlace;
        private System.Windows.Forms.ErrorProvider ErrorWorkStatus;
        private System.Windows.Forms.ErrorProvider ErrorName2;
        private System.Windows.Forms.ErrorProvider ErrorLastname2;
        private System.Windows.Forms.ErrorProvider ErrorRelationship;
        private System.Windows.Forms.ErrorProvider ErrorCellphone;
        private System.Windows.Forms.ErrorProvider ErrorLandline;
        private System.Windows.Forms.ErrorProvider ErrorEmail2;
    }
}
