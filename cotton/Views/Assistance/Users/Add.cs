﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Models;
using Cotton.Controllers;
using System.Text.RegularExpressions;
using System.Threading;

namespace Cotton.Views.Assistance.Users
{
	public partial class Add : Form
	{
        private UserController userController;

        // Indica si el fomulario está listo para usarse
        private Task<List<academic_level_type>> filled1;
        private Task<List<skill_level_type>> filled2;
        private CancellationTokenSource tokenSource;

        private bool isStudent = false;
        private Regex emailRegex;
        private Regex duiRegex;
        private Regex cellphoneRegex;
        private Regex landlineRegex;
        private Match match;
        private List<string> skillLevelId;
        private List<string> academicLevelId;

        public Add()
		{
            userController = new UserController();
            emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            duiRegex = new Regex(@"^\d{9}$");
            cellphoneRegex = new Regex(@"^[6|7]{1}[\d]{7}$");
            landlineRegex = new Regex(@"^[2]{1}[\d]{7}$");
            skillLevelId = new List<string>();
            academicLevelId = new List<string>();
            InitializeComponent();
		}

		private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
		{

		}

		private void splitContainer2_SplitterMoved(object sender, SplitterEventArgs e)
		{

		}

		private void btnConfirm_Click(object sender, EventArgs e)
		{
			if (cboUserTypes.selectedIndex > -1)
			{
				isStudent = false;
				splFormTeacher.Visible = true;
				if(cboUserTypes.selectedIndex == 0 || cboUserTypes.selectedIndex == 2)
				{
					pnlWorkProfile.Visible = true;
					//infoAlert1.Visible = btnConfirm.Visible = false;
				}else if(cboUserTypes.selectedIndex == 1)
				{
					//infoAlert1.Visible = btnConfirm.Visible = false;
					pnlWorkProfile.Visible = false;
					isStudent = true;
				}
			}
		}

		private void ShowInfoAlert(int interval)
		{
			//fadeIn.Show(infoAlert1);
			//infoAlert1.Visible = true;
			if(interval > -1)
			{
				tmrAlert.Interval = 3000;
				tmrAlert.Start();
			}
		}

		private void tmrAlert_Tick(object sender, EventArgs e)
		{
			tmrAlert.Stop();
			//infoAlert1.Visible = false;
		}

		private List<Panel> tabs;
		private List<Label> lables;
		private List<Panel> pnlOptionsForm;
		private async void Add_Load(object sender, EventArgs e)
		{
			ShowInfoAlert(-1);
			tabs = new List<Panel>();
			lables = new List<Label>();
			pnlOptionsForm = new List<Panel>();
			tabs.Add(pnlGeneralData);
			tabs.Add(pnlAcademicProfile);
			tabs.Add(pnlWorkProfile);
			tabs.Add(pnlOthers);
			lables.Add(lblGeneralData);
			lables.Add(lblAcademicInfo);
			lables.Add(lblWorkProfile);
			lables.Add(lblOthers);
			pnlOptionsForm.Add(pnlDatosGenerales);
			pnlOptionsForm.Add(pnlPerfilAcademicoForm);
			pnlOptionsForm.Add(pnlPerfilTrabajoForm);
			pnlOptionsForm.Add(pnlOtrosForm);

            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            try
            {
                filled1 = Task.Run(() => academic_level_type.List(), token);
                List<academic_level_type> academicLevels = await filled1;

                foreach (academic_level_type level in academicLevels)
                {
                    drdAcademicLevel.AddItem(level.code);
                    academicLevelId.Add(level.Id);
                }
            }
            catch (OperationCanceledException)
            {

                filled1.Dispose();
            }

            tokenSource = new CancellationTokenSource();
            token = tokenSource.Token;
            try
            {
                filled2 = Task.Run(() => skill_level_type.List(), token);
                List<skill_level_type> skillLevels = await filled2;

                foreach (skill_level_type level in skillLevels)
                {
                    drdComputationLevel.AddItem(level.code);
                    drdEnglish.AddItem(level.code);
                    drdInternet.AddItem(level.code);
                    skillLevelId.Add(level.Id);
                }
            }
            catch (OperationCanceledException)
            {

                filled1.Dispose();
            }
        }

		private void btnGeneralDataEnd_Click(object sender, EventArgs e)
		{
            bool validation = true;
            if (txtName1.Text.Trim() == String.Empty)
            {
                ErrorName1.SetError(txtName1, "Este campo es obligatorio");
                validation = false;
            }
            else
            {
                ErrorName1.Clear();
            }
            if (txtLastname1.Text.Trim() == String.Empty)
            {
                ErrorLastname1.SetError(txtLastname1, "Este campo es obligatorio");
                validation = false;
            }
            else
            {
                ErrorLastname1.Clear();
            }
            match = emailRegex.Match(txtEmail1.Text.Trim());
            if (txtEmail1.Text.Trim() == String.Empty)
            {
                ErrorEmail1.SetError(txtEmail1, "Este campo es obligatorio");
                validation = false;
            }
            else if (!match.Success)
            {
                ErrorEmail1.SetError(txtEmail1, "Debe ingresar un email valido");
                validation = false;
            }
            else
            {
                ErrorEmail1.Clear();
            }
            match = duiRegex.Match(txtDUI.Text.Trim());
            if (txtDUI.Text.Trim() == String.Empty && isStudent == false)
            {
                ErrorDUI.SetError(txtDUI, "Este campo es obligatorio");
                validation = false;
            }
            else if (!match.Success && isStudent == false)
            {
                ErrorDUI.SetError(txtDUI, "Esta campo debe cumpplir el formato: #########");
                validation = false;
            }
            else
            {
                ErrorDUI.Clear();
            }
            if (drdGender.selectedIndex == -1)
            {
                ErrorGenero.SetError(drdGender, "Debe seleccionar un genero");
                validation = false;
            }
            else
            {
                ErrorGenero.Clear();
            }
            if (txtAdress.Text.Trim() == String.Empty)
            {
                ErrorAdress.SetError(txtAdress, "Este campo es obligatorio");
                validation = false;
            }
            else
            {
                ErrorAdress.Clear();
            }
            if (!validation)
                return;

            offLabelAndPanel(lblGeneralData, pnlGeneralData);
			onLabelAndPanel(lblAcademicInfo, pnlAcademicProfile);
			pnlDatosGenerales.Visible = false;
			pnlPerfilAcademicoForm.Visible = true;
		}

		private void offLabelAndPanel(Label lbl, Panel pnl)
		{
			lbl.BackColor = Color.FromArgb(5, 23, 30);
			pnl.Padding = new Padding(0,2,0,0);
			lbl.ForeColor = Color.Gray;
		}
		private void onLabelAndPanel(Label lbl, Panel pnl)
		{
			lbl.BackColor = Color.FromArgb(16, 70, 89);
			pnl.Padding = new Padding(0);
			lbl.ForeColor = Color.White;
		}

		private void lblAcademicInfo_Click(object sender, EventArgs e)
		{
			//offAllTabs();
			//onLabelAndPanel(lblAcademicInfo, pnlAcademicProfile);
			//hideAllPanelTabs();
			//pnlPerfilAcademicoForm.Visible = true;
		}

		private void btnGoToWorkProfile_Click(object sender, EventArgs e)
		{
			offLabelAndPanel(lblOthers, pnlOthers);
			pnlOtrosForm.Visible = false;
			if (isStudent)
			{
				onLabelAndPanel(lblAcademicInfo, pnlAcademicProfile);
				pnlPerfilAcademicoForm.Visible = true;
			}else
			{
				onLabelAndPanel(lblWorkProfile, pnlWorkProfile);
				pnlPerfilTrabajoForm.Visible = true;
			}
		}

		private void btnGoToOthers_Click(object sender, EventArgs e)
		{
            bool validation = true;
            if (txtVocation.Text.Trim() == String.Empty)
            {
                ErrorVocation.SetError(txtVocation, "Este campo es obligatorio");
                validation = false;
            }
            else
            {
                ErrorVocation.Clear();
            }
            if (txtWorkPlace.Text.Trim() == String.Empty && ckbWork.Checked == true)
            {
                ErrorWorkPlace.SetError(txtWorkPlace, "Este campo es obligatorio");
                validation = false;
            }
            else
            {
                ErrorWorkPlace.Clear();
            }
            if (txtWorkStatus.Text.Trim() == String.Empty && ckbWork.Checked == true)
            {
                ErrorWorkStatus.SetError(txtWorkStatus, "Este campo es obligatorio");
                validation = false;
            }
            else
            {
                ErrorWorkStatus.Clear();
            }
            if (!validation)
                return;

            offLabelAndPanel(lblWorkProfile, pnlWorkProfile);
			onLabelAndPanel(lblOthers, pnlOthers);
			pnlPerfilTrabajoForm.Visible = false;
			pnlOtrosForm.Visible = true;
		}

		private void btnGoToPerfilAcademico_Click(object sender, EventArgs e)
		{
			offLabelAndPanel(lblWorkProfile, pnlWorkProfile);
			onLabelAndPanel(lblAcademicInfo, pnlAcademicProfile);
			pnlPerfilTrabajoForm.Visible = false;
			pnlPerfilAcademicoForm.Visible = true;
		}

		private void btnNextWorkProfile_Click(object sender, EventArgs e)
		{
            bool validation = true;
            if ((txtReasons.Text.Trim() == String.Empty) && (ckbStudent.Checked == false))
            {
                ErrorReazons.SetError(txtReasons, "Este campo es obligatorio");
                validation = false;
            }
            else
            {
                ErrorReazons.Clear();
            }
            if (drdAcademicLevel.selectedIndex == -1)
            {
                ErrorAcademicLevel.SetError(drdAcademicLevel, "Debe seleccionar un nivel academico");
                validation = false;
            }
            else
            {
                ErrorAcademicLevel.Clear();
            }
            if (drdComputationLevel.selectedIndex == -1 && ckbComputation.Checked == true)
            {
                ErrorComputatitonLevel.SetError(drdComputationLevel, "Debe seleccionar un nivel");
                validation = false;
            }
            else
            {
                ErrorComputatitonLevel.Clear();
            }
            if (drdEnglish.selectedIndex == -1 && ckbEnglish.Checked == true)
            {
                ErrorEnglishLevel.SetError(drdEnglish, "Debe seleccionar un nivel");
                validation = false;
            }
            else
            {
                ErrorEnglishLevel.Clear();
            }
            if (drdInternet.selectedIndex == -1 && ckbInternet.Checked == true)
            {
                ErrorInternetLevel.SetError(drdInternet, "Debe seleccionar un nivel");
                validation = false;
            }
            else
            {
                ErrorInternetLevel.Clear();
            }
            if (!validation)
                return;

            offLabelAndPanel(lblAcademicInfo, pnlAcademicProfile);
			pnlPerfilAcademicoForm.Visible = false;
			if (isStudent)
			{
				onLabelAndPanel(lblOthers, pnlOthers);
				pnlOtrosForm.Visible = true;
			}else
			{
				onLabelAndPanel(lblWorkProfile, pnlWorkProfile);
				pnlPerfilTrabajoForm.Visible = true;

			}
		}

		private void btnGoToGeneralData_Click(object sender, EventArgs e)
		{
			offLabelAndPanel(lblAcademicInfo, pnlAcademicProfile);
			onLabelAndPanel(lblGeneralData, pnlGeneralData);
			pnlPerfilAcademicoForm.Visible = false;
			pnlDatosGenerales.Visible = true;
		}
		private void offAllTabs()
		{
			for (int i = 0; i < lables.Count; i++)
			{
				offLabelAndPanel(lables[i], tabs[i]);
			}
		}
		private void hideAllPanelTabs()
		{
			foreach (var item in pnlOptionsForm)
			{
				if (item.Visible == true) item.Visible = false;
			}
		}
		private void lblGeneralData_Click(object sender, EventArgs e)
		{
			//offAllTabs();	
			//onLabelAndPanel(lblGeneralData, pnlGeneralData);
			//hideAllPanelTabs();
			//pnlDatosGenerales.Visible = true;
		}

		private void lblWorkProfile_Click(object sender, EventArgs e)
		{
			//offAllTabs();
			//onLabelAndPanel(lblWorkProfile, pnlWorkProfile);
			//hideAllPanelTabs();
			//pnlPerfilTrabajoForm.Visible = true;
		}

		private void lblOthers_Click(object sender, EventArgs e)
		{
			//offAllTabs();
			//onLabelAndPanel(lblOthers, pnlOthers);
			//hideAllPanelTabs();
			//pnlOtrosForm.Visible = true;
		}

		private void cboUserTypes_onItemSelected(object sender, EventArgs e)
		{

			btnConfirm.Visible = true;
		}

        private void ckbStudent_OnChange(object sender, EventArgs e)
        {
            if(ckbStudent.Checked == true)
            {
                txtReasons.Text = "";
                txtReasons.BackColor = Color.Silver;
                txtReasons.HintText = "";
                txtReasons.Enabled = false;
            } else
            {
                txtReasons.BackColor = Color.White;
                txtReasons.Enabled = true;
                txtReasons.HintText = "Motivo o circunstancia que le impide estudiar...";
            }
            ErrorReazons.Clear();
        }

        private void ckbComputation_OnChange(object sender, EventArgs e)
        {
            if (ckbComputation.Checked == true)
            {
                drdComputationLevel.Enabled = true;
                drdComputationLevel.BackColor = Color.FromArgb(46, 139, 87);
            }
            else
            {
                drdComputationLevel.Enabled = false;
                drdComputationLevel.BackColor = Color.Silver;
            }
            ErrorComputatitonLevel.Clear();
        }

        private void ckbEnglish_OnChange(object sender, EventArgs e)
        {
            if (ckbEnglish.Checked == true)
            {
                drdEnglish.Enabled = true;
                drdEnglish.BackColor = Color.FromArgb(46, 139, 87);
            }
            else
            {
                drdEnglish.Enabled = false;
                drdEnglish.BackColor = Color.Silver;
            }
            ErrorEnglishLevel.Clear();
        }

        private void ckbInternet_OnChange(object sender, EventArgs e)
        {
            if (ckbInternet.Checked == true)
            {
                drdInternet.Enabled = true;
                drdInternet.BackColor = Color.FromArgb(46, 139, 87);
            }
            else
            {
                drdInternet.Enabled = false;
                drdInternet.BackColor = Color.Silver;
            }
            ErrorInternetLevel.Clear();
        }

        private void ckbWork_OnChange(object sender, EventArgs e)
        {
            if (ckbWork.Checked == false)
            {
                txtWorkPlace.Text = "";
                txtWorkPlace.BackColor = Color.Silver;
                txtWorkPlace.HintText = "";
                txtWorkPlace.Enabled = false;

                txtWorkStatus.Text = "";
                txtWorkStatus.BackColor = Color.Silver;
                txtWorkStatus.HintText = "";
                txtWorkStatus.Enabled = false;

                ErrorWorkPlace.Clear();
                ErrorWorkStatus.Clear();
            }
            else
            {
                txtWorkPlace.BackColor = Color.White;
                txtWorkPlace.Enabled = true;
                txtWorkPlace.HintText = "Ingrese el lugar donde se encuentra trabajando";

                txtWorkStatus.BackColor = Color.White;
                txtWorkStatus.Enabled = true;
                txtWorkStatus.HintText = "Operador, Secretario, Supervisor, etc...";

                ErrorWorkPlace.Clear();
                ErrorWorkStatus.Clear();
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            bool validation = true;
            if (txtName2.Text.Trim() == String.Empty)
            {
                ErrorName2.SetError(txtName2, "Este campo es obligatorio");
                validation = false;
            }
            else
            {
                ErrorName2.Clear();
            }
            if (txtLastname2.Text.Trim() == String.Empty)
            {
                ErrorLastname2.SetError(txtLastname2, "Este campo es obligatorio");
                validation = false;
            }
            else
            {
                ErrorLastname2.Clear();
            }
            match = emailRegex.Match(txtEmail2.Text.Trim());
            if (txtEmail2.Text.Trim() == String.Empty)
            {
                ErrorEmail2.SetError(txtEmail2, "Este campo es obligatorio");
                validation = false;
            }
            else if (!match.Success)
            {
                ErrorEmail2.SetError(txtEmail2, "Debe ingresar un email valido");
                validation = false;
            }
            else
            {
                ErrorEmail2.Clear();
            }
            match = cellphoneRegex.Match(txtCellphone.Text.Trim());
            if (txtCellphone.Text.Trim() == String.Empty)
            {
                ErrorCellphone.SetError(txtCellphone, "Este campo es obligatorio");
                validation = false;
            }
            else if (!match.Success && isStudent == false)
            {
                ErrorCellphone.SetError(txtCellphone, "Esta campo debe cumplir el formato de telefono salvadoreño");
                validation = false;
            }
            else
            {
                ErrorCellphone.Clear();
            }
            match = landlineRegex.Match(txtLandline.Text.Trim());
            if (txtLandline.Text.Trim() == String.Empty)
            {
                ErrorLandline.SetError(txtLandline, "Este campo es obligatorio");
                validation = false;
            }
            else if (!match.Success && isStudent == false)
            {
                ErrorLandline.SetError(txtLandline, "Esta campo debe cumplir el formato de telefono salvadoreño");
                validation = false;
            }
            else
            {
                ErrorLandline.Clear();
            }
            if (drdRelationship.selectedIndex == -1)
            {
                ErrorRelationship.SetError(drdRelationship, "Debe seleccionar un parentesco");
                validation = false;
            }
            else
            {
                ErrorRelationship.Clear();
            }
            if (!validation)
                return;

            User user = new User();
            user.Name = txtName1.Text.Trim();
            user.Lastname = txtLastname1.Text.Trim();
            user.Code = Guid.NewGuid().ToString();
            user.Email = txtEmail1.Text.Trim();
            if(txtDUI.Text.Trim() != String.Empty)
            {
                user.Dui = txtDUI.Text.Trim();
            }
            user.Gender = drdGender.selectedValue=="Hombre"?"M":"F";
            user.Adress = txtAdress.Text.Trim();
            user.StatusId = 1;

            user.Insert();
            CleanGeneralForm();

            User lastUser = UserController.getLastInsertered();

            academic_profile academicProfile = new academic_profile();
            academicProfile.study = ckbStudent.Checked?(byte)1:(byte)0;
            academicProfile.academic_leve_typel_id = academicLevelId[drdAcademicLevel.selectedIndex];
            if (ckbStudent.Checked)
            {
                academicProfile.description_not_study = txtReasons.Text.Trim();
            }
            academicProfile.other_courses = txtOtherCourses.Text.Trim() != String.Empty ? txtOtherCourses.Text.Trim() : null;
            academicProfile.other_skills = txtOtherHabilities.Text.Trim() != String.Empty ? txtOtherHabilities.Text.Trim() : null;
            academicProfile.computation_skills = ckbComputation.Checked ? (byte)1 : (byte)0;
            academicProfile.computation_level_id = ckbComputation.Checked ?skillLevelId[drdComputationLevel.selectedIndex]: "36e77385-b48d-41f4-9fbf-bc960639a41f";
            academicProfile.english_skills = ckbEnglish.Checked ? (byte)1 : (byte)0;
            academicProfile.english_level_id = ckbEnglish.Checked ? skillLevelId[drdEnglish.selectedIndex] : "36e77385-b48d-41f4-9fbf-bc960639a41f";
            academicProfile.internet_skills = ckbInternet.Checked ? (byte)1 : (byte)0;
            academicProfile.internet_level_id = ckbInternet.Checked ? skillLevelId[drdInternet.selectedIndex] : "36e77385-b48d-41f4-9fbf-bc960639a41f";
            academicProfile.user_id = lastUser.Id;
            academicProfile.Insert();
            CleanAcademicForm();

            if (!isStudent)
            {
                work_profile workProfile = new work_profile();
                workProfile.work = ckbWork.Checked ? (byte)1:(byte)0;
                if (ckbWork.Checked)
                {
                    workProfile.work_place = txtWorkPlace.Text.Trim();
                    workProfile.work_position = txtWorkPlace.Text.Trim();
                }
                workProfile.vocation = txtVocation.Text.Trim();
                workProfile.user_id = lastUser.Id;
                workProfile.Insert();
                CleanLaboralForm();
            }

            emergency_contacts contact = new emergency_contacts();
            contact.name = txtName2.Text.Trim();
            contact.last_name = txtLastname2.Text.Trim();
            contact.relationship = drdRelationship.selectedValue;
            contact.landline = txtLandline.Text;
            contact.mobile_phone = txtCellphone.Text;
            contact.email = txtEmail2.Text;
            contact.user_id = lastUser.Id;
            contact.Insert();
            CleanContactForm();

            offAllTabs();
            onLabelAndPanel(lblGeneralData, pnlGeneralData);
            hideAllPanelTabs();
            pnlDatosGenerales.Visible = true;

            MessageBox.Show("Usuario añadido correctamente");

        }

        private void CleanGeneralForm()
        {
            txtName1.Text = "";
            txtLastname1.Text = "";
            txtEmail1.Text = "";
            txtDUI.Text = "";
            drdGender.selectedIndex = 0;
            txtAdress.Text = "";
        }

        private void CleanAcademicForm()
        {
            ckbStudent.Checked = true;
            ckbComputation.Checked = true;
            ckbEnglish.Checked = true;
            ckbInternet.Checked = true;
            drdAcademicLevel.selectedIndex = 0;
            drdComputationLevel.selectedIndex = 0;
            drdEnglish.selectedIndex = 0;
            drdInternet.selectedIndex = 0;
            txtReasons.Text = "";
            txtOtherCourses.Text = "";
            txtOtherHabilities.Text = "";
        }

        private void CleanContactForm()
        {
            txtName2.Text = "";
            txtLastname2.Text = "";
            txtEmail2.Text = "";
            drdRelationship.selectedIndex = 0;
            txtLandline.Text = "";
            txtCellphone.Text = "";
        }

        private void CleanLaboralForm()
        {
            ckbWork.Checked = true;
            txtVocation.Text = "";
            txtWorkPlace.Text = "";
            txtWorkStatus.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CleanLaboralForm();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CleanAcademicForm();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            CleanGeneralForm();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CleanContactForm();
        }
    }
}
