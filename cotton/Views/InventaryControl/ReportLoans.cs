﻿using Cotton.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cotton.Views.InventaryControl
{
    public partial class ReportLoans : Form
    {
        private FontFamily Poppins, PoppinsL = null;
		public delegate void ResponseEvent(int lostQuantity);
		public event ResponseEvent ResponseReport;
		public ReportLoans(int quantity)
        {
            InitializeComponent();
            Poppins = Utils.ImportFont(nameof(Properties.Resources.Poppins_Bold)).Families[0];
            PoppinsL = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
            lblMessage.Font = new Font(Poppins, lblMessage.Font.Size);
			this.nudQuantityEquipment.Minimum = 1;
			this.nudQuantityEquipment.Maximum = quantity;

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

		private void ReportLoans_Load(object sender, EventArgs e)
		{

		}

		private void btnAceptar_Click(object sender, EventArgs e)
        {

            this.DialogResult = DialogResult.OK;
			this.ResponseReport?.Invoke(Convert.ToInt32(nudQuantityEquipment.Value));
			this.Close();
        }
    }
}
