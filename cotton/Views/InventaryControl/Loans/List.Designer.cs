﻿namespace Cotton.Views.InventaryControl.Loans
{
	partial class List
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(List));
            this.materialContextMenuStrip1 = new MaterialSkin.Controls.MaterialContextMenuStrip();
            this.menuParaEstoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.segundaOpciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terceraOpciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblTitle = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlLoading = new System.Windows.Forms.Panel();
            this.pcbLoading = new System.Windows.Forms.PictureBox();
            this.dgvPrestamos = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.nameEquipment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.state = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.lblTableTitle = new System.Windows.Forms.Label();
            this.searchIcon = new FontAwesome.Sharp.IconPictureBox();
            this.btnSearch = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.alert1 = new Cotton.Controls.Alert();
            this.grbControls = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.drpSortColumn = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.chkDesc = new Bunifu.Framework.UI.BunifuCheckbox();
            this.lblDesc = new System.Windows.Forms.Label();
            this.lblSortOption = new System.Windows.Forms.Label();
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnReport = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnConfirmDevolution = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnDialogFolder = new Bunifu.Framework.UI.BunifuFlatButton();
            this.fadeOut = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.materialContextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlLoading.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrestamos)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchIcon)).BeginInit();
            this.grbControls.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // materialContextMenuStrip1
            // 
            this.materialContextMenuStrip1.AutoClose = false;
            this.materialContextMenuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.fadeOut.SetDecoration(this.materialContextMenuStrip1, BunifuAnimatorNS.DecorationType.None);
            this.materialContextMenuStrip1.Depth = 0;
            this.materialContextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.materialContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuParaEstoToolStripMenuItem,
            this.toolStripSeparator1,
            this.toolStripMenuItem1});
            this.materialContextMenuStrip1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialContextMenuStrip1.Name = "materialContextMenuStrip1";
            this.materialContextMenuStrip1.Size = new System.Drawing.Size(157, 54);
            // 
            // menuParaEstoToolStripMenuItem
            // 
            this.menuParaEstoToolStripMenuItem.Name = "menuParaEstoToolStripMenuItem";
            this.menuParaEstoToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.menuParaEstoToolStripMenuItem.Text = "Menu para esto";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(153, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.segundaOpciónToolStripMenuItem,
            this.terceraOpciónToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.toolStripMenuItem1.Text = "Abrir aquí";
            // 
            // segundaOpciónToolStripMenuItem
            // 
            this.segundaOpciónToolStripMenuItem.Name = "segundaOpciónToolStripMenuItem";
            this.segundaOpciónToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.segundaOpciónToolStripMenuItem.Text = "Segunda opción";
            // 
            // terceraOpciónToolStripMenuItem
            // 
            this.terceraOpciónToolStripMenuItem.Name = "terceraOpciónToolStripMenuItem";
            this.terceraOpciónToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.terceraOpciónToolStripMenuItem.Text = "Tercera opción";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.fadeOut.SetDecoration(this.lblTitle, BunifuAnimatorNS.DecorationType.None);
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.lblTitle.Location = new System.Drawing.Point(3, 39);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(194, 25);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Lista de préstamos";
            // 
            // splitContainer1
            // 
            this.fadeOut.SetDecoration(this.splitContainer1, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.fadeOut.SetDecoration(this.splitContainer1.Panel1, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.fadeOut.SetDecoration(this.splitContainer1.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 72;
            this.splitContainer1.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.materialDivider1);
            this.flowLayoutPanel1.Controls.Add(this.lblTitle);
            this.fadeOut.SetDecoration(this.flowLayoutPanel1, BunifuAnimatorNS.DecorationType.None);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(20, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(780, 72);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.fadeOut.SetDecoration(this.materialDivider1, BunifuAnimatorNS.DecorationType.None);
            this.materialDivider1.Depth = 0;
            this.materialDivider1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.materialDivider1.Location = new System.Drawing.Point(10, 67);
            this.materialDivider1.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(131, 2);
            this.materialDivider1.TabIndex = 2;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainer3);
            this.fadeOut.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(20, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(780, 374);
            this.panel1.TabIndex = 1;
            // 
            // splitContainer3
            // 
            this.fadeOut.SetDecoration(this.splitContainer3, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.panel2);
            this.splitContainer3.Panel1.Controls.Add(this.panel3);
            this.fadeOut.SetDecoration(this.splitContainer3.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.alert1);
            this.splitContainer3.Panel2.Controls.Add(this.grbControls);
            this.splitContainer3.Panel2.Controls.Add(this.btnDialogFolder);
            this.fadeOut.SetDecoration(this.splitContainer3.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer3.Size = new System.Drawing.Size(780, 374);
            this.splitContainer3.SplitterDistance = 519;
            this.splitContainer3.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnlLoading);
            this.panel2.Controls.Add(this.dgvPrestamos);
            this.fadeOut.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 53);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(12, 15, 12, 15);
            this.panel2.Size = new System.Drawing.Size(519, 265);
            this.panel2.TabIndex = 1;
            // 
            // pnlLoading
            // 
            this.pnlLoading.Controls.Add(this.pcbLoading);
            this.fadeOut.SetDecoration(this.pnlLoading, BunifuAnimatorNS.DecorationType.None);
            this.pnlLoading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLoading.Location = new System.Drawing.Point(12, 15);
            this.pnlLoading.Name = "pnlLoading";
            this.pnlLoading.Size = new System.Drawing.Size(495, 235);
            this.pnlLoading.TabIndex = 1;
            // 
            // pcbLoading
            // 
            this.pcbLoading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(242)))), ((int)(((byte)(243)))));
            this.fadeOut.SetDecoration(this.pcbLoading, BunifuAnimatorNS.DecorationType.None);
            this.pcbLoading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcbLoading.Image = global::Cotton.Properties.Resources.loading;
            this.pcbLoading.Location = new System.Drawing.Point(0, 0);
            this.pcbLoading.Name = "pcbLoading";
            this.pcbLoading.Size = new System.Drawing.Size(495, 235);
            this.pcbLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pcbLoading.TabIndex = 0;
            this.pcbLoading.TabStop = false;
            // 
            // dgvPrestamos
            // 
            this.dgvPrestamos.AllowUserToAddRows = false;
            this.dgvPrestamos.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvPrestamos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPrestamos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPrestamos.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgvPrestamos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvPrestamos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvPrestamos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPrestamos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPrestamos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrestamos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameEquipment,
            this.quantity,
            this.state,
            this.nameUser,
            this.dateFrom,
            this.dateTo,
            this.id});
            this.fadeOut.SetDecoration(this.dgvPrestamos, BunifuAnimatorNS.DecorationType.None);
            this.dgvPrestamos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPrestamos.DoubleBuffered = true;
            this.dgvPrestamos.EnableHeadersVisualStyles = false;
            this.dgvPrestamos.GridColor = System.Drawing.SystemColors.Control;
            this.dgvPrestamos.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.dgvPrestamos.HeaderForeColor = System.Drawing.Color.Azure;
            this.dgvPrestamos.Location = new System.Drawing.Point(12, 15);
            this.dgvPrestamos.Name = "dgvPrestamos";
            this.dgvPrestamos.ReadOnly = true;
            this.dgvPrestamos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(15)))), ((int)(((byte)(9)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPrestamos.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPrestamos.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(15)))), ((int)(((byte)(9)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(46)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.dgvPrestamos.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPrestamos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPrestamos.Size = new System.Drawing.Size(495, 235);
            this.dgvPrestamos.TabIndex = 0;
            this.dgvPrestamos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEquipos_CellClick);
            this.dgvPrestamos.Sorted += new System.EventHandler(this.dgvEquipos_Sorted);
            // 
            // nameEquipment
            // 
            this.nameEquipment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.nameEquipment.FillWeight = 15F;
            this.nameEquipment.Frozen = true;
            this.nameEquipment.HeaderText = "Equipo";
            this.nameEquipment.MinimumWidth = 100;
            this.nameEquipment.Name = "nameEquipment";
            this.nameEquipment.ReadOnly = true;
            // 
            // quantity
            // 
            this.quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.quantity.HeaderText = "Cantidad Prestada";
            this.quantity.MinimumWidth = 100;
            this.quantity.Name = "quantity";
            this.quantity.ReadOnly = true;
            // 
            // state
            // 
            this.state.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.state.HeaderText = "Estado";
            this.state.MinimumWidth = 80;
            this.state.Name = "state";
            this.state.ReadOnly = true;
            this.state.Width = 80;
            // 
            // nameUser
            // 
            this.nameUser.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.nameUser.FillWeight = 35F;
            this.nameUser.HeaderText = "Usuario";
            this.nameUser.MinimumWidth = 100;
            this.nameUser.Name = "nameUser";
            this.nameUser.ReadOnly = true;
            // 
            // dateFrom
            // 
            this.dateFrom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.dateFrom.HeaderText = "Prestado desde:";
            this.dateFrom.MinimumWidth = 150;
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.ReadOnly = true;
            this.dateFrom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dateFrom.Width = 150;
            // 
            // dateTo
            // 
            this.dateTo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dateTo.HeaderText = "Prestado Hasta:";
            this.dateTo.MinimumWidth = 100;
            this.dateTo.Name = "dateTo";
            this.dateTo.ReadOnly = true;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splitContainer2);
            this.fadeOut.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(519, 53);
            this.panel3.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.fadeOut.SetDecoration(this.splitContainer2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.lblTableTitle);
            this.fadeOut.SetDecoration(this.splitContainer2.Panel1, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer2.Panel1MinSize = 100;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.searchIcon);
            this.splitContainer2.Panel2.Controls.Add(this.btnSearch);
            this.fadeOut.SetDecoration(this.splitContainer2.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer2.Size = new System.Drawing.Size(519, 53);
            this.splitContainer2.SplitterDistance = 150;
            this.splitContainer2.TabIndex = 0;
            // 
            // lblTableTitle
            // 
            this.lblTableTitle.AutoSize = true;
            this.fadeOut.SetDecoration(this.lblTableTitle, BunifuAnimatorNS.DecorationType.None);
            this.lblTableTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTableTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.lblTableTitle.Location = new System.Drawing.Point(3, 15);
            this.lblTableTitle.Name = "lblTableTitle";
            this.lblTableTitle.Size = new System.Drawing.Size(138, 20);
            this.lblTableTitle.TabIndex = 3;
            this.lblTableTitle.Text = "Préstamos activos";
            // 
            // searchIcon
            // 
            this.searchIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchIcon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.fadeOut.SetDecoration(this.searchIcon, BunifuAnimatorNS.DecorationType.None);
            this.searchIcon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.searchIcon.IconChar = FontAwesome.Sharp.IconChar.Search;
            this.searchIcon.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.searchIcon.IconSize = 23;
            this.searchIcon.Location = new System.Drawing.Point(335, 12);
            this.searchIcon.Name = "searchIcon";
            this.searchIcon.Size = new System.Drawing.Size(24, 23);
            this.searchIcon.TabIndex = 2;
            this.searchIcon.TabStop = false;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeOut.SetDecoration(this.btnSearch, BunifuAnimatorNS.DecorationType.None);
            this.btnSearch.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSearch.HintForeColor = System.Drawing.Color.Empty;
            this.btnSearch.HintText = "Buscar...";
            this.btnSearch.isPassword = false;
            this.btnSearch.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.btnSearch.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.btnSearch.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.btnSearch.LineThickness = 3;
            this.btnSearch.Location = new System.Drawing.Point(78, 7);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(281, 33);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.btnSearch_KeyPress);
            // 
            // alert1
            // 
            this.alert1.AlertType = Cotton.Controls.AlertType.Danger;
            this.alert1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.alert1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fadeOut.SetDecoration(this.alert1, BunifuAnimatorNS.DecorationType.None);
            this.alert1.FontSizeBody = 9.75F;
            this.alert1.FontSizeTitle = 12F;
            this.alert1.Location = new System.Drawing.Point(10, 9);
            this.alert1.MaximumSize = new System.Drawing.Size(380, 10000);
            this.alert1.Message = resources.GetString("alert1.Message");
            this.alert1.Name = "alert1";
            this.alert1.Size = new System.Drawing.Size(240, 102);
            this.alert1.TabIndex = 4;
            this.alert1.TimeLapse = 5000;
            this.alert1.Title = "Message Title Alert";
            this.alert1.Visible = false;
            // 
            // grbControls
            // 
            this.grbControls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbControls.Controls.Add(this.panel4);
            this.grbControls.Controls.Add(this.bunifuFlatButton2);
            this.grbControls.Controls.Add(this.btnReport);
            this.grbControls.Controls.Add(this.btnConfirmDevolution);
            this.fadeOut.SetDecoration(this.grbControls, BunifuAnimatorNS.DecorationType.None);
            this.grbControls.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbControls.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbControls.Location = new System.Drawing.Point(17, 53);
            this.grbControls.Name = "grbControls";
            this.grbControls.Size = new System.Drawing.Size(228, 250);
            this.grbControls.TabIndex = 5;
            this.grbControls.TabStop = false;
            this.grbControls.Text = "Controles";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.drpSortColumn);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.lblSortOption);
            this.fadeOut.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(3, 147);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(222, 100);
            this.panel4.TabIndex = 8;
            // 
            // drpSortColumn
            // 
            this.drpSortColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.drpSortColumn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(84)))), ((int)(((byte)(65)))));
            this.fadeOut.SetDecoration(this.drpSortColumn, BunifuAnimatorNS.DecorationType.None);
            this.drpSortColumn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpSortColumn.ForeColor = System.Drawing.Color.White;
            this.drpSortColumn.FormattingEnabled = true;
            this.drpSortColumn.Location = new System.Drawing.Point(20, 61);
            this.drpSortColumn.Name = "drpSortColumn";
            this.drpSortColumn.Size = new System.Drawing.Size(180, 26);
            this.drpSortColumn.TabIndex = 13;
            this.drpSortColumn.SelectedIndexChanged += new System.EventHandler(this.chkDesc_OnChange);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.chkDesc);
            this.panel5.Controls.Add(this.lblDesc);
            this.fadeOut.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.panel5.Location = new System.Drawing.Point(20, 30);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(180, 24);
            this.panel5.TabIndex = 11;
            // 
            // chkDesc
            // 
            this.chkDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.chkDesc.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.chkDesc.Checked = false;
            this.chkDesc.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(84)))), ((int)(((byte)(65)))));
            this.fadeOut.SetDecoration(this.chkDesc, BunifuAnimatorNS.DecorationType.None);
            this.chkDesc.ForeColor = System.Drawing.Color.White;
            this.chkDesc.Location = new System.Drawing.Point(4, 3);
            this.chkDesc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkDesc.Name = "chkDesc";
            this.chkDesc.Size = new System.Drawing.Size(20, 20);
            this.chkDesc.TabIndex = 9;
            this.chkDesc.OnChange += new System.EventHandler(this.chkDesc_OnChange);
            // 
            // lblDesc
            // 
            this.lblDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDesc.AutoSize = true;
            this.fadeOut.SetDecoration(this.lblDesc, BunifuAnimatorNS.DecorationType.None);
            this.lblDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesc.Location = new System.Drawing.Point(30, 3);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(95, 18);
            this.lblDesc.TabIndex = 10;
            this.lblDesc.Text = "Descendente";
            this.lblDesc.Click += new System.EventHandler(this.lblDesc_Click);
            // 
            // lblSortOption
            // 
            this.lblSortOption.AutoSize = true;
            this.fadeOut.SetDecoration(this.lblSortOption, BunifuAnimatorNS.DecorationType.None);
            this.lblSortOption.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSortOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSortOption.Location = new System.Drawing.Point(0, 0);
            this.lblSortOption.Name = "lblSortOption";
            this.lblSortOption.Size = new System.Drawing.Size(129, 18);
            this.lblSortOption.TabIndex = 7;
            this.lblSortOption.Text = "Ordenando como:";
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 0;
            this.bunifuFlatButton2.ButtonText = "MODIFICAR";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeOut.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Enabled = false;
            this.bunifuFlatButton2.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton2.Iconimage")));
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = false;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = false;
            this.bunifuFlatButton2.IconZoom = 90D;
            this.bunifuFlatButton2.IsTab = false;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(7, 107);
            this.bunifuFlatButton2.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(214, 33);
            this.bunifuFlatButton2.TabIndex = 6;
            this.bunifuFlatButton2.Text = "MODIFICAR";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton2.Visible = false;
            // 
            // btnReport
            // 
            this.btnReport.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnReport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.btnReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReport.BorderRadius = 0;
            this.btnReport.ButtonText = "REPORTAR COMO PERDIDO";
            this.btnReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeOut.SetDecoration(this.btnReport, BunifuAnimatorNS.DecorationType.None);
            this.btnReport.DisabledColor = System.Drawing.Color.Gray;
            this.btnReport.Enabled = false;
            this.btnReport.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReport.Iconcolor = System.Drawing.Color.Transparent;
            this.btnReport.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnReport.Iconimage")));
            this.btnReport.Iconimage_right = null;
            this.btnReport.Iconimage_right_Selected = null;
            this.btnReport.Iconimage_Selected = null;
            this.btnReport.IconMarginLeft = 0;
            this.btnReport.IconMarginRight = 0;
            this.btnReport.IconRightVisible = false;
            this.btnReport.IconRightZoom = 0D;
            this.btnReport.IconVisible = false;
            this.btnReport.IconZoom = 90D;
            this.btnReport.IsTab = false;
            this.btnReport.Location = new System.Drawing.Point(7, 66);
            this.btnReport.Margin = new System.Windows.Forms.Padding(4);
            this.btnReport.Name = "btnReport";
            this.btnReport.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.btnReport.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnReport.OnHoverTextColor = System.Drawing.Color.White;
            this.btnReport.selected = false;
            this.btnReport.Size = new System.Drawing.Size(214, 33);
            this.btnReport.TabIndex = 5;
            this.btnReport.Text = "REPORTAR COMO PERDIDO";
            this.btnReport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnReport.Textcolor = System.Drawing.Color.White;
            this.btnReport.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnConfirmDevolution
            // 
            this.btnConfirmDevolution.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnConfirmDevolution.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfirmDevolution.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnConfirmDevolution.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConfirmDevolution.BorderRadius = 0;
            this.btnConfirmDevolution.ButtonText = "CONFIRMAR DEVOLUCIÓN";
            this.btnConfirmDevolution.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeOut.SetDecoration(this.btnConfirmDevolution, BunifuAnimatorNS.DecorationType.None);
            this.btnConfirmDevolution.DisabledColor = System.Drawing.Color.Gray;
            this.btnConfirmDevolution.Enabled = false;
            this.btnConfirmDevolution.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmDevolution.Iconcolor = System.Drawing.Color.Transparent;
            this.btnConfirmDevolution.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnConfirmDevolution.Iconimage")));
            this.btnConfirmDevolution.Iconimage_right = null;
            this.btnConfirmDevolution.Iconimage_right_Selected = null;
            this.btnConfirmDevolution.Iconimage_Selected = null;
            this.btnConfirmDevolution.IconMarginLeft = 0;
            this.btnConfirmDevolution.IconMarginRight = 0;
            this.btnConfirmDevolution.IconRightVisible = false;
            this.btnConfirmDevolution.IconRightZoom = 0D;
            this.btnConfirmDevolution.IconVisible = false;
            this.btnConfirmDevolution.IconZoom = 90D;
            this.btnConfirmDevolution.IsTab = false;
            this.btnConfirmDevolution.Location = new System.Drawing.Point(7, 25);
            this.btnConfirmDevolution.Margin = new System.Windows.Forms.Padding(4);
            this.btnConfirmDevolution.Name = "btnConfirmDevolution";
            this.btnConfirmDevolution.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnConfirmDevolution.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnConfirmDevolution.OnHoverTextColor = System.Drawing.Color.White;
            this.btnConfirmDevolution.selected = false;
            this.btnConfirmDevolution.Size = new System.Drawing.Size(214, 33);
            this.btnConfirmDevolution.TabIndex = 4;
            this.btnConfirmDevolution.Text = "CONFIRMAR DEVOLUCIÓN";
            this.btnConfirmDevolution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnConfirmDevolution.Textcolor = System.Drawing.Color.White;
            this.btnConfirmDevolution.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmDevolution.Click += new System.EventHandler(this.btnConfirmDevolution_Click);
            // 
            // btnDialogFolder
            // 
            this.btnDialogFolder.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnDialogFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDialogFolder.BackColor = System.Drawing.Color.SeaGreen;
            this.btnDialogFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDialogFolder.BorderRadius = 0;
            this.btnDialogFolder.ButtonText = "Generar PDF";
            this.btnDialogFolder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeOut.SetDecoration(this.btnDialogFolder, BunifuAnimatorNS.DecorationType.None);
            this.btnDialogFolder.DisabledColor = System.Drawing.Color.Gray;
            this.btnDialogFolder.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDialogFolder.Iconcolor = System.Drawing.Color.Transparent;
            this.btnDialogFolder.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnDialogFolder.Iconimage")));
            this.btnDialogFolder.Iconimage_right = null;
            this.btnDialogFolder.Iconimage_right_Selected = null;
            this.btnDialogFolder.Iconimage_Selected = null;
            this.btnDialogFolder.IconMarginLeft = 0;
            this.btnDialogFolder.IconMarginRight = 0;
            this.btnDialogFolder.IconRightVisible = false;
            this.btnDialogFolder.IconRightZoom = 0D;
            this.btnDialogFolder.IconVisible = false;
            this.btnDialogFolder.IconZoom = 90D;
            this.btnDialogFolder.IsTab = false;
            this.btnDialogFolder.Location = new System.Drawing.Point(24, 15);
            this.btnDialogFolder.Margin = new System.Windows.Forms.Padding(4);
            this.btnDialogFolder.Name = "btnDialogFolder";
            this.btnDialogFolder.Normalcolor = System.Drawing.Color.SeaGreen;
            this.btnDialogFolder.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnDialogFolder.OnHoverTextColor = System.Drawing.Color.White;
            this.btnDialogFolder.selected = false;
            this.btnDialogFolder.Size = new System.Drawing.Size(214, 33);
            this.btnDialogFolder.TabIndex = 11;
            this.btnDialogFolder.Text = "Generar PDF";
            this.btnDialogFolder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnDialogFolder.Textcolor = System.Drawing.Color.White;
            this.btnDialogFolder.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDialogFolder.Click += new System.EventHandler(this.btnDialogFolder_Click);
            // 
            // fadeOut
            // 
            this.fadeOut.AnimationType = BunifuAnimatorNS.AnimationType.Transparent;
            this.fadeOut.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 1F;
            this.fadeOut.DefaultAnimation = animation1;
            // 
            // List
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.fadeOut.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Name = "List";
            this.Text = "Categories";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.List_FormClosing);
            this.Load += new System.EventHandler(this.List_Load);
            this.materialContextMenuStrip1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlLoading.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrestamos)).EndInit();
            this.panel3.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchIcon)).EndInit();
            this.grbControls.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private MaterialSkin.Controls.MaterialContextMenuStrip materialContextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem menuParaEstoToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem segundaOpciónToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem terceraOpciónToolStripMenuItem;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private MaterialSkin.Controls.MaterialDivider materialDivider1;
		private Bunifu.Framework.UI.BunifuCustomDataGrid dgvPrestamos;
		private System.Windows.Forms.Panel panel1;
		private FontAwesome.Sharp.IconPictureBox searchIcon;
		private Bunifu.Framework.UI.BunifuMaterialTextbox btnSearch;
		private System.Windows.Forms.Label lblTableTitle;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.SplitContainer splitContainer3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel pnlLoading;
		private BunifuAnimatorNS.BunifuTransition fadeOut;
		private System.Windows.Forms.PictureBox pcbLoading;
        private Controls.Alert alert1;
		private System.Windows.Forms.GroupBox grbControls;
		private Bunifu.Framework.UI.BunifuFlatButton btnConfirmDevolution;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel5;
		private Bunifu.Framework.UI.BunifuCheckbox chkDesc;
		private System.Windows.Forms.Label lblDesc;
		private System.Windows.Forms.Label lblSortOption;
		private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
		private Bunifu.Framework.UI.BunifuFlatButton btnReport;
		private System.Windows.Forms.ComboBox drpSortColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameEquipment;
		private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
		private System.Windows.Forms.DataGridViewTextBoxColumn state;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameUser;
		private System.Windows.Forms.DataGridViewTextBoxColumn dateFrom;
		private System.Windows.Forms.DataGridViewTextBoxColumn dateTo;
		private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private Bunifu.Framework.UI.BunifuFlatButton btnDialogFolder;
    }
}