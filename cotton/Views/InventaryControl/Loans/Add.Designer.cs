﻿namespace Cotton.Views.InventaryControl.Loans
{
	partial class Add
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splContainer = new System.Windows.Forms.SplitContainer();
            this.leftForm = new System.Windows.Forms.SplitContainer();
            this.lblTitleForm = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.bunifuSeparator3 = new Bunifu.Framework.UI.BunifuSeparator();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.bunifuSeparator4 = new Bunifu.Framework.UI.BunifuSeparator();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.nudHours = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.nudMinutes = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblFinalHour = new System.Windows.Forms.Label();
            this.bunifuSeparator2 = new Bunifu.Framework.UI.BunifuSeparator();
            this.lblDate = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btnChooseEquipment = new Bunifu.Framework.UI.BunifuFlatButton();
            this.txtEquipmentName = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lblName = new System.Windows.Forms.Label();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAddLoan = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnChooseUser = new Bunifu.Framework.UI.BunifuFlatButton();
            this.txtUserName = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.bunifuSeparator5 = new Bunifu.Framework.UI.BunifuSeparator();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.nudQuantityEquipment = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.bunifuSeparator6 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label6 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.lblRestEquipment = new System.Windows.Forms.Label();
            this.bunifuSeparator7 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label8 = new System.Windows.Forms.Label();
            this.ErrorName = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorStock = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorType = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorDescription = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorDate = new System.Windows.Forms.ErrorProvider(this.components);
            this.alert1 = new Cotton.Controls.Alert();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splContainer)).BeginInit();
            this.splContainer.Panel1.SuspendLayout();
            this.splContainer.Panel2.SuspendLayout();
            this.splContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftForm)).BeginInit();
            this.leftForm.Panel1.SuspendLayout();
            this.leftForm.Panel2.SuspendLayout();
            this.leftForm.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinutes)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantityEquipment)).BeginInit();
            this.panel14.SuspendLayout();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorDate)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.splitContainer1.Size = new System.Drawing.Size(910, 499);
            this.splitContainer1.SplitterDistance = 72;
            this.splitContainer1.TabIndex = 4;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.materialDivider1);
            this.flowLayoutPanel1.Controls.Add(this.lblTitle);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(20, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(890, 72);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.materialDivider1.Location = new System.Drawing.Point(10, 67);
            this.materialDivider1.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(131, 2);
            this.materialDivider1.TabIndex = 2;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.lblTitle.Location = new System.Drawing.Point(3, 39);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(234, 25);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Añadir nuevo préstamo";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splContainer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(20, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(870, 423);
            this.panel1.TabIndex = 4;
            // 
            // splContainer
            // 
            this.splContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splContainer.IsSplitterFixed = true;
            this.splContainer.Location = new System.Drawing.Point(0, 0);
            this.splContainer.Name = "splContainer";
            // 
            // splContainer.Panel1
            // 
            this.splContainer.Panel1.Controls.Add(this.leftForm);
            // 
            // splContainer.Panel2
            // 
            this.splContainer.Panel2.Controls.Add(this.splitContainer3);
            this.splContainer.Size = new System.Drawing.Size(870, 423);
            this.splContainer.SplitterDistance = 434;
            this.splContainer.SplitterWidth = 1;
            this.splContainer.TabIndex = 4;
            // 
            // leftForm
            // 
            this.leftForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftForm.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.leftForm.IsSplitterFixed = true;
            this.leftForm.Location = new System.Drawing.Point(0, 0);
            this.leftForm.Name = "leftForm";
            this.leftForm.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // leftForm.Panel1
            // 
            this.leftForm.Panel1.Controls.Add(this.lblTitleForm);
            // 
            // leftForm.Panel2
            // 
            this.leftForm.Panel2.AutoScroll = true;
            this.leftForm.Panel2.Controls.Add(this.panel15);
            this.leftForm.Size = new System.Drawing.Size(434, 423);
            this.leftForm.SplitterDistance = 56;
            this.leftForm.TabIndex = 0;
            // 
            // lblTitleForm
            // 
            this.lblTitleForm.AutoSize = true;
            this.lblTitleForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleForm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.lblTitleForm.Location = new System.Drawing.Point(6, 30);
            this.lblTitleForm.Name = "lblTitleForm";
            this.lblTitleForm.Size = new System.Drawing.Size(241, 20);
            this.lblTitleForm.TabIndex = 4;
            this.lblTitleForm.Text = "Complete los siguientes campos:";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.bunifuSeparator3);
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(434, 264);
            this.panel15.TabIndex = 4;
            // 
            // bunifuSeparator3
            // 
            this.bunifuSeparator3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator3.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuSeparator3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator3.LineThickness = 1;
            this.bunifuSeparator3.Location = new System.Drawing.Point(429, 0);
            this.bunifuSeparator3.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator3.Name = "bunifuSeparator3";
            this.bunifuSeparator3.Size = new System.Drawing.Size(5, 264);
            this.bunifuSeparator3.TabIndex = 9;
            this.bunifuSeparator3.Transparency = 255;
            this.bunifuSeparator3.Vertical = true;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.panel3);
            this.panel16.Controls.Add(this.panel2);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(434, 264);
            this.panel16.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splitContainer2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 91);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(434, 131);
            this.panel3.TabIndex = 5;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.bunifuSeparator4);
            this.splitContainer2.Panel1.Controls.Add(this.panel5);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.panel7);
            this.splitContainer2.Size = new System.Drawing.Size(434, 131);
            this.splitContainer2.SplitterDistance = 215;
            this.splitContainer2.SplitterWidth = 1;
            this.splitContainer2.TabIndex = 0;
            // 
            // bunifuSeparator4
            // 
            this.bunifuSeparator4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator4.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuSeparator4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bunifuSeparator4.LineThickness = 1;
            this.bunifuSeparator4.Location = new System.Drawing.Point(210, 0);
            this.bunifuSeparator4.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator4.Name = "bunifuSeparator4";
            this.bunifuSeparator4.Size = new System.Drawing.Size(5, 131);
            this.bunifuSeparator4.TabIndex = 8;
            this.bunifuSeparator4.Transparency = 0;
            this.bunifuSeparator4.Vertical = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.lblQuantity);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel5.Size = new System.Drawing.Size(215, 131);
            this.panel5.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.flowLayoutPanel2);
            this.panel6.Controls.Add(this.bunifuSeparator1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(15, 40);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(185, 79);
            this.panel6.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.nudHours);
            this.flowLayoutPanel2.Controls.Add(this.label1);
            this.flowLayoutPanel2.Controls.Add(this.nudMinutes);
            this.flowLayoutPanel2.Controls.Add(this.label2);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(185, 44);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // nudHours
            // 
            this.nudHours.BackColor = System.Drawing.Color.White;
            this.nudHours.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudHours.Dock = System.Windows.Forms.DockStyle.Left;
            this.nudHours.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudHours.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.nudHours.Location = new System.Drawing.Point(3, 3);
            this.nudHours.Name = "nudHours";
            this.nudHours.Size = new System.Drawing.Size(60, 23);
            this.nudHours.TabIndex = 2;
            this.nudHours.ThousandsSeparator = true;
            this.nudHours.ValueChanged += new System.EventHandler(this.nudMinutes_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "H:";
            // 
            // nudMinutes
            // 
            this.nudMinutes.BackColor = System.Drawing.Color.White;
            this.nudMinutes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudMinutes.Dock = System.Windows.Forms.DockStyle.Left;
            this.nudMinutes.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudMinutes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.nudMinutes.Location = new System.Drawing.Point(95, 3);
            this.nudMinutes.Name = "nudMinutes";
            this.nudMinutes.Size = new System.Drawing.Size(60, 23);
            this.nudMinutes.TabIndex = 4;
            this.nudMinutes.ThousandsSeparator = true;
            this.nudMinutes.ValueChanged += new System.EventHandler(this.nudMinutes_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(161, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "M";
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.bunifuSeparator1.LineThickness = 3;
            this.bunifuSeparator1.Location = new System.Drawing.Point(0, 44);
            this.bunifuSeparator1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(185, 35);
            this.bunifuSeparator1.TabIndex = 2;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // lblQuantity
            // 
            this.lblQuantity.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantity.Location = new System.Drawing.Point(15, 12);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(185, 28);
            this.lblQuantity.TabIndex = 1;
            this.lblQuantity.Text = "Tiempo a prestar:";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.lblDate);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel7.Size = new System.Drawing.Size(218, 131);
            this.panel7.TabIndex = 3;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.lblFinalHour);
            this.panel8.Controls.Add(this.bunifuSeparator2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(15, 40);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(188, 79);
            this.panel8.TabIndex = 2;
            // 
            // lblFinalHour
            // 
            this.lblFinalHour.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblFinalHour.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinalHour.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblFinalHour.Location = new System.Drawing.Point(0, 16);
            this.lblFinalHour.Name = "lblFinalHour";
            this.lblFinalHour.Size = new System.Drawing.Size(188, 28);
            this.lblFinalHour.TabIndex = 4;
            this.lblFinalHour.Text = "Aún no ha selecciona hora";
            this.lblFinalHour.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.bunifuSeparator2.LineThickness = 3;
            this.bunifuSeparator2.Location = new System.Drawing.Point(0, 44);
            this.bunifuSeparator2.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Size = new System.Drawing.Size(188, 35);
            this.bunifuSeparator2.TabIndex = 3;
            this.bunifuSeparator2.Transparency = 255;
            this.bunifuSeparator2.Vertical = false;
            // 
            // lblDate
            // 
            this.lblDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(15, 12);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(188, 28);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "Hora de entrega:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel17);
            this.panel2.Controls.Add(this.lblName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel2.Size = new System.Drawing.Size(434, 91);
            this.panel2.TabIndex = 2;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.btnChooseEquipment);
            this.panel17.Controls.Add(this.txtEquipmentName);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(15, 40);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(404, 39);
            this.panel17.TabIndex = 2;
            // 
            // btnChooseEquipment
            // 
            this.btnChooseEquipment.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnChooseEquipment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChooseEquipment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnChooseEquipment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnChooseEquipment.BorderRadius = 0;
            this.btnChooseEquipment.ButtonText = "Elegir";
            this.btnChooseEquipment.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChooseEquipment.DisabledColor = System.Drawing.Color.Gray;
            this.btnChooseEquipment.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChooseEquipment.Iconcolor = System.Drawing.Color.Transparent;
            this.btnChooseEquipment.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnChooseEquipment.Iconimage")));
            this.btnChooseEquipment.Iconimage_right = null;
            this.btnChooseEquipment.Iconimage_right_Selected = null;
            this.btnChooseEquipment.Iconimage_Selected = null;
            this.btnChooseEquipment.IconMarginLeft = 0;
            this.btnChooseEquipment.IconMarginRight = 0;
            this.btnChooseEquipment.IconRightVisible = false;
            this.btnChooseEquipment.IconRightZoom = 0D;
            this.btnChooseEquipment.IconVisible = false;
            this.btnChooseEquipment.IconZoom = 90D;
            this.btnChooseEquipment.IsTab = false;
            this.btnChooseEquipment.Location = new System.Drawing.Point(314, 5);
            this.btnChooseEquipment.Margin = new System.Windows.Forms.Padding(4);
            this.btnChooseEquipment.Name = "btnChooseEquipment";
            this.btnChooseEquipment.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnChooseEquipment.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnChooseEquipment.OnHoverTextColor = System.Drawing.Color.White;
            this.btnChooseEquipment.selected = false;
            this.btnChooseEquipment.Size = new System.Drawing.Size(87, 30);
            this.btnChooseEquipment.TabIndex = 3;
            this.btnChooseEquipment.Text = "Elegir";
            this.btnChooseEquipment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnChooseEquipment.Textcolor = System.Drawing.Color.White;
            this.btnChooseEquipment.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChooseEquipment.Click += new System.EventHandler(this.btnChooseEquipment_Click);
            // 
            // txtEquipmentName
            // 
            this.txtEquipmentName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEquipmentName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEquipmentName.Enabled = false;
            this.txtEquipmentName.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtEquipmentName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtEquipmentName.HintForeColor = System.Drawing.Color.Gray;
            this.txtEquipmentName.HintText = "Aún no ha seleccionado equipo";
            this.txtEquipmentName.isPassword = false;
            this.txtEquipmentName.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtEquipmentName.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtEquipmentName.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtEquipmentName.LineThickness = 3;
            this.txtEquipmentName.Location = new System.Drawing.Point(0, 3);
            this.txtEquipmentName.Margin = new System.Windows.Forms.Padding(4);
            this.txtEquipmentName.Name = "txtEquipmentName";
            this.txtEquipmentName.Size = new System.Drawing.Size(306, 33);
            this.txtEquipmentName.TabIndex = 1;
            this.txtEquipmentName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // lblName
            // 
            this.lblName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(15, 12);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(404, 28);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Equipo a prestar:";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.AutoScroll = true;
            this.splitContainer3.Panel2.Controls.Add(this.panel4);
            this.splitContainer3.Panel2.Controls.Add(this.panel9);
            this.splitContainer3.Panel2.Controls.Add(this.panel11);
            this.splitContainer3.Size = new System.Drawing.Size(435, 423);
            this.splitContainer3.SplitterDistance = 56;
            this.splitContainer3.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnCancel);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Controls.Add(this.btnAddLoan);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 222);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(435, 54);
            this.panel4.TabIndex = 5;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.btnCancel.Location = new System.Drawing.Point(54, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(151, 44);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "CANCELAR";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.btnClear.Location = new System.Drawing.Point(54, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(151, 44);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "LIMPIAR";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAddLoan
            // 
            this.btnAddLoan.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAddLoan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddLoan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAddLoan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddLoan.BorderRadius = 0;
            this.btnAddLoan.ButtonText = "Prestar equipo";
            this.btnAddLoan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddLoan.DisabledColor = System.Drawing.Color.Gray;
            this.btnAddLoan.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddLoan.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAddLoan.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAddLoan.Iconimage")));
            this.btnAddLoan.Iconimage_right = null;
            this.btnAddLoan.Iconimage_right_Selected = null;
            this.btnAddLoan.Iconimage_Selected = null;
            this.btnAddLoan.IconMarginLeft = 0;
            this.btnAddLoan.IconMarginRight = 0;
            this.btnAddLoan.IconRightVisible = false;
            this.btnAddLoan.IconRightZoom = 0D;
            this.btnAddLoan.IconVisible = false;
            this.btnAddLoan.IconZoom = 90D;
            this.btnAddLoan.IsTab = false;
            this.btnAddLoan.Location = new System.Drawing.Point(212, 4);
            this.btnAddLoan.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddLoan.Name = "btnAddLoan";
            this.btnAddLoan.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAddLoan.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnAddLoan.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAddLoan.selected = false;
            this.btnAddLoan.Size = new System.Drawing.Size(165, 44);
            this.btnAddLoan.TabIndex = 0;
            this.btnAddLoan.Text = "Prestar equipo";
            this.btnAddLoan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAddLoan.Textcolor = System.Drawing.Color.White;
            this.btnAddLoan.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddLoan.Click += new System.EventHandler(this.btnAddEquipment_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.label4);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 131);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel9.Size = new System.Drawing.Size(435, 91);
            this.panel9.TabIndex = 6;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btnChooseUser);
            this.panel10.Controls.Add(this.txtUserName);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(15, 40);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(405, 39);
            this.panel10.TabIndex = 2;
            // 
            // btnChooseUser
            // 
            this.btnChooseUser.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnChooseUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChooseUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnChooseUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnChooseUser.BorderRadius = 0;
            this.btnChooseUser.ButtonText = "Elegir";
            this.btnChooseUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChooseUser.DisabledColor = System.Drawing.Color.Gray;
            this.btnChooseUser.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChooseUser.Iconcolor = System.Drawing.Color.Transparent;
            this.btnChooseUser.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnChooseUser.Iconimage")));
            this.btnChooseUser.Iconimage_right = null;
            this.btnChooseUser.Iconimage_right_Selected = null;
            this.btnChooseUser.Iconimage_Selected = null;
            this.btnChooseUser.IconMarginLeft = 0;
            this.btnChooseUser.IconMarginRight = 0;
            this.btnChooseUser.IconRightVisible = false;
            this.btnChooseUser.IconRightZoom = 0D;
            this.btnChooseUser.IconVisible = false;
            this.btnChooseUser.IconZoom = 90D;
            this.btnChooseUser.IsTab = false;
            this.btnChooseUser.Location = new System.Drawing.Point(315, 5);
            this.btnChooseUser.Margin = new System.Windows.Forms.Padding(4);
            this.btnChooseUser.Name = "btnChooseUser";
            this.btnChooseUser.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnChooseUser.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnChooseUser.OnHoverTextColor = System.Drawing.Color.White;
            this.btnChooseUser.selected = false;
            this.btnChooseUser.Size = new System.Drawing.Size(87, 30);
            this.btnChooseUser.TabIndex = 3;
            this.btnChooseUser.Text = "Elegir";
            this.btnChooseUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnChooseUser.Textcolor = System.Drawing.Color.White;
            this.btnChooseUser.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChooseUser.Click += new System.EventHandler(this.btnChooseUser_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUserName.Enabled = false;
            this.txtUserName.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtUserName.HintForeColor = System.Drawing.Color.Gray;
            this.txtUserName.HintText = "Aún no ha seleccionado usuario";
            this.txtUserName.isPassword = false;
            this.txtUserName.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtUserName.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtUserName.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtUserName.LineThickness = 3;
            this.txtUserName.Location = new System.Drawing.Point(0, 0);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(306, 39);
            this.txtUserName.TabIndex = 1;
            this.txtUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(405, 28);
            this.label4.TabIndex = 1;
            this.label4.Text = "Usuario que presta:";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.splitContainer4);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(435, 131);
            this.panel11.TabIndex = 7;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.bunifuSeparator5);
            this.splitContainer4.Panel1.Controls.Add(this.panel12);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.panel14);
            this.splitContainer4.Size = new System.Drawing.Size(435, 131);
            this.splitContainer4.SplitterDistance = 215;
            this.splitContainer4.SplitterWidth = 1;
            this.splitContainer4.TabIndex = 0;
            // 
            // bunifuSeparator5
            // 
            this.bunifuSeparator5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator5.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuSeparator5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bunifuSeparator5.LineThickness = 1;
            this.bunifuSeparator5.Location = new System.Drawing.Point(210, 0);
            this.bunifuSeparator5.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator5.Name = "bunifuSeparator5";
            this.bunifuSeparator5.Size = new System.Drawing.Size(5, 131);
            this.bunifuSeparator5.TabIndex = 8;
            this.bunifuSeparator5.Transparency = 0;
            this.bunifuSeparator5.Vertical = true;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Controls.Add(this.label6);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel12.Name = "panel12";
            this.panel12.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel12.Size = new System.Drawing.Size(215, 131);
            this.panel12.TabIndex = 2;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.flowLayoutPanel3);
            this.panel13.Controls.Add(this.bunifuSeparator6);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(15, 40);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(185, 79);
            this.panel13.TabIndex = 2;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.nudQuantityEquipment);
            this.flowLayoutPanel3.Controls.Add(this.label3);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(185, 44);
            this.flowLayoutPanel3.TabIndex = 3;
            // 
            // nudQuantityEquipment
            // 
            this.nudQuantityEquipment.BackColor = System.Drawing.Color.White;
            this.nudQuantityEquipment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudQuantityEquipment.Dock = System.Windows.Forms.DockStyle.Left;
            this.nudQuantityEquipment.Enabled = false;
            this.nudQuantityEquipment.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudQuantityEquipment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.nudQuantityEquipment.Location = new System.Drawing.Point(3, 3);
            this.nudQuantityEquipment.Name = "nudQuantityEquipment";
            this.nudQuantityEquipment.Size = new System.Drawing.Size(110, 23);
            this.nudQuantityEquipment.TabIndex = 2;
            this.nudQuantityEquipment.ThousandsSeparator = true;
            this.nudQuantityEquipment.ValueChanged += new System.EventHandler(this.nudQuantityEquipment_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(119, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Unidades";
            // 
            // bunifuSeparator6
            // 
            this.bunifuSeparator6.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bunifuSeparator6.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.bunifuSeparator6.LineThickness = 3;
            this.bunifuSeparator6.Location = new System.Drawing.Point(0, 44);
            this.bunifuSeparator6.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator6.Name = "bunifuSeparator6";
            this.bunifuSeparator6.Size = new System.Drawing.Size(185, 35);
            this.bunifuSeparator6.TabIndex = 2;
            this.bunifuSeparator6.Transparency = 255;
            this.bunifuSeparator6.Vertical = false;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(185, 28);
            this.label6.TabIndex = 1;
            this.label6.Text = "Cantidad a prestar:";
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.panel18);
            this.panel14.Controls.Add(this.label8);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel14.Name = "panel14";
            this.panel14.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel14.Size = new System.Drawing.Size(219, 131);
            this.panel14.TabIndex = 3;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.lblRestEquipment);
            this.panel18.Controls.Add(this.bunifuSeparator7);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(15, 40);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(189, 79);
            this.panel18.TabIndex = 2;
            // 
            // lblRestEquipment
            // 
            this.lblRestEquipment.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblRestEquipment.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRestEquipment.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblRestEquipment.Location = new System.Drawing.Point(0, 16);
            this.lblRestEquipment.Name = "lblRestEquipment";
            this.lblRestEquipment.Size = new System.Drawing.Size(189, 28);
            this.lblRestEquipment.TabIndex = 4;
            this.lblRestEquipment.Text = "Seleccione un equipo antes";
            this.lblRestEquipment.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // bunifuSeparator7
            // 
            this.bunifuSeparator7.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bunifuSeparator7.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.bunifuSeparator7.LineThickness = 3;
            this.bunifuSeparator7.Location = new System.Drawing.Point(0, 44);
            this.bunifuSeparator7.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator7.Name = "bunifuSeparator7";
            this.bunifuSeparator7.Size = new System.Drawing.Size(189, 35);
            this.bunifuSeparator7.TabIndex = 3;
            this.bunifuSeparator7.Transparency = 255;
            this.bunifuSeparator7.Vertical = false;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(15, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(189, 28);
            this.label8.TabIndex = 1;
            this.label8.Text = "Restante total:";
            // 
            // ErrorName
            // 
            this.ErrorName.ContainerControl = this;
            // 
            // ErrorStock
            // 
            this.ErrorStock.ContainerControl = this;
            // 
            // ErrorType
            // 
            this.ErrorType.ContainerControl = this;
            // 
            // ErrorDescription
            // 
            this.ErrorDescription.ContainerControl = this;
            // 
            // ErrorDate
            // 
            this.ErrorDate.ContainerControl = this;
            // 
            // alert1
            // 
            this.alert1.AlertType = Cotton.Controls.AlertType.Danger;
            this.alert1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.alert1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.alert1.FontSizeBody = 9.75F;
            this.alert1.FontSizeTitle = 12F;
            this.alert1.Location = new System.Drawing.Point(502, 28);
            this.alert1.MaximumSize = new System.Drawing.Size(400, 1000);
            this.alert1.Message = "Todo bien todo correcto esta alerta está bien puesta.";
            this.alert1.Name = "alert1";
            this.alert1.Size = new System.Drawing.Size(400, 98);
            this.alert1.TabIndex = 5;
            this.alert1.TimeLapse = 3500;
            this.alert1.Title = "Si aparezco";
            this.alert1.Visible = false;
            // 
            // Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(910, 499);
            this.Controls.Add(this.alert1);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Add";
            this.Text = "Add";
            this.Load += new System.EventHandler(this.Add_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splContainer.Panel1.ResumeLayout(false);
            this.splContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splContainer)).EndInit();
            this.splContainer.ResumeLayout(false);
            this.leftForm.Panel1.ResumeLayout(false);
            this.leftForm.Panel1.PerformLayout();
            this.leftForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leftForm)).EndInit();
            this.leftForm.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinutes)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantityEquipment)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorDate)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private MaterialSkin.Controls.MaterialDivider materialDivider1;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.SplitContainer splContainer;
		private System.Windows.Forms.SplitContainer leftForm;
		private System.Windows.Forms.Label lblTitleForm;
		private System.Windows.Forms.SplitContainer splitContainer3;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button btnClear;
		private Bunifu.Framework.UI.BunifuFlatButton btnAddLoan;
		private System.Windows.Forms.Panel panel15;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator3;
		private System.Windows.Forms.Panel panel16;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator4;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Panel panel6;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
		private System.Windows.Forms.Label lblQuantity;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.Panel panel8;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator2;
		private System.Windows.Forms.Label lblDate;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ErrorProvider ErrorName;
        private System.Windows.Forms.ErrorProvider ErrorStock;
        private System.Windows.Forms.ErrorProvider ErrorType;
        private System.Windows.Forms.ErrorProvider ErrorDescription;
        private Controls.Alert alert1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ErrorProvider ErrorDate;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.NumericUpDown nudHours;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown nudMinutes;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lblFinalHour;
		private System.Windows.Forms.Panel panel17;
		private Bunifu.Framework.UI.BunifuFlatButton btnChooseEquipment;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtEquipmentName;
		private System.Windows.Forms.Panel panel9;
		private System.Windows.Forms.Panel panel10;
		private Bunifu.Framework.UI.BunifuFlatButton btnChooseUser;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtUserName;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel panel11;
		private System.Windows.Forms.SplitContainer splitContainer4;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator5;
		private System.Windows.Forms.Panel panel12;
		private System.Windows.Forms.Panel panel13;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
		private System.Windows.Forms.NumericUpDown nudQuantityEquipment;
		private System.Windows.Forms.Label label3;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator6;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Panel panel14;
		private System.Windows.Forms.Panel panel18;
		private System.Windows.Forms.Label lblRestEquipment;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator7;
		private System.Windows.Forms.Label label8;
	}
}