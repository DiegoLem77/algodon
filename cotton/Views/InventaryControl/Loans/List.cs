﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.General;
using Cotton.Models;
using Cotton.Controllers;
using Cotton.Controls;
using Cotton.PDF;

namespace Cotton.Views.InventaryControl.Loans
{
	public partial class List : Form
	{

		// Indica si el fomulario está listo para usarse
		private Task<Hashtable> filled;
		private CancellationTokenSource tokenSource;
		private equipment equipmentInEdition;
		// Carga fuente poppins
		private FontFamily Poppins, PoppinsL, PoppinsM = null;
		public List(bool updated = false)
		{
			InitializeComponent();
			Poppins = Utils.ImportFont(nameof(Properties.Resources.Poppins_Bold)).Families[0];
			PoppinsL = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
			PoppinsM = Utils.ImportFont(nameof(Properties.Resources.Montserrat_Medium)).Families[0];
			lblTitle.Font = new Font(Poppins, lblTitle.Font.Size);
			lblTableTitle.Font = new Font(PoppinsM, lblTableTitle.Font.Size);
			alert1.Font = new Font(PoppinsL, alert1.Font.Size);
			alert1.FontSizeTitle = 12f;
			alert1.FontSizeBody = 10f;
			grbControls.Font = new Font(PoppinsM, lblTableTitle.Font.Size);
			lblSortOption.Font = new Font(PoppinsM, lblSortOption.Font.Size);
        }

        private void fillDGV(Hashtable list)
        {
            ICollection keys = list.Keys;
			fadeOut.Hide(pnlLoading);
			foreach (string key in keys)
			{
				int n = dgvPrestamos.Rows.Add();
				loan _l = (loan)list[key];
				dgvPrestamos.Rows[n].Cells["nameEquipment"].Value = _l.equipments.name;
				dgvPrestamos.Rows[n].Cells["nameUser"].Value = _l.users.Name;
				dgvPrestamos.Rows[n].Cells["dateFrom"].Value = (_l.CreatedAt is DateTime dateFrom) ? dateFrom.ToString("HH:mm tt") : "-";
				dgvPrestamos.Rows[n].Cells["dateTo"].Value = (_l.devolution_date is DateTime dateTo) ? dateTo.ToString("HH:mm tt") : "-";
				dgvPrestamos.Rows[n].Cells["quantity"].Value = (_l.quantity is int quant) ? quant : 0;
				if(_l.returned == null || _l.returned == false)
				{
					var devolutionTime = (_l.devolution_date is DateTime toD) ? toD : DateTime.Now;
					if (DateTime.Compare(DateTime.Now,  devolutionTime)> 0)
					{
						dgvPrestamos.Rows[n].Cells["state"].Value = "Retrasado";
						dgvPrestamos.Rows[n].Cells["state"].Style.ForeColor = Color.DarkRed;

					}else
					{
						dgvPrestamos.Rows[n].Cells["state"].Value = "Pendiente";
						dgvPrestamos.Rows[n].Cells["state"].Style.ForeColor = Color.DarkOrange;
					}
				}else
				{
					dgvPrestamos.Rows[n].Cells["state"].Value = "Entregado";
					dgvPrestamos.Rows[n].Cells["state"].Style.ForeColor = Color.SeaGreen;
				}
				dgvPrestamos.Rows[n].Cells["id"].Value = _l.Id;
			}
		}

		private async void List_Load(object sender, EventArgs e)
		{
			tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;
			try
			{
				filled = Task.Run(() => LoanController.list(token), token);
				Hashtable types = await filled;
                fillDGV(types);
				foreach (DataGridViewColumn column in dgvPrestamos.Columns)
				{
					this.drpSortColumn.Items.Add(column.HeaderText);
				}
			}
			catch (OperationCanceledException)
			{

				filled.Dispose();
			}
		}

		private void List_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!filled.IsCompleted) tokenSource.Cancel();
			e.Cancel = false;

		}

		private async void btnSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            dgvPrestamos.Rows.Clear();
            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            try
            {
                filled = Task.Run(() => EquipmentTypeController.searchByName(token, btnSearch.Text.Trim()), token);
                Hashtable types = await filled;
                fillDGV(types);
            }
            catch (OperationCanceledException)
            {

                filled.Dispose();
            }
        }



		private void dgvEquipos_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if(!dgvPrestamos.Rows[dgvPrestamos.CurrentRow.Index].Cells["state"].Value.Equals("Entregado"))
			{
				btnConfirmDevolution.Enabled = true;
				btnReport.Enabled = true;
			}else
			{
				btnConfirmDevolution.Enabled = false;
				btnReport.Enabled = false;
			}
			//btnDelete.Enabled = true;
		}

		private void dgvEquipos_Sorted(object sender, EventArgs e)
		{
			drpSortColumn.SelectedIndex = dgvPrestamos.SortedColumn.Index;
			this.chkDesc.Checked = dgvPrestamos.SortOrder.Equals(SortOrder.Descending);
		}

		private void lblDesc_Click(object sender, EventArgs e)
		{
			this.chkDesc.Checked = !chkDesc.Checked;
		}

		private async void btnReport_Click(object sender, EventArgs e)
		{
			DialogResult result = new DialogResult();
			tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;
			try
			{
				loan _loanReturn = await LoanController.find(token, dgvPrestamos.Rows[dgvPrestamos.CurrentRow.Index].Cells["id"].Value.ToString());
				if(_loanReturn.returned != null && _loanReturn.returned == false)
				{

					var availableStockOriginal = _loanReturn.equipments.available_stock;
					equipmentInEdition = _loanReturn.equipments;
					int quantity = (int)dgvPrestamos.Rows[dgvPrestamos.CurrentRow.Index].Cells["quantity"].Value;
					var FrmReport = new ReportLoans(quantity);
					FrmReport.ResponseReport += DescontReportResult;
					result = FrmReport.ShowDialog();
					if(result == DialogResult.OK)
					{
						int? returned = availableStockOriginal - equipmentInEdition.available_stock;
						equipmentInEdition.available_stock += quantity;
						equipmentInEdition.Update(equipmentInEdition.Id);
						_loanReturn.returned = true;
						_loanReturn.Update(_loanReturn.Id);
						dgvPrestamos.Rows[dgvPrestamos.CurrentRow.Index].Cells["state"].Value = "Entregado";
						dgvPrestamos.Rows[dgvPrestamos.CurrentRow.Index].Cells["state"].Style.ForeColor = Color.SeaGreen;
						alert1.changeAlertType(AlertType.Success);
						alert1.Title = "Operación realizada!";
						alert1.Message = String.Format("Se ha regrasado una cantidad de {0} {1} y reportado {2}", returned, equipmentInEdition.name, quantity - returned);
						alert1.Show();
					}
				}else
				{
					btnConfirmDevolution.Enabled = false;
					btnReport.Enabled = false;
				}

			}
			catch (Exception)
			{

				throw;
			}
		}

		private void DescontReportResult(int quantity)
		{
			if(equipmentInEdition != null)
			{
				equipmentInEdition.stock -= quantity;
				equipmentInEdition.available_stock -= quantity;
			}
		}

		private async void btnConfirmDevolution_Click(object sender, EventArgs e)
		{
			tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;
			try
			{
				loan _loanReturn = await LoanController.find(token, dgvPrestamos.Rows[dgvPrestamos.CurrentRow.Index].Cells["id"].Value.ToString());
				equipmentInEdition = _loanReturn.equipments;
				if (_loanReturn.returned == null || _loanReturn.returned == false)
				{

					int quantity = (int)dgvPrestamos.Rows[dgvPrestamos.CurrentRow.Index].Cells["quantity"].Value;
					equipmentInEdition.available_stock += quantity;
					equipmentInEdition.Update(equipmentInEdition.Id);
					_loanReturn.returned = true;
					_loanReturn.Update(_loanReturn.Id);
					dgvPrestamos.Rows[dgvPrestamos.CurrentRow.Index].Cells["state"].Value = "Entregado";
					dgvPrestamos.Rows[dgvPrestamos.CurrentRow.Index].Cells["state"].Style.ForeColor = Color.SeaGreen;
					alert1.changeAlertType(AlertType.Success);
					alert1.Title = "Operación realizada!";
					alert1.Message = String.Format("Se ha regresado el equipo correctamente!");
					alert1.Show();
				}
				else
				{
					btnConfirmDevolution.Enabled = false;
					btnReport.Enabled = false;
				}

			}
			catch (Exception)
			{

				throw;
			}
		}

        private void btnDialogFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                InventoryPDF.generateLoanPDF(fbd.SelectedPath);
                alert1.changeAlertType(AlertType.Success);
                alert1.Message = String.Format("Se ha generado el PDF correctamente!!!");
                alert1.Title = "Equipos";
                alert1.Start();
            }
        }

        private void chkDesc_OnChange(object sender, EventArgs e)
		{
			dgvPrestamos.Sort(dgvPrestamos.Columns[drpSortColumn.SelectedIndex],
				(chkDesc.Checked) ? ListSortDirection.Descending : ListSortDirection.Ascending);
		}


		private void btnUpdate_Click(object sender, EventArgs e)
        {
            var parentForm = (IParentForm)this.Parent.FindForm();
            parentForm.openSecondFormInMultiTask(new Categories.Add(dgvPrestamos.Rows[dgvPrestamos.CurrentRow.Index].Cells["id"].Value.ToString()));
        }
	}
}
