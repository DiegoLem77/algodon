﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using Cotton.Controllers;
using System.Threading;
using Cotton.Models;
using Cotton.General;
using Cotton.Controls;
using System.Drawing;
using Users = Cotton.Views.Assistance.Users;

namespace Cotton.Views.InventaryControl.Loans
{
	public partial class Add : Form
	{
        // Indica si el fomulario está listo para usarse
        private Task<Hashtable> filled, filled2;
        private CancellationTokenSource tokenSource;
        private List<string> categoryId;
		private FontFamily PoppinsL = null;
        private string edit = null;
		private equipment equipmentToLoan;
		private User userAtLoan;
		private DateTime LimitHourToDay = DateTime.Now;
		private DateTime devolutionTime;
        public Add(string idEdit = null)
		{
			PoppinsL = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
            categoryId = new List<string>();
			InitializeComponent();
			alert1.Font = new Font(PoppinsL, alert1.Font.Size);
			alert1.FontSizeTitle = 12f;
			alert1.FontSizeBody = 10f;

            if (idEdit == null)
            {
                btnAddLoan.Enabled = true;
                btnAddLoan.Show();
                //btnUpdate.Enabled = false;
                //btnUpdate.Hide();
                btnClear.Enabled = true;
                btnClear.Show();
                btnCancel.Enabled = false;
                btnCancel.Hide();
                lblTitle.Text = "Añadir nuevo equipo";
            }
            else
            {
                btnAddLoan.Enabled = false;
                btnAddLoan.Hide();
                //btnUpdate.Enabled = true;
                //btnUpdate.Show();
                btnClear.Enabled = false;
                btnClear.Hide();
                btnCancel.Enabled = true;
                btnCancel.Show();
                lblTitle.Text = "Editar equipo (" + idEdit + ")";
                edit = idEdit;
            }
        }

        private void Add_Load(object sender, EventArgs e)
        {
			calculateMaxTime();

		}

		private void calculateMaxTime()
		{
			LimitHourToDay = DateTime.Parse(DateTime.Now.Date.ToString("d") + " 18:59:59");
			var hours = (LimitHourToDay - DateTime.Now).TotalHours;
			nudHours.Maximum = Math.Floor(Convert.ToDecimal(hours));
			var minutes = (LimitHourToDay - DateTime.Now).TotalMinutes;
			nudMinutes.Maximum = Math.Ceiling((Convert.ToDecimal(minutes) - Convert.ToDecimal(nudHours.Maximum * 60)));
		}

        public bool validations()
        {
            bool validation = true;
            if (Validations.isNotEmpty(txtEquipmentName.Text))
            {
                ErrorName.SetError(txtEquipmentName, "Este campo es obligatorio");
                lblName.ForeColor = Color.FromArgb(247, 93, 94);
                validation = false;
            }
            else
            {
                ErrorName.Clear();
                lblName.ForeColor = Color.FromName("ControlText");
            }
            if (!Validations.isGreatherThanOrEqual(nudHours.Value, 0))
            {
                ErrorStock.SetError(nudHours, "Este campo debe ser mayor a cero");
                lblQuantity.ForeColor = Color.FromArgb(247, 93, 94);
                validation = false;
            }
            else
            {
                ErrorStock.Clear();
                lblQuantity.ForeColor = Color.FromName("ControlText");
            }

            //if (!Validations.isBeforeOrEqualToNow(dtpAdquisition.Value))
            //{
            //    ErrorDate.SetError(dtpAdquisition, "Este campo no debe ser mayor a la fecha actual");
            //    lblDate.ForeColor = Color.FromArgb(247, 93, 94);
            //    validation = false;
            //}
            //else
            //{
            //    ErrorDate.Clear();
            //    lblDate.ForeColor = Color.FromName("ControlText");
            //}

            //if (drdTipo.selectedIndex == -1)
            //{
            //    ErrorType.SetError(drdTipo, "Debe seleccionar un tipo");
            //    lblType.ForeColor = Color.FromArgb(247, 93, 94);
            //    validation = false;
            //}
            //else
            //{
            //    ErrorType.Clear();
            //    lblType.ForeColor = Color.FromName("ControlText");
            //}
            //if (Validations.isNotEmpty(txtDescription.Text))
            //{
            //    ErrorDescription.SetError(txtDescription, "Este campo es obligatorio");
            //    lblDescription.ForeColor = Color.FromArgb(247, 93, 94);
            //    validation = false;
            //}
            //else
            //{
            //    ErrorDescription.Clear();
            //    lblDescription.ForeColor = Color.FromName("ControlText");
            //}
            return validation;
        }

        private void btnAddEquipment_Click(object sender, EventArgs e)
        {
            if (!validations())
                return;

            loan _l = new loan();
			_l.equipment_id = equipmentToLoan.Id;
			_l.user_id = userAtLoan.Id;
			_l.devolution_date = devolutionTime;
			_l.quantity = Convert.ToInt32(nudQuantityEquipment.Value);
			_l.returned = false;
			_l.Insert();
            Clean();
			equipmentToLoan.available_stock -= _l.quantity;
			equipmentToLoan.Update(equipmentToLoan.Id);

            alert1.changeAlertType(AlertType.Success);
            alert1.Message = String.Format("El equipo {0} se ha prestado a {1}", equipmentToLoan.name, userAtLoan.Name);
            alert1.Title = "Equipo prestado";
            alert1.Start();
			calculateMaxTime();
            //MessageBox.Show("Equipo agregado correctamente");

        }

        private void Clean()
        {
			txtEquipmentName.Text = "";
			ChangeButtonStyle(btnChooseEquipment, true);
			ChangeButtonStyle(btnChooseUser, true);
			txtUserName.Text = "";
			nudHours.Value = 0;
			nudMinutes.Value = 0;
			lblFinalHour.Text = "Aún no ha seleccionado hora";
			nudQuantityEquipment.Enabled = false;
			nudQuantityEquipment.Minimum = 0;
			nudQuantityEquipment.Value = 0;
			this.lblRestEquipment.Text = "Seleccione un equipo antes";
		}

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clean();
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!validations())
                return;


            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            try
            {
                filled = Task.Run(() => EquipmentController.find(token, edit), token);
                Hashtable type = await filled;
                equipment equip = (equipment)type[edit];
                equip.name = txtEquipmentName.Text;
                int diff = int.Parse(nudHours.Value.ToString()) - (int)equip.stock;
                equip.stock = int.Parse(nudHours.Value.ToString());
                if (((int)equip.available_stock + diff) >= 0)
                    equip.available_stock = (int)equip.available_stock + diff;
                else
                    equip.available_stock = 0;
                //equip.acquisition_date = dtpAdquisition.Value;
                //equip.type_id = categoryId[drdTipo.selectedIndex];
                //equip.description = txtDescription.Text;
                equip.ubication_id = "0e3d17a0-8r57-4f4c-ab03-da0r794yc412";
                equip.Update(edit);
                var parentForm = (IParentForm)this.Parent.FindForm();
                parentForm.openSecondFormInMultiTask(new Equipments.List(1));
            }
            catch (OperationCanceledException)
            {

                filled.Dispose();
            }
        }

		private void btnChooseEquipment_Click(object sender, EventArgs e)
		{

			bool isActive = (bool)((btnChooseEquipment.Tag != null) ? btnChooseEquipment.Tag : true );
			if (isActive )
			{
				var FrmListEquipment = new Equipments.List(0, true);
				FrmListEquipment.ResponseEquipment += GetEquipment;
				FrmListEquipment.ShowDialog();
			}else
			{
				equipmentToLoan = null;
				ChangeButtonStyle(btnChooseEquipment, true);
				txtEquipmentName.Text = "";
				txtEquipmentName.HintText = txtEquipmentName.HintText;
				nudQuantityEquipment.Enabled = false;
				nudQuantityEquipment.Minimum = 0;
				nudQuantityEquipment.Value = 0;
				this.lblRestEquipment.Text = "Seleccione un equipo antes";
			}
		}

		private void GetEquipment(equipment e)
		{
			equipmentToLoan = e;
			this.txtEquipmentName.Text = e.name;
			txtEquipmentName.ForeColor = Color.DarkGray;
			this.ChangeButtonStyle(btnChooseEquipment, false);
			nudQuantityEquipment.Enabled = true;
			nudQuantityEquipment.Minimum = 1;
			nudQuantityEquipment.Maximum = Convert.ToDecimal((equipmentToLoan.available_stock != null) ? equipmentToLoan.available_stock : 1);
			lblRestEquipment.Text = equipmentToLoan.available_stock.ToString() + " unidades restantes";
		}
		private void ChangeButtonStyle(Bunifu.Framework.UI.BunifuFlatButton button, bool activate)
		{
			button.Tag = activate;
			if (activate)
			{
				button.BackColor = Color.SeaGreen;
				button.OnHovercolor = button.BackColor;
				button.Normalcolor = button.BackColor;
				button.Text = "Elegir";
			}else
			{
				button.BackColor = Color.FromArgb(236, 148, 84);
				button.OnHovercolor = button.BackColor;
				button.Normalcolor = button.BackColor;
				button.Text = "Limpiar";
			}
		}

		private void nudMinutes_ValueChanged(object sender, EventArgs e)
		{
			var finishDateTime = DateTime.Now.AddHours(Convert.ToDouble(nudHours.Value));
			finishDateTime = finishDateTime.AddMinutes(Convert.ToDouble(nudMinutes.Value));
			lblFinalHour.Text = finishDateTime.ToString("HH:mm tt");
			devolutionTime = finishDateTime;
		}

		private void btnChooseUser_Click(object sender, EventArgs e)
		{
			bool isActive = (bool)((btnChooseUser.Tag != null) ? btnChooseUser.Tag : true);
			if (isActive)
			{
				var FrmListUser = new Users.List(0, true);
				FrmListUser.ResponseUser += GetUser;
				FrmListUser.ShowDialog();
			}
			else
			{
				equipmentToLoan = null;
				ChangeButtonStyle(btnChooseEquipment, true);
				txtEquipmentName.Text = "";
				txtEquipmentName.HintText = txtEquipmentName.HintText;
			}
		}

		private void GetUser(User user)
		{
			userAtLoan = user;
			this.txtUserName.Text = user.Name;
			txtEquipmentName.ForeColor = Color.DarkGray;
			this.ChangeButtonStyle(btnChooseUser, false);
		}

		private void nudQuantityEquipment_ValueChanged(object sender, EventArgs e)
		{
			lblRestEquipment.Text = (equipmentToLoan.available_stock - nudQuantityEquipment.Value).ToString() + " Unidades restantes";
		}

		private void btnCancel_Click(object sender, EventArgs e)
        {
            var parentForm = (IParentForm)this.Parent.FindForm();
            parentForm.openSecondFormInMultiTask(new Equipments.List());
        }
    }
}
