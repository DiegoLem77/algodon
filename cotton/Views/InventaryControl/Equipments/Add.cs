﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using Cotton.Controllers;
using System.Threading;
using Cotton.Models;
using Cotton.General;
using Cotton.Controls;
using System.Drawing;

namespace Cotton.Views.InventaryControl.Equipments
{
	public partial class Add : Form
	{
        // Indica si el fomulario está listo para usarse
        private Task<Hashtable> filled, filled2, filled3, filled4;
        private CancellationTokenSource tokenSource;
        private List<string> categoryId, ubicationId;
		private FontFamily PoppinsL = null;
        private string edit = null;

        public Add(string idEdit = null)
		{
			PoppinsL = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
            categoryId = new List<string>();
            ubicationId = new List<string>();
            InitializeComponent();
			alert1.Font = new Font(PoppinsL, alert1.Font.Size);
			alert1.FontSizeTitle = 12f;
			alert1.FontSizeBody = 10f;

            if (idEdit == null)
            {
                btnAddEquipment.Enabled = true;
                btnAddEquipment.Show();
                btnUpdate.Enabled = false;
                btnUpdate.Hide();
                btnClear.Enabled = true;
                btnClear.Show();
                btnCancel.Enabled = false;
                btnCancel.Hide();
                lblTitle.Text = "Añadir nuevo equipo";
            }
            else
            {
                btnAddEquipment.Enabled = false;
                btnAddEquipment.Hide();
                btnUpdate.Enabled = true;
                btnUpdate.Show();
                btnClear.Enabled = false;
                btnClear.Hide();
                btnCancel.Enabled = true;
                btnCancel.Show();
                lblTitle.Text = "Editar equipo (" + idEdit + ")";
                edit = idEdit;
            }
        }

        private async void Add_Load(object sender, EventArgs e)
        {
            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            try
            {
                Hashtable equip = null;
                equipment equipment = null;
                if (edit != null)
                {
                    filled2 = Task.Run(() => EquipmentController.find(token, edit), token);
                    equip = await filled2;
                    equipment = (equipment)equip[edit];
                }
                filled = Task.Run(() => EquipmentTypeController.list(token), token);
                Hashtable types = await filled;
                ICollection keys = types.Keys;

                filled = Task.Run(() => UbicationController.list(token), token);
                Hashtable ubications = await filled;
                ICollection keysU = ubications.Keys;

                int i = 0;
                foreach (string key in keys)
                {
                    Category category = (Category)types[key];
                    drdTipo.AddItem(category.Name);
                    categoryId.Add(category.Id);
                    if (edit != null)
                        if (equipment.categories.Id == category.Id)
                            drdTipo.selectedIndex = i;
                    i++;
                }

                i = 0;
                foreach (string key in keysU)
                {
                    ubications ubic = (ubications)ubications[key];
                    drdUbication.AddItem(ubic.ubication);
                    ubicationId.Add(ubic.Id);
                    if (edit != null)
                        if (equipment.ubication_id == ubic.Id)
                            drdUbication.selectedIndex = i;
                    i++;
                }

                if (edit != null)
                {
                    equipment = (equipment)equip[edit];
                    txtName.Text = equipment.name;
                    txtDescription.Text = equipment.description;
                    nudStock.Value = (decimal)equipment.stock;
                    dtpAdquisition.Value = (DateTime)equipment.acquisition_date;
                }
            }
            catch (OperationCanceledException)
            {

                filled.Dispose();
            }
        }

        public bool validations()
        {
            bool validation = true;
            if (txtName.Text.Trim() == String.Empty)
            {
                ErrorName.SetError(txtName, "Este campo es obligatorio");
                lblName.ForeColor = Color.FromArgb(247, 93, 94);
                validation = false;
            }
            else
            {
                ErrorName.Clear();
                lblName.ForeColor = Color.FromName("ControlText");
            }
            if (!Validations.isGreatherThanOrEqual(nudStock.Value, 0))
            {
                ErrorStock.SetError(nudStock, "Este campo debe ser mayor a cero");
                lblQuantity.ForeColor = Color.FromArgb(247, 93, 94);
                validation = false;
            }
            else
            {
                ErrorStock.Clear();
                lblQuantity.ForeColor = Color.FromName("ControlText");
            }

            if (!Validations.isBeforeOrEqualToNow(dtpAdquisition.Value))
            {
                ErrorDate.SetError(dtpAdquisition, "Este campo no debe ser mayor a la fecha actual");
                lblDate.ForeColor = Color.FromArgb(247, 93, 94);
                validation = false;
            }
            else
            {
                ErrorDate.Clear();
                lblDate.ForeColor = Color.FromName("ControlText");
            }

            if (drdTipo.selectedIndex == -1)
            {
                ErrorType.SetError(drdTipo, "Debe seleccionar un tipo");
                lblType.ForeColor = Color.FromArgb(247, 93, 94);
                validation = false;
            }
            else
            {
                ErrorType.Clear();
                lblType.ForeColor = Color.FromName("ControlText");
            }
            if (txtDescription.Text.Trim() == String.Empty)
            {
                ErrorDescription.SetError(txtDescription, "Este campo es obligatorio");
                lblDescription.ForeColor = Color.FromArgb(247, 93, 94);
                validation = false;
            }
            else
            {
                ErrorDescription.Clear();
                lblDescription.ForeColor = Color.FromName("ControlText");
            }
            return validation;
        }

        private void btnAddEquipment_Click(object sender, EventArgs e)
        {
            if (!validations())
                return;

            equipment equip = new equipment();
            equip.name = txtName.Text;
            equip.stock = int.Parse(nudStock.Value.ToString());
            equip.available_stock = int.Parse(nudStock.Value.ToString());
            equip.acquisition_date = dtpAdquisition.Value;
            equip.type_id = categoryId[drdTipo.selectedIndex];
            equip.description = txtDescription.Text;
            equip.ubication_id = (drdUbication.selectedIndex != -1) ? ubicationId[drdUbication.selectedIndex] : null;
            equip.Insert();
            Clean();
            alert1.changeAlertType(AlertType.Success);
            alert1.Title = "Equipos";
            alert1.Message = "Equipo agregado correctamente!!!";
            alert1.Show();

        }

        private void Clean()
        {
            txtName.Text = "";
            txtDescription.Text = "";
            nudStock.Value = 0;
            dtpAdquisition.Value = DateTime.Now;
            drdTipo.selectedIndex = -1;
            ErrorType.Clear();
            ErrorName.Clear();
            ErrorStock.Clear();
            ErrorDescription.Clear();
            ErrorDate.Clear();
            lblName.ForeColor = Color.FromName("ControlText");
            lblQuantity.ForeColor = Color.FromName("ControlText");
            lblType.ForeColor = Color.FromName("ControlText");
            lblDescription.ForeColor = Color.FromName("ControlText");
            lblDate.ForeColor = Color.FromName("ControlText");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clean();
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!validations())
                return;


            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            try
            {
                filled = Task.Run(() => EquipmentController.find(token, edit), token);
                Hashtable type = await filled;
                equipment equip = (equipment)type[edit];
                equip.name = txtName.Text;
                int diff = int.Parse(nudStock.Value.ToString()) - (int)equip.stock;
                equip.stock = int.Parse(nudStock.Value.ToString());
                if (((int)equip.available_stock + diff) >= 0)
                    equip.available_stock = (int)equip.available_stock + diff;
                else
                    equip.available_stock = 0;
                equip.acquisition_date = dtpAdquisition.Value;
                equip.type_id = categoryId[drdTipo.selectedIndex];
                equip.description = txtDescription.Text;
                equip.ubication_id = (drdUbication.selectedIndex != -1)?ubicationId[drdUbication.selectedIndex]:null;
                equip.Update(edit);
                var parentForm = (IParentForm)this.Parent.FindForm();
                parentForm.openSecondFormInMultiTask(new Equipments.List(1));
            }
            catch (OperationCanceledException)
            {

                filled.Dispose();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            var parentForm = (IParentForm)this.Parent.FindForm();
            parentForm.openSecondFormInMultiTask(new Equipments.List());
        }
    }
}
