﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using Cotton.Controllers;
using Cotton.Models;
using Cotton.General;
using Cotton.Controls;
using Cotton.PDF;

namespace Cotton.Views.InventaryControl.Equipments
{
	// public enum EquipmentEventResponse { Equipment }
	public partial class List : Form
	{
        // Indica si el fomulario está listo para usarse
        private Task<Hashtable> filled;
		private FontFamily PoppinsM = null;
        private CancellationTokenSource tokenSource;
		private bool filterByAvailableStock = false;
		public delegate void ResponseEvent(equipment EquipmentResponse);
		public event ResponseEvent ResponseEquipment;

        public List(int action = 0, bool isForAChoice = false)
		{
			InitializeComponent();
            PoppinsM = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
			lblSortOption.Font = new Font(PoppinsM, lblSortOption.Font.Size);
			drpSortColumn.Font = new Font(PoppinsM, drpSortColumn.Font.Size);
            if (action == 1)
            {
                alert1.changeAlertType(AlertType.Success);
                alert1.Message = String.Format("Se ha actualizado el equipo correctamente!!!");
                alert1.Title = "Equipos";
                alert1.Start();
            }
            else if (action == 2)
            {
                alert1.changeAlertType(AlertType.Success);
                alert1.Message = String.Format("Se ha eliminado el equipo correctamente!!!");
                alert1.Title = "Equipos";
                alert1.Start();
            }else if (isForAChoice)
			{
				this.lblTableTitle.Text = "Elija un equipo:";
				btnDelete.Visible = false;
				btnUpdate.Visible = false;
				btnSelect.Visible = true;
				btnCancel.Visible = true;
                btnDialogFolder.Hide();
				filterByAvailableStock = isForAChoice;
			}
        }

        private void fillDGV(Hashtable equipments)
        {
            ICollection keys = equipments.Keys;
            fadeOut.Hide(pnlLoading);
            foreach (string key in keys)
            {
                int n = dgvEquipos.Rows.Add();
                equipment equip = (equipment)equipments[key];
                dgvEquipos.Rows[n].Cells["name"].Value = equip.name;
                dgvEquipos.Rows[n].Cells["quantity"].Value = equip.stock;
                dgvEquipos.Rows[n].Cells["available"].Value = equip.available_stock;
                dgvEquipos.Rows[n].Cells["acquisition"].Value = equip.acquisition_date;
                dgvEquipos.Rows[n].Cells["type"].Value = equip.categories.Name;
                dgvEquipos.Rows[n].Cells["id"].Value = equip.Id;
            }
        }

        private async void List_Load(object sender, EventArgs e)
        {
            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            try
            {
				if(filterByAvailableStock) filled = Task.Run(() => EquipmentController.GetAvailableEquipments(token), token);
				else filled = Task.Run(() => EquipmentController.list(token), token);
                Hashtable equipments = await filled;
                fillDGV(equipments);
				foreach (DataGridViewColumn column in dgvEquipos.Columns)
				{
					this.drpSortColumn.Items.Add(column.HeaderText);
				}
			}
            catch (OperationCanceledException)
            {

                filled.Dispose();
            }
        }

        private void List_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!filled.IsCompleted) tokenSource.Cancel();
            e.Cancel = false;
        }

        private async void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            dgvEquipos.Rows.Clear();
            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            try
            {
                filled = Task.Run(() => EquipmentController.searchByNameCategory(token, txtSearch.Text), token);
                Hashtable equipments = await filled;
                fillDGV(equipments);
            }
            catch (OperationCanceledException)
            {

                filled.Dispose();
            }
        }

        private void dgvEquipos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
			btnSelect.Enabled = true;
        }

		private void dgvEquipos_Sorted(object sender, EventArgs e)
		{
			drpSortColumn.SelectedIndex = dgvEquipos.SortedColumn.Index;
			this.chkDesc.Checked = dgvEquipos.SortOrder.Equals(SortOrder.Descending);
		}

		private void lblDesc_Click(object sender, EventArgs e)
		{
			this.chkDesc.Checked = !chkDesc.Checked;
		}
		private void chkDesc_OnChange(object sender, EventArgs e)
		{
            if (drpSortColumn.SelectedIndex > -1)
            {
                dgvEquipos.Sort(dgvEquipos.Columns[drpSortColumn.SelectedIndex],
                (chkDesc.Checked) ? ListSortDirection.Descending : ListSortDirection.Ascending);
            }
		}

		private void btnUpdate_Click(object sender, EventArgs e)
        {
            var parentForm = (IParentForm)this.Parent.FindForm();
            parentForm.openSecondFormInMultiTask(new Equipments.Add(dgvEquipos.Rows[dgvEquipos.CurrentRow.Index].Cells["id"].Value.ToString()));
        }

        private async void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult result = new DialogResult();
            Form deleteModal = new DeleteModal("¿Desea eliminar el equipo?");
            result = deleteModal.ShowDialog();

            if (result == DialogResult.OK)
            {
                tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                try
                {
                    filled = Task.Run(() => EquipmentController.find(token, dgvEquipos.Rows[dgvEquipos.CurrentRow.Index].Cells["id"].Value.ToString()), token);
                    Hashtable equipment = await filled;
                    equipment equip = (equipment)equipment[dgvEquipos.Rows[dgvEquipos.CurrentRow.Index].Cells["id"].Value.ToString()];
                    if (equip.Delete())
                    {
                        var parentForm = (IParentForm)this.Parent.FindForm();
                        parentForm.openSecondFormInMultiTask(new Equipments.List(2));
                    }
                    else
                    {
                        alert1.changeAlertType(AlertType.Danger);
                        alert1.Message = String.Format("No se pudo eliminar el equipo!!!");
                        alert1.Title = "Equipos";
                        alert1.Start();
                    }
                }
                catch (OperationCanceledException)
                {
                    filled.Dispose();
                }
            }
        }

		private async void btnSelect_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
			tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;
			try
			{
				filled = Task.Run(() => EquipmentController.find(token, dgvEquipos.Rows[dgvEquipos.CurrentRow.Index].Cells["id"].Value.ToString()), token);
				Hashtable equipments = await filled;
				equipment eq = (equipment)equipments[dgvEquipos.Rows[dgvEquipos.CurrentRow.Index].Cells["id"].Value.ToString()];
				ResponseEquipment?.Invoke(eq);
				this.Close();
			}
			catch (OperationCanceledException)
			{
				filled.Dispose();
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}

        private void btnDialogFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                InventoryPDF.generateEquipmentPDF(fbd.SelectedPath);
                alert1.changeAlertType(AlertType.Success);
                alert1.Message = String.Format("Se ha generado el PDF correctamente!!!");
                alert1.Title = "Equipos";
                alert1.Start();
            }
        }
    }
}
