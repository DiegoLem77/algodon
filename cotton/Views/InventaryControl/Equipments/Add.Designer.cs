﻿namespace Cotton.Views.InventaryControl.Equipments
{
	partial class Add
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splContainer = new System.Windows.Forms.SplitContainer();
            this.leftForm = new System.Windows.Forms.SplitContainer();
            this.lblTitleForm = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.bunifuSeparator3 = new Bunifu.Framework.UI.BunifuSeparator();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.bunifuSeparator4 = new Bunifu.Framework.UI.BunifuSeparator();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.nudStock = new System.Windows.Forms.NumericUpDown();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.dtpAdquisition = new Bunifu.Framework.UI.BunifuDatepicker();
            this.bunifuSeparator2 = new Bunifu.Framework.UI.BunifuSeparator();
            this.lblDate = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtName = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lblName = new System.Windows.Forms.Label();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnUpdate = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAddEquipment = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtDescription = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.bunifuSeparator5 = new Bunifu.Framework.UI.BunifuSeparator();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.drdUbication = new Bunifu.Framework.UI.BunifuDropdown();
            this.label5 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.drdTipo = new Bunifu.Framework.UI.BunifuDropdown();
            this.lblType = new System.Windows.Forms.Label();
            this.ErrorName = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorStock = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorType = new System.Windows.Forms.ErrorProvider(this.components);
            this.ErrorDescription = new System.Windows.Forms.ErrorProvider(this.components);
            this.alert1 = new Cotton.Controls.Alert();
            this.ErrorDate = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splContainer)).BeginInit();
            this.splContainer.Panel1.SuspendLayout();
            this.splContainer.Panel2.SuspendLayout();
            this.splContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftForm)).BeginInit();
            this.leftForm.Panel1.SuspendLayout();
            this.leftForm.Panel2.SuspendLayout();
            this.leftForm.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudStock)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorDate)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.splitContainer1.Size = new System.Drawing.Size(910, 499);
            this.splitContainer1.SplitterDistance = 72;
            this.splitContainer1.TabIndex = 4;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.materialDivider1);
            this.flowLayoutPanel1.Controls.Add(this.lblTitle);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(20, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(890, 72);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.materialDivider1.Location = new System.Drawing.Point(10, 67);
            this.materialDivider1.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(131, 2);
            this.materialDivider1.TabIndex = 2;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.lblTitle.Location = new System.Drawing.Point(3, 39);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(210, 25);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Añadir nuevo equipo";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splContainer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(20, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(870, 423);
            this.panel1.TabIndex = 4;
            // 
            // splContainer
            // 
            this.splContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splContainer.IsSplitterFixed = true;
            this.splContainer.Location = new System.Drawing.Point(0, 0);
            this.splContainer.Name = "splContainer";
            // 
            // splContainer.Panel1
            // 
            this.splContainer.Panel1.Controls.Add(this.leftForm);
            // 
            // splContainer.Panel2
            // 
            this.splContainer.Panel2.Controls.Add(this.splitContainer3);
            this.splContainer.Size = new System.Drawing.Size(870, 423);
            this.splContainer.SplitterDistance = 434;
            this.splContainer.SplitterWidth = 1;
            this.splContainer.TabIndex = 4;
            // 
            // leftForm
            // 
            this.leftForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftForm.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.leftForm.IsSplitterFixed = true;
            this.leftForm.Location = new System.Drawing.Point(0, 0);
            this.leftForm.Name = "leftForm";
            this.leftForm.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // leftForm.Panel1
            // 
            this.leftForm.Panel1.Controls.Add(this.lblTitleForm);
            // 
            // leftForm.Panel2
            // 
            this.leftForm.Panel2.AutoScroll = true;
            this.leftForm.Panel2.Controls.Add(this.panel15);
            this.leftForm.Size = new System.Drawing.Size(434, 423);
            this.leftForm.SplitterDistance = 56;
            this.leftForm.TabIndex = 0;
            // 
            // lblTitleForm
            // 
            this.lblTitleForm.AutoSize = true;
            this.lblTitleForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleForm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.lblTitleForm.Location = new System.Drawing.Point(6, 30);
            this.lblTitleForm.Name = "lblTitleForm";
            this.lblTitleForm.Size = new System.Drawing.Size(241, 20);
            this.lblTitleForm.TabIndex = 4;
            this.lblTitleForm.Text = "Complete los siguientes campos:";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.bunifuSeparator3);
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(434, 264);
            this.panel15.TabIndex = 4;
            // 
            // bunifuSeparator3
            // 
            this.bunifuSeparator3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator3.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuSeparator3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator3.LineThickness = 1;
            this.bunifuSeparator3.Location = new System.Drawing.Point(429, 0);
            this.bunifuSeparator3.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator3.Name = "bunifuSeparator3";
            this.bunifuSeparator3.Size = new System.Drawing.Size(5, 264);
            this.bunifuSeparator3.TabIndex = 9;
            this.bunifuSeparator3.Transparency = 255;
            this.bunifuSeparator3.Vertical = true;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.panel3);
            this.panel16.Controls.Add(this.panel2);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(434, 264);
            this.panel16.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splitContainer2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 91);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(434, 131);
            this.panel3.TabIndex = 5;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.bunifuSeparator4);
            this.splitContainer2.Panel1.Controls.Add(this.panel5);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.panel7);
            this.splitContainer2.Size = new System.Drawing.Size(434, 131);
            this.splitContainer2.SplitterDistance = 215;
            this.splitContainer2.SplitterWidth = 1;
            this.splitContainer2.TabIndex = 0;
            // 
            // bunifuSeparator4
            // 
            this.bunifuSeparator4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator4.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuSeparator4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bunifuSeparator4.LineThickness = 1;
            this.bunifuSeparator4.Location = new System.Drawing.Point(210, 0);
            this.bunifuSeparator4.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator4.Name = "bunifuSeparator4";
            this.bunifuSeparator4.Size = new System.Drawing.Size(5, 131);
            this.bunifuSeparator4.TabIndex = 8;
            this.bunifuSeparator4.Transparency = 0;
            this.bunifuSeparator4.Vertical = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.lblQuantity);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel5.Size = new System.Drawing.Size(215, 131);
            this.panel5.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.nudStock);
            this.panel6.Controls.Add(this.bunifuSeparator1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(15, 40);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(185, 79);
            this.panel6.TabIndex = 2;
            // 
            // nudStock
            // 
            this.nudStock.BackColor = System.Drawing.Color.White;
            this.nudStock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudStock.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nudStock.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudStock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.nudStock.Location = new System.Drawing.Point(0, 21);
            this.nudStock.Name = "nudStock";
            this.nudStock.Size = new System.Drawing.Size(185, 23);
            this.nudStock.TabIndex = 1;
            this.nudStock.ThousandsSeparator = true;
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.bunifuSeparator1.LineThickness = 3;
            this.bunifuSeparator1.Location = new System.Drawing.Point(0, 44);
            this.bunifuSeparator1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(185, 35);
            this.bunifuSeparator1.TabIndex = 2;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // lblQuantity
            // 
            this.lblQuantity.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantity.Location = new System.Drawing.Point(15, 12);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(185, 28);
            this.lblQuantity.TabIndex = 1;
            this.lblQuantity.Text = "Cantidad:";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.lblDate);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel7.Size = new System.Drawing.Size(218, 131);
            this.panel7.TabIndex = 3;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.dtpAdquisition);
            this.panel8.Controls.Add(this.bunifuSeparator2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(15, 40);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(188, 79);
            this.panel8.TabIndex = 2;
            // 
            // dtpAdquisition
            // 
            this.dtpAdquisition.BackColor = System.Drawing.Color.SeaGreen;
            this.dtpAdquisition.BorderRadius = 0;
            this.dtpAdquisition.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dtpAdquisition.ForeColor = System.Drawing.Color.White;
            this.dtpAdquisition.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtpAdquisition.FormatCustom = null;
            this.dtpAdquisition.Location = new System.Drawing.Point(0, 21);
            this.dtpAdquisition.Margin = new System.Windows.Forms.Padding(4);
            this.dtpAdquisition.Name = "dtpAdquisition";
            this.dtpAdquisition.Size = new System.Drawing.Size(188, 23);
            this.dtpAdquisition.TabIndex = 0;
            this.dtpAdquisition.Value = new System.DateTime(2020, 5, 3, 0, 0, 0, 0);
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.bunifuSeparator2.LineThickness = 3;
            this.bunifuSeparator2.Location = new System.Drawing.Point(0, 44);
            this.bunifuSeparator2.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Size = new System.Drawing.Size(188, 35);
            this.bunifuSeparator2.TabIndex = 3;
            this.bunifuSeparator2.Transparency = 255;
            this.bunifuSeparator2.Vertical = false;
            // 
            // lblDate
            // 
            this.lblDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(15, 12);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(188, 28);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "Fecha de adquisición:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtName);
            this.panel2.Controls.Add(this.lblName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel2.Size = new System.Drawing.Size(434, 91);
            this.panel2.TabIndex = 2;
            // 
            // txtName
            // 
            this.txtName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtName.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtName.HintForeColor = System.Drawing.Color.Gray;
            this.txtName.HintText = "Digite nombre de equipo";
            this.txtName.isPassword = false;
            this.txtName.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtName.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtName.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtName.LineThickness = 3;
            this.txtName.Location = new System.Drawing.Point(15, 46);
            this.txtName.Margin = new System.Windows.Forms.Padding(4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(404, 33);
            this.txtName.TabIndex = 0;
            this.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // lblName
            // 
            this.lblName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(15, 12);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(404, 28);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Nombre:";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.AutoScroll = true;
            this.splitContainer3.Panel2.Controls.Add(this.panel4);
            this.splitContainer3.Panel2.Controls.Add(this.panel9);
            this.splitContainer3.Panel2.Controls.Add(this.panel10);
            this.splitContainer3.Size = new System.Drawing.Size(435, 423);
            this.splitContainer3.SplitterDistance = 56;
            this.splitContainer3.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnCancel);
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Controls.Add(this.btnAddEquipment);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 210);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(435, 54);
            this.panel4.TabIndex = 5;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.btnCancel.Location = new System.Drawing.Point(54, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(151, 44);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "CANCELAR";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUpdate.BorderRadius = 0;
            this.btnUpdate.ButtonText = "ACTUALIZAR";
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.DisabledColor = System.Drawing.Color.Gray;
            this.btnUpdate.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Iconcolor = System.Drawing.Color.Transparent;
            this.btnUpdate.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Iconimage")));
            this.btnUpdate.Iconimage_right = null;
            this.btnUpdate.Iconimage_right_Selected = null;
            this.btnUpdate.Iconimage_Selected = null;
            this.btnUpdate.IconMarginLeft = 0;
            this.btnUpdate.IconMarginRight = 0;
            this.btnUpdate.IconRightVisible = false;
            this.btnUpdate.IconRightZoom = 0D;
            this.btnUpdate.IconVisible = false;
            this.btnUpdate.IconZoom = 90D;
            this.btnUpdate.IsTab = false;
            this.btnUpdate.Location = new System.Drawing.Point(212, 4);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnUpdate.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnUpdate.OnHoverTextColor = System.Drawing.Color.White;
            this.btnUpdate.selected = false;
            this.btnUpdate.Size = new System.Drawing.Size(165, 44);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "ACTUALIZAR";
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnUpdate.Textcolor = System.Drawing.Color.White;
            this.btnUpdate.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.btnClear.Location = new System.Drawing.Point(54, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(151, 44);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "LIMPIAR";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAddEquipment
            // 
            this.btnAddEquipment.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAddEquipment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddEquipment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAddEquipment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddEquipment.BorderRadius = 0;
            this.btnAddEquipment.ButtonText = "AGREGAR EQUIPO";
            this.btnAddEquipment.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddEquipment.DisabledColor = System.Drawing.Color.Gray;
            this.btnAddEquipment.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddEquipment.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAddEquipment.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAddEquipment.Iconimage")));
            this.btnAddEquipment.Iconimage_right = null;
            this.btnAddEquipment.Iconimage_right_Selected = null;
            this.btnAddEquipment.Iconimage_Selected = null;
            this.btnAddEquipment.IconMarginLeft = 0;
            this.btnAddEquipment.IconMarginRight = 0;
            this.btnAddEquipment.IconRightVisible = false;
            this.btnAddEquipment.IconRightZoom = 0D;
            this.btnAddEquipment.IconVisible = false;
            this.btnAddEquipment.IconZoom = 90D;
            this.btnAddEquipment.IsTab = false;
            this.btnAddEquipment.Location = new System.Drawing.Point(212, 4);
            this.btnAddEquipment.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddEquipment.Name = "btnAddEquipment";
            this.btnAddEquipment.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAddEquipment.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnAddEquipment.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAddEquipment.selected = false;
            this.btnAddEquipment.Size = new System.Drawing.Size(165, 44);
            this.btnAddEquipment.TabIndex = 0;
            this.btnAddEquipment.Text = "AGREGAR EQUIPO";
            this.btnAddEquipment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAddEquipment.Textcolor = System.Drawing.Color.White;
            this.btnAddEquipment.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddEquipment.Click += new System.EventHandler(this.btnAddEquipment_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txtDescription);
            this.panel9.Controls.Add(this.lblDescription);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 91);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel9.Size = new System.Drawing.Size(435, 119);
            this.panel9.TabIndex = 2;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoScroll = true;
            this.txtDescription.AutoSize = true;
            this.txtDescription.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDescription.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtDescription.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtDescription.HintForeColor = System.Drawing.Color.Gray;
            this.txtDescription.HintText = "Describa el equipo...";
            this.txtDescription.isPassword = false;
            this.txtDescription.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtDescription.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtDescription.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtDescription.LineThickness = 3;
            this.txtDescription.Location = new System.Drawing.Point(15, 40);
            this.txtDescription.Margin = new System.Windows.Forms.Padding(4);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(405, 67);
            this.txtDescription.TabIndex = 0;
            this.txtDescription.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // lblDescription
            // 
            this.lblDescription.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(15, 12);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(405, 28);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.Text = "Descripción:";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.splitContainer4);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(435, 91);
            this.panel10.TabIndex = 4;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.bunifuSeparator5);
            this.splitContainer4.Panel1.Controls.Add(this.panel11);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.panel13);
            this.splitContainer4.Size = new System.Drawing.Size(435, 91);
            this.splitContainer4.SplitterDistance = 217;
            this.splitContainer4.SplitterWidth = 1;
            this.splitContainer4.TabIndex = 0;
            // 
            // bunifuSeparator5
            // 
            this.bunifuSeparator5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator5.Dock = System.Windows.Forms.DockStyle.Right;
            this.bunifuSeparator5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bunifuSeparator5.LineThickness = 1;
            this.bunifuSeparator5.Location = new System.Drawing.Point(212, 0);
            this.bunifuSeparator5.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator5.Name = "bunifuSeparator5";
            this.bunifuSeparator5.Size = new System.Drawing.Size(5, 91);
            this.bunifuSeparator5.TabIndex = 8;
            this.bunifuSeparator5.Transparency = 0;
            this.bunifuSeparator5.Vertical = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.label5);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel11.Name = "panel11";
            this.panel11.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel11.Size = new System.Drawing.Size(217, 91);
            this.panel11.TabIndex = 2;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.drdUbication);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(15, 40);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(187, 39);
            this.panel12.TabIndex = 2;
            // 
            // drdUbication
            // 
            this.drdUbication.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.drdUbication.BackColor = System.Drawing.Color.Transparent;
            this.drdUbication.BorderRadius = 3;
            this.drdUbication.DisabledColor = System.Drawing.Color.Gray;
            this.drdUbication.ForeColor = System.Drawing.Color.White;
            this.drdUbication.Items = new string[0];
            this.drdUbication.Location = new System.Drawing.Point(3, 6);
            this.drdUbication.Margin = new System.Windows.Forms.Padding(4);
            this.drdUbication.Name = "drdUbication";
            this.drdUbication.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.drdUbication.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.drdUbication.selectedIndex = -1;
            this.drdUbication.Size = new System.Drawing.Size(154, 30);
            this.drdUbication.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(187, 28);
            this.label5.TabIndex = 1;
            this.label5.Text = "Ubicación:";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Controls.Add(this.lblType);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel13.Name = "panel13";
            this.panel13.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel13.Size = new System.Drawing.Size(217, 91);
            this.panel13.TabIndex = 3;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.drdTipo);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(15, 40);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(187, 39);
            this.panel14.TabIndex = 2;
            // 
            // drdTipo
            // 
            this.drdTipo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.drdTipo.BackColor = System.Drawing.Color.Transparent;
            this.drdTipo.BorderRadius = 3;
            this.drdTipo.DisabledColor = System.Drawing.Color.Gray;
            this.drdTipo.ForeColor = System.Drawing.Color.White;
            this.drdTipo.Items = new string[0];
            this.drdTipo.Location = new System.Drawing.Point(3, 4);
            this.drdTipo.Margin = new System.Windows.Forms.Padding(4);
            this.drdTipo.Name = "drdTipo";
            this.drdTipo.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.drdTipo.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.drdTipo.selectedIndex = -1;
            this.drdTipo.Size = new System.Drawing.Size(155, 30);
            this.drdTipo.TabIndex = 6;
            // 
            // lblType
            // 
            this.lblType.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblType.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblType.Location = new System.Drawing.Point(15, 12);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(187, 28);
            this.lblType.TabIndex = 1;
            this.lblType.Text = "Tipo:";
            // 
            // ErrorName
            // 
            this.ErrorName.ContainerControl = this;
            // 
            // ErrorStock
            // 
            this.ErrorStock.ContainerControl = this;
            // 
            // ErrorType
            // 
            this.ErrorType.ContainerControl = this;
            // 
            // ErrorDescription
            // 
            this.ErrorDescription.ContainerControl = this;
            // 
            // alert1
            // 
            this.alert1.AlertType = Cotton.Controls.AlertType.Danger;
            this.alert1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.alert1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.alert1.FontSizeBody = 9.75F;
            this.alert1.FontSizeTitle = 12F;
            this.alert1.Location = new System.Drawing.Point(502, 28);
            this.alert1.MaximumSize = new System.Drawing.Size(400, 1000);
            this.alert1.Message = "Todo bien todo correcto esta alerta está bien puesta.";
            this.alert1.Name = "alert1";
            this.alert1.Size = new System.Drawing.Size(400, 98);
            this.alert1.TabIndex = 5;
            this.alert1.TimeLapse = 3500;
            this.alert1.Title = "Si aparezco";
            this.alert1.Visible = false;
            // 
            // ErrorDate
            // 
            this.ErrorDate.ContainerControl = this;
            // 
            // Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(910, 499);
            this.Controls.Add(this.alert1);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Add";
            this.Text = "Add";
            this.Load += new System.EventHandler(this.Add_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splContainer.Panel1.ResumeLayout(false);
            this.splContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splContainer)).EndInit();
            this.splContainer.ResumeLayout(false);
            this.leftForm.Panel1.ResumeLayout(false);
            this.leftForm.Panel1.PerformLayout();
            this.leftForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leftForm)).EndInit();
            this.leftForm.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudStock)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorDate)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private MaterialSkin.Controls.MaterialDivider materialDivider1;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.SplitContainer splContainer;
		private System.Windows.Forms.SplitContainer leftForm;
		private System.Windows.Forms.Label lblTitleForm;
		private System.Windows.Forms.SplitContainer splitContainer3;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button btnClear;
		private Bunifu.Framework.UI.BunifuFlatButton btnAddEquipment;
		private System.Windows.Forms.Panel panel9;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtDescription;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.Panel panel10;
		private System.Windows.Forms.SplitContainer splitContainer4;
		private System.Windows.Forms.Panel panel11;
		private System.Windows.Forms.Panel panel12;
		private Bunifu.Framework.UI.BunifuDropdown drdUbication;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Panel panel13;
		private System.Windows.Forms.Panel panel14;
		private Bunifu.Framework.UI.BunifuDropdown drdTipo;
		private System.Windows.Forms.Label lblType;
		private System.Windows.Forms.Panel panel15;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator5;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator3;
		private System.Windows.Forms.Panel panel16;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator4;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.NumericUpDown nudStock;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
		private System.Windows.Forms.Label lblQuantity;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.Panel panel8;
		private Bunifu.Framework.UI.BunifuDatepicker dtpAdquisition;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator2;
		private System.Windows.Forms.Label lblDate;
		private System.Windows.Forms.Panel panel2;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtName;
		private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ErrorProvider ErrorName;
        private System.Windows.Forms.ErrorProvider ErrorStock;
        private System.Windows.Forms.ErrorProvider ErrorType;
        private System.Windows.Forms.ErrorProvider ErrorDescription;
        private Controls.Alert alert1;
        private System.Windows.Forms.Button btnCancel;
        private Bunifu.Framework.UI.BunifuFlatButton btnUpdate;
        private System.Windows.Forms.ErrorProvider ErrorDate;
    }
}