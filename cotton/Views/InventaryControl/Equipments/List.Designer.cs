﻿namespace Cotton.Views.InventaryControl.Equipments
{
	partial class List
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
					components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(List));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlLoading = new System.Windows.Forms.Panel();
            this.pcbLoading = new System.Windows.Forms.PictureBox();
            this.dgvEquipos = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.available = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acquisition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.lblTableTitle = new System.Windows.Forms.Label();
            this.searchIcon = new FontAwesome.Sharp.IconPictureBox();
            this.txtSearch = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.alert1 = new Cotton.Controls.Alert();
            this.grbControls = new System.Windows.Forms.GroupBox();
            this.btnSelect = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.drpSortColumn = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.chkDesc = new Bunifu.Framework.UI.BunifuCheckbox();
            this.lblDesc = new System.Windows.Forms.Label();
            this.lblSortOption = new System.Windows.Forms.Label();
            this.btnDelete = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnUpdate = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnCancel = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnDialogFolder = new Bunifu.Framework.UI.BunifuFlatButton();
            this.fadeOut = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlLoading.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquipos)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchIcon)).BeginInit();
            this.grbControls.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.fadeOut.SetDecoration(this.splitContainer1, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.fadeOut.SetDecoration(this.splitContainer1.Panel1, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.fadeOut.SetDecoration(this.splitContainer1.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.splitContainer1.Size = new System.Drawing.Size(887, 494);
            this.splitContainer1.SplitterDistance = 72;
            this.splitContainer1.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.materialDivider1);
            this.flowLayoutPanel1.Controls.Add(this.lblTitle);
            this.fadeOut.SetDecoration(this.flowLayoutPanel1, BunifuAnimatorNS.DecorationType.None);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(20, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(867, 72);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.fadeOut.SetDecoration(this.materialDivider1, BunifuAnimatorNS.DecorationType.None);
            this.materialDivider1.Depth = 0;
            this.materialDivider1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.materialDivider1.Location = new System.Drawing.Point(10, 67);
            this.materialDivider1.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(131, 2);
            this.materialDivider1.TabIndex = 2;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.fadeOut.SetDecoration(this.lblTitle, BunifuAnimatorNS.DecorationType.None);
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.lblTitle.Location = new System.Drawing.Point(3, 39);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(170, 25);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Lista de equipos";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainer3);
            this.fadeOut.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(20, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(867, 418);
            this.panel1.TabIndex = 1;
            // 
            // splitContainer3
            // 
            this.fadeOut.SetDecoration(this.splitContainer3, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.panel2);
            this.splitContainer3.Panel1.Controls.Add(this.panel3);
            this.fadeOut.SetDecoration(this.splitContainer3.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.alert1);
            this.splitContainer3.Panel2.Controls.Add(this.grbControls);
            this.splitContainer3.Panel2.Controls.Add(this.btnDialogFolder);
            this.fadeOut.SetDecoration(this.splitContainer3.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer3.Size = new System.Drawing.Size(867, 418);
            this.splitContainer3.SplitterDistance = 577;
            this.splitContainer3.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnlLoading);
            this.panel2.Controls.Add(this.dgvEquipos);
            this.fadeOut.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 53);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(12, 15, 12, 15);
            this.panel2.Size = new System.Drawing.Size(577, 265);
            this.panel2.TabIndex = 1;
            // 
            // pnlLoading
            // 
            this.pnlLoading.Controls.Add(this.pcbLoading);
            this.fadeOut.SetDecoration(this.pnlLoading, BunifuAnimatorNS.DecorationType.None);
            this.pnlLoading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLoading.Location = new System.Drawing.Point(12, 15);
            this.pnlLoading.Name = "pnlLoading";
            this.pnlLoading.Size = new System.Drawing.Size(553, 235);
            this.pnlLoading.TabIndex = 1;
            // 
            // pcbLoading
            // 
            this.pcbLoading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(242)))), ((int)(((byte)(243)))));
            this.fadeOut.SetDecoration(this.pcbLoading, BunifuAnimatorNS.DecorationType.None);
            this.pcbLoading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcbLoading.Image = global::Cotton.Properties.Resources.loading;
            this.pcbLoading.Location = new System.Drawing.Point(0, 0);
            this.pcbLoading.Name = "pcbLoading";
            this.pcbLoading.Size = new System.Drawing.Size(553, 235);
            this.pcbLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pcbLoading.TabIndex = 0;
            this.pcbLoading.TabStop = false;
            // 
            // dgvEquipos
            // 
            this.dgvEquipos.AllowUserToAddRows = false;
            this.dgvEquipos.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvEquipos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEquipos.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgvEquipos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEquipos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvEquipos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEquipos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEquipos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEquipos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.quantity,
            this.available,
            this.acquisition,
            this.type,
            this.id});
            this.fadeOut.SetDecoration(this.dgvEquipos, BunifuAnimatorNS.DecorationType.None);
            this.dgvEquipos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEquipos.DoubleBuffered = true;
            this.dgvEquipos.EnableHeadersVisualStyles = false;
            this.dgvEquipos.GridColor = System.Drawing.SystemColors.Control;
            this.dgvEquipos.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.dgvEquipos.HeaderForeColor = System.Drawing.Color.Azure;
            this.dgvEquipos.Location = new System.Drawing.Point(12, 15);
            this.dgvEquipos.Name = "dgvEquipos";
            this.dgvEquipos.ReadOnly = true;
            this.dgvEquipos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(15)))), ((int)(((byte)(9)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(46)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEquipos.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvEquipos.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(15)))), ((int)(((byte)(9)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(46)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.dgvEquipos.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvEquipos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEquipos.Size = new System.Drawing.Size(553, 235);
            this.dgvEquipos.TabIndex = 0;
            this.dgvEquipos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEquipos_CellClick);
            this.dgvEquipos.Sorted += new System.EventHandler(this.dgvEquipos_Sorted);
            // 
            // name
            // 
            this.name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.name.FillWeight = 20F;
            this.name.HeaderText = "Nombre";
            this.name.MinimumWidth = 100;
            this.name.Name = "name";
            this.name.ReadOnly = true;
            // 
            // quantity
            // 
            this.quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.quantity.FillWeight = 10F;
            this.quantity.HeaderText = "Cantidad";
            this.quantity.MinimumWidth = 50;
            this.quantity.Name = "quantity";
            this.quantity.ReadOnly = true;
            this.quantity.Width = 87;
            // 
            // available
            // 
            this.available.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.available.FillWeight = 20F;
            this.available.HeaderText = "Disponibles";
            this.available.MinimumWidth = 100;
            this.available.Name = "available";
            this.available.ReadOnly = true;
            // 
            // acquisition
            // 
            this.acquisition.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.acquisition.FillWeight = 20F;
            this.acquisition.HeaderText = "Fecha Adquisición";
            this.acquisition.MinimumWidth = 100;
            this.acquisition.Name = "acquisition";
            this.acquisition.ReadOnly = true;
            // 
            // type
            // 
            this.type.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.type.FillWeight = 20F;
            this.type.HeaderText = "Categoría";
            this.type.MinimumWidth = 100;
            this.type.Name = "type";
            this.type.ReadOnly = true;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splitContainer2);
            this.fadeOut.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(577, 53);
            this.panel3.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.fadeOut.SetDecoration(this.splitContainer2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.lblTableTitle);
            this.fadeOut.SetDecoration(this.splitContainer2.Panel1, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer2.Panel1MinSize = 100;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.searchIcon);
            this.splitContainer2.Panel2.Controls.Add(this.txtSearch);
            this.fadeOut.SetDecoration(this.splitContainer2.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer2.Size = new System.Drawing.Size(577, 53);
            this.splitContainer2.SplitterDistance = 121;
            this.splitContainer2.TabIndex = 0;
            // 
            // lblTableTitle
            // 
            this.lblTableTitle.AutoSize = true;
            this.fadeOut.SetDecoration(this.lblTableTitle, BunifuAnimatorNS.DecorationType.None);
            this.lblTableTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTableTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.lblTableTitle.Location = new System.Drawing.Point(3, 15);
            this.lblTableTitle.Name = "lblTableTitle";
            this.lblTableTitle.Size = new System.Drawing.Size(67, 20);
            this.lblTableTitle.TabIndex = 3;
            this.lblTableTitle.Text = "Equipos";
            // 
            // searchIcon
            // 
            this.searchIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchIcon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.fadeOut.SetDecoration(this.searchIcon, BunifuAnimatorNS.DecorationType.None);
            this.searchIcon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.searchIcon.IconChar = FontAwesome.Sharp.IconChar.Search;
            this.searchIcon.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.searchIcon.IconSize = 23;
            this.searchIcon.Location = new System.Drawing.Point(422, 12);
            this.searchIcon.Name = "searchIcon";
            this.searchIcon.Size = new System.Drawing.Size(24, 23);
            this.searchIcon.TabIndex = 2;
            this.searchIcon.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeOut.SetDecoration(this.txtSearch, BunifuAnimatorNS.DecorationType.None);
            this.txtSearch.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtSearch.HintForeColor = System.Drawing.Color.Empty;
            this.txtSearch.HintText = "Buscar...";
            this.txtSearch.isPassword = false;
            this.txtSearch.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtSearch.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtSearch.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtSearch.LineThickness = 3;
            this.txtSearch.Location = new System.Drawing.Point(78, 7);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(368, 33);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // alert1
            // 
            this.alert1.AlertType = Cotton.Controls.AlertType.Danger;
            this.alert1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.alert1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fadeOut.SetDecoration(this.alert1, BunifuAnimatorNS.DecorationType.None);
            this.alert1.FontSizeBody = 9.75F;
            this.alert1.FontSizeTitle = 12F;
            this.alert1.Location = new System.Drawing.Point(18, 12);
            this.alert1.MaximumSize = new System.Drawing.Size(400, 10000);
            this.alert1.Message = resources.GetString("alert1.Message");
            this.alert1.Name = "alert1";
            this.alert1.Size = new System.Drawing.Size(251, 104);
            this.alert1.TabIndex = 5;
            this.alert1.TimeLapse = 5000;
            this.alert1.Title = "Message Title Alert";
            this.alert1.Visible = false;
            // 
            // grbControls
            // 
            this.grbControls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbControls.Controls.Add(this.btnSelect);
            this.grbControls.Controls.Add(this.panel4);
            this.grbControls.Controls.Add(this.btnDelete);
            this.grbControls.Controls.Add(this.btnUpdate);
            this.grbControls.Controls.Add(this.btnCancel);
            this.fadeOut.SetDecoration(this.grbControls, BunifuAnimatorNS.DecorationType.None);
            this.grbControls.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbControls.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbControls.Location = new System.Drawing.Point(30, 68);
            this.grbControls.Name = "grbControls";
            this.grbControls.Size = new System.Drawing.Size(228, 250);
            this.grbControls.TabIndex = 7;
            this.grbControls.TabStop = false;
            this.grbControls.Text = "Controles";
            // 
            // btnSelect
            // 
            this.btnSelect.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelect.BackColor = System.Drawing.Color.SeaGreen;
            this.btnSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSelect.BorderRadius = 0;
            this.btnSelect.ButtonText = "Seleccionar";
            this.btnSelect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeOut.SetDecoration(this.btnSelect, BunifuAnimatorNS.DecorationType.None);
            this.btnSelect.DisabledColor = System.Drawing.Color.Gray;
            this.btnSelect.Enabled = false;
            this.btnSelect.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Iconcolor = System.Drawing.Color.Transparent;
            this.btnSelect.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnSelect.Iconimage")));
            this.btnSelect.Iconimage_right = null;
            this.btnSelect.Iconimage_right_Selected = null;
            this.btnSelect.Iconimage_Selected = null;
            this.btnSelect.IconMarginLeft = 0;
            this.btnSelect.IconMarginRight = 0;
            this.btnSelect.IconRightVisible = false;
            this.btnSelect.IconRightZoom = 0D;
            this.btnSelect.IconVisible = false;
            this.btnSelect.IconZoom = 90D;
            this.btnSelect.IsTab = false;
            this.btnSelect.Location = new System.Drawing.Point(7, 25);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelect.MaximumSize = new System.Drawing.Size(500, 1000);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Normalcolor = System.Drawing.Color.SeaGreen;
            this.btnSelect.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnSelect.OnHoverTextColor = System.Drawing.Color.White;
            this.btnSelect.selected = false;
            this.btnSelect.Size = new System.Drawing.Size(214, 33);
            this.btnSelect.TabIndex = 9;
            this.btnSelect.Text = "Seleccionar";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSelect.Textcolor = System.Drawing.Color.White;
            this.btnSelect.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Visible = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.drpSortColumn);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.lblSortOption);
            this.fadeOut.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(3, 147);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(222, 100);
            this.panel4.TabIndex = 8;
            // 
            // drpSortColumn
            // 
            this.drpSortColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.drpSortColumn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(84)))), ((int)(((byte)(65)))));
            this.fadeOut.SetDecoration(this.drpSortColumn, BunifuAnimatorNS.DecorationType.None);
            this.drpSortColumn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpSortColumn.ForeColor = System.Drawing.Color.White;
            this.drpSortColumn.FormattingEnabled = true;
            this.drpSortColumn.Location = new System.Drawing.Point(20, 61);
            this.drpSortColumn.Name = "drpSortColumn";
            this.drpSortColumn.Size = new System.Drawing.Size(180, 26);
            this.drpSortColumn.TabIndex = 12;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.chkDesc);
            this.panel5.Controls.Add(this.lblDesc);
            this.fadeOut.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.panel5.Location = new System.Drawing.Point(20, 30);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(180, 24);
            this.panel5.TabIndex = 11;
            // 
            // chkDesc
            // 
            this.chkDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.chkDesc.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.chkDesc.Checked = false;
            this.chkDesc.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(84)))), ((int)(((byte)(65)))));
            this.fadeOut.SetDecoration(this.chkDesc, BunifuAnimatorNS.DecorationType.None);
            this.chkDesc.ForeColor = System.Drawing.Color.White;
            this.chkDesc.Location = new System.Drawing.Point(4, 3);
            this.chkDesc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkDesc.Name = "chkDesc";
            this.chkDesc.Size = new System.Drawing.Size(20, 20);
            this.chkDesc.TabIndex = 9;
            this.chkDesc.OnChange += new System.EventHandler(this.chkDesc_OnChange);
            // 
            // lblDesc
            // 
            this.lblDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDesc.AutoSize = true;
            this.fadeOut.SetDecoration(this.lblDesc, BunifuAnimatorNS.DecorationType.None);
            this.lblDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesc.Location = new System.Drawing.Point(30, 3);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(95, 18);
            this.lblDesc.TabIndex = 10;
            this.lblDesc.Text = "Descendente";
            this.lblDesc.Click += new System.EventHandler(this.lblDesc_Click);
            // 
            // lblSortOption
            // 
            this.lblSortOption.AutoSize = true;
            this.fadeOut.SetDecoration(this.lblSortOption, BunifuAnimatorNS.DecorationType.None);
            this.lblSortOption.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSortOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSortOption.Location = new System.Drawing.Point(0, 0);
            this.lblSortOption.Name = "lblSortOption";
            this.lblSortOption.Size = new System.Drawing.Size(129, 18);
            this.lblSortOption.TabIndex = 7;
            this.lblSortOption.Text = "Ordenando como:";
            // 
            // btnDelete
            // 
            this.btnDelete.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelete.BorderRadius = 0;
            this.btnDelete.ButtonText = "Eliminar";
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeOut.SetDecoration(this.btnDelete, BunifuAnimatorNS.DecorationType.None);
            this.btnDelete.DisabledColor = System.Drawing.Color.Gray;
            this.btnDelete.Enabled = false;
            this.btnDelete.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Iconcolor = System.Drawing.Color.Transparent;
            this.btnDelete.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnDelete.Iconimage")));
            this.btnDelete.Iconimage_right = null;
            this.btnDelete.Iconimage_right_Selected = null;
            this.btnDelete.Iconimage_Selected = null;
            this.btnDelete.IconMarginLeft = 0;
            this.btnDelete.IconMarginRight = 0;
            this.btnDelete.IconRightVisible = false;
            this.btnDelete.IconRightZoom = 0D;
            this.btnDelete.IconVisible = false;
            this.btnDelete.IconZoom = 90D;
            this.btnDelete.IsTab = false;
            this.btnDelete.Location = new System.Drawing.Point(7, 66);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.MaximumSize = new System.Drawing.Size(500, 1000);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.btnDelete.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnDelete.OnHoverTextColor = System.Drawing.Color.White;
            this.btnDelete.selected = false;
            this.btnDelete.Size = new System.Drawing.Size(214, 33);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Eliminar";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnDelete.Textcolor = System.Drawing.Color.White;
            this.btnDelete.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.btnUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUpdate.BorderRadius = 0;
            this.btnUpdate.ButtonText = "MODIFICAR";
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeOut.SetDecoration(this.btnUpdate, BunifuAnimatorNS.DecorationType.None);
            this.btnUpdate.DisabledColor = System.Drawing.Color.Gray;
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Iconcolor = System.Drawing.Color.Transparent;
            this.btnUpdate.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Iconimage")));
            this.btnUpdate.Iconimage_right = null;
            this.btnUpdate.Iconimage_right_Selected = null;
            this.btnUpdate.Iconimage_Selected = null;
            this.btnUpdate.IconMarginLeft = 0;
            this.btnUpdate.IconMarginRight = 0;
            this.btnUpdate.IconRightVisible = false;
            this.btnUpdate.IconRightZoom = 0D;
            this.btnUpdate.IconVisible = false;
            this.btnUpdate.IconZoom = 90D;
            this.btnUpdate.IsTab = false;
            this.btnUpdate.Location = new System.Drawing.Point(7, 25);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdate.MaximumSize = new System.Drawing.Size(500, 1000);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.btnUpdate.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.btnUpdate.OnHoverTextColor = System.Drawing.Color.White;
            this.btnUpdate.selected = false;
            this.btnUpdate.Size = new System.Drawing.Size(214, 33);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "MODIFICAR";
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnUpdate.Textcolor = System.Drawing.Color.White;
            this.btnUpdate.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Firebrick;
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.BorderRadius = 0;
            this.btnCancel.ButtonText = "Cancel";
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeOut.SetDecoration(this.btnCancel, BunifuAnimatorNS.DecorationType.None);
            this.btnCancel.DisabledColor = System.Drawing.Color.Gray;
            this.btnCancel.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Iconcolor = System.Drawing.Color.Transparent;
            this.btnCancel.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnCancel.Iconimage")));
            this.btnCancel.Iconimage_right = null;
            this.btnCancel.Iconimage_right_Selected = null;
            this.btnCancel.Iconimage_Selected = null;
            this.btnCancel.IconMarginLeft = 0;
            this.btnCancel.IconMarginRight = 0;
            this.btnCancel.IconRightVisible = false;
            this.btnCancel.IconRightZoom = 0D;
            this.btnCancel.IconVisible = false;
            this.btnCancel.IconZoom = 90D;
            this.btnCancel.IsTab = false;
            this.btnCancel.Location = new System.Drawing.Point(7, 66);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.MaximumSize = new System.Drawing.Size(500, 1000);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Normalcolor = System.Drawing.Color.Firebrick;
            this.btnCancel.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnCancel.OnHoverTextColor = System.Drawing.Color.White;
            this.btnCancel.selected = false;
            this.btnCancel.Size = new System.Drawing.Size(214, 33);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCancel.Textcolor = System.Drawing.Color.White;
            this.btnCancel.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDialogFolder
            // 
            this.btnDialogFolder.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnDialogFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDialogFolder.BackColor = System.Drawing.Color.SeaGreen;
            this.btnDialogFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDialogFolder.BorderRadius = 0;
            this.btnDialogFolder.ButtonText = "Generar PDF";
            this.btnDialogFolder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeOut.SetDecoration(this.btnDialogFolder, BunifuAnimatorNS.DecorationType.None);
            this.btnDialogFolder.DisabledColor = System.Drawing.Color.Gray;
            this.btnDialogFolder.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDialogFolder.Iconcolor = System.Drawing.Color.Transparent;
            this.btnDialogFolder.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnDialogFolder.Iconimage")));
            this.btnDialogFolder.Iconimage_right = null;
            this.btnDialogFolder.Iconimage_right_Selected = null;
            this.btnDialogFolder.Iconimage_Selected = null;
            this.btnDialogFolder.IconMarginLeft = 0;
            this.btnDialogFolder.IconMarginRight = 0;
            this.btnDialogFolder.IconRightVisible = false;
            this.btnDialogFolder.IconRightZoom = 0D;
            this.btnDialogFolder.IconVisible = false;
            this.btnDialogFolder.IconZoom = 90D;
            this.btnDialogFolder.IsTab = false;
            this.btnDialogFolder.Location = new System.Drawing.Point(37, 20);
            this.btnDialogFolder.Margin = new System.Windows.Forms.Padding(4);
            this.btnDialogFolder.MaximumSize = new System.Drawing.Size(500, 1000);
            this.btnDialogFolder.Name = "btnDialogFolder";
            this.btnDialogFolder.Normalcolor = System.Drawing.Color.SeaGreen;
            this.btnDialogFolder.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnDialogFolder.OnHoverTextColor = System.Drawing.Color.White;
            this.btnDialogFolder.selected = false;
            this.btnDialogFolder.Size = new System.Drawing.Size(214, 33);
            this.btnDialogFolder.TabIndex = 10;
            this.btnDialogFolder.Text = "Generar PDF";
            this.btnDialogFolder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnDialogFolder.Textcolor = System.Drawing.Color.White;
            this.btnDialogFolder.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDialogFolder.Click += new System.EventHandler(this.btnDialogFolder_Click);
            // 
            // fadeOut
            // 
            this.fadeOut.AnimationType = BunifuAnimatorNS.AnimationType.Transparent;
            this.fadeOut.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 1F;
            this.fadeOut.DefaultAnimation = animation1;
            // 
            // List
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(887, 494);
            this.Controls.Add(this.splitContainer1);
            this.fadeOut.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Name = "List";
            this.Text = "List";
            this.Load += new System.EventHandler(this.List_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlLoading.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquipos)).EndInit();
            this.panel3.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchIcon)).EndInit();
            this.grbControls.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private MaterialSkin.Controls.MaterialDivider materialDivider1;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.SplitContainer splitContainer3;
		private System.Windows.Forms.Panel panel2;
		private Bunifu.Framework.UI.BunifuCustomDataGrid dgvEquipos;
		private System.Windows.Forms.Panel pnlLoading;
		private System.Windows.Forms.PictureBox pcbLoading;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.Label lblTableTitle;
		private FontAwesome.Sharp.IconPictureBox searchIcon;
		private Bunifu.Framework.UI.BunifuMaterialTextbox txtSearch;
        private BunifuAnimatorNS.BunifuTransition fadeOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn available;
        private System.Windows.Forms.DataGridViewTextBoxColumn acquisition;
        private System.Windows.Forms.DataGridViewTextBoxColumn type;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private Controls.Alert alert1;
		private System.Windows.Forms.GroupBox grbControls;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.ComboBox drpSortColumn;
		private System.Windows.Forms.Panel panel5;
		private Bunifu.Framework.UI.BunifuCheckbox chkDesc;
		private System.Windows.Forms.Label lblDesc;
		private System.Windows.Forms.Label lblSortOption;
		private Bunifu.Framework.UI.BunifuFlatButton btnDelete;
		private Bunifu.Framework.UI.BunifuFlatButton btnUpdate;
		private Bunifu.Framework.UI.BunifuFlatButton btnSelect;
		private Bunifu.Framework.UI.BunifuFlatButton btnCancel;
        private Bunifu.Framework.UI.BunifuFlatButton btnDialogFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}