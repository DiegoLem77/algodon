﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Cotton.General;
using Cotton.Models;
using Cotton.Controls;
using System.Threading;
using System.Threading.Tasks;
using System.Collections;
using Cotton.Controllers;

namespace Cotton.Views.InventaryControl.Ubications
{
	public partial class Add : Form
	{
		private FontFamily Poppins, PoppinsL, Monserrat = null;
        private EquipmentTypeModel equipmentTypeModel = new EquipmentTypeModel();
        private Task<Hashtable> filled;
        private CancellationTokenSource tokenSource;
        private string edit = null;
        public Add(string idEdit = null)
		{
			InitializeComponent();
			Poppins = Utils.ImportFont(nameof(Properties.Resources.Poppins_Medium)).Families[0];
			PoppinsL = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
			Monserrat = Utils.ImportFont(nameof(Properties.Resources.Montserrat_Light)).Families[0];
			lblTitle.Font = new Font(Utils.ImportFont(nameof(Properties.Resources.Poppins_Regular)).Families[0], lblTitle.Font.Size);
			lblTitleForm.Font = new Font(Poppins, lblTitleForm.Font.Size);
			lblSubtitle.Font = new Font(Poppins, lblSubtitle.Font.Size);
			lblUbication.Font = new Font(Monserrat, lblUbication.Font.Size);
			alert1.Font = new Font(PoppinsL, alert1.Font.Size);
			alert1.FontSizeTitle = 12f;
			alert1.FontSizeBody = 10f;
            if(idEdit == null)
            {
                bunifuFlatButton1.Enabled = true;
                bunifuFlatButton1.Show();
                bunifuFlatButton2.Enabled = false;
                bunifuFlatButton2.Hide();
                btnClear.Enabled = true;
                btnClear.Show();
                btnCancelar.Enabled = false;
                btnCancelar.Hide();
                lblTitle.Text = "Añadir nueva ubicación";
            }
            else
            {
                bunifuFlatButton1.Enabled = false;
                bunifuFlatButton1.Hide();
                bunifuFlatButton2.Enabled = true;
                bunifuFlatButton2.Show();
                btnClear.Enabled = false;
                btnClear.Hide();
                btnCancelar.Enabled = true;
                btnCancelar.Show();
                lblTitle.Text = "Editar ubicación (" + idEdit + ")";
                edit = idEdit;                
            }
		}

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clean();
        }

        private bool validation()
        {
            bool validation = true;
            if (Validations.isNotEmpty(txtUbication.Text))
            {
                ErrorUbication.SetError(txtUbication, "Este campo es obligatorio");
                lblUbication.ForeColor = Color.FromArgb(247, 93, 94);
                validation = false;
            }
            else
            {
                ErrorUbication.Clear();
                lblUbication.ForeColor = Color.FromName("ControlText");
            }

            return validation;
        }

		private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            if (!validation())
			{
                return;
			}


            ubications ubic = new ubications();
            ubic.ubication = txtUbication.Text;
			if (ubic.Insert())
			{
                alert1.changeAlertType(AlertType.Success);
                alert1.Message = String.Format("Se ha añadido la ubicación correctamente!!!");
                alert1.Title = "Ubicación: Equipos";
                alert1.Start();
                Clean();
			}
		}

		private void Clean()
        {
            txtUbication.Text = "";
            ErrorUbication.Clear();
            lblUbication.ForeColor = Color.FromName("ControlText");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            var parentForm = (IParentForm) this.Parent.FindForm();
            parentForm.openSecondFormInMultiTask(new Ubications.List());
        }

        private async void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            if (!validation())
            {
                return;
            }

            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            try
            {
                filled = Task.Run(() => UbicationController.find(token, edit), token);
                Hashtable ubication = await filled;
                ubications ubic = (ubications)ubication[edit];
                ubic.ubication = txtUbication.Text;
                ubic.Update(edit);
                var parentForm = (IParentForm)this.Parent.FindForm();
                parentForm.openSecondFormInMultiTask(new Ubications.List(1));
            }
            catch (OperationCanceledException)
            {

                filled.Dispose();
            }

        }

        private void tmrAlert_Tick(object sender, EventArgs e)
		{
			tmrAlert.Stop();
		}

		private async void Add_Load(object sender, EventArgs e)
		{
            if (edit != null)
            {
                tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                try
                {
                    filled = Task.Run(() => UbicationController.find(token, edit), token);
                    Hashtable ubication = await filled;
                    ubications ubic = (ubications)ubication[edit];
                    txtUbication.Text = ubic.ubication;
                }
                catch (OperationCanceledException)
                {

                    filled.Dispose();
                }
            }
        }
	}
}
