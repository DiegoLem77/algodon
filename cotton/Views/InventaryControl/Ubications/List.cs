﻿using System;
using System.Collections;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.General;
using Cotton.Models;
using Cotton.Controllers;
using Cotton.Controls;
using System.ComponentModel;

namespace Cotton.Views.InventaryControl.Ubications
{
	public partial class List : Form
	{

		// Indica si el fomulario está listo para usarse
		private Task<Hashtable> filled;
		private CancellationTokenSource tokenSource;
		// Carga fuente poppins
		private FontFamily Poppins, PoppinsL = null;
		public List(int action = 0)
		{
			InitializeComponent();
			Poppins = Utils.ImportFont(nameof(Properties.Resources.Poppins_Bold)).Families[0];
			PoppinsL = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
			lblTitle.Font = new Font(Poppins, lblTitle.Font.Size);
			lblTableTitle.Font = new Font(Utils.ImportFont(nameof(Properties.Resources.Poppins_Medium)).Families[0], lblTableTitle.Font.Size);
			alert1.Font = new Font(PoppinsL, alert1.Font.Size);
			alert1.FontSizeTitle = 12f;
			alert1.FontSizeBody = 10f;
            if (action == 1)
            {
                alert1.changeAlertType(AlertType.Success);
                alert1.Message = String.Format("Se ha actualizado la ubicación correctamente!!!");
                alert1.Title = "Ubicación: Equipos";
                alert1.Start();
            }

            if (action == 2)
            {
                alert1.changeAlertType(AlertType.Success);
                alert1.Message = String.Format("Se ha eliminado la ubicación correctamente!!!");
                alert1.Title = "Ubicación: Equipos";
                alert1.Start();
            }
        }

        private void fillDGV(Hashtable list)
        {
            ICollection keys = list.Keys;
            fadeOut.Hide(pnlLoading);
            foreach (string key in keys)
            {
                int n = dgvUbicacion.Rows.Add();
                ubications ubication = (ubications)list[key];
                dgvUbicacion.Rows[n].Cells["ubication"].Value = ubication.ubication;
                dgvUbicacion.Rows[n].Cells["id"].Value = ubication.Id;
            }
        }

		private async void List_Load(object sender, EventArgs e)
		{
			tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;
			try
			{
				filled = Task.Run(() => UbicationController.list(token), token);
				Hashtable ubications = await filled;
                fillDGV(ubications);
			}
			catch (OperationCanceledException)
			{

				filled.Dispose();
			}
		}

		private void List_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!filled.IsCompleted) tokenSource.Cancel();
			e.Cancel = false;

		}

		private async void btnSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            dgvUbicacion.Rows.Clear();
            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            try
            {
                filled = Task.Run(() => UbicationController.searchByUbication(token, btnSearch.Text.Trim()), token);
                Hashtable types = await filled;
                fillDGV(types);
            }
            catch (OperationCanceledException)
            {

                filled.Dispose();
            }

        }

        private void dgvEquipos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
			btnUpdate.Enabled = true;
			btnEliminar.Enabled = true;
		}

        private async void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            DialogResult result = new DialogResult();
            Form deleteModal = new DeleteModal("¿Desea eliminar la ubicación?");
            result = deleteModal.ShowDialog();

            if (result == DialogResult.OK)
            {
                tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                try
                {
                    filled = Task.Run(() => UbicationController.find(token, dgvUbicacion.Rows[dgvUbicacion.CurrentRow.Index].Cells["id"].Value.ToString()), token);
                    Hashtable ubication = await filled;
                    ubications ubic = (ubications)ubication[dgvUbicacion.Rows[dgvUbicacion.CurrentRow.Index].Cells["id"].Value.ToString()];
                    if (ubic.Delete())
                    {
                        var parentForm = (IParentForm)this.Parent.FindForm();
                        parentForm.openSecondFormInMultiTask(new Ubications.List(2));
                    } 
                    else
                    {
                        alert1.changeAlertType(AlertType.Danger);
                        alert1.Message = String.Format("No se pudo eliminar la ubicación, debe tener equipos asociados!!!");
                        alert1.Title = "Ubicación: Equipos";
                        alert1.Start();
                    }
                }
                catch (OperationCanceledException)
                {
                    filled.Dispose();
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var parentForm = (IParentForm)this.Parent.FindForm();
            parentForm.openSecondFormInMultiTask(new Ubications.Add(dgvUbicacion.Rows[dgvUbicacion.CurrentRow.Index].Cells["id"].Value.ToString()));
        }

		private void dgvEquipos_Sorted(object sender, EventArgs e)
		{
			drpSortColumn.SelectedIndex = dgvUbicacion.SortedColumn.Index;
			this.chkDesc.Checked = dgvUbicacion.SortOrder.Equals(SortOrder.Descending);
		}

		private void lblDesc_Click(object sender, EventArgs e)
		{
			this.chkDesc.Checked = !chkDesc.Checked;
		}
		private void chkDesc_OnChange(object sender, EventArgs e)
		{
            if (drpSortColumn.SelectedIndex > -1)
            {
                dgvUbicacion.Sort(dgvUbicacion.Columns[drpSortColumn.SelectedIndex],
                (chkDesc.Checked) ? ListSortDirection.Descending : ListSortDirection.Ascending);
            }
		}
	}
}
