﻿namespace Cotton.Views.InventaryControl.Ubications
{
    partial class Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splContainer = new System.Windows.Forms.SplitContainer();
            this.leftForm = new System.Windows.Forms.SplitContainer();
            this.lblSubtitle = new System.Windows.Forms.Label();
            this.lblTitleForm = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnClear = new System.Windows.Forms.Button();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtUbication = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lblUbication = new System.Windows.Forms.Label();
            this.alert1 = new Cotton.Controls.Alert();
            this.ErrorUbication = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrAlert = new System.Windows.Forms.Timer(this.components);
            this.fadeIn = new BunifuAnimatorNS.BunifuTransition(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splContainer)).BeginInit();
            this.splContainer.Panel1.SuspendLayout();
            this.splContainer.Panel2.SuspendLayout();
            this.splContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftForm)).BeginInit();
            this.leftForm.Panel1.SuspendLayout();
            this.leftForm.Panel2.SuspendLayout();
            this.leftForm.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorUbication)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.fadeIn.SetDecoration(this.splitContainer1, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.fadeIn.SetDecoration(this.splitContainer1.Panel1, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.fadeIn.SetDecoration(this.splitContainer1.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.splitContainer1.Size = new System.Drawing.Size(854, 450);
            this.splitContainer1.SplitterDistance = 72;
            this.splitContainer1.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.materialDivider1);
            this.flowLayoutPanel1.Controls.Add(this.lblTitle);
            this.fadeIn.SetDecoration(this.flowLayoutPanel1, BunifuAnimatorNS.DecorationType.None);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(20, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(834, 72);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(70)))), ((int)(((byte)(89)))));
            this.fadeIn.SetDecoration(this.materialDivider1, BunifuAnimatorNS.DecorationType.None);
            this.materialDivider1.Depth = 0;
            this.materialDivider1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.materialDivider1.Location = new System.Drawing.Point(10, 67);
            this.materialDivider1.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(131, 2);
            this.materialDivider1.TabIndex = 2;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.fadeIn.SetDecoration(this.lblTitle, BunifuAnimatorNS.DecorationType.None);
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(148)))), ((int)(((byte)(84)))));
            this.lblTitle.Location = new System.Drawing.Point(3, 39);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(237, 25);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Añadir nueva ubicación";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splContainer);
            this.fadeIn.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(20, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(834, 374);
            this.panel1.TabIndex = 4;
            // 
            // splContainer
            // 
            this.fadeIn.SetDecoration(this.splContainer, BunifuAnimatorNS.DecorationType.None);
            this.splContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splContainer.IsSplitterFixed = true;
            this.splContainer.Location = new System.Drawing.Point(0, 0);
            this.splContainer.Name = "splContainer";
            // 
            // splContainer.Panel1
            // 
            this.splContainer.Panel1.Controls.Add(this.leftForm);
            this.fadeIn.SetDecoration(this.splContainer.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // splContainer.Panel2
            // 
            this.splContainer.Panel2.Controls.Add(this.alert1);
            this.fadeIn.SetDecoration(this.splContainer.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.splContainer.Size = new System.Drawing.Size(834, 374);
            this.splContainer.SplitterDistance = 554;
            this.splContainer.SplitterWidth = 1;
            this.splContainer.TabIndex = 4;
            // 
            // leftForm
            // 
            this.fadeIn.SetDecoration(this.leftForm, BunifuAnimatorNS.DecorationType.None);
            this.leftForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftForm.IsSplitterFixed = true;
            this.leftForm.Location = new System.Drawing.Point(0, 0);
            this.leftForm.Name = "leftForm";
            this.leftForm.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // leftForm.Panel1
            // 
            this.leftForm.Panel1.Controls.Add(this.lblSubtitle);
            this.leftForm.Panel1.Controls.Add(this.lblTitleForm);
            this.fadeIn.SetDecoration(this.leftForm.Panel1, BunifuAnimatorNS.DecorationType.None);
            // 
            // leftForm.Panel2
            // 
            this.leftForm.Panel2.AutoScroll = true;
            this.leftForm.Panel2.Controls.Add(this.panel4);
            this.leftForm.Panel2.Controls.Add(this.panel2);
            this.fadeIn.SetDecoration(this.leftForm.Panel2, BunifuAnimatorNS.DecorationType.None);
            this.leftForm.Size = new System.Drawing.Size(554, 374);
            this.leftForm.SplitterDistance = 64;
            this.leftForm.TabIndex = 0;
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.AutoSize = true;
            this.fadeIn.SetDecoration(this.lblSubtitle, BunifuAnimatorNS.DecorationType.None);
            this.lblSubtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(93)))), ((int)(((byte)(94)))));
            this.lblSubtitle.Location = new System.Drawing.Point(6, 43);
            this.lblSubtitle.Name = "lblSubtitle";
            this.lblSubtitle.Size = new System.Drawing.Size(141, 16);
            this.lblSubtitle.TabIndex = 5;
            this.lblSubtitle.Text = "* Campos obligatorios";
            // 
            // lblTitleForm
            // 
            this.lblTitleForm.AutoSize = true;
            this.fadeIn.SetDecoration(this.lblTitleForm, BunifuAnimatorNS.DecorationType.None);
            this.lblTitleForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleForm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.lblTitleForm.Location = new System.Drawing.Point(6, 15);
            this.lblTitleForm.Name = "lblTitleForm";
            this.lblTitleForm.Size = new System.Drawing.Size(241, 20);
            this.lblTitleForm.TabIndex = 4;
            this.lblTitleForm.Text = "Complete los siguientes campos:";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnCancelar);
            this.panel4.Controls.Add(this.bunifuFlatButton2);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Controls.Add(this.bunifuFlatButton1);
            this.fadeIn.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 91);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(554, 54);
            this.panel4.TabIndex = 2;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeIn.SetDecoration(this.btnCancelar, BunifuAnimatorNS.DecorationType.None);
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.btnCancelar.Location = new System.Drawing.Point(227, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(151, 44);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "CANCELAR";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 0;
            this.bunifuFlatButton2.ButtonText = "ACTUALIZAR";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeIn.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton2.Iconimage")));
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = false;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = false;
            this.bunifuFlatButton2.IconZoom = 90D;
            this.bunifuFlatButton2.IsTab = false;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(385, 4);
            this.bunifuFlatButton2.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(165, 44);
            this.bunifuFlatButton2.TabIndex = 2;
            this.bunifuFlatButton2.Text = "ACTUALIZAR";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton2.Click += new System.EventHandler(this.bunifuFlatButton2_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeIn.SetDecoration(this.btnClear, BunifuAnimatorNS.DecorationType.None);
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.btnClear.Location = new System.Drawing.Point(227, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(151, 44);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "LIMPIAR";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "AGREGAR CATEGORÍA";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fadeIn.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Font = new System.Drawing.Font("Corbel", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton1.Iconimage")));
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = false;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = false;
            this.bunifuFlatButton1.IconZoom = 90D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(385, 4);
            this.bunifuFlatButton1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(165, 44);
            this.bunifuFlatButton1.TabIndex = 0;
            this.bunifuFlatButton1.Text = "AGREGAR CATEGORÍA";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Click += new System.EventHandler(this.bunifuFlatButton1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtUbication);
            this.panel2.Controls.Add(this.lblUbication);
            this.fadeIn.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
            this.panel2.Size = new System.Drawing.Size(554, 91);
            this.panel2.TabIndex = 0;
            // 
            // txtUbication
            // 
            this.txtUbication.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fadeIn.SetDecoration(this.txtUbication, BunifuAnimatorNS.DecorationType.None);
            this.txtUbication.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtUbication.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtUbication.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtUbication.HintForeColor = System.Drawing.Color.Empty;
            this.txtUbication.HintText = "Digite una ubicación de almacén de equipos";
            this.txtUbication.isPassword = false;
            this.txtUbication.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtUbication.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
            this.txtUbication.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(212)))), ((int)(((byte)(152)))));
            this.txtUbication.LineThickness = 3;
            this.txtUbication.Location = new System.Drawing.Point(15, 46);
            this.txtUbication.Margin = new System.Windows.Forms.Padding(4);
            this.txtUbication.Name = "txtUbication";
            this.txtUbication.Size = new System.Drawing.Size(524, 33);
            this.txtUbication.TabIndex = 0;
            this.txtUbication.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // lblUbication
            // 
            this.fadeIn.SetDecoration(this.lblUbication, BunifuAnimatorNS.DecorationType.None);
            this.lblUbication.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUbication.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUbication.Location = new System.Drawing.Point(15, 12);
            this.lblUbication.Name = "lblUbication";
            this.lblUbication.Size = new System.Drawing.Size(524, 28);
            this.lblUbication.TabIndex = 1;
            this.lblUbication.Text = "Ubicación*:";
            // 
            // alert1
            // 
            this.alert1.AlertType = Cotton.Controls.AlertType.Danger;
            this.alert1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.alert1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fadeIn.SetDecoration(this.alert1, BunifuAnimatorNS.DecorationType.None);
            this.alert1.FontSizeBody = 9.75F;
            this.alert1.FontSizeTitle = 12F;
            this.alert1.Location = new System.Drawing.Point(8, 4);
            this.alert1.MaximumSize = new System.Drawing.Size(400, 10000);
            this.alert1.Message = resources.GetString("alert1.Message");
            this.alert1.Name = "alert1";
            this.alert1.Size = new System.Drawing.Size(277, 104);
            this.alert1.TabIndex = 0;
            this.alert1.TimeLapse = 5000;
            this.alert1.Title = "Message Title Alert";
            this.alert1.Visible = false;
            // 
            // ErrorUbication
            // 
            this.ErrorUbication.ContainerControl = this;
            // 
            // tmrAlert
            // 
            this.tmrAlert.Tick += new System.EventHandler(this.tmrAlert_Tick);
            // 
            // fadeIn
            // 
            this.fadeIn.AnimationType = BunifuAnimatorNS.AnimationType.Transparent;
            this.fadeIn.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 1F;
            this.fadeIn.DefaultAnimation = animation1;
            // 
            // Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(854, 450);
            this.Controls.Add(this.splitContainer1);
            this.fadeIn.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Name = "Add";
            this.Text = "Add";
            this.Load += new System.EventHandler(this.Add_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splContainer.Panel1.ResumeLayout(false);
            this.splContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splContainer)).EndInit();
            this.splContainer.ResumeLayout(false);
            this.leftForm.Panel1.ResumeLayout(false);
            this.leftForm.Panel1.PerformLayout();
            this.leftForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leftForm)).EndInit();
            this.leftForm.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorUbication)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.Label lblTitle;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtUbication;
        private System.Windows.Forms.Label lblUbication;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splContainer;
        private System.Windows.Forms.SplitContainer leftForm;
        private System.Windows.Forms.Label lblTitleForm;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnClear;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private System.Windows.Forms.ErrorProvider ErrorUbication;
        private System.Windows.Forms.Timer tmrAlert;
        private BunifuAnimatorNS.BunifuTransition fadeIn;
        private System.Windows.Forms.Label lblSubtitle;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
        private System.Windows.Forms.Button btnCancelar;
        private Controls.Alert alert1;
    }
}