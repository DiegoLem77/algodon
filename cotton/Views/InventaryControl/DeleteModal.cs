﻿using Cotton.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cotton.Views.InventaryControl
{
    public partial class DeleteModal : Form
    {
        private FontFamily Poppins, PoppinsL = null;
        public DeleteModal(string message)
        {
            InitializeComponent();
            Poppins = Utils.ImportFont(nameof(Properties.Resources.Poppins_Bold)).Families[0];
            PoppinsL = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
            lblMessage.Font = new Font(Poppins, lblMessage.Font.Size);
            lblMessage.Text = message;

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
