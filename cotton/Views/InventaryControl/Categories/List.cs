﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.General;
using Cotton.Models;
using Cotton.Controllers;
using Cotton.Controls;

namespace Cotton.Views.InventaryControl.Categories
{
	public partial class List : Form
	{

		// Indica si el fomulario está listo para usarse
		private Task<Hashtable> filled;
		private CancellationTokenSource tokenSource;
		// Carga fuente poppins
		private FontFamily Poppins, PoppinsL, PoppinsM = null;
		public List(int action = 0)
		{
			InitializeComponent();
			Poppins = Utils.ImportFont(nameof(Properties.Resources.Poppins_Bold)).Families[0];
			PoppinsL = Utils.ImportFont(nameof(Properties.Resources.Poppins_Light)).Families[0];
			PoppinsM = Utils.ImportFont(nameof(Properties.Resources.Montserrat_Medium)).Families[0];
			lblTitle.Font = new Font(Poppins, lblTitle.Font.Size);
			lblTableTitle.Font = new Font(Utils.ImportFont(nameof(Properties.Resources.Poppins_Medium)).Families[0], lblTableTitle.Font.Size);
			alert1.Font = new Font(PoppinsL, alert1.Font.Size);
			alert1.FontSizeTitle = 12f;
			alert1.FontSizeBody = 10f;
			grbControls.Font = new Font(PoppinsM, lblTableTitle.Font.Size);
			lblSortOption.Font = new Font(PoppinsM, lblSortOption.Font.Size);
			drpSortColumn.Font = new Font(PoppinsM, drpSortColumn.Font.Size);
			if (action == 1)
            {
                alert1.changeAlertType(AlertType.Success);
                alert1.Message = String.Format("Se ha actualizado la categoria correctamente!!!");
                alert1.Title = "Categoria: Equipos";
                alert1.Start();
            }
            else if (action == 2)
            {
                alert1.changeAlertType(AlertType.Success);
                alert1.Message = String.Format("Se ha eliminado la categoria correctamente!!!");
                alert1.Title = "Categoria: Equipos";
                alert1.Start();
            }
        }

        private void fillDGV(Hashtable list)
        {
            ICollection keys = list.Keys;
            fadeOut.Hide(pnlLoading);
            foreach (string key in keys)
            {
                int n = dgvEquipos.Rows.Add();
                Category category = (Category)list[key];
                dgvEquipos.Rows[n].Cells["name"].Value = category.Name;
                dgvEquipos.Rows[n].Cells["description"].Value = category.Description;
                dgvEquipos.Rows[n].Cells["id"].Value = category.Id;
            }
        }

		private async void List_Load(object sender, EventArgs e)
		{
			tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;
			try
			{
				filled = Task.Run(() => EquipmentTypeController.list(token), token);
				Hashtable types = await filled;
                fillDGV(types);
				foreach(DataGridViewColumn column in dgvEquipos.Columns)
				{
					this.drpSortColumn.Items.Add(column.HeaderText);
				}
			}
			catch (OperationCanceledException)
			{

				filled.Dispose();
			}
		}

		private void List_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!filled.IsCompleted) tokenSource.Cancel();
			e.Cancel = false;

		}

		private async void btnSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            dgvEquipos.Rows.Clear();
            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            try
            {
                filled = Task.Run(() => EquipmentTypeController.searchByName(token, btnSearch.Text.Trim()), token);
                Hashtable types = await filled;
                fillDGV(types);
            }
            catch (OperationCanceledException)
            {

                filled.Dispose();
            }

        }

        private void dgvEquipos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
			btnDelete.Enabled = true;
        }

        private async void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult result = new DialogResult();
            Form deleteModal = new DeleteModal("¿Desea eliminar la categoría?");
            result = deleteModal.ShowDialog();

            if (result == DialogResult.OK)
            {
                tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                try
                {
                    filled = Task.Run(() => EquipmentTypeController.find(token, dgvEquipos.Rows[dgvEquipos.CurrentRow.Index].Cells["id"].Value.ToString()), token);
                    Hashtable type = await filled;
                    Category t = (Category)type[dgvEquipos.Rows[dgvEquipos.CurrentRow.Index].Cells["id"].Value.ToString()];
                    if (t.Delete())
                    {
                        var parentForm = (IParentForm)this.Parent.FindForm();
                        parentForm.openSecondFormInMultiTask(new Categories.List(2));
                    }
                    else
                    {
                        alert1.changeAlertType(AlertType.Danger);
                        alert1.Message = String.Format("No se pudo eliminar la categoria, debe tener equipos asociados!!!");
                        alert1.Title = "Categoría: Equipos";
                        alert1.Start();
                    }
                }
                catch (OperationCanceledException)
                {
                    filled.Dispose();
                }
            }
        }

		private void dgvEquipos_Sorted(object sender, EventArgs e)
		{
			drpSortColumn.SelectedIndex = dgvEquipos.SortedColumn.Index;
			this.chkDesc.Checked = dgvEquipos.SortOrder.Equals(SortOrder.Descending);
		}

		private void lblDesc_Click(object sender, EventArgs e)
		{
			this.chkDesc.Checked = !chkDesc.Checked;
		}

		private void chkDesc_OnChange(object sender, EventArgs e)
		{
			dgvEquipos.Sort(dgvEquipos.Columns[drpSortColumn.SelectedIndex], 
				(chkDesc.Checked) ? ListSortDirection.Descending : ListSortDirection.Ascending);
		}


		private void btnUpdate_Click(object sender, EventArgs e)
        {
            var parentForm = (IParentForm)this.Parent.FindForm();
            parentForm.openSecondFormInMultiTask(new Categories.Add(dgvEquipos.Rows[dgvEquipos.CurrentRow.Index].Cells["id"].Value.ToString()));
        }
	}
}
