﻿namespace Cotton.Views.InventaryControl
{
	partial class ReportLoans
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnAceptar = new System.Windows.Forms.Button();
			this.btnCancelar = new System.Windows.Forms.Button();
			this.lblMessage = new System.Windows.Forms.Label();
			this.iconAlert = new FontAwesome.Sharp.IconPictureBox();
			this.panel12 = new System.Windows.Forms.Panel();
			this.panel13 = new System.Windows.Forms.Panel();
			this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
			this.nudQuantityEquipment = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.bunifuSeparator6 = new Bunifu.Framework.UI.BunifuSeparator();
			this.label6 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.iconAlert)).BeginInit();
			this.panel12.SuspendLayout();
			this.panel13.SuspendLayout();
			this.flowLayoutPanel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudQuantityEquipment)).BeginInit();
			this.SuspendLayout();
			// 
			// btnAceptar
			// 
			this.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAceptar.BackColor = System.Drawing.Color.Transparent;
			this.btnAceptar.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnAceptar.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
			this.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAceptar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAceptar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
			this.btnAceptar.Location = new System.Drawing.Point(288, 214);
			this.btnAceptar.Name = "btnAceptar";
			this.btnAceptar.Size = new System.Drawing.Size(151, 44);
			this.btnAceptar.TabIndex = 4;
			this.btnAceptar.Text = "ACEPTAR";
			this.btnAceptar.UseVisualStyleBackColor = false;
			this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
			// 
			// btnCancelar
			// 
			this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancelar.BackColor = System.Drawing.Color.Transparent;
			this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
			this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancelar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCancelar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(67)))), ((int)(((byte)(220)))));
			this.btnCancelar.Location = new System.Drawing.Point(131, 214);
			this.btnCancelar.Name = "btnCancelar";
			this.btnCancelar.Size = new System.Drawing.Size(151, 44);
			this.btnCancelar.TabIndex = 5;
			this.btnCancelar.Text = "CANCELAR";
			this.btnCancelar.UseVisualStyleBackColor = false;
			this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
			// 
			// lblMessage
			// 
			this.lblMessage.Font = new System.Drawing.Font("Bahnschrift SemiBold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMessage.ForeColor = System.Drawing.Color.DarkCyan;
			this.lblMessage.Location = new System.Drawing.Point(86, 12);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(367, 58);
			this.lblMessage.TabIndex = 7;
			this.lblMessage.Text = "¿Cuántos equipos desea reportar como perdidos o dañados?";
			// 
			// iconAlert
			// 
			this.iconAlert.BackColor = System.Drawing.Color.Transparent;
			this.iconAlert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.iconAlert.ForeColor = System.Drawing.Color.DarkCyan;
			this.iconAlert.IconChar = FontAwesome.Sharp.IconChar.InfoCircle;
			this.iconAlert.IconColor = System.Drawing.Color.DarkCyan;
			this.iconAlert.IconSize = 57;
			this.iconAlert.Location = new System.Drawing.Point(12, 12);
			this.iconAlert.Name = "iconAlert";
			this.iconAlert.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
			this.iconAlert.Size = new System.Drawing.Size(57, 73);
			this.iconAlert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.iconAlert.TabIndex = 8;
			this.iconAlert.TabStop = false;
			// 
			// panel12
			// 
			this.panel12.Controls.Add(this.panel13);
			this.panel12.Controls.Add(this.label6);
			this.panel12.Location = new System.Drawing.Point(89, 75);
			this.panel12.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.panel12.Name = "panel12";
			this.panel12.Padding = new System.Windows.Forms.Padding(15, 12, 15, 12);
			this.panel12.Size = new System.Drawing.Size(350, 131);
			this.panel12.TabIndex = 9;
			// 
			// panel13
			// 
			this.panel13.Controls.Add(this.flowLayoutPanel3);
			this.panel13.Controls.Add(this.bunifuSeparator6);
			this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel13.Location = new System.Drawing.Point(15, 40);
			this.panel13.Name = "panel13";
			this.panel13.Size = new System.Drawing.Size(320, 79);
			this.panel13.TabIndex = 2;
			// 
			// flowLayoutPanel3
			// 
			this.flowLayoutPanel3.Controls.Add(this.nudQuantityEquipment);
			this.flowLayoutPanel3.Controls.Add(this.label3);
			this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
			this.flowLayoutPanel3.Name = "flowLayoutPanel3";
			this.flowLayoutPanel3.Size = new System.Drawing.Size(320, 44);
			this.flowLayoutPanel3.TabIndex = 3;
			// 
			// nudQuantityEquipment
			// 
			this.nudQuantityEquipment.BackColor = System.Drawing.Color.White;
			this.nudQuantityEquipment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.nudQuantityEquipment.Dock = System.Windows.Forms.DockStyle.Left;
			this.nudQuantityEquipment.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudQuantityEquipment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(147)))), ((int)(((byte)(111)))));
			this.nudQuantityEquipment.Location = new System.Drawing.Point(3, 3);
			this.nudQuantityEquipment.Name = "nudQuantityEquipment";
			this.nudQuantityEquipment.Size = new System.Drawing.Size(236, 23);
			this.nudQuantityEquipment.TabIndex = 2;
			this.nudQuantityEquipment.ThousandsSeparator = true;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.DimGray;
			this.label3.Location = new System.Drawing.Point(245, 5);
			this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(62, 16);
			this.label3.TabIndex = 3;
			this.label3.Text = "Unidades";
			// 
			// bunifuSeparator6
			// 
			this.bunifuSeparator6.BackColor = System.Drawing.Color.Transparent;
			this.bunifuSeparator6.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.bunifuSeparator6.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(139)))), ((int)(((byte)(139)))));
			this.bunifuSeparator6.LineThickness = 3;
			this.bunifuSeparator6.Location = new System.Drawing.Point(0, 44);
			this.bunifuSeparator6.Margin = new System.Windows.Forms.Padding(4);
			this.bunifuSeparator6.Name = "bunifuSeparator6";
			this.bunifuSeparator6.Size = new System.Drawing.Size(320, 35);
			this.bunifuSeparator6.TabIndex = 2;
			this.bunifuSeparator6.Transparency = 255;
			this.bunifuSeparator6.Vertical = false;
			// 
			// label6
			// 
			this.label6.Dock = System.Windows.Forms.DockStyle.Top;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.ForeColor = System.Drawing.Color.DarkCyan;
			this.label6.Location = new System.Drawing.Point(15, 12);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(320, 28);
			this.label6.TabIndex = 1;
			this.label6.Text = "Cantidad a reportar:";
			// 
			// ReportLoans
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(451, 270);
			this.ControlBox = false;
			this.Controls.Add(this.panel12);
			this.Controls.Add(this.iconAlert);
			this.Controls.Add(this.lblMessage);
			this.Controls.Add(this.btnCancelar);
			this.Controls.Add(this.btnAceptar);
			this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ReportLoans";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.ReportLoans_Load);
			((System.ComponentModel.ISupportInitialize)(this.iconAlert)).EndInit();
			this.panel12.ResumeLayout(false);
			this.panel13.ResumeLayout(false);
			this.flowLayoutPanel3.ResumeLayout(false);
			this.flowLayoutPanel3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudQuantityEquipment)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnAceptar;
		private System.Windows.Forms.Button btnCancelar;
		private System.Windows.Forms.Label lblMessage;
		private FontAwesome.Sharp.IconPictureBox iconAlert;
		private System.Windows.Forms.Panel panel12;
		private System.Windows.Forms.Panel panel13;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
		private System.Windows.Forms.NumericUpDown nudQuantityEquipment;
		private System.Windows.Forms.Label label3;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator6;
		private System.Windows.Forms.Label label6;
	}
}