﻿namespace Cotton.Views
{
    partial class ForgotPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ForgotPassword));
			this.BtnRecover = new System.Windows.Forms.Button();
			this.BtnCancel = new System.Windows.Forms.Button();
			this.LblEmail = new System.Windows.Forms.Label();
			this.TmrAlert = new System.Windows.Forms.Timer(this.components);
			this.FadeIn = new BunifuAnimatorNS.BunifuTransition(this.components);
			this.TxtEmail = new Cotton.Controls.MaterialTextBox();
			this.FrmAlert = new Cotton.Controls.Alert();
			this.errorMessage = new Cotton.Controls.ErrorMessage();
			this.SuspendLayout();
			// 
			// BtnRecover
			// 
			this.BtnRecover.BackColor = System.Drawing.Color.SeaGreen;
			this.FadeIn.SetDecoration(this.BtnRecover, BunifuAnimatorNS.DecorationType.None);
			this.BtnRecover.FlatAppearance.BorderSize = 0;
			this.BtnRecover.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
			this.BtnRecover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.BtnRecover.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
			this.BtnRecover.ForeColor = System.Drawing.Color.White;
			this.BtnRecover.Location = new System.Drawing.Point(32, 147);
			this.BtnRecover.Name = "BtnRecover";
			this.BtnRecover.Size = new System.Drawing.Size(169, 44);
			this.BtnRecover.TabIndex = 9;
			this.BtnRecover.Text = "Recuperar contraseña";
			this.BtnRecover.UseVisualStyleBackColor = false;
			this.BtnRecover.Click += new System.EventHandler(this.BtnRecover_Click);
			// 
			// BtnCancel
			// 
			this.BtnCancel.BackColor = System.Drawing.Color.Red;
			this.FadeIn.SetDecoration(this.BtnCancel, BunifuAnimatorNS.DecorationType.None);
			this.BtnCancel.FlatAppearance.BorderSize = 0;
			this.BtnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
			this.BtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.BtnCancel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
			this.BtnCancel.ForeColor = System.Drawing.Color.White;
			this.BtnCancel.Location = new System.Drawing.Point(236, 147);
			this.BtnCancel.Name = "BtnCancel";
			this.BtnCancel.Size = new System.Drawing.Size(118, 44);
			this.BtnCancel.TabIndex = 10;
			this.BtnCancel.Text = "Cancelar";
			this.BtnCancel.UseVisualStyleBackColor = false;
			this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
			// 
			// LblEmail
			// 
			this.LblEmail.AutoSize = true;
			this.FadeIn.SetDecoration(this.LblEmail, BunifuAnimatorNS.DecorationType.None);
			this.LblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblEmail.ForeColor = System.Drawing.Color.SeaGreen;
			this.LblEmail.Location = new System.Drawing.Point(30, 29);
			this.LblEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.LblEmail.Name = "LblEmail";
			this.LblEmail.Size = new System.Drawing.Size(53, 20);
			this.LblEmail.TabIndex = 12;
			this.LblEmail.Text = "Email";
			// 
			// TmrAlert
			// 
			this.TmrAlert.Tick += new System.EventHandler(this.TmrAlert_Tick);
			// 
			// FadeIn
			// 
			this.FadeIn.AnimationType = BunifuAnimatorNS.AnimationType.Transparent;
			this.FadeIn.Cursor = null;
			animation1.AnimateOnlyDifferences = true;
			animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
			animation1.LeafCoeff = 0F;
			animation1.MaxTime = 1F;
			animation1.MinTime = 0F;
			animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
			animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
			animation1.MosaicSize = 0;
			animation1.Padding = new System.Windows.Forms.Padding(0);
			animation1.RotateCoeff = 0F;
			animation1.RotateLimit = 0F;
			animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
			animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
			animation1.TimeCoeff = 0F;
			animation1.TransparencyCoeff = 1F;
			this.FadeIn.DefaultAnimation = animation1;
			// 
			// TxtEmail
			// 
			this.TxtEmail.BackColor = System.Drawing.SystemColors.Window;
			this.TxtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.FadeIn.SetDecoration(this.TxtEmail, BunifuAnimatorNS.DecorationType.None);
			this.TxtEmail.Location = new System.Drawing.Point(32, 58);
			this.TxtEmail.Margin = new System.Windows.Forms.Padding(4);
			this.TxtEmail.Name = "TxtEmail";
			this.TxtEmail.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
			this.TxtEmail.PasswordChar = '\0';
			this.TxtEmail.Size = new System.Drawing.Size(322, 32);
			this.TxtEmail.TabIndex = 11;
			// 
			// FrmAlert
			// 
			this.FrmAlert.AlertType = Cotton.Controls.AlertType.Danger;
			this.FrmAlert.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FrmAlert.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.FadeIn.SetDecoration(this.FrmAlert, BunifuAnimatorNS.DecorationType.None);
			this.FrmAlert.FontSizeBody = 9.75F;
			this.FrmAlert.FontSizeTitle = 12F;
			this.FrmAlert.Location = new System.Drawing.Point(0, 0);
			this.FrmAlert.MaximumSize = new System.Drawing.Size(400, 10000);
			this.FrmAlert.Message = "";
			this.FrmAlert.Name = "FrmAlert";
			this.FrmAlert.Size = new System.Drawing.Size(259, 104);
			this.FrmAlert.TabIndex = 0;
			this.FrmAlert.TimeLapse = 5000;
			this.FrmAlert.Title = "Message Title Alert";
			this.FrmAlert.Visible = false;
			// 
			// errorMessage
			// 
			this.errorMessage.BackColor = System.Drawing.Color.Transparent;
			this.FadeIn.SetDecoration(this.errorMessage, BunifuAnimatorNS.DecorationType.None);
			this.errorMessage.Location = new System.Drawing.Point(32, 87);
			this.errorMessage.Name = "errorMessage";
			this.errorMessage.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
			this.errorMessage.Size = new System.Drawing.Size(322, 48);
			this.errorMessage.TabIndex = 13;
			this.errorMessage.Text = "errorMessage1";
			this.errorMessage.Visible = false;
			// 
			// ForgotPassword
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(400, 216);
			this.Controls.Add(this.LblEmail);
			this.Controls.Add(this.TxtEmail);
			this.Controls.Add(this.BtnCancel);
			this.Controls.Add(this.BtnRecover);
			this.Controls.Add(this.errorMessage);
			this.FadeIn.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "ForgotPassword";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ForgotPassword";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnRecover;
        private System.Windows.Forms.Button BtnCancel;
        private Controls.MaterialTextBox TxtEmail;
        private System.Windows.Forms.Label LblEmail;
        private Controls.Alert FrmAlert;
        private System.Windows.Forms.Timer TmrAlert;
        private BunifuAnimatorNS.BunifuTransition FadeIn;
		private Controls.ErrorMessage errorMessage;
	}
}