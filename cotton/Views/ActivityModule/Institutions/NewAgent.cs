﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Controllers;
using Cotton.Models;
using Bunifu.Framework.UI;
using System.Text.RegularExpressions;

namespace Cotton.Views.ActivityModule.Institutions
{
    public partial class NewAgent : Form
    {
        private Institution institution = null;
        private List<User> userList = new List<User>();
        private UserController userController = null;
        private Assign frmAssign;

        public NewAgent(Institution institution, Assign frmAssign)
        {
            InitializeComponent();
            this.institution = institution;
            this.frmAssign = frmAssign;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private async void BunifuFlatButton1_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider1.Clear();
                Boolean flag = true;
                Regex regexMail = new Regex(@"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
                Regex regexDUI = new Regex("[0-9]{9}");

                validateEmpty(txtName, "El nombre es requerido", ref flag);
                validateEmpty(txtLast, "El apellido es requerido", ref flag);
                validateEmpty(txtAddress, "La direccion es requerida", ref flag);
                validateEmpty(txtCorreo, "El correo es requerido", ref flag);

                validateRegex(txtCorreo, "Formato de correo incorrecto", regexMail, ref flag);

                foreach (User user in userList)
                {
                    if (user.Email == txtCorreo.Text.Trim())
                    {
                        errorProvider1.SetError(txtCorreo, "Correo ya regstrado con otro usuario");
                        flag = false;
                        break;
                    }
                }

                if (cmbSex.selectedIndex == -1)
                {
                    flag = false;
                    errorProvider1.SetError(cmbSex, "El sexo es requerido");
                }

                if (flag)
                {
                    enable(false);
                    User user = new User();
                    user.Name = txtName.Text.Trim();
                    user.Lastname = txtLast.Text.Trim();
                    user.Gender = (cmbSex.selectedIndex == 0) ? "M" : "F";
                    user.Email = txtCorreo.Text.Trim();
                    user.Adress = txtAddress.Text.Trim();
                    user.StatusId = 1;
                    await Task.Run(() => this.userController.storeAgent(user, this.institution.Id));
                    this.frmAssign.fill();
                    this.Hide();
                    MessageBox.Show("Representante creado y asignado", "Realizado");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            enable(true);
        }

        private void validateEmpty(BunifuMaterialTextbox txt, String message, ref Boolean flag)
        {
            if (txt.Text.Trim() == "")
            {
                flag = false;
                errorProvider1.SetError(txt, message);
            }
        }

        private void validateRegex(BunifuMaterialTextbox txt, String message, Regex regex, ref Boolean flag)
        {
            if (!regex.IsMatch(txtCorreo.Text.Trim()))
            {
                flag = false;
                errorProvider1.SetError(txt, message);
            }
        }

        private async void NewAgent_Load(object sender, EventArgs e)
        {
            enable(false);
            this.userController = new UserController();
            userList = await Task.Run(() => userController.All());
            enable(true);
        }

        private void enable(bool status)
        {
            pictureBox1.Visible = !status;
            txtAddress.Enabled = status;
            txtCorreo.Enabled = status;
            txtLast.Enabled = status;
            txtName.Enabled = status;
            cmbSex.Enabled = status;
            bunifuFlatButton1.Enabled = status;
            button1.Enabled = status;
        }
    }
}
