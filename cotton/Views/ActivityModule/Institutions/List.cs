﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Controllers;
using Cotton.Models;
using Cotton.PDF;

namespace Cotton.Views.ActivityModule.Institutions
{
    public partial class List : Form
    {
        private InstitutionController controller = new InstitutionController();
        private List<Institution> institutionList = new List<Institution>();

        public List()
        {
            InitializeComponent();
        }

        private void DataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Institution inst = (Institution)dataGrid.Rows[e.RowIndex].DataBoundItem;
                if (e.ColumnIndex == dataGrid.Columns["AssignButton"].Index)
                {
                    Assign frmAssign = new Assign(inst);
                    frmAssign.Show();
                }
                else if (e.ColumnIndex == dataGrid.Columns["DeleteButton"].Index)
                {
                    if (MessageBox.Show("Eliminar " + inst.Name, "Confirmación", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        inst.Delete();
                        fill();
                    }
                }
                else if (e.ColumnIndex == dataGrid.Columns["AgentButton"].Index)
                {
                    Agent frmAgent = new Agent(inst);
                    frmAgent.Show();
                } else if (e.ColumnIndex == dataGrid.Columns["UpdateButton"].Index)
                {
                    Edit frmEdit = new Edit(inst, this);
                    frmEdit.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void TxtSearch_OnValueChanged(object sender, EventArgs e)
        {
            String val = txtSearch.Text.Trim().ToLower();
            if (val != "")
            {
                var query = from institution in institutionList
                            where institution.Email.ToLower().Contains(val) || institution.Name.Trim().ToLower().Contains(val) || institution.Adress.Trim().ToLower().Contains(val)
                            select institution;
                dataGrid.DataSource = query.ToList();
            }
            else
            {
                dataGrid.DataSource = institutionList;
            }
        }

        public void List_Load(object sender, EventArgs e)
        {
            fill();
        }

        public async void fill()
        {
            dataGrid.Columns.Clear();
            pcbLoad.Visible = true;
            institutionList = await Task.Run(() => controller.All());

            dataGrid.DataSource = institutionList;

            dataGrid.AutoGenerateColumns = false;
            dataGrid.Columns["Id"].Visible = false;
            dataGrid.Columns["CreatedAt"].Visible = false;
            dataGrid.Columns["UpdatedAt"].Visible = false;
            dataGrid.Columns["StatusId"].Visible = false;

            dataGrid.Columns.Add(createButton("AgentButton", "Representantes"));
            dataGrid.Columns.Add(createButton("AssignButton", "Vincular"));
            dataGrid.Columns.Add(createButton("UpdateButton", "Actualizar"));
            dataGrid.Columns.Add(createButton("DeleteButton", "Eliminar"));
            pcbLoad.Visible = false;
        }

        private DataGridViewButtonColumn createButton(String name, String text)
        {
            DataGridViewButtonColumn button = new DataGridViewButtonColumn();
            button.Name = name;
            button.HeaderText = text;
            button.Text = text;
            button.UseColumnTextForButtonValue = true;
            return button;
        }

        private void GenPDF(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PDF.Institutions.GeneratePDF(fbd.SelectedPath);
                MessageBox.Show("El PDF ha sido generado correctamente!", "Documento generado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
