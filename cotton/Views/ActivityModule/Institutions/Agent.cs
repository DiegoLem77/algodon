﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Models;
using Cotton.Controllers;

namespace Cotton.Views.ActivityModule.Institutions
{
    public partial class Agent : Form
    {
        private Institution institution = null;
        private List<UserActivity> userList = new List<UserActivity>();
        public Agent(Institution institution)
        {
            InitializeComponent();
            this.institution = institution;

        }

        private void DataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGrid.Columns["DeleteButton"].Index)
            {
                UserActivity user = (UserActivity)dataGrid.Rows[e.RowIndex].DataBoundItem;
                if (MessageBox.Show("Seguro desea eliminar a: " + user.Name + " " + user.LastName + " de los representantes?", "Confirmacion", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    InstitutionUserController.delete(user.Id);
                    fill();
                    MessageBox.Show("Representante eliminado", "Realizado");
                }
            }
        }

        private void BunifuMaterialTextbox1_OnValueChanged(object sender, EventArgs e)
        {
            String val = txtSearch.Text.Trim().ToLower();
            if (val != "")
            {
                var query = from user in userList
                            where user.Name.Trim().ToLower().Contains(val) || user.LastName.Trim().ToLower().Contains(val) || user.DUI.Trim().ToLower().Contains(val)
                            select user;
                dataGrid.DataSource = query.ToList();
            }
            else
            {
                dataGrid.DataSource = userList;
            }
        }

        private void Agent_Load(object sender, EventArgs e)
        {
            fill();
        }

        private async void fill()
        {
            dataGrid.Columns.Clear();
            pictureBox1.Visible = true;
            userList = await Task.Run(() => InstitutionUserController.getLinked(institution.Id));
            dataGrid.DataSource = userList;
            dataGrid.Columns["Id"].Visible = false;
            dataGrid.Columns["Iu"].Visible = false;
            dataGrid.Columns["Institution"].Visible = false;
            dataGrid.Columns["essentialInformation"].Visible = false;
            dataGrid.Columns["LastName"].HeaderText = "Last name";

            DataGridViewButtonColumn button = new DataGridViewButtonColumn();
            button.Name = "DeleteButton";
            button.HeaderText = "Eliminar";
            button.Text = "Eliminar";
            button.UseColumnTextForButtonValue = true;
            dataGrid.Columns.Add(button);
            pictureBox1.Visible = false;
        }
    }
}
