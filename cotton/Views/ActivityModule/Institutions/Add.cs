﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Bunifu.Framework.UI;
using Cotton.Controllers;
using Cotton.Models;

namespace Cotton.Views.ActivityModule.Institutions
{
    public partial class Add : Form
    {
        private List<Institution> institutionList = new List<Institution>();
        public Add()
        {
            InitializeComponent();
        }

        private void clear()
        {
            txtAddress.Text = "";
            txtEmail.Text = "";
            txtName.Text = "";
            txtName.Focus();
        }
        private void BtnClean_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void validateEmpty(BunifuMaterialTextbox txt, String message, ref Boolean flag)
        {
            if (txt.Text.Trim() == "")
            {
                flag = false;
                errorProvider1.SetError(txt, message);
            }
        }

        private async void BtnSave_Click(object sender, EventArgs e)
        {
            enable(false);
            try
            {
                errorProvider1.Clear();
                Boolean flag = true;
                Regex regex = new Regex(@"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");

                validateEmpty(txtName, "El nombre es necesario", ref flag);
                validateEmpty(txtAddress, "La dirección es necesara", ref flag);
                validateEmpty(txtEmail, "El correo es necesario", ref flag);

                if (!regex.IsMatch(txtEmail.Text.Trim()))
                {
                    errorProvider1.SetError(txtEmail, "Formato incorrecto");
                    flag = false;
                }

                foreach(Institution institution in institutionList)
                {
                    if (txtEmail.Text.Trim() == institution.Email)
                    {
                        flag = false;
                        errorProvider1.SetError(txtEmail, "Correo ya registrado con otra institución");
                        break;
                    }
                }

                if (flag)
                {
                    Institution institution = new Institution();
                    institution.Name = txtName.Text.Trim();
                    institution.Email = txtEmail.Text.Trim();
                    institution.Adress = txtAddress.Text.Trim();
                    institution.StatusId = 1;

                    InstitutionController controller = new InstitutionController();
                    await Task.Run(() => controller.Store(institution));

                    this.clear();
                    MessageBox.Show("Institución guardada", "Realizado");
                }
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            enable(true);
        }
    
        private void enable(bool status)
        {
            pictureBox1.Visible = !status;
            txtAddress.Enabled = status;
            txtName.Enabled = status;
            txtEmail.Enabled = status;
            btnClean.Enabled = status;
            btnSave.Enabled = status;
        }

        private async void Add_Load(object sender, EventArgs e)
        {
            enable(false);
            InstitutionController controller = new InstitutionController();
            await Task.Run(() => institutionList = controller.All());
            enable(true);
        }
    }
}
