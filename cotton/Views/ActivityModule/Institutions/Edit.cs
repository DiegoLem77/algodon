﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Models;
using Cotton.Controllers;
using System.Text.RegularExpressions;
using Bunifu.Framework.UI;

namespace Cotton.Views.ActivityModule.Institutions
{
    public partial class Edit : Form
    {
        private Institution institution = new Institution();
        private List frmList;
        public Edit(Institution institution, List origin)
        {
            InitializeComponent();
            frmList = origin;
            this.institution = institution;
            txtName.Text = institution.Name;
            txtEmail.Text = institution.Email;
            txtAddress.Text = institution.Adress;
            lblName.Text = "Actualizar " + institution.Name;
            pictureBox1.Visible = false;
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider1.Clear();
                Boolean flag = true;
                Regex regex = new Regex(@"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");

                validateEmpty(txtName, "El nombre es necesario", ref flag);
                validateEmpty(txtAddress, "La dirección es necesara", ref flag);
                validateEmpty(txtEmail, "El correo es necesario", ref flag);

                if (!regex.IsMatch(txtEmail.Text.Trim()))
                {
                    errorProvider1.SetError(txtEmail, "Formato incorrecto");
                    flag = false;
                }

                if (flag)
                {
                    enable(false);
                    institution.Name = txtName.Text.Trim();
                    institution.Email = txtEmail.Text.Trim();
                    institution.Adress = txtAddress.Text.Trim();

                    InstitutionController controller = new InstitutionController();
                    await Task.Run(() => controller.Update(institution, institution.Id));
                    this.Hide();
                    this.frmList.fill();
                    MessageBox.Show("Institución actualizada", "Realizado");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            enable(true);
        }

        private void validateEmpty(BunifuMaterialTextbox txt, String message, ref Boolean flag)
        {
            if (txt.Text.Trim() == "")
            {
                flag = false;
                errorProvider1.SetError(txt, message);
            }
        }

        private void enable(bool status)
        {
            pictureBox1.Visible = !status;
            txtAddress.Enabled = status;
            txtName.Enabled = status;
            txtEmail.Enabled = status;
            btnClean.Enabled = status;
            btnSave.Enabled = status;
        }
    }
}
