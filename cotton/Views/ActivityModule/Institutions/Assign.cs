﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Controllers;
using Cotton.Models;

namespace Cotton.Views.ActivityModule.Institutions
{
    public partial class Assign : Form
    {
        private Institution institution = null;
        private List<User> userList;

        public Assign(Institution institution)
        {
            InitializeComponent();
            this.institution = institution;
        }

        private void DataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGrid.Columns["AssignButton"].Index)
            {
                User user = (User)dataGrid.Rows[e.RowIndex].DataBoundItem;
                if (MessageBox.Show("Seguro de vincular a " + user.Name + " " + user.Lastname + " como representante?", "Confirmación", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    InstitutionsUsers institutionsUsers = new InstitutionsUsers();
                    institutionsUsers.InstitutionId = this.institution.Id;
                    institutionsUsers.UserId = user.Id;
                    if (InstitutionUserController.store(institutionsUsers))
                    {
                        fill();
                        MessageBox.Show("Vinculación correcta", "Exito");
                    }
                }
            }
        }

        private void TxtSearch_OnValueChanged(object sender, EventArgs e)
        {
            String val = txtSearch.Text.Trim().ToLower();
            if (val != "")
            {
                var query = from user in userList
                            where user.Email.ToLower().Contains(val) || user.Name.Trim().ToLower().Contains(val) || user.Adress.Trim().ToLower().Contains(val) || user.Lastname.Trim().ToLower().Contains(val) || user.Dui.Trim().ToLower().Contains(val)
                            select user;
                dataGrid.DataSource = query.ToList();
            }
            else
            {
                dataGrid.DataSource = userList;
            }
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            NewAgent frmAgent = new NewAgent(institution, this);
            frmAgent.Show();
        }

        private void Assign_Load(object sender, EventArgs e)
        {
            fill();
        }

        public async void fill()
        {
            dataGrid.Columns.Clear();
            pictureBox1.Visible = true;
            userList = await Task.Run(() => InstitutionUserController.getUnrelated(institution.Id));
            dataGrid.DataSource = userList;
            dataGrid.Columns["Id"].Visible = false;
            dataGrid.Columns["CreatedAt"].Visible = false;
            dataGrid.Columns["UpdatedAt"].Visible = false;
            dataGrid.Columns["StatusId"].Visible = false;
            dataGrid.Columns["Code"].Visible = false;
            dataGrid.Columns["essentialInfo"].Visible = false;
            dataGrid.Columns["LastName"].HeaderText = "Last name";

            DataGridViewButtonColumn button = new DataGridViewButtonColumn();
            button.Name = "AssignButton";
            button.HeaderText = "Asignar";
            button.Text = "Asignar";
            button.UseColumnTextForButtonValue = true;
            dataGrid.Columns.Add(button);
            pictureBox1.Visible = false;
        }
    }
}
