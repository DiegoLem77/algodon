﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Controllers;
using Cotton.Models;

namespace Cotton.Views.ActivityModule.Actitivies
{
    public partial class List : Form
    {
        private ActivitiesController controller = new ActivitiesController();
        private List<Activity> activityList = new List<Activity>();

        public List()
        {
            InitializeComponent();
        }

        private void List_Load(object sender, EventArgs e)
        {
            fillGrid();
        }

        private async void fillGrid()
        {
            dataGrid.Columns.Clear();
            pictureBox1.Visible = true;
            activityList = await Task.Run(() => controller.All());
            dataGrid.DataSource = activityList;
            dataGrid.AutoGenerateColumns = false;
            dataGrid.Columns["Id"].Visible = false;
            dataGrid.Columns["CreatedAt"].Visible = false;
            dataGrid.Columns["UpdatedAt"].Visible = false;
            dataGrid.Columns["StatusId"].Visible = false;

            dataGrid.Columns.Add(createButton("ResourceButton", "Detalles"));
            dataGrid.Columns.Add(createButton("AssistanceButton", "Asistencia"));
            dataGrid.Columns.Add(createButton("DeleteButton", "Eliminar"));
            pictureBox1.Visible = false;
        }

        private DataGridViewButtonColumn createButton(String name, String text)
        {
            DataGridViewButtonColumn button = new DataGridViewButtonColumn();
            button.Name = name;
            button.HeaderText = text;
            button.Text = text;
            button.UseColumnTextForButtonValue = true;
            return button;
        }

        private void dataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Activity activity = (Activity)dataGrid.Rows[e.RowIndex].DataBoundItem;
                if (e.ColumnIndex == dataGrid.Columns["ResourceButton"].Index)
                {
                    Resources frmResource = new Resources(activity);
                    frmResource.Show();
                }
                else if (e.ColumnIndex == dataGrid.Columns["DeleteButton"].Index)
                {
                    if (MessageBox.Show("Eliminar " + activity.Name, "Confirmación", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (activity.Delete())
                        {
                            MessageBox.Show("Actividad eliminada", "Exito");
                            fillGrid();
                        } else
                        {
                            MessageBox.Show("No se puede borrar la actividad", "Error");
                        }
                    }
                }
                else if (e.ColumnIndex == dataGrid.Columns["AssistanceButton"].Index)
                {
                    Assistance frmAssistance = new Assistance(activity);
                    frmAssistance.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void txtSearch_OnValueChanged(object sender, EventArgs e)
        {
            String val = txtSearch.Text.Trim().ToLower();
            if (val != "")
            {
                var query = from activity in activityList
                            where activity.Name.ToLower().Contains(val) || activity.Description.Trim().ToLower().Contains(val) 
                            select activity;
                dataGrid.DataSource = query.ToList();
            }
            else
            {
                dataGrid.DataSource = activityList;
            }
        }

        private void btnGenPDF_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                bool status = PDF.Activities.GeneratePDF(fbd.SelectedPath);

                if(status)
                {
                    MessageBox.Show("El PDF ha sido generado correctamente!", "Documento generado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                } else
                {
                    MessageBox.Show("Ha ocurrido un error, intentalo mas tarde...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
