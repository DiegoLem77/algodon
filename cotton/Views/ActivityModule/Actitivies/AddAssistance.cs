﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Controllers;
using Cotton.Models;

namespace Cotton.Views.ActivityModule.Actitivies
{
    public partial class AddAssistance : Form
    {
        private List<Institution> institutionList = new List<Institution>();
        private List<UserActivity> UserList = new List<UserActivity>();
        private Activity activity = new Activity();
        Assistance frmAssistance;
        public AddAssistance(Activity activity, Assistance frmAssistance)
        {
            InitializeComponent();
            this.activity = activity;
            this.frmAssistance = frmAssistance;
        }

        private async void Assistance_Load(object sender, EventArgs e)
        {
            enable(false);
            InstitutionController controller = new InstitutionController();
            institutionList = await Task.Run(() => controller.All());
            cmbInstitution.DataSource = institutionList;
            cmbInstitution.DisplayMember = "Name";
            cmbInstitution.ValueMember = "Id";
            enable(true);
        }

        private void enable(bool status)
        {
            pictureBox1.Visible = !status;
            cmbInstitution.Enabled = status;
            cmbVisitor.Enabled = status;
            btnFinish.Enabled = status;
            btnSave.Enabled = status;
        }

        private void cmbInstitution_SelectedIndexChanged(object sender, EventArgs e)
        {
            enable(false);
            Institution inst = (Institution)cmbInstitution.SelectedItem;
            UserList = InstitutionUserController.getLinked(inst.Id);
            if (UserList.Count > 0)
            {
                cmbVisitor.DataSource = UserList;
                cmbVisitor.DisplayMember = "essentialInformation";
            }
            enable(true);
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            enable(false);
            List<UserActivity> list = new List<UserActivity>();
            list = ActivityVisitorController.FindByActivity(activity.Id);
            bool flag = true;
            activities_visitors av = new activities_visitors();
            av.activity_id = activity.Id;
            UserActivity user = (UserActivity)cmbVisitor.SelectedItem;
            av.visitor_id = user.Id;
            foreach (UserActivity ua in list)
            {
                if (ua.Iu == user.Id)
                {
                    flag = false;
                }
            }
            if (flag)
            {
                ActivityVisitorController.Store(av);
                this.frmAssistance.fill();
            }
            else
            {
                MessageBox.Show("Asistencia ya registrada", "Error");
            }
            enable(true);
        }
    }
}
