﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Controllers;
using Cotton.Models;
using System.IO;
using System.Security;

namespace Cotton.Views.ActivityModule.Actitivies
{
    public partial class AddResource : Form
    {
        private Activity activity = new Activity();
        // Variables to gallery
        ImageList myImageList = new ImageList();
        FileInfo file;
        Stack<string> filePaths = new Stack<string>();
        ActivitiesController ac = new ActivitiesController();
        Resources frmDetails;

        public AddResource(Activity activity, Resources frmDetails)
        {
            InitializeComponent();
            this.activity = activity;
            // Open Dialog validation
            ofdResources.Multiselect = true;
            ofdResources.ValidateNames = true;
            ofdResources.Filter = "Resource files|*.jpg;*.png;*.pdf;*.mp4;*mp3";
            this.frmDetails = frmDetails;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            int count = 0;
            this.myImageList.ImageSize = new Size(50, 50);
            myImageList.Images.Clear();
            if (ofdResources.ShowDialog() == DialogResult.OK)
            {
                lvPictures.Items.Clear();
                try
                {
                    foreach (string fileName in ofdResources.FileNames)
                    {
                        try
                        {
                            file = new FileInfo(fileName);
                            if (file.Extension == ".jpg" || file.Extension == ".png")
                            {
                                using (FileStream stream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read))
                                {
                                    myImageList.Images.Add(Image.FromStream(stream));
                                }
                            }
                            else
                            {
                                Bitmap image = null;
                                switch (file.Extension)
                                {
                                    case ".pdf":
                                        image = Properties.Resources.pdf;
                                        break;
                                    case ".mp3":
                                        image = Properties.Resources.mp3;
                                        break;
                                    case ".mp4":
                                        image = Properties.Resources.mp4;
                                        break;
                                }
                                myImageList.Images.Add(image);
                            }
                            lvPictures.LargeImageList = myImageList;
                            lvPictures.Items.Add(new ListViewItem { ImageIndex = count, Text = file.Name, Tag = file.FullName });
                            count++;
                            filePaths.Push(fileName);
                        }
                        catch (SecurityException ex)
                        {
                            // The user lacks appropriate permissions to read files, discover paths, etc.
                            MessageBox.Show("Security error. Please contact your administrator for details.\n\n" +
                                "Error message: " + ex.Message + "\n\n" +
                                "Details (send to Support):\n\n" + ex.StackTrace
                            );
                        }
                        catch (Exception ex)
                        {
                            // Could not load the image - probably related to Windows file system permissions.
                            MessageBox.Show("Cannot display the image: " + fileName.Substring(fileName.LastIndexOf('\\'))
                                + ". You may not have permission to read the file, or " +
                                "it may be corrupt.\n\nReported error: " + ex.Message);
                        }
                    }
                }
                catch (SecurityException ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro de eliminar los registros seleccionados?", "Confirmación", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                foreach (ListViewItem lv in lvPictures.SelectedItems)
                {
                    lvPictures.Items.Remove(lv);
                }
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (ac.SetActivityResources(activity, filePaths))
            {
                MessageBox.Show("Los nuevos recursos han sido agregados éxitosamente", "Operación finalizada", MessageBoxButtons.OK, MessageBoxIcon.Information);
                frmDetails.fill();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Ocurrió un error al guardar los recursos, intentalo más tarde...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
