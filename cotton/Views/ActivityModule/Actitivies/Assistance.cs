﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Controllers;
using Cotton.Models;

namespace Cotton.Views.ActivityModule.Actitivies
{
    public partial class Assistance : Form
    {
        private Activity activity = new Activity();
        private List<UserActivity> userList = new List<UserActivity>();
        public Assistance(Activity activity)
        {
            InitializeComponent();
            this.activity = activity;
            lblTitle.Text = activity.Name;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddAssistance frmAdd = new AddAssistance(activity, this);
            frmAdd.Show();
        }

        private void Assistance_Load(object sender, EventArgs e)
        {
            fill();
        }

        public async void fill()
        {
            dataGrid.Columns.Clear();
            try
            {
                pictureBox1.Visible = true;
                userList = await Task.Run(() => ActivityVisitorController.FindByActivity(this.activity.Id));
                dataGrid.DataSource = userList;
                dataGrid.Columns["Id"].Visible = false;
                dataGrid.Columns["Iu"].Visible = false;
                dataGrid.Columns["essentialInformation"].Visible = false;
                dataGrid.Columns["LastName"].HeaderText = "Last name";

                DataGridViewButtonColumn button = new DataGridViewButtonColumn();
                button.Name = "DeleteButton";
                button.HeaderText = "Eliminar";
                button.Text = "Eliminar";
                button.UseColumnTextForButtonValue = true;
                dataGrid.Columns.Add(button);

                pictureBox1.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGrid.Columns["DeleteButton"].Index)
            {
                UserActivity user = (UserActivity)dataGrid.Rows[e.RowIndex].DataBoundItem;
                if (MessageBox.Show("Seguro desea eliminar a: " + user.Name + " " + user.LastName + " de los que asistieron?", "Confirmacion", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ActivityVisitorController.Delete(user.Id);
                    fill();
                    MessageBox.Show("Asistencia eliminada", "Realizado");
                }
            }
        }
    }
}
