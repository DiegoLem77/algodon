﻿namespace Cotton.Views.ActivityModule.Actitivies
{
    partial class AddResource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddResource));
            this.btnUpload = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lvPictures = new System.Windows.Forms.ListView();
            this.ofdResources = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // btnUpload
            // 
            this.btnUpload.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnUpload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnUpload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUpload.BorderRadius = 0;
            this.btnUpload.ButtonText = "Subir";
            this.btnUpload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpload.DisabledColor = System.Drawing.Color.Gray;
            this.btnUpload.Iconcolor = System.Drawing.Color.Transparent;
            this.btnUpload.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnUpload.Iconimage")));
            this.btnUpload.Iconimage_right = null;
            this.btnUpload.Iconimage_right_Selected = null;
            this.btnUpload.Iconimage_Selected = null;
            this.btnUpload.IconMarginLeft = 0;
            this.btnUpload.IconMarginRight = 0;
            this.btnUpload.IconRightVisible = true;
            this.btnUpload.IconRightZoom = 0D;
            this.btnUpload.IconVisible = true;
            this.btnUpload.IconZoom = 90D;
            this.btnUpload.IsTab = false;
            this.btnUpload.Location = new System.Drawing.Point(31, 145);
            this.btnUpload.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnUpload.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnUpload.OnHoverTextColor = System.Drawing.Color.White;
            this.btnUpload.selected = false;
            this.btnUpload.Size = new System.Drawing.Size(133, 59);
            this.btnUpload.TabIndex = 0;
            this.btnUpload.Text = "Subir";
            this.btnUpload.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpload.Textcolor = System.Drawing.Color.White;
            this.btnUpload.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelect.Location = new System.Drawing.Point(32, 14);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(133, 59);
            this.btnSelect.TabIndex = 1;
            this.btnSelect.Text = "Seleccionar";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.ForeColor = System.Drawing.Color.DarkRed;
            this.btnCancel.Location = new System.Drawing.Point(29, 214);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(133, 59);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Tomato;
            this.btnDelete.Location = new System.Drawing.Point(32, 80);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(133, 59);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Eliminar";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lvPictures
            // 
            this.lvPictures.HideSelection = false;
            this.lvPictures.Location = new System.Drawing.Point(180, 16);
            this.lvPictures.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvPictures.Name = "lvPictures";
            this.lvPictures.Size = new System.Drawing.Size(439, 258);
            this.lvPictures.TabIndex = 4;
            this.lvPictures.UseCompatibleStateImageBehavior = false;
            // 
            // AddResource
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 284);
            this.Controls.Add(this.lvPictures);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.btnUpload);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AddResource";
            this.Text = "Agregar archivo";
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuFlatButton btnUpload;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ListView lvPictures;
        private System.Windows.Forms.OpenFileDialog ofdResources;
    }
}