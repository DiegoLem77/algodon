﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Models;
using Cotton.Controllers;
using System.Collections;
using System.IO;
using System.Security;
using System.Diagnostics;

namespace Cotton.Views.ActivityModule.Actitivies
{
    public partial class Resources : Form
    {
        private Activity activity = new Activity();
        private ActivitiesController controller = new ActivitiesController();
        private List<ActivityResource> resourceList = new List<ActivityResource>();
        // Variables to gallery
        ImageList myImageList = new ImageList();
        FileInfo file;

        public Resources(Activity activity)
        {
            InitializeComponent();
            this.activity = activity;
            lblTitle.Text = activity.Name;
            txtDescription.Text = activity.Description + "\n\n" + ", realizacion: " + activity.StartDatetime.ToString() + " - " + activity.EndDatetime.ToString();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddResource frmAdd = new AddResource(this.activity, this);
            frmAdd.Show();
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro de eliminar los registros seleccionados?", "Confirmación", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                pcbLoad.Visible = true;
                Queue<String> toRemove = new Queue<String>();
                foreach (ListViewItem lv in lvPictures.SelectedItems)
                {
                    toRemove.Enqueue(lv.Tag.ToString());
                    lvPictures.Items.Remove(lv);
                }
                bool success = await Task.Run(() => controller.DeleteResource(toRemove));
                if (success)
                {
                    MessageBox.Show("Recuersos eliminados", "Exito");
                }
                else
                {
                    MessageBox.Show("Error al eliminar los recursos, al momento de mostrar no saldran; pero aun siguen guardados", "Error");
                }
                pcbLoad.Visible = false;
            }
        }

        private void Resources_Load(object sender, EventArgs e)
        {
            fill();
        }

        public async void fill()
        {
            resourceList = await Task.Run(() => controller.GetResources(activity.Id));
            Queue<ActivityResource> resources = new Queue<ActivityResource>(resourceList);
            int count = 0;
            this.myImageList.ImageSize = new Size(50, 50);
            myImageList.Images.Clear();
            lvPictures.Items.Clear();
            foreach (ActivityResource resource in resources)
            {
                try
                {
                    file = new FileInfo(resource.Path);
                    if(file.Exists)
                    {
                        if (file.Extension == ".jpg" || file.Extension == ".png")
                        {
                            using (FileStream stream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read))
                            {
                                myImageList.Images.Add(Image.FromStream(stream));
                            }
                        }
                        else
                        {
                            Bitmap image = null;
                            switch (file.Extension)
                            {
                                case ".pdf":
                                    image = Properties.Resources.pdf;
                                    break;
                                case ".mp3":
                                    image = Properties.Resources.mp3;
                                    break;
                                case ".mp4":
                                    image = Properties.Resources.mp4;
                                    break;
                            }
                            myImageList.Images.Add(image);
                        }
                        lvPictures.LargeImageList = myImageList;
                        lvPictures.Items.Add(new ListViewItem { ImageIndex = count, Text = file.Name, Tag = resource.Id });
                        count++;
                    }
                }
                catch (SecurityException ex)
                {
                    // The user lacks appropriate permissions to read files, discover paths, etc.
                    MessageBox.Show("Security error. Please contact your administrator for details.\n\n" +
                        "Error message: " + ex.Message + "\n\n" +
                        "Details (send to Support):\n\n" + ex.StackTrace
                    );
                }
                catch (Exception ex)
                {
                    // Could not load the image - probably related to Windows file system permissions.
                    MessageBox.Show("Cannot display the image: " + resource.Path.Substring(resource.Path.LastIndexOf('\\'))
                        + ". You may not have permission to read the file, or " +
                        "it may be corrupt.\n\nReported error: " + ex.Message);
                }
            }
            pcbLoad.Visible = false;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            String path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + "Cotton" + Path.DirectorySeparatorChar + activity.Id;
            Process.Start("explorer.exe", path);
        }
    }
}
