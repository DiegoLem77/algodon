﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cotton.Controllers;
using Cotton.Models;
using Cotton.General;
using Bunifu.Framework.UI;
using System.Security;
using System.IO;
using System.Collections.Generic;

namespace Cotton.Views.ActivityModule.Actitivies
{
    public partial class Add : Form
    {
        ActivitiesController Controller = new ActivitiesController();
        // Variables to gallery
        ImageList myImageList = new ImageList();
        FileInfo file;
        Stack<string> filePaths = new Stack<string>();

        public Add()
        {
            InitializeComponent();
            // Open Dialog validation
            OfdResources.Multiselect = true;
            OfdResources.ValidateNames = true;
            OfdResources.Filter = "Resource files|*.jpg;*.png;*.pdf;*.mp4;*mp3";
            pictureBox1.Visible = false;
        }

        private async void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider1.Clear();
                Boolean flag = true;

                ValidateEmpty(txtName, "El nombre es necesario", ref flag);

                if (dtpInit.Value.Date + dtpInitTime.Value.TimeOfDay > dtpFinal.Value.Date + dtpFinalTime.Value.TimeOfDay)
                {
                    flag = false;
                    errorProvider1.SetError(dtpFinalTime, "La finalización debe ser posterior al inicio");
                }

                if (flag)
                {
                    enable(false);
                    Activity a = new Activity
                    {
                        Name = txtName.Text,
                        Description = txtDescription.Text,
                        StartDatetime = dtpInit.Value.Date + dtpInitTime.Value.TimeOfDay,
                        EndDatetime = dtpFinal.Value.Date + dtpFinalTime.Value.TimeOfDay,
                        StatusId = 1,
                    };

                    bool status = await Task.Run(() => Controller.Store(a, filePaths));

                    if(!status)
                    {
                        MessageBox.Show("Ha ocurrido un error al guardar la actividad, inténtalo más tarde...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } else
                    {
                        Clean();
                        MessageBox.Show("Actividad guardada", "Exito");
                    }
                    enable(true);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void BtnClean_Click(object sender, EventArgs e)
        {
            Clean();
        }

        private void Clean()
        {
            txtName.Text = "";
            txtDescription.Text = "";
            dtpInitTime.Value = DateTime.Now;
            dtpFinalTime.Value = DateTime.Now;
            lvPictures.Items.Clear();
            myImageList.Images.Clear();
        }

        private void ValidateEmpty(BunifuMaterialTextbox txt, String message, ref Boolean flag)
        {
            if (txt.Text.Trim() == "")
            {
                flag = false;
                errorProvider1.SetError(txt, message);
            }
        }

        private void BtnSelectFiles_Click(object sender, EventArgs e)
        {
            int count = 0;
            this.myImageList.ImageSize = new Size(50, 50);
            myImageList.Images.Clear();
            if (OfdResources.ShowDialog() == DialogResult.OK)
            {
                lvPictures.Items.Clear();
                try
                {
                    foreach (string fileName in OfdResources.FileNames)
                    {
                        try
                        {
                            file = new FileInfo(fileName);
                            if(file.Extension == ".jpg" || file.Extension ==".png")
                            {
                                using (FileStream stream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read))
                                {
                                    myImageList.Images.Add(Image.FromStream(stream));
                                }
                            }else
                            {
                                Bitmap image = null;
                                switch (file.Extension)
                                {
                                    case ".pdf":
                                        image = Properties.Resources.pdf;
                                        break;
                                    case ".mp3":
                                        image = Properties.Resources.mp3;
                                        break;
                                    case ".mp4":
                                        image = Properties.Resources.mp4;
                                        break;
                                }
                                myImageList.Images.Add(image);
                            }
                            lvPictures.LargeImageList = myImageList;
                            lvPictures.Items.Add(new ListViewItem { ImageIndex = count, Text = file.Name, Tag = file.FullName });
                            count++;
                            filePaths.Push(fileName);
                            //string appPath = Controller.CopyFileToAppFolder(file);
                            //MessageBox.Show(appPath);

                            /**
                                Código para crear picturebox con las imágenes seleccionadas en el dialog.
                                Sí querés obtener información de los archivos seleccionados (Tipo, nombre, peso, etc) podés usar 
                                una instancia de FileInfo (Lo usé en el método del CopyFileToAppFolder del ActivityController)
                             */

                            //PictureBox pb = new PictureBox();
                            //Image loadedImage = Image.FromFile(file);
                            //pb.Height = loadedImage.Height;
                            //pb.Width = loadedImage.Width;
                            //pb.Image = loadedImage;
                            //flowLayoutPanel1.Controls.Add(pb);
                        }
                        catch (SecurityException ex)
                        {
                            // The user lacks appropriate permissions to read files, discover paths, etc.
                            MessageBox.Show("Security error. Please contact your administrator for details.\n\n" +
                                "Error message: " + ex.Message + "\n\n" +
                                "Details (send to Support):\n\n" + ex.StackTrace
                            );
                        }
                        catch (Exception ex)
                        {
                            // Could not load the image - probably related to Windows file system permissions.
                            MessageBox.Show("Cannot display the image: " + fileName.Substring(fileName.LastIndexOf('\\'))
                                + ". You may not have permission to read the file, or " +
                                "it may be corrupt.\n\nReported error: " + ex.Message);
                        }
                    }
                }
                catch (SecurityException ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }

        private void btnErase_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro de eliminar los registros seleccionados?", "Confirmación", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                foreach (ListViewItem lv in lvPictures.SelectedItems)
                {
                    lvPictures.Items.Remove(lv);
                }
            }
        }
    
        private void enable(bool status)
        {
            pictureBox1.Visible = !status;
            txtName.Enabled = status;
            txtDescription.Enabled = status;
            dtpInitTime.Enabled = status;
            dtpFinalTime.Enabled = status;
            BtnSelectFiles.Enabled = status;
            btnClean.Enabled = status;
            btnSave.Enabled = status;
            btnErase.Enabled = status;
            dtpInit.Enabled = status;
            dtpFinal.Enabled = status;
        }
    }
}
