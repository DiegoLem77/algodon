﻿using Cotton.Controllers;
using System;
using System.Drawing;
using System.Windows.Forms;
using FontAwesome.Sharp;
using Categories = Cotton.Views.InventaryControl.Categories;
using Equipments = Cotton.Views.InventaryControl.Equipments;
using Activities = Cotton.Views.ActivityModule.Actitivies;
using Users = Cotton.Views.Assistance.Users;
using Institutions = Cotton.Views.ActivityModule.Institutions;
using Ubications = Cotton.Views.InventaryControl.Ubications;
using Loans = Cotton.Views.InventaryControl.Loans;
using Cotton.General;
using System.Threading;

namespace Cotton
{
    public partial class LayoutPane : Form, IParentForm
    {
        private readonly InstitutionController institutionController = new InstitutionController();
		Thread th;
		// Fields
		private IconButton currentBtn;
        private Panel leftBorderBtn;
        private bool sideBarIsCollapse = false;
        private Panel submenuActive;

        // Constructor
        public LayoutPane()
        {
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            leftBorderBtn = new Panel();
            leftBorderBtn.Size = new Size(7, 54);
            panelMenu.Controls.Add(leftBorderBtn);
            lblFromLink.ForeColor = Color.FromArgb(59, 84, 65);
        }

        private void customizeDesign()
        {
            hideSubmenu();
        }

        private void showSubmenu(Panel submenu)
        {
            if (submenu.Visible == false)
            {
                hideSubmenu();
                submenuActive = submenu;
                submenu.Visible = true;
            }
            else
            {
                submenuActive = null;
                submenu.Visible = false;
            }
        }
        private void hideSubmenu()
        {
            submenuActive = null;
            if (pnlCategoriesSubmenu.Visible == true)
                pnlCategoriesSubmenu.Visible = false;
            if (pnlSubmenuEquipos.Visible == true)
                pnlSubmenuEquipos.Visible = false;
            if (pnlSubmenuUsers.Visible == true)
                pnlSubmenuUsers.Visible = false;
            if (pnlSubmenuInstitutions.Visible == true)
                pnlSubmenuInstitutions.Visible = false;
			if (pnlUbicacionesSubMenu.Visible == true)
				pnlUbicacionesSubMenu.Visible = false;
            if (pnlSubmenuLoans.Visible == true)
                pnlSubmenuLoans.Visible = false;
        }

        private void ActivateButton(Object senderBtn, Color customColor)
        {
            if (senderBtn != null)
            {
                DisabledButton();
                // Button
                currentBtn = senderBtn as IconButton;
                int parentLocation = currentBtn.Parent.Location.Y;
                currentBtn.BackColor = Color.FromArgb(19, 84, 106);
                currentBtn.ForeColor = customColor;
                currentBtn.IconColor = customColor;
                if (!sideBarIsCollapse)
                {
                    currentBtn.TextAlign = ContentAlignment.MiddleCenter;
                    currentBtn.ImageAlign = ContentAlignment.MiddleRight;
                    currentBtn.TextImageRelation = TextImageRelation.TextBeforeImage;
                }
                else
                {
                    currentBtn.TextAlign = ContentAlignment.MiddleRight;
                    currentBtn.ImageAlign = ContentAlignment.MiddleLeft;
                    currentBtn.TextImageRelation = TextImageRelation.ImageBeforeText;
                }

                // Left Border
                leftBorderBtn.BackColor = customColor;
                leftBorderBtn.Height = currentBtn.Height + ((submenuActive != null) ? submenuActive.Height : 0);
                leftBorderBtn.Location = new Point(0, parentLocation + currentBtn.Location.Y);
                leftBorderBtn.Visible = true;
                leftBorderBtn.BringToFront();
                //Current Form icon
                iconCurrentForm.IconChar = currentBtn.IconChar;
                iconCurrentForm.IconColor = customColor;
                // Current Label selected
                lblFromLink.Text = currentBtn.Text;
                lblFromLink.ForeColor = customColor;
            }
        }

        private void DisabledButton()
        {
            if (currentBtn != null)
            {
                leftBorderBtn.Visible = false;
                currentBtn.BackColor = Color.FromArgb(16, 70, 89);
                currentBtn.ForeColor = Color.Gainsboro;
                currentBtn.IconColor = Color.Gainsboro;
                currentBtn.TextAlign = ContentAlignment.MiddleRight;
                currentBtn.ImageAlign = ContentAlignment.MiddleLeft;
                currentBtn.TextImageRelation = TextImageRelation.ImageBeforeText;
            }
        }

        private void btnDashboard_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, Color.PaleVioletRed);
        }

        private void btnEquipos_Click(object sender, EventArgs e)
        {
            showSubmenu(pnlSubmenuEquipos);
            ActivateButton(sender, Color.Orange);
            openChildForm(new Equipments.List());
        }

        private void btnPrestamos_Click(object sender, EventArgs e)
        {
            showSubmenu(pnlSubmenuLoans);
            ActivateButton(sender, Color.Cyan);
			openChildForm(new Loans.List());
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            showSubmenu(pnlSubmenuUsers);
            ActivateButton(sender, Color.MediumPurple);
            openChildForm(new Users.List());
            //openChildForm(new Institutions.List());
        }

        private void toggleSideBar_Click(object sender, EventArgs e)
        {
            sideBarIsCollapse = !sideBarIsCollapse;
            hideSubmenu();
            this.DisabledButton();
            if (panelMenu.Width == 200)
            {
                panelMenu.Width = 62;
                pnlLogoTop.Visible = false;
            }
            else
            {
                panelMenu.Width = 200;
                pnlLogoTop.Visible = true;
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnClose_MouseDown(object sender, MouseEventArgs e)
        {
            //btnClose.
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            btnMaximize.Visible = false;
            btnRestore.Visible = true;
            WindowState = FormWindowState.Maximized;
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            btnMaximize.Visible = true;
            btnRestore.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private Form activeForm = null;
		private void openChildForm(Form childForm)
		{
			if (activeForm != null && activeForm.GetType() == childForm.GetType())
				return;
			if (activeForm != null)
				activeForm.Close();
			activeForm = childForm;
			childForm.TopLevel = false;
			childForm.FormBorderStyle = FormBorderStyle.None;
			childForm.Dock = DockStyle.Fill;
			childForm.BackColor = Color.White;
            //childForm.MdiParent = this;
            pnlContainerForm.Controls.Add(childForm);
            pnlContainerForm.Tag = childForm;
            //childForm.FormClosed += ChildForm_Closing;
            childForm.Show();
        }

        private void ChildForm_Closing(object sender, FormClosedEventArgs e)
        {
			MessageBox.Show("Cerrado con éxito");
        }
        public void AbrirNuevo()
        {
            if (activeForm != null && activeForm.Tag != null)
                if (activeForm.Tag.ToString() == "Alumno")
                {
                    activeForm.Hide();
                    //th = new Thread(secondThread);
                    var childForm = new Users.Student();
                    activeForm = childForm;
                    childForm.TopLevel = false;
                    childForm.FormBorderStyle = FormBorderStyle.None;
                    childForm.Dock = DockStyle.Fill;
                    pnlContainerForm.Controls.Add(childForm);
                    pnlContainerForm.Tag = childForm;
                    childForm.Show();

                }
        }
        private void secondThread()
        {
            var childForm = new Users.Student();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            pnlContainerForm.Controls.Add(childForm);
            pnlContainerForm.Tag = childForm;
            childForm.Show();
        }
        private void btnCategories_Click(object sender, EventArgs e)
        {
            showSubmenu(pnlCategoriesSubmenu);
            ActivateButton(sender, Color.BurlyWood);
            openChildForm(new Categories.List());
        }

        private void btnCategoriesAdd_Click_1(object sender, EventArgs e)
        {
            openChildForm(new Categories.Add());
        }

        private void btnCategoriesList_Click_1(object sender, EventArgs e)
        {
            openChildForm(new Categories.List());
        }

        private void btnListEquipos_Click(object sender, EventArgs e)
        {
            openChildForm(new Equipments.List());
        }

        private void btnAddEquipos_Click(object sender, EventArgs e)
        {
            openChildForm(new Equipments.Add());
        }

        private void LayoutPane_Load(object sender, EventArgs e)
        {
            customizeDesign();
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            openChildForm(new Users.Add());
        }

        private void btnListUser_Click(object sender, EventArgs e)
        {
            openChildForm(new Users.List());
        }

        private void btnInstituciones_Click(object sender, EventArgs e)
        {
            showSubmenu(pnlSubmenuInstitutions);
            openChildForm(new Institutions.List());
            ActivateButton(sender, Color.CadetBlue);
        }

        private void btnListInstitutions_Click(object sender, EventArgs e)
        {
            openChildForm(new Institutions.List());
        }

        private void btnAddInstitution_Click(object sender, EventArgs e)
        {
            openChildForm(new Institutions.Add());
        }

        private void btnRepresentante_Click(object sender, EventArgs e)
        {
            //openChildForm(new Institutions.Agent());
        }

        private void btnNewAgent_Click(object sender, EventArgs e)
        {
            //openChildForm(new Institutions.NewAgent());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //openChildForm(new Institutions.Assign());
        }

        private void btnActivitiesAdd_Click(object sender, EventArgs e)
        {
            openChildForm(new Activities.Add());
        }

        private void btnActivities_Click(object sender, EventArgs e)
        {
            showSubmenu(pnlSubmenuActivities);
            ActivateButton(sender, Color.LightSeaGreen);
            //openChildForm(new Activities.List());
        }

        private void btnActivitiesList_Click(object sender, EventArgs e)
        {
            openChildForm(new Activities.List());
        }

		private void LayoutPane_FormClosing(object sender, FormClosingEventArgs e)
		{
			e.Cancel = true;
		}
		public void openInSecondThread()
		{
			var childForm = new Categories.Add();
			activeForm = childForm;
			childForm.TopLevel = false;
			childForm.FormBorderStyle = FormBorderStyle.None;
			childForm.Dock = DockStyle.Fill;
			childForm.MdiParent = this;
			pnlContainerForm.Controls.Add(childForm);
			pnlContainerForm.Tag = childForm;
			//childForm.FormClosed += ChildForm_Closing;
			childForm.Show();
		}
		public void openSecondFormInMultiTask(Form childForm)
		{
			this.pnlContainerForm.Controls.Remove(activeForm);
			childForm.TopLevel = false;
			childForm.FormBorderStyle = FormBorderStyle.None;
			childForm.Dock = DockStyle.Fill;
			childForm.BackColor = Color.White;
			pnlContainerForm.Controls.Add(childForm);
			childForm.Show();
			activeForm = childForm;
		}

		private void btnUbication_Click(object sender, EventArgs e)
		{
			showSubmenu(pnlUbicacionesSubMenu);
			ActivateButton(sender, Color.LightSeaGreen);
            openChildForm(new Ubications.List());
        }

		private void btnUbicationsList_Click(object sender, EventArgs e)
		{
			openChildForm(new Ubications.List());
		}

		private void btnAddUbication_Click(object sender, EventArgs e)
		{
			openChildForm(new Ubications.Add());
		}

		private void btnAddLoan_Click(object sender, EventArgs e)
		{
			openChildForm(new Loans.Add());
		}

		private void btnListLoan_Click(object sender, EventArgs e)
		{
			openChildForm(new Loans.List());
		}
	}
}
