use master;
CREATE DATABASE [cotton];
USE [cotton];

-- SQL Server
--
-- Host: 127.0.0.1    Database: cotton
-- ------------------------------------------------------
-- Server version	5.6.46-log

--
-- Table structure for table `status`
--

CREATE TABLE status (
  id int NOT NULL IDENTITY,
  code varchar(50) NOT NULL UNIQUE,
  description text,
  PRIMARY KEY (id),
)

--
-- Dumping data for table `status`
--
INSERT INTO status(code,description) VALUES ('ACTIVE','Registro activo');
INSERT INTO status(code,description) VALUES('DISABLED','Registro desactivado')
INSERT INTO status(code,description) VALUES('DELETED','Registro eliminado')
--
-- Table structure for table `activities`
--

CREATE TABLE [activities] (
  id char(36) NOT NULL,
  name varchar(200) NOT NULL,
  start_datetime datetime NOT NULL,
  end_datetime datetime NOT NULL,
  description text,
  status_id int NOT NULL,
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (status_id) REFERENCES status(id)
)

--
-- Table structure for table `categories`
--

CREATE TABLE categories (
  id char(36) NOT NULL,
  name varchar(200) NOT NULL,
  description text NOT NULL,
  status_id int NOT NULL,
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (status_id) REFERENCES status(id)
)

--
-- Table structure for table `activities_categories`
--

CREATE TABLE activities_categories (
  id int NOT NULL IDENTITY,
  activity_id char(36) NOT NULL,
  category_id char(36) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (activity_id) REFERENCES activities(id),
  FOREIGN KEY (category_id) REFERENCES categories(id)
)

--
-- Table structure for table `institutions`
--

CREATE TABLE institutions (
  id char(36) NOT NULL,
  name varchar(200) NOT NULL,
  email varchar(200) NOT NULL,
  adress varchar(400) DEFAULT NULL,
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  status_id int NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (status_id) REFERENCES status (id)
)

--
-- Table structure for table `users`
--

CREATE TABLE users (
  id char(36) NOT NULL,
  name varchar(200) NOT NULL,
  lastname varchar(200) NOT NULL,
  code varchar(100) NOT NULL UNIQUE,
  email varchar(200) NOT NULL UNIQUE,
  dui varchar(9) DEFAULT NULL UNIQUE,
  gender varchar(10) DEFAULT 'F' CHECK(gender IN('F', 'M')),
  password varchar(200) DEFAULT NULL,
  adress varchar(400) DEFAULT NULL,
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  status_id int NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (status_id) REFERENCES status (id)
)

INSERT INTO users(id, name, lastname, code, email, gender, password, status_id) 
VALUES ('77c6cb53-e83e-415b-be65-5bd16516e4ed', 'Diego', 'Lemus', 'LT171997', 'diego.lem077@gmail.com', 'M', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 1);

--
-- Table structure for table `institutions_users`
--

CREATE TABLE institutions_users (
  id int NOT NULL IDENTITY,
  institution_id char(36) NOT NULL,
  user_id char(36) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (institution_id) REFERENCES institutions(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
)

--
-- Table structure for table `activities_visitors`
--

CREATE TABLE activities_visitors (
  id int NOT NULL IDENTITY,
  activity_id char(36) NOT NULL,
  visitor_id int NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (activity_id) REFERENCES activities(id),
  FOREIGN KEY (visitor_id) REFERENCES institutions_users(id)
)

--
-- Table structure for table `activity_resources`
--

CREATE TABLE activity_resources (
  id char(36) NOT NULL,
  path varchar(300) NOT NULL,
  name varchar(200) NOT NULL,
  description text,
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  status_id int NOT NULL,
  activity_id char(36) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (activity_id) REFERENCES activities (id),
  FOREIGN KEY (status_id) REFERENCES status (id)
)

--
-- Table structure for table `category_types`
--

CREATE TABLE category_types (
  id char(36) NOT NULL,
  code varchar(10) NOT NULL UNIQUE,
  name varchar(200) NOT NULL,
  PRIMARY KEY (id)
)

--
-- Dumping data for table `category_types`
--

INSERT INTO category_types(id,code,name) VALUES('0e61165a-65ce-47cd-b164-df8aae2ac5b1','ACTIVITIES','Categoría para actividades');
INSERT INTO category_types(id,code,name) VALUES('148f7b8c-f52c-4df2-bfb7-b269b38e1653','INVENTORY','Categoría para inventario');
INSERT INTO category_types(id,code,name) VALUES('d1dca765-257e-4d6b-8c43-4ac5880939a0','ASSISTANCE','Categoría para asistencias');

--
-- Table structure for table `categories_category_types`
--

CREATE TABLE categories_category_types (
  id int NOT NULL IDENTITY,
  category_id char(36) NOT NULL,
  type_id char(36) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (category_id) REFERENCES categories (id),
  FOREIGN KEY (type_id) REFERENCES category_types (id)
)

--
-- Table structure for table `user_types`
--

CREATE TABLE user_types (
  id int NOT NULL IDENTITY,
  code varchar(20) NOT NULL UNIQUE,
  description varchar(300) DEFAULT NULL,
  PRIMARY KEY (id)
)

--
-- Dumping data for table `user_types`
--

INSERT INTO user_types(code, description) VALUES('COORDINATOR','Coordinador');
INSERT INTO user_types(code, description) VALUES('TEACHER','Docente');
INSERT INTO user_types(code, description) VALUES('STUDENT','Estudiante');
INSERT INTO user_types(code, description) VALUES('VISITOR','Visitante por parte de una institución');
INSERT INTO user_types(code, description) VALUES('VOLUNTEER','Voluntario');

--
-- Table structure for table `academic_level_type`
--
CREATE TABLE academic_level_type (
  id CHAR(36) NOT NULL,
  code VARCHAR(45) NOT NULL,
  description VARCHAR(50) NULL,
  PRIMARY KEY (id)
  )

--
-- Dumping data for table `academic_level_type`
--

INSERT INTO academic_level_type VALUES('c8ed7dfa-731e-4846-88d2-d7bc06521d88','Parvularia',NULL);
INSERT INTO academic_level_type VALUES('224d5f0e-d329-4ca2-a0f0-0fd0136b76bb','Primer Ciclo',NULL);
INSERT INTO academic_level_type VALUES('64a4e569-b0a5-471b-86b1-e4b2b5cf09e9','Segundo Ciclo',NULL);
INSERT INTO academic_level_type VALUES('b069aa06-51ed-4410-8f4f-cd08bacadfb2','Tercer Ciclo',NULL);
INSERT INTO academic_level_type VALUES('84fb7073-fe87-42ac-87af-a0b31ed75074','Bachillerato',NULL);
INSERT INTO academic_level_type VALUES('ef2defb0-6df5-4189-9c65-a440d3d4c6e8','Educación Superior',NULL);
INSERT INTO academic_level_type VALUES('aa6d3443-452c-47d7-8767-c625cbd45bfb','Ninguno',NULL);

--
-- Table structure for table `skill_level_type`
--
CREATE TABLE skill_level_type (
  id CHAR(36) NOT NULL,
  code VARCHAR(45) NOT NULL UNIQUE,
  description VARCHAR(50) NULL,
  PRIMARY KEY (id)
  )

--
-- Dumping data for table `skill_level_type`
--

INSERT INTO skill_level_type VALUES('dcc7cf50-079c-403c-932c-a304c77d7f60','Avanzado',NULL);
INSERT INTO skill_level_type VALUES('0eed17a0-8257-4f4c-ab03-da05794dc412','Intermedio',NULL);
INSERT INTO skill_level_type VALUES('36e77385-b48d-41f4-9fbf-bc960639a41f','Bajo',NULL);

--
-- Table structure for table `academic_profile`
--
CREATE TABLE academic_profile (
  id CHAR(36) NOT NULL,
  study TINYINT NULL DEFAULT 0,
  academic_leve_typel_id CHAR(36) NOT NULL,
  description_not_study VARCHAR(255) NULL,
  other_courses VARCHAR(200) NULL,
  computation_skills TINYINT NULL DEFAULT 0,
  computation_level_id CHAR(36) NOT NULL,
  english_skills TINYINT NULL DEFAULT 0,
  english_level_id CHAR(36) NOT NULL,
  internet_skills TINYINT NULL,
  internet_level_id CHAR(36) NOT NULL,
  other_skills VARCHAR(100) NULL,
  user_id CHAR(36) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (academic_leve_typel_id) REFERENCES academic_level_type (id),
  FOREIGN KEY (computation_level_id) REFERENCES skill_level_type (id),
  FOREIGN KEY (english_level_id) REFERENCES skill_level_type (id),
  FOREIGN KEY (internet_level_id) REFERENCES skill_level_type (id)
  )

--
-- Table structure for table `work_profile`
--
CREATE TABLE work_profile (
  id CHAR(36) NOT NULL,
  work TINYINT NULL DEFAULT 0,
  vocation VARCHAR(100) NULL,
  work_place VARCHAR(100) NULL,
  work_position VARCHAR(100) NULL,
  user_id CHAR(36) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
  )

--
-- Table structure for table `emergency_contacts`
--
CREATE TABLE emergency_contacts (
  id CHAR(36) NOT NULL,
  name VARCHAR(45) NOT NULL,
  last_name VARCHAR(45) NOT NULL,
  relationship VARCHAR(45) NOT NULL,
  landline VARCHAR(8) NULL,
  mobile_phone VARCHAR(8)  NOT NULL,
  email VARCHAR(100) NULL,
  user_id CHAR(36) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
  )

--
-- Table structure for table `attendance`
--
CREATE TABLE attendance (
  id CHAR(36) NOT NULL,
  date DATE NULL,
  entry_time TIME NULL,
  departure_time TIME NULL,
  user_id CHAR(36) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
  )

--
-- Table structure for table `users_user_types`
--

CREATE TABLE users_user_types (
  id int NOT NULL IDENTITY,
  user_id char(36) NOT NULL,
  type_id int NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (type_id) REFERENCES user_types (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
)

INSERT INTO users_user_types(user_id, type_id) VALUES('77c6cb53-e83e-415b-be65-5bd16516e4ed', 1);

--
-- Modulo de sistema de inventario
--

CREATE TABLE ubications(
  id CHAR(36) NOT NULL,
  ubication VARCHAR(500) NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  PRIMARY KEY (id)
)

CREATE TABLE equipments (
  id CHAR(36) NOT NULL,
  name VARCHAR(100) NOT NULL,
  stock INT,
  available_stock INT,
  acquisition_date DATETIME,
  description VARCHAR(1000) NOT NULL,
  ubication_id CHAR(36),
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  type_id CHAR(36) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY(type_id) REFERENCES categories(id)
)

CREATE TABLE loans (
  id CHAR(36) NOT NULL,
  equipment_id CHAR(36) NOT NULL,
  user_id CHAR(36) NOT NULL,
  created_at DATETIME DEFAULT NULL,
  devolution_date DATETIME,
  updated_at DATETIME DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY(equipment_id) REFERENCES equipments (id),
  FOREIGN KEY (user_id) REFERENCES users (id),
)

ALTER TABLE equipments
ADD FOREIGN KEY (ubication_id) REFERENCES ubications (id)

ALTER TABLE academic_level_type
ADD created_at DATETIME DEFAULT NULL

ALTER TABLE academic_level_type
ADD updated_at DATETIME DEFAULT NULL

ALTER TABLE skill_level_type
ADD created_at DATETIME DEFAULT NULL

ALTER TABLE skill_level_type
ADD updated_at DATETIME DEFAULT NULL

ALTER TABLE work_profile
ADD created_at DATETIME DEFAULT NULL

ALTER TABLE work_profile
ADD updated_at DATETIME DEFAULT NULL

ALTER TABLE emergency_contacts
ADD created_at DATETIME DEFAULT NULL

ALTER TABLE emergency_contacts
ADD updated_at DATETIME DEFAULT NULL

ALTER TABLE academic_profile
ADD created_at DATETIME DEFAULT NULL

ALTER TABLE academic_profile
ADD updated_at DATETIME DEFAULT NULL

ALTER TABLE loans
ADD quantity INT DEFAULT null


ALTER TABLE loans
ADD returned bit DEFAULT null
